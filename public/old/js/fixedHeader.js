
window.onscroll = function() { myFunction() };

var header = document.getElementById ("fixedHeader");
var scrollPosition = header.offsetTop;

function myFunction() 
{
  if (window.pageYOffset > scrollPosition) 
  {
    header.classList.add ("addFixedHeader");
  }

  else 
  {
    header.classList.remove ("addFixedHeader");
  }
}