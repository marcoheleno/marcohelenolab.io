
function setup() 
{
  //loadMD ("https://gitlab.com/marcoheleno/test2/-/raw/master/README.md");//"README.md");
  //loadMD ("https://gitlab.com/marcoheleno/test2/raw/master/README.md");
  //loadMD ("https://gl.githack.com/marcoheleno/test2/-/raw/master/README.md");

}

function draw() 
{
}

function loadMD (url) 
{
  let target = document.getElementById('placeHolder');
  let converter = new showdown.Converter();

  fetch(url)

    .then(function (response) 
    {
      response.text().then(function (text) 
      {
        let md = text;
        let html = converter.makeHtml(md);
        //console.log (md);
        target.innerHTML = html;
      })
    })
    
    .catch(function (err) 
    {
      console.log("Something went wrong!", err);
    });
}