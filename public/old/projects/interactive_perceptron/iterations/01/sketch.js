let perceptronInst;
let desiredOutput, points, prevIterationCoordinate;
let interationNum, interationSectionWidth, interationSectionHeight;
let font, fontBold;


function preload() 
{
  font = loadFont ("IBM_Plex_Mono/IBMPlexMono-Regular.ttf");
  fontBold = loadFont ("IBM_Plex_Mono/IBMPlexMono-Bold.ttf");
}


function setup() 
{
  createCanvas (windowWidth, windowHeight);
  startSystem ();
}


function startSystem () 
{
  noSmooth();
  loop();
  background (30);


  // To keep track of the number of interations.
  interationNum = 0;
  // Controlling the "frameRate()" for interation visualization.
  frameRate (1);


  // Defining "interationSection" Width and Height.
  interationSectionWidth  = width/2;
  interationSectionHeight = 60;
  
  
  // Generate random "x" coordinate.
  let x = 100;//random (-width/2, width/2);
  let y = 0;
  
  // Defining the "bias" for the point.
  let bias = 1;
  
  
  // Defining "desiredOutput" for the point
  // This means that this perceptronInst is going to be 
  // trained with Supervised Learning.
  desiredOutput = 1;
  if (x < 0) desiredOutput = -1;


  // Creating a point object.
  // Each point instance has:
  // inputs: [horizontal coordinate, bias].
  // output: desiredOutput for Supervised Learning.
  // y: vertical coordinate.
  // s: point's size.
  points = 
  {
    inputs: [x, bias],
    output: desiredOutput,
    y: y,
    s: interationSectionHeight/4
  };


  // "perceptronInst" receives 2 inputs:
  // 1º input: Number of weights, they are "x" and "bias".
  // 2º input: "learningRate", lower means slower.
  perceptronInst = new Perceptron (2, 0.0005);

  
  prevIterationCoordinate = 
  {
    x: 0, 
    y: (interationSectionHeight)/4
  };


  drawTitleBar();
}


function drawTitleBar() 
{
  let titleHeight = 60;

  noStroke();   
  fill (100);
  rect (0, 0, width, titleHeight);

  fill (0, 255, 0);
  textSize (14);
  textFont (fontBold);
  textAlign (LEFT, CENTER);

  let desiredOutputPanel = "";
  if (desiredOutput < 0) desiredOutputPanel = "LEFT";
  else desiredOutputPanel = "RIGHT";

  let  desiredOutputText =  "GOAL: GO TO " + desiredOutputPanel +" PANEL (CHOSEN RANDOMLY)";
  text (desiredOutputText, 12, titleHeight/2);

  fill(255);
  textSize (11);
  textFont (font);
  text ("WEIGHTS NEARER TO 0 RESULT IN LESS ITERATIONS",                width/2 + 12, (titleHeight/3)*1);
  text ("HORIZONTAL COORDINATES NEARER TO 0 RESULT IN MORE ITERATIONS", width/2 + 12, (titleHeight/3)*2);
}



function draw() 
{
  // Keeping track of the number of interations.
  print ("Interation Number: " + (interationNum+1) );
  
  
  push();
      // (0, 0) at the center of the canvas.
      translate (width/2, 60 + interationSectionHeight*interationNum + interationSectionHeight/2);
      

      // "feedForward()" the point's input to the Perceptron
      // for it to guess or predict 
      // towards the "desiredOutput",
      // left or Right Section, 
      // -1 or +1.
      perceptronInst.feedForward (points.inputs);

      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = perceptronInst.getActivationResult();
      print ("Perceptron's Guess: " + neuronGuessing);


      
      
      
      // "train()" the perceptronInst based on
      // the difference between the 
      // "desiredOutput" and "neuronGuessing".
      // This will result on a deviation, the guessing error.
      // This deviation/error is used to adjust the weight and bias.
      perceptronInst.train (points.output, neuronGuessing);


      guessedOutputPanel = "";
      if (neuronGuessing < 0) guessedOutputPanel = "LEFT";
      else guessedOutputPanel = "RIGHT";


      // Coloring the iteration graph background based on the point's "desiredOutput".
      let backgroundFirstQuad =  color (0);
      let backgroundSecondQuad = color (0);
      
      if (points.output < 0) 
      {
        if (points.output == neuronGuessing) backgroundFirstQuad =  color (120, 230, 120);
        else backgroundFirstQuad = color (255);
        backgroundSecondQuad = color (200);
      }
      else 
      {
        backgroundFirstQuad =  color (200);
        if (points.output == neuronGuessing) backgroundSecondQuad =  color (120, 230, 120);
        else backgroundSecondQuad = color (255);
      }
      
      noStroke();
      fill (backgroundFirstQuad);
      rect (-width/2, -interationSectionHeight/2, interationSectionWidth, interationSectionHeight);
      
      fill (backgroundSecondQuad);
      rect (0,        -interationSectionHeight/2, interationSectionWidth, interationSectionHeight);
      print ("Desired Output: " + points.output);
      
      
      // Drawing the number of the iteration.
      fill (30);
      textSize (13);
      textFont (font);
      textAlign (LEFT, CENTER);

      // Stopping iteration once the perceptronInst is trained
      let trainingStatus = "";
      if (neuronGuessing == points.output) 
      {
        noLoop();
        
        if (interationNum==0) 
        {
          trainingStatus = "INITIAL RANDOM WEIGHTS: " + perceptronInst.getSumWeights() + " (PERCEPTRON GUESSES " + guessedOutputPanel + " PANEL)";
        }
        else 
        {
          trainingStatus = "ALGORITHM OPTIMIZED!    " + perceptronInst.getSumWeights() + " (GOAL REACHED)";
        }
      }
      
      else
      {
        if (interationNum==0) 
        {
          trainingStatus = "INITIAL RANDOM WEIGHTS: " + perceptronInst.getSumWeights() + " (PERCEPTRON GUESSES " + guessedOutputPanel + " PANEL)";
        }
        else 
        {
          trainingStatus = "OPTIMIZING ALGORITHM:   " + perceptronInst.getSumWeights();
        }
      }
      
      
      text ((interationNum+1) +" - "+ trainingStatus, -interationSectionWidth + 12, (interationSectionHeight/4)*-1-2);
      print("Optimizing: " + perceptronInst.getSumWeights());
      
      

      // Keeping track of the 
      // tranformations/calculations 
      // of each point and iteration
      // Aka the horizontal coordinate of the perceptronInst's Guess.
      let x = perceptronInst.getFeedForwardGuess();
      // Simplifying the vertical coordinate.
      let y = (interationSectionHeight)/4 + points.y;

      // Drawing a graphical representation 
      // of the perceptronInst's guessing.
      stroke (30);
      strokeWeight (points.s/3);
      point (x, y);
      
      strokeWeight (1.5);
      noFill();
      ellipse (x, y, points.s, points.s);
      
      
      // Drawing a graphical representation 
      // of the connection between iterations.
      // A representation of the continuous 
      // transformations the perceptronInst is apply to the data.
      line (prevIterationCoordinate.x, prevIterationCoordinate.y, 
                                    x, prevIterationCoordinate.y);
      
      // Keeping track of the previous 
      // iteration coordinates.
      prevIterationCoordinate = 
      {
        x: x, 
        y: (interationSectionHeight)/4
      };
      

      // Iteration divider
      print ("--------------------------");
      interationNum++;

  pop();
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);

  // Restart system
  startSystem();
}

