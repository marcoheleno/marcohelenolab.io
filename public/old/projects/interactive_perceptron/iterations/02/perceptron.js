

// Perceptron Class.
class Perceptron 
{

  constructor (numWeights, learningRate) 
  {
    this.weights = new Array (numWeights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // Start with random weights.
      this.weights[i] = -0.1;//random (-1, 1);
    }

    this.learningRate = learningRate;
  }



  //Guessing, Prediction Making.
  feedForward (inputs) 
  {
    this.inputs = inputs;

    this.sumWeightedInputs = 0;
    this.sumWeights = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs.
      this.sumWeightedInputs += inputs[i] * this.weights[i];

      // sum of all the weights at each iteration.
      this.sumWeights += this.weights[i];
    }

    // Keeping track of the 
    // tranformations/calculations 
    // of each iteration.
    this.feedForwardGuess = this.sumWeightedInputs;

    // Calling and saving the resultes from the "activationFunction".
    // Result is sign of the sum, -1 or 1.
    this.activationResult = this.activationFunction (this.sumWeightedInputs);
  }

  activationFunction (sumWeightedInputs) 
  {
    if (sumWeightedInputs > 0) return 1; // Right Quad
    else return -1;                      // Left  Quad
  }

  // Function for getting the Activation Result.
  getActivationResult() 
  {
    return this.activationResult;
  }

  // Function for getting the Fitness Value???
  getFeedForwardGuess() 
  {
    return this.feedForwardGuess;
  }

  // Function for getting the 
  // sum of all the weights at each iteration.
  getSumWeights() 
  {
    return this.sumWeights;
  }


  // Function to train the Perceptron against known data.
  train (desiredOutput, guessedOutput) 
  {
    // "guessedOutput" comes from feedForward().
    this.guessedOutput = guessedOutput;

    // Calculate the error 
    // difference between 
    // "desiredOutput" and "guessedOutput".
    // Note this can only be -2, 0, or 2.
    this.error = desiredOutput - this.guessedOutput;

    // Adjust all the "weights" according
    // to the "error" and "learningRate".
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * this.error * this.inputs[i];
    }
  }

}

