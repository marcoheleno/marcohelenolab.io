
class InfectedLetters  
{
  constructor () 
  {
  }
  
  
  textToImageMask (text, textSize, font, x, y) 
  {
    this.textImageMask = createGraphics (width, height, P2D);
    this.textImageMask.background (0, 0, 0, 0);
    this.textImageMask.fill (255);
    this.textImageMask.noStroke();
    this.textImageMask.textSize (textSize);
    this.textImageMask.textAlign (CENTER, CENTER);
    this.textImageMask.textFont (font);
    this.textImageMask.text (text, x, y-textSize/6);
    
    return this.textImageMask;
  }

  
  textToControlPoints (text, textSize, font, x, y) 
  {
    this.textBox = font.textBounds (text, x, y, textSize);

    this.controlPoints = font.textToPoints (text, x-this.textBox.w/2, y+this.textBox.h/2, textSize, 
    {
      sampleFactor: 0.1,  //quanto maior for mais pontos acrescenta
      simplifyThreshold: 0 // se for maior que zero, retira pontos paralelos
    });

    return this.controlPoints;
  }
}

