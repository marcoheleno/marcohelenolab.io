

class PrintCanvas 
{
  constructor ()
  {
    pixelDensity( displayDensity() );
  }
  
  
  createPaperCanvas (paperSize_, paperOrientation_, paperRenderer_) 
  {
    this.paperSize = paperSize_;
    this.paperOrientation = paperOrientation_;
    this.paperRenderer = paperRenderer_;
    this.screenSize = {w:0, h:0}

    //https://www.papersizes.org/a-sizes-in-pixels.htm

    if (this.paperSize == "A1"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:1684, h:2384} 
    if (this.paperSize == "A1"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:2384, h:1684} 

    if (this.paperSize == "A2"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:1191, h:1684} 
    if (this.paperSize == "A2"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:1684, h:1191} 
    
    if (this.paperSize == "A3"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:842, h:1191} 
    if (this.paperSize == "A3"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:1191, h:842} 
    
    if (this.paperSize == "A4"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:595, h:842} 
    if (this.paperSize == "A4"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:842, h:595} 
    
    if (this.paperSize == "A5"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:420, h:595} 
    if (this.paperSize == "A5"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:595, h:420} 
    
    if (this.paperSize == "A6"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:298, h:420} 
    if (this.paperSize == "A6"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:420, h:298} 
    
    if (this.paperSize == "A7"  &&  this.paperOrientation == "PORTRAIT" ) this.screenSize = {w:210, h:298} 
    if (this.paperSize == "A7"  &&  this.paperOrientation == "LANDSCAPE") this.screenSize = {w:298, h:210}

    createCanvas (this.screenSize.w, this.screenSize.h, this.paperRenderer);
  }
  
  
  defineHiResCanvas()
  {
    this.screenDPItoPrintDPI = 4.18;  // 72DPI.h/300DPI.h = 4.16652684563758
    pixelDensity (this.screenDPItoPrintDPI);
  }
}

