

let impressao
let tipoLetraRotativo, textoPontosRotativo, pontosRotativo, angulo;
let tipoLetraCentral, textoPontosCentral, pontosCentral;


function preload() 
{
  tipoLetraRotativo = loadFont ("IBMPlexMono-Thin.ttf");
  //tipoLetraCentral = loadFont ("IBMPlexMono-Bold.ttf");
}


function setup() 
{
  createCanvas (935, 1355, WEBGL);
  impressao = new PrintCanvas();
  //impressao.createPaperCanvas ("A1", "PORTRAIT", WEBGL);
  // A1 || A2 || A3 || A4, PORTRAIT || LANDSCAPE, P2D || WEBGL
  impressao.defineHiResCanvas();


  textoPontosRotativo = new InfectedLetters();
  pontosRotativo = textoPontosRotativo.textToControlPoints ("V A X", 330, tipoLetraRotativo, -15, 0);
  angulo = -90;

  /*
  textoPontosCentral = new InfectedLetters();
  pontosCentral = textoPontosCentral.textToControlPoints ("REALITYS", 180, tipoLetraCentral, -15, 0);
  */
}


function draw() 
{
  /*
  push();
    translate (0, 0, 0);
      fill(0, 255/2);
      stroke(0, 255/2);
      beginShape();
        for (let i=0; i<pontosCentral.length; i++) 
        {
          vertex (pontosCentral[i].x, pontosCentral[i].y);
        }
      endShape();
  pop();
  */

  push();
    //rotateZ( radians(-60) );
    rotateY( radians(angulo) );
    noFill();
    stroke(255, 165);
    strokeWeight(1);
  
  	push();
  		translate (0, 0, 0);

        beginShape();
          for (let i=0; i<pontosRotativo.length; i++) 
          {
            vertex (pontosRotativo[i].x, pontosRotativo[i].y);
          }
        endShape();
  	pop();
  pop();
  
  
  if (angulo < (360/4)-2.5) angulo+=2.5;
  else 
  {
    angulo = 0;
    noLoop();
    saveCanvas ("print", "png");
  }
}


function keyPressed() 
{
  if (key == "s") saveCanvas ("print", "png");
}
