

let ptron;
let points = new Array(2000);
let count = 0;


// The function to describe a line
function f(x) 
{
  // y = mx + b
  let y = 2 * x + 1;
  return y;
}


function setup() 
{
  createCanvas (windowWidth, windowHeight);

  ptron = new Perceptron(3, 0.001); //3 inputs for x, y, and bias //Learning Rate lower==slower

  
  for (let i=0; i<points.length; i++) 
  {
    // Generate training points.
    let x = random (-width/2 , width/2);
    let y = random (-height/2, height/2);

    // Feed the correct answer, 1 or -1, to the Trainer
    let answer = 1;
    if (y < f(x)) answer = -1;
    points[i] = new Trainer(x, y, answer);
  }
}


function draw() 
{
  background(255);

  push();

  translate (width/2,height/2);

  line (-width/2, f(-width/2), width/2, f(width/2));

  // For visualization, we are training one point at each frame.
  // count is iterated with mousePressed()
  count = (count + 1) % points.length;
  ptron.train (points[count].inputs, points[count].answer);
  

  for (let i=0; i<points.length; i++) 
  {
    stroke(0);
    let guess = ptron.feedforward (points[i].inputs);
    //Show the classification—no fill for -1, black for +1.
    if (guess > 0) noFill();
    else           fill(0);

    ellipse (points[i].inputs[0], points[i].inputs[1], 8, 8);
  }
  pop();
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}

