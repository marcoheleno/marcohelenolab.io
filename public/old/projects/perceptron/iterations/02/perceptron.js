class Perceptron 
{

  // The Perceptron stores its weights and learning constants.
  constructor (numWeights, learningRate) 
  {
    this.weights = new Array (numWeights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = random(-1, 1); // Start with random weights
    }

    this.learningRate = learningRate;
  }
  

  // Function to train the Perceptron against known data.
  // Weights are adjusted based on "correctAnswer"
  train(inputs, correctAnswer) 
  {
    // Guess according to the received inputs.
    let guess = this.feedforward(inputs);

    // Calculate the error 
    // (difference between 
    // correctAnswer output - guessed output).
    // Note this can only be 0, -2, or 2
    let error = correctAnswer - guess;

    // Adjust all the weights according
    // to the error and learning constant.
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * error * inputs[i];
    }
  }

  // Guess output -1 or 1, based on input values
  feedforward (inputs) 
  {
    let sum = 0;

    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs
      sum += inputs[i] * this.weights[i];
    }

    // Result is sign of the sum, -1 or 1
    return this.activate(sum);
  }


  // Activation function, output is -1 or 1
  activate(sum) 
  {
    if (sum > 0) return 1;
    else return -1;
  }

}

