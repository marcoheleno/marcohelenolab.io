

let neuron;
let points, yArray, count;


function setup() 
{
  createCanvas (400, 400);

  // 3 inputs — x, y and bias.
  // Learning Rate — lower == slower.
  neuron = new Perceptron(2, 0.0001);
  

  points = new Array(100);
  yArray = new Array(points.length);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random points.
    let x = random (-width/2 , width/2);
    yArray [i] = random (-height/2, height/2);

    // Add "bias".
    let bias = 1;

    // Add "desiredOutput" for Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;
    //if (y < f(x)) desiredOutput = -1;

    // Point
    points[i] = {
      inputs: [x, bias],
      output: desiredOutput
    };
  }

  count = 0;
}


function draw() 
{
  // For visualization, we are training a Point at each frame.
  neuron.train (points[count].inputs, points[count].output);

  // If difference between desired output and guessed output = 0
  if (neuron.trained()) background (0, 255, 0);
  else background (255, 0, 0);

  print (neuron.showWeightsStatus()); //Reasoning
  //print (neuron.showGuessingError());

  // Iterate count to train next point.
  count = (count + 1) % points.length; //4 Telephone Game
  //print(count);


  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, height/2);
    
    // Draw a line divider
    stroke (0);
    let x1 = 0;//-width/2;
    let y1 = -height/2;//f(x1);
    let x2 = 0;//width/2;
    let y2 = height/2;//f(x2);
    line (x1, y1, x2, y2);


    // Draw each ellipse with or without fill, 
    // depending on which side is drawn.
    for (let i=0; i<points.length; i++) 
    {
      stroke (0);
      strokeWeight (2);

      let guess = neuron.feedForward (points[i].inputs);
      if (guess > 0) noFill(); // "guess" can be from -1 to +1
      else fill(0);

      ellipse (points[i].inputs[0], yArray[i], 10, 10);
    }
  pop();
}


// Function to describe the line
function f(x) 
{
  let y = 1*x + 0; // y = mx + b
  return y;
}

