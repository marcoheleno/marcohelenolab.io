

let neuron;
let points, yArray, count;


function setup() 
{
  createCanvas (200, 200);

  // 2 inputs — x and bias.
  // Learning Rate — lower == slower.
  neuron = new Perceptron(2, 0.001);
  

  points = new Array(2);
  yArray = new Array(2);

  for (let i=0; i<points.length; i++) 
  {
    // Generates random x, y coordinates.
    let x = random (-width/2 , width/2);
    yArray [i] = random (-height/2, height/2);

    // Add "bias".
    let bias = 1;

    // Add "desiredOutput" for Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;

    // Point, only x is processed through the Perceptron
    points[i] = {
      inputs: [x, bias],
      output: desiredOutput
    };
  }

  noLoop();
}


function draw() 
{
  strokeWeight(1);
  rect (0, 0, width, height);

  push();
    translate (width/2, height/2);
    
    stroke (0);
    let x1 = 0;
    let y1 = -height/2;
    let x2 = 0;
    let y2 = height/2;
    line (x1, y1, x2, y2);


    // Draw each ellipse with or without fill, 
    // depending on which side is drawn.
    for (let i=0; i<points.length; i++) 
    {
      neuron.train (points[i].inputs, points[i].output);

      // If difference between desired output and guessed output = 0
      if (neuron.trained()) stroke (0, 255, 0);
      else stroke (255, 0, 0);
      strokeWeight (2);

      //print (neuron.showWeightsStatus());
      print (neuron.showGuessingError());

      let guess = neuron.feedForward (points[i].inputs);
      if (guess > 0) noFill(); // "guess" can be from -1 to +1
      else fill(0);

      ellipse (points[i].inputs[0], yArray[i], 20, 20);
    }
  pop();
}


function keyPressed() 
{
  redraw();
  print("train");
}

