
class Perceptron 
{
  constructor (numWeights, learningRate) 
  {
    this.weights = new Array (numWeights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // Start with random weights.
      this.weights[i] = random (-1, 1);
    }

    this.learningRate = learningRate;
  }


  // Function to train the Perceptron against known data.
  // Weights are adjusted based on "desiredOutput"
  train (inputs, desiredOutput) 
  {
    // Guess according to the received inputs.
    this.guessedOutput = this.feedForward (inputs);

    // Calculate the error 
    // difference between 
    // "desiredOutput" and "guessedOutput".
    // Note this can only be -2, 0, or 2
    this.error = desiredOutput - this.guessedOutput;

    // Adjust all the "weights" according
    // to the "error" and "learningRate".
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * this.error * inputs[i];
    }
  }


  showWeightsStatus()
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      return this.weights[i];
    }
  }

  showGuessedOutput()
  {
    return this.guessedOutput;
  }


  showGuessingError()
  {
    return this.error;
  }


  trained()
  {
    this.trainedStatus = false;

    // If difference between desired output and guessed output = 0
    if (this.error == 0) this.trainedStatus = true;
    
    return (this.trainedStatus);
  }


  feedForward (inputs) //Decision-making
  {
    this.sum = 0;

    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs
      this.sum += inputs[i] * this.weights[i];
    }

    // Result is sign of the sum, -1 or 1
    return this.activate (this.sum);
  }

  activate (sum) 
  {
    if (sum > 0) return 1;
    else return -1;
  }
}

