/*
a step-by-step visualization is triggered. 
Each step visualization reveals the transformations 
that are occurring in the introduced information 
at each iteration. This allows for “passive observation” 
and interpretation at each iteration. As the entire 
process reaches the end — the output, the system 
reveals to the observer a visual history of the 
accumulated transformations that the information 
endured at each iteration.

Weights Viz = accumulated transformations 
that the information 
endured at each iteration
= reasoning
= information mutation
= decision-making
= at each iteration
*/

let neuron;
let points, pointSize;
let interationNum;



function setup() 
{
  createCanvas (windowWidth, windowHeight);

  // Controlling the "frameRate()" for "interationNum" visualization
  frameRate(1);
  // Starting "interationNum" at 1
  interationNum = 1;


  // "neuron" receives 2 inputs
  // 1º input: Number of weights, they are "x" and "bias".
  // 2º input: "learningRate", lower means slower.
  neuron = new Perceptron(2, 0.001);
  

  // Defining each point's size
  pointSize = 20;
  // Working with 1 point only
  points = new Array (1);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random "x" coordinate for each point.
    let x = random (-width/2, width/2);


    // Defining "bias" for each point.
    let bias = 1;


    // Defining "desiredOutput" for each point
    // This means that this Neuron is going to be 
    // trained with Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;


    // Defining the point object 
    // and adding it to the points array.
    // Each point instance has:
    // inputs: [x, bias]
    // output: desiredOutput for Supervised Learning
    // y: vertical coordinate
    points[i] = 
    {
      inputs: [x, bias],
      output: desiredOutput,
      y: 0
    };
  }
  
  //noLoop();
}



function draw() 
{
  background(30);


  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, height/2);


    // Keeping track of the number of interations
    print ("Interation Number: " + interationNum);
    interationNum++;


    for (let i=0; i<points.length; i++) 
    {
      // Coloring the graph section background based on the point's "desiredOutput"
      let backgroundFirstQuad =  color (0);
      let backgroundSecondQuad = color (0);

      if (points[i].output < 0) 
      {
        backgroundFirstQuad =  color (120, 220, 120);
        backgroundSecondQuad = color (220, 130, 130);
      }
      else 
      {
        backgroundFirstQuad =  color (220, 130, 130);
        backgroundSecondQuad = color (120, 220, 120);
      }
      
      noStroke();      
      fill (backgroundFirstQuad);
      rect(-width/2, -height/2, width/2, height);
      fill (backgroundSecondQuad);
      rect(0, -height/2, width/2, height);
      print ("Desired Output: " + points[i].output);


      // "feedForward()" each point's input to the neuron
      // to guess or predict 
      // on the "desiredOutput",
      // left or Right Quad, 
      // -1 or +1
      neuron.feedForward (points[i].inputs);
      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = neuron.getActivationResult();
      print ("Neuron Guesses: " + neuronGuessing);


      // "train()" the neuron based on
      // the difference between the 
      // "desiredOutput" and "neuronGuessing"
      // of each point.
      neuron.train (points[i].output, neuronGuessing);

      // This will result on a deviation, the guessing error.
      // This deviation/error is used to adjust the weight and bias
      let neuronWeight = neuron.getWeights();
      print ("Neuron Weights: " + neuronWeight);
      let neuronBias   = neuron.getBias();
      print ("Neuron Bias: " + neuronBias);

      
      strokeWeight (1);
      stroke (0);
      // Drawing the point at the "desiredOutput"
      fill (0);
      ellipse (points[i].inputs[0], points[i].y, pointSize/2, pointSize/2);

      // Drawing the point at the "neuronGuessing"
      noFill();
      ellipse (points[i].output*neuronGuessing*points[i].inputs[0], points[i].y, 
               pointSize/2, pointSize/2);
      

      // Stoping iteration once the Neuron is trained 
      if (neuronGuessing == points[i].output) 
      {
        print ("Neuron trained!");
        noLoop();
      }
      else print ("Neuron not yeat trained...");


      // Iteration divider
      print ("--------------------------");
    }
  pop();
}



function keyPressed() 
{
  //redraw();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}
