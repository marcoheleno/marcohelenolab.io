/*
a step-by-step visualization is triggered. 
Each step visualization reveals the transformations 
that are occurring in the introduced information 
at each iteration. This allows for “passive observation” 
and interpretation at each iteration. As the entire 
process reaches the end — the output, the system 
reveals to the observer a visual history of the 
accumulated transformations that the information 
endured at each iteration.

Weights Viz = accumulated transformations 
that the information 
endured at each iteration
= reasoning
= information mutation
= decision-making
= at each iteration
*/

let neuron;
let points, pointSize;
let interationNum;
let quadWidth, quadHeight;
let prevPointCoordinate;


function setup() 
{
  createCanvas (windowWidth, windowHeight);
  //noLoop();

  // Controlling the "frameRate()" for "interationNum" visualization
  frameRate(1);
  // To keep track of the number of interations
  interationNum = 0;


  quadWidth = width;
  quadHeight = 30;
  

  // Defining each point's size
  pointSize = 18;
  // Working with 1 point only
  points = new Array (1);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random "x" coordinate for each point.
    let x = random (-width/2, width/2);


    // Defining "bias" for each point.
    let bias = 1;


    // Defining "desiredOutput" for each point
    // This means that this Neuron is going to be 
    // trained with Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;


    // Defining the point object 
    // and adding it to the points array.
    // Each point instance has:
    // inputs: [x, bias]
    // output: desiredOutput for Supervised Learning
    // y: vertical coordinate
    points[i] = 
    {
      inputs: [x, bias],
      output: desiredOutput,
      y: 0
    };
  }


  // "neuron" receives 2 inputs
  // 1º input: Number of weights, they are "x" and "bias".
  // 2º input: "learningRate", lower means slower.
  neuron = new Perceptron(2, 0.0005);

  prevPointCoordinate = 
      {
        x: 0, 
        y: 0
      };
}



function draw() 
{
  // Keeping track of the number of interations
  print ("Interation Number: " + (interationNum+1) );


  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, quadHeight*(interationNum) + quadHeight/2);


    for (let i=0; i<points.length; i++) 
    {
      // "feedForward()" each point's input to the neuron
      // to guess or predict 
      // on the "desiredOutput",
      // left or Right Quad, 
      // -1 or +1
      let xPos = neuron.feedForward (points[i].inputs);
      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = neuron.getActivationResult();
      print ("Neuron's Guess: " + neuronGuessing);



      // "train()" the neuron based on
      // the difference between the 
      // "desiredOutput" and "neuronGuessing"
      // of each point.
      neuron.train (points[i].output, neuronGuessing);

      // This will result on a deviation, the guessing error.
      // This deviation/error is used to adjust the weight and bias
      let neuronWeight = neuron.getWeights();
      //print ("Neuron's Weights: " + neuronWeight);
      let neuronBias   = neuron.getBias();
      //print ("Neuron's Bias: " + neuronBias);



      // Coloring the graph section background based on the point's "desiredOutput"
      let backgroundFirstQuad =  color (0);
      let backgroundSecondQuad = color (0);

      if (points[i].output < 0) 
      {
        if (points[i].output == neuronGuessing) backgroundFirstQuad =  color (120, 220, 120);
        else backgroundFirstQuad = color (255);
        backgroundSecondQuad = color (200);//(220, 130, 130);
      }
      else 
      {
        backgroundFirstQuad =  color (200);//(220, 130, 130);
        //backgroundSecondQuad = color (120, 220, 120);
        if (points[i].output == neuronGuessing) backgroundSecondQuad =  color (120, 220, 120);
        else backgroundSecondQuad = color (255);
      }

      strokeWeight (1);
      stroke (30);
      noStroke();   
      fill (backgroundFirstQuad);
      rect (-width/2, -quadHeight/2, quadWidth, quadHeight);
      fill (backgroundSecondQuad);
      rect (0,        -quadHeight/2, quadWidth, quadHeight);
      print ("Desired Output: " + points[i].output);



      strokeWeight (1);
      stroke (0);
      noFill();
      let x = xPos;
      let y = points[i].y;
      let s = pointSize/2;
      ellipse (x, y, s, s);

      
      print(prevPointCoordinate.x);
      line (prevPointCoordinate.x, prevPointCoordinate.y, x, y);

      prevPointCoordinate = 
      {
        x: x, 
        y: -quadHeight
      };



      // Stoping iteration once the Neuron is trained 
      if (neuronGuessing == points[i].output) 
      {
        print ("Neuron trained!");
        noLoop();
      }
      else print ("Neuron not yeat trained...");



      // Iteration divider
      print ("--------------------------");
      interationNum++;
    }
  pop();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}
