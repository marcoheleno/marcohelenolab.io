

// Perceptron Class.
class Perceptron 
{

  constructor (numWeights, learningRate) 
  {
    this.weights = new Array (numWeights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // Start with random weights.
      this.weights[i] = random (-1, 1); //-0.5;//
    }

    this.learningRate = learningRate;
  }



  //Guessing, Prediction Making.
  feedForward (inputs) 
  {
    this.inputs = inputs;

    this.sum = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs.
      this.sum += inputs[i] * this.weights[i];
    }

    // Keeping track of the 
    // tranformations/calculations 
    // of each iteration.
    this.feedForwardGuess = this.sum;

    // Calling and saving the resultes from the "activationFunction".
    // Result is sign of the sum, -1 or 1.
    this.activationResult = this.activationFunction (this.sum);
  }

  activationFunction (sum) 
  {
    if (sum > 0) return 1; // Right Quad
    else return -1;        // Left  Quad
  }

  // Function for getting the Activation Result.
  getActivationResult() 
  {
    return this.activationResult;
  }

  // Function for getting the Fitness Value???
  getFeedForwardGuess() 
  {
    return this.feedForwardGuess;
  }



  // Function to train the Perceptron against known data.
  train (desiredOutput, guessedOutput) 
  {
    this.desiredOutput = desiredOutput;
    // teste, lixo
    this.teste1 = 0;
    this.teste2 = 0;
    for (let i=0; i<this.weights.length; i++) 
    {
      this.teste1 += this.weights[i];
    }


    // "guessedOutput" comes from feedForward().
    this.guessedOutput = guessedOutput;

    // Calculate the error 
    // difference between 
    // "desiredOutput" and "guessedOutput".
    // Note this can only be -2, 0, or 2.
    this.error = desiredOutput - this.guessedOutput;

    // Adjust all the "weights" according
    // to the "error" and "learningRate".
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * this.error * this.inputs[i];
      this.teste2 += this.weights[i];
    }

    //print (this.error);
    //print (this.teste);
    // Difrence between the trained weights and the previous weights
    this.teste3 = (this.teste2 - this.teste1)*this.guessedOutput;
    
    print (this.teste3);
  }
  
  getTeste1() 
  {
    return this.teste3 * 100;
  }
}

