

// Perceptron Class.
class Perceptron 
{

  constructor (numWeights, learningRate) 
  {
    this.weights = new Array (numWeights);
    
    this.teste6 = 0;
    for (let i=0; i<this.weights.length; i++) 
    {
      // Start with random weights.
      this.weights[i] = random (-1, 1); //-0.5;//
      // pesos positivos acertam sempre
      

      // porque é que em alguns casos o treino se resolve em 2 iterações 
      // mas noutros o processo é muito mais longo…?
      
      // mais lento quando x estiver mais perto do 0
      // mais lento quando o peso estiver mais long do 0
      // mais rápido quando o peso estiver mais perto do 0
      // mais rápido quando x estiver mais long do 0

      // quanto mais longe estiver o peso do 0 mais longe fica o x do 0

      this.teste6 += this.weights[i];
    }
    //print (this.teste6);

    this.learningRate = learningRate;

    this.teste2 = 0;
  }



  //Guessing, Prediction Making.
  feedForward (inputs) 
  {
    this.inputs = inputs;

    this.sum = 0;
    this.teste1 = 0;
    this.teste5 = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs.
      this.sum += inputs[i] * this.weights[i];
      this.teste5 += this.weights[i];
    }

    // Keeping track of the 
    // tranformations/calculations 
    // of each iteration.
    this.feedForwardGuess = this.sum;
    this.teste1 = this.sum;

    // Calling and saving the resultes from the "activationFunction".
    // Result is sign of the sum, -1 or 1.
    this.activationResult = this.activationFunction (this.sum);
  }

  activationFunction (sum) 
  {
    if (sum > 0) return 1; // Right Quad
    else return -1;        // Left  Quad
  }

  // Function for getting the Activation Result.
  getActivationResult() 
  {
    return this.activationResult;
  }

  // Function for getting the Fitness Value???
  getFeedForwardGuess() 
  {
    return this.feedForwardGuess;
  }



  // Function to train the Perceptron against known data.
  train (desiredOutput, guessedOutput) 
  {
    // diferença entre uma previsão treinada e a anterior (não treinada
    // na 1 iteração, ou menos treinada nas seguintes iterações)
    //print(this.teste1);
    //print(this.teste2);
    this.teste3 = this.teste2 - this.teste1;
    this.teste2 = 0;
    this.teste4 = 0;


    // "guessedOutput" comes from feedForward().
    this.guessedOutput = guessedOutput;

    // Calculate the error 
    // difference between 
    // "desiredOutput" and "guessedOutput".
    // Note this can only be -2, 0, or 2.
    //print (this.guessedOutput);
    this.error = desiredOutput - this.guessedOutput;
    //print (this.error);

    // Adjust all the "weights" according
    // to the "error" and "learningRate".
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * this.error * this.inputs[i];
      this.teste2 += this.inputs[i] * this.weights[i];
      this.teste4 += this.weights[i];
    }
    
    print (this.teste4);
    // porque é que as correções vão sendo feitas a cada passo?
    this.teste7 = (this.teste5);//*this.guessedOutput);
    //print("Optimizing: " + this.teste7); // À previsão aplicas os novos pesos treinados

    //print(this.teste5 - this.teste4); // A difença entre os pesos
    //print( (this.guessedOutput - this.teste4) - (this.teste5 - this.teste4) );
  }
  
  getTeste1() 
  {
    //print ("Weights Dif.: "+this.teste3);
    return this.teste7;//map(this.teste7, -3, 3, -100, 100);
    //return (this.guessedOutput - this.teste4);
  }
}

