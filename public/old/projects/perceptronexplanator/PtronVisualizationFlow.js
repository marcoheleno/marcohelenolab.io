

//
// In this function you are able to define
// the Perceptron's Activation Function
//
const ptronActivationFunction = function (sumWeightedInputs) 
{
  // The result is the sign of the sum, 
  // returns -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}





const ptronIterationsCalculus = new Array (0);


//
// This function runs the Perceptron (feedForward + train)
// with the selected data
// and saves all iterations calaculations.
// The name "... Script" is an analogy to an animation script
//
function runPtronAndGetIterations (dataset) 
{
  if (dataset.length > 0) 
  {
    // See transparentPerceptron.js
    const transparentPerceptronInstance = new TransparentPerceptron (dataset[0].inputs.length, ptronActivationFunction, 0.0005);


    // Creates a two-dimensional array.
    // For each dataPoint a collection of the Perceptron's calculations.
    // The second dimension's length is number of iterations 
    // the Perceptron took until trainingError of 0.
    // The name "... Script" is an analogy to an animation script.
    for (let i=0; i<dataset.length; i++) 
    {
      ptronIterationsCalculus.push( new Array (0) );
    }


    let i = 0;
    let numDataPointsPerceptronTrained  = 0;
    let highestNumOfIterations = 0;
    do 
    {
      // For each dataPoint...
      // Check if the Perceptron is not on it's first iteration and
      // if it has a trainingError of 0 on current (i) dataPoint
      if (ptronIterationsCalculus[i].length > 0 && 
          ptronIterationsCalculus[i][ptronIterationsCalculus[i].length-1].trainingError === 0) 
      {
        // Count the number of dataset that the Perceptron is trained on 
        numDataPointsPerceptronTrained++;
      }

      // For each dataPoint...
      // If the Perceptron on this iteration does not have a trainingError of 0
      // we FeedForward the inputs
      // and Train the Perceptron
      else
      {
        transparentPerceptronInstance.feedForward (dataset[i].inputs);
        transparentPerceptronInstance.train (dataset[i].goal);

        // We add to the second dimension of the array ptronIterationsCalculus
        // the Perceptron's calculations of this iteration
        ptronIterationsCalculus[i].push( transparentPerceptronInstance.getAllTransparentPerceptronCalculations() ); 
      }

      // Iterate through all the dataset
      if (i < ptronIterationsCalculus.length-1) i++;

      // If allready iterated through all the dataset
      else 
      {
        i = 0; // Restart iteration counter
        
        // If the Perceptron has not trained successfully on all dataset
        // also Restart the numDataPointsPerceptronTrained counter
        if (numDataPointsPerceptronTrained < ptronIterationsCalculus.length) numDataPointsPerceptronTrained = 0;
      }

      // Save the highest number of Iterations the Perceptron performed
      if (highestNumOfIterations < ptronIterationsCalculus[i].length) 
      {
        highestNumOfIterations = ptronIterationsCalculus[i].length;
      }

      // Repeat while the Perceptron has not successfully trained on all dataset
    }
    while (numDataPointsPerceptronTrained < dataset.length);

    //console.log(highestNumOfIterations);
    return highestNumOfIterations;
  }
}


function drawUserIteration (dataset) 
{
  let verticalSpaceBetweenDataPoints = p5_containerSize.h / (dataset.length+1);
  let x, y;
  const selectedWithDOMinputRangeValue = false;
  const pickedByUser = true;

  for (let i=0; i<dataset.length; i++) 
  {
    x = dataset[i].inputs[0];
    y = verticalSpaceBetweenDataPoints * (i+1);
    drawPtronStoryboardElementWithp5 (x, y, selectedWithDOMinputRangeValue, pickedByUser);
  }
}


function drawPtronIteration (dataset, iteration) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let verticalSpaceBetweenDataPoints = p5_containerSize.h / (dataset.length+1);
    let x, y;
    let selectedWithDOMinputRangeValue = false;
    const pickedByUser = false;
    
    for (let i=0; i<ptronIterationsCalculus.length; i++) 
    {
      x = p5_containerSize.w/2 + ptronIterationsCalculus[i][iteration].feedForwardCalculation;
      y = verticalSpaceBetweenDataPoints * (i+1);

      drawPtronStoryboardElementWithp5 (x, y, selectedWithDOMinputRangeValue, pickedByUser);
    }

  }
}


//
// This function draws the Perceptron's Storyboard for Visualization.
// It drawns on the previously created ptronIterationsCalculus two-dimensional array values.
// For each dataPoint it is drawAllPtronIterations with all the iterations (drawPtronStoryboardElementWithp5) 
// the Perceptron went through.
//
function drawAllPtronIterations (dataset, DOMinputRangeValue) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let verticalSpaceBetweenDataPoints = p5_containerSize.h / (dataset.length+1);
    let x, y;
    let selectedWithDOMinputRangeValue = false;
    const pickedByUser = false;

    // c = column
    for (let c=0; c<ptronIterationsCalculus.length; c++) 
    {
      // l = line
      for (let l=0; l<ptronIterationsCalculus[c].length; l++) 
      {
        x = p5_containerSize.w/2 + ptronIterationsCalculus[c][l].feedForwardCalculation;
        y = verticalSpaceBetweenDataPoints * (c+1);

        // Iteration selected by DOMinputRangeValue?
        if( l === Math.trunc(DOMinputRangeValue) ) selectedWithDOMinputRangeValue = true; 
        else selectedWithDOMinputRangeValue = false;

        drawPtronStoryboardElementWithp5 (x, y, selectedWithDOMinputRangeValue, pickedByUser);
      }
    }

  }
}


function drawRouteFromSelected2TrainedIteration (dataset, DOMinputRangeValue) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let selectedWithDOMinputRangeValue = false;
    let verticalSpaceBetweenDataPoints = p5_containerSize.h / (dataset.length+1);
    let beginning, end, y;

    // c = column
    for (let c=0; c<ptronIterationsCalculus.length; c++) 
    {
      // l = line
      for (let l=0; l<ptronIterationsCalculus[c].length; l++) 
      {
        if( l === Math.trunc(DOMinputRangeValue) ) selectedWithDOMinputRangeValue = true; 
        else selectedWithDOMinputRangeValue = false;

        if (selectedWithDOMinputRangeValue && ptronIterationsCalculus[c].length>1) 
        {
          beginning = p5_containerSize.w/2 + ptronIterationsCalculus[c][l].feedForwardCalculation;
          end = p5_containerSize.w/2 + ptronIterationsCalculus[c][ptronIterationsCalculus[c].length-1].feedForwardCalculation;
          y = verticalSpaceBetweenDataPoints * (c+1);

          drawPtronStoryboardRouteWithp5 (beginning, end, y);
        }

      }
    }

  }
}


function drawRouteFromUser2PtronIteration (dataset) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let xUser, xPtron, verticalSpaceBetweenDataPoints, y, distance, xPositions, userPickedXonLeftPanel;
    let xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight;

    for (let i=0; i<dataset.length; i++) 
    {
      xUser  = dataset[i].inputs[0];
      xPtron = p5_containerSize.w/2 + ptronIterationsCalculus[i][0].feedForwardCalculation;

      verticalSpaceBetweenDataPoints = p5_containerSize.h / (dataset.length+1);
      y = verticalSpaceBetweenDataPoints * (i+1);

      distance = Math.abs(xUser - xPtron);

      xPositions = [xUser, xPtron];
      userPickedXonLeftPanel = true;
      if (xPtron > xUser) 
      {
        xPositions.reverse();
        userPickedXonLeftPanel = false;
      }
      xLeft  = xPositions[0];
      xRight = xPositions[1];

      yLeft  = y;
      yRight = y;
      xLeftControl = xLeft + distance/6;
      yLeftControl = yLeft - distance;
      xRightControl = xRight - distance/6;
      yRightControl = yRight - distance;

      drawRouteFromUserPicked2InitialPtronStoryboardWithp5 (xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight, distance, userPickedXonLeftPanel);
    }
  }
}

