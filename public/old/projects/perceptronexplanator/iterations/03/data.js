

function getData (canvasHeight) 
{
  const y = int( random(-canvasHeight/2, canvasHeight/2) );
  const b = 1; //Bias

  let g = 1; //Goal
  if (y < 0) g = -1;
  
  const randomDataPoint = 
  {
    inputs: [y, b],
    goal: g
  };

  return randomDataPoint;
}