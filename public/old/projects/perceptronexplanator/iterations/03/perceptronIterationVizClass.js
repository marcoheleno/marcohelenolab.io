

class PerceptronIterationViz 
{

  constructor (ptronIterationVals, ptronIterationIndex, ptronNumIterations, vizPanelWidth, vizPanelHeight) 
  {
    this.feedForwardCalculation = ptronIterationVals.feedForwardCalculation;
    //this.transformedWeights = ptronIterationVals.transformedWeights;
    //this.activationResult = ptronIterationVals.activationResult;
    this.trainingError = ptronIterationVals.trainingError;

    this.ptronIterationIndex = ptronIterationIndex;
    this.ptronNumIterations = ptronNumIterations;
    
    this.vizPanelWidth  = vizPanelWidth;
    this.vizPanelHeight = vizPanelHeight;
  }

  

  drawPerceptronIterationViz()
  {
    this.x = width/2;
    this.y = height/2 + this.feedForwardCalculation;


    if (this.trainingError != 0) 
    {
      this.c = lerpColor(colorLayer2, colorLayer4, map(this.ptronIterationIndex, 0, this.ptronNumIterations, 0, 1));
    }
    else this.c = colorLayer4;
    
    stroke (this.c);
    strokeWeight (3);
    point (this.x, this.y);
  }
  
}
