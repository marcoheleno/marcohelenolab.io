


let perceptronActivationFunction = function (sumWeightedInputs) 
{
  // Here you can define the 
  // Perceptron's Activation Function

  // The below result is the sign of the sum, -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}



let randomDataPoint, transparentPerceptronInstance;
let perceptronIterationsCalculations, perceptronIterationsViz;


function runPerceptron() 
{
  randomDataPoint = getData (height); // See data.js

  transparentPerceptronInstance = new TransparentPerceptron (randomDataPoint.inputs.length, perceptronActivationFunction, 0.0005);

  let i = 0;
  perceptronIterationsCalculations = [];
  do 
  {
    transparentPerceptronInstance.feedForward (randomDataPoint.inputs);
    transparentPerceptronInstance.train (randomDataPoint.goal);
    
    perceptronIterationsCalculations[i] = transparentPerceptronInstance.getAllTransparentPerceptronCalculations();

    i++;
  }
  while (perceptronIterationsCalculations[i-1].trainingError != 0);
}



function createPerceptronVizIterations() 
{
  perceptronIterationsViz = [];

  for (let i=0; i<perceptronIterationsCalculations.length; i++) 
  {
    perceptronIterationsViz[i] = new PerceptronIterationViz 
    (
      perceptronIterationsCalculations[i], 
      i, 
      perceptronIterationsCalculations.length, 
      width, 
      height
    );
  }
}



function drawPerceptronVizIterations() 
{
  for (let i=0; i<perceptronIterationsViz.length; i++) 
  {
    perceptronIterationsViz[i].drawPerceptronIterationViz();
  }
}
