

let canvasContainerWidth, canvasContainerHeight;
let colorLayer1, colorLayer2, colorLayer3, colorLayer4;
let startSys;


function getContainerDimensions() 
{
  let containerDimensions = 
  {
    w: container.offsetWidth,
    h: container.offsetHeight
  };

  return containerDimensions;
}


function setup() 
{
  const canvas = createCanvas (getContainerDimensions().w, getContainerDimensions().h);
  canvas.parent("container");

  colorLayer1 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer1'));
  colorLayer2 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer2'));
  colorLayer3 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer3'));
  colorLayer4 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer4'));

  startSys = true;
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;
    runPerceptron();
    updateDOMslider();
    createPerceptronVizIterations();

    background (colorLayer1);
  }


  push();
    translate (width/2, height/2);
    
      stroke (colorLayer2);
      strokeWeight (1);
      noFill();
      
      beginShape();
        vertex (-width/2, -height/2);
        vertex ( width/2, -height/2);
        vertex ( width/2,         0);
        vertex (-width/2,         0);
      endShape (CLOSE);

      beginShape();
        vertex (-width/2, 0);
        vertex ( width/2, 0);
        vertex ( width/2,  height/2);
        vertex (-width/2,  height/2);
      endShape (CLOSE);
  pop();


  drawPerceptronVizIterations();
}


function updateDOMslider()
{
  console.log (perceptronIterationsCalculations.length);
  document.getElementsByTagName("input")[0].setAttribute("max", perceptronIterationsCalculations.length);
  
  if (perceptronIterationsCalculations.length > 1) 
  {
    document.getElementsByTagName("input")[0].setAttribute("value", perceptronIterationsCalculations.length);

    if (randomDataPoint.goal < 0) document.getElementsByTagName("input")[0].style.transform  = "rotate(-90deg)";
    else document.getElementsByTagName("input")[0].style.transform  = "rotate(90deg)";

    document.getElementsByTagName("input")[0].style.visibility = "visible";
  }
}


function windowResized() 
{
  resizeCanvas (getContainerDimensions().w, getContainerDimensions().h);

  startSys = true;
}
