

let colorLayer1, colorLayer2, colorLayer3, colorLayer4;
let startSys;


//function preload() { img = loadImage("dummy.jpg"); }


function p5_containerSize() 
{
  let containerDimensions = 
  {
    w: p5_container.offsetWidth,
    h: p5_container.offsetHeight
  };

  return containerDimensions;
}


function setup() 
{
  const canvas = createCanvas (p5_containerSize().w, p5_containerSize().h);
  canvas.parent("p5_container");

  colorLayer1 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer1'));
  colorLayer2 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer2'));
  colorLayer3 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer3'));
  colorLayer4 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer4'));

  startSys = true;
  p5_backgroundImageExported = false;
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;

    runPerceptron();
    updateDOMslider();
    createPerceptronVizIterations();

    background (colorLayer1);
  }


  push();
    translate (width/2, height/2);
    
      stroke (colorLayer3);
      strokeWeight (1);
      noFill();
      
      beginShape();
        vertex (-width/2, -height/2);
        vertex (       0, -height/2);
        vertex (       0,  height/2);
        vertex (-width/2,  height/2);
      endShape (CLOSE);

      beginShape();
        vertex (       0, -height/2);
        vertex ( width/2, -height/2);
        vertex ( width/2,  height/2);
        vertex (       0,  height/2);
      endShape (CLOSE);
  pop();

  drawPerceptronVizIterations();
}


function updateDOMslider()
{
  //console.log (perceptronIterationsCalculations.length);
  document.getElementsByTagName("input")[0].setAttribute("max", perceptronIterationsCalculations.length);
  
  if (perceptronIterationsCalculations.length > 1) 
  {
    document.getElementsByTagName("input")[0].setAttribute("value", perceptronIterationsCalculations.length);

    if (randomDataPoint.goal < 0) document.getElementsByTagName("input")[0].style.transform  = "rotate(-180deg)";

    document.getElementsByTagName("input")[0].style.visibility = "visible";
  }
}
