

function getData (canvasWidth) 
{
  const x = int( random(-canvasWidth/2, canvasWidth/2) );
  const b = 1; //Bias

  let g = 1; //Goal
  if (x < 0) g = -1;
  
  const randomDataPoint = 
  {
    inputs: [x, b],
    goal: g
  };

  return randomDataPoint;
}