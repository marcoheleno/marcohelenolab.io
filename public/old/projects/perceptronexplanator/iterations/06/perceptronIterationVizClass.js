

class PerceptronIterationViz 
{
  constructor (ptronIterationVals, ptronIterationIndex, ptronNumIterations, vizPanelWidth, vizPanelHeight) 
  {
    this.feedForwardCalculation = ptronIterationVals.feedForwardCalculation;
    //this.activationResult = ptronIterationVals.activationResult;
    this.trainingError = ptronIterationVals.trainingError;

    this.ptronIterationIndex = ptronIterationIndex;
    this.ptronNumIterations = ptronNumIterations;
    
    this.vizPanelWidth  = vizPanelWidth;
    this.vizPanelHeight = vizPanelHeight;
  }

  
  drawPerceptronIterationViz()
  {
    this.x = this.vizPanelWidth /2 + this.feedForwardCalculation;
    this.y = this.vizPanelHeight/2;

    perceptronIterationVizGraphics (this.x, this.y, this.trainingError, this.ptronIterationIndex, this.ptronNumIterations);
  }
  
  drawLine() 
  {
    this.x = this.vizPanelWidth /2 + this.feedForwardCalculation;
    this.y = this.vizPanelHeight/2;

    perceptronIterationVizLine (this.x, this.y, this.trainingError);
  }
}

