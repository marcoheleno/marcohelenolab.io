

let colorBackground, colorBackgroundMiddle, colorForegroundMiddle, colorForeground;
let loadSystemEnabled;


function p5_containerSize() 
{
  let containerDimensions = 
  {
    w: p5_container.offsetWidth,
    h: p5_container.offsetHeight
  };
  
  return containerDimensions;
}


function setup() 
{
  const canvas = createCanvas (p5_containerSize().w, p5_containerSize().h);
  canvas.parent("p5_container");

  colorBackground = color (getComputedStyle(document.documentElement).getPropertyValue("--colorBackground"));
  colorBackgroundMiddle = color (getComputedStyle(document.documentElement).getPropertyValue("--colorBackgroundMiddle"));
  colorForegroundMiddle = color (getComputedStyle(document.documentElement).getPropertyValue("--colorForegroundMiddle"));
  colorForeground = color (getComputedStyle(document.documentElement).getPropertyValue("--colorForeground"));

  loadSystem();
}


function loadSystem() 
{
  loadSystemEnabled = true;
}


function draw() 
{
  if (loadSystemEnabled) 
  {
    loadSystemEnabled = false;

    runAndGetPerceptronVizCalculations();
    updateDOMinputRange();
    createPerceptronVizIterations();
  }

  background (colorBackground);
  drawLayout();
  drawPerceptronVizIterations (document.getElementsByTagName("input")[0].value);
}


function drawLayout() 
{
  push();
    translate (width/2, height/2);
    
      stroke (colorForegroundMiddle);
      strokeWeight (1);
      
      if (randomDataPoint.goal === -1) fill(0, 200, 0);
      else noFill();
      beginShape();
        vertex (-width/2, -height/2);
        vertex (       0, -height/2);
        vertex (       0,  height/2);
        vertex (-width/2,  height/2);
      endShape (CLOSE);

      if (randomDataPoint.goal === 1) fill(0, 200, 0);
      else noFill();
      beginShape();
        vertex (       0, -height/2);
        vertex ( width/2, -height/2);
        vertex ( width/2,  height/2);
        vertex (       0,  height/2);
      endShape (CLOSE);
  pop();
}


function perceptronIterationVizGraphics (ptronX, ptronY, ptronTrainingError, ptronIterationIndex, ptronNumIterations)
{
  let c = colorForeground;

  if (ptronTrainingError != 0) 
  {
    c = lerpColor(colorBackgroundMiddle, colorForeground, map(ptronIterationIndex, 0, ptronNumIterations, 0, 1));
  }
  
  stroke (c);
  strokeWeight (5);
  point (ptronX, ptronY);
}


function perceptronIterationVizLine (ptronX, ptronY, ptronTrainingError)
{
  if (ptronTrainingError != 0) 
  {
    strokeWeight (2);
    stroke (255, 0, 0);
    line (ptronX, ptronY, width/2, ptronY);
  }
}