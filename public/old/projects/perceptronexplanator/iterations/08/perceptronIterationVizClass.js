

class PerceptronIterationViz 
{
  constructor (ptronIterationVals, ptronIterationIndex, ptronNumIterations, vizPanelWidth, vizPanelHeight) 
  {
    this.feedForwardCalculation = ptronIterationVals.feedForwardCalculation;
    //this.activationResult = ptronIterationVals.activationResult;
    this.trainingError = ptronIterationVals.trainingError;

    this.ptronIterationIndex = ptronIterationIndex;
    this.ptronNumIterations = ptronNumIterations;
    
    this.vizPanelWidth  = vizPanelWidth;
    this.vizPanelHeight = vizPanelHeight;
  }

  
  drawPerceptronIterationViz (selected)
  {
    this.x = this.vizPanelWidth /2 + this.feedForwardCalculation;
    this.y = this.vizPanelHeight/2;
    
    if (this.trainingError === 0) fill(colorForegroundMiddle);
    else noFill();

    if (selected) stroke (colorForeground);
    else stroke (colorMiddle);

    strokeWeight (1);
    ellipse (this.x, this.y, 20, 20);
  }

  
  getPerceptronIterationX() 
  {
    return this.x;
  }

  getPerceptronIterationY() 
  {
    return this.y;
  }
}

