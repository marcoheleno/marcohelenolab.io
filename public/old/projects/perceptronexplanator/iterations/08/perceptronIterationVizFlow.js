

const perceptronActivationFunction = function (sumWeightedInputs) 
{
  // Here you can define the 
  // Perceptron's Activation Function

  // The below result is the sign of the sum, -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}





let randomDataPoint, perceptronIterationsCalculations, perceptronIterationsViz;


function runAndGetPerceptronVizCalculations() 
{
  randomDataPoint = getData (p5_containerSize().w); // See data.js

  const transparentPerceptronInstance = new TransparentPerceptron (randomDataPoint.inputs.length, perceptronActivationFunction, 0.0005);

  perceptronIterationsCalculations = new Array (0);
  do 
  {
    transparentPerceptronInstance.feedForward (randomDataPoint.inputs);
    transparentPerceptronInstance.train (randomDataPoint.goal);
    
    perceptronIterationsCalculations.push( transparentPerceptronInstance.getAllTransparentPerceptronCalculations() );
  }
  while (perceptronIterationsCalculations[perceptronIterationsCalculations.length-1].trainingError != 0);
}


function updateDOMinputRange()
{
  //document.getElementsByTagName("datalist")[0].style.visibility = "hidden";
  document.getElementsByTagName("input")[0].style.visibility = "hidden";
  document.getElementsByTagName("input")[0].setAttribute ("max",   0);
  document.getElementsByTagName("input")[0].value = 0;
  
  if (perceptronIterationsCalculations.length > 1) 
  {
    document.getElementsByTagName("input")[0].setAttribute ("max", perceptronIterationsCalculations.length-1);
    document.getElementsByTagName("input")[0].value = perceptronIterationsCalculations.length-1;

    /*
    for (let i=0; i<perceptronIterationsCalculations.length; i++) 
    {
      document.getElementsByTagName("datalist")[0].appendChild (document.createElement("option"));
      document.getElementsByTagName("option")[i].setAttribute ("value", i);
    }
    document.getElementsByTagName("datalist")[0].style.visibility = "visible";
    */

    document.getElementsByTagName("input")[0].style.visibility = "visible";
  }
}


function createPerceptronVizIterations() 
{
  perceptronIterationsViz = new Array (0);

  for (let i=0; i<perceptronIterationsCalculations.length; i++) 
  {
    perceptronIterationsViz.push 
    (
      new PerceptronIterationViz 
      (
        perceptronIterationsCalculations[i], 
        i, 
        perceptronIterationsCalculations.length, 
        p5_containerSize().w, 
        p5_containerSize().h
      )
    );
  }

}


function drawPerceptronVizIterations (DOMinputRangeValue) 
{
  let selectedIteration = false;
  for (let i=0; i<perceptronIterationsViz.length; i++) 
  {
    if (i === int(DOMinputRangeValue)) selectedIteration = true;
    else selectedIteration = false;
    perceptronIterationsViz[i].drawPerceptronIterationViz (selectedIteration);
    
    if (i<perceptronIterationsViz.length-1) 
    {
      drawDashedLine 
      (
        perceptronIterationsViz[i].getPerceptronIterationX(), 
        perceptronIterationsViz[i+1].getPerceptronIterationX(), 
        perceptronIterationsViz[i].getPerceptronIterationY()
      );
    }
  }
}


function drawDashedLine (posX, nextposX, posY) 
{
  strokeWeight (1);
  stroke (colorMiddle);
  
  let dashWidth = 2;
  let i = 0;

  if (posX < nextposX) 
  {
    for (let x=posX; x<nextposX; x+=dashWidth)
    {
      if (i%2 === 0) line (x, posY, x+dashWidth, posY);
      i++;
    }
  }
  else 
  {
    for (let x=posX; x>nextposX; x-=dashWidth)
    {
      if (i%2 === 0) line (x, posY, x-dashWidth, posY);
      i++;
    }
  }
  
}
