

//
// This function returns a random data point, 
// width a random horizontal position and goal
//  
function getData (numDataPoints, boundary) 
{
  const dataPoints = new Array (numDataPoints);

  for (let i=0; i<dataPoints.length; i++) 
  {
    const x = Math.trunc( Math.random() * (boundary/2 - (-boundary/2)) + (-boundary/2) );
    const b = 1; //Bias
    
    let g = 1; //Goal
    if (x < 0) g = -1;
    
    dataPoints[i]= 
    {
      inputs: [x, b],
      goal: g
    };
  }

  return dataPoints;
}

