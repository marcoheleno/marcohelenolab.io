

let colorBackground, colorBackgroundMiddle, colorMiddle, colorForegroundMiddle, colorForeground;
let loadSystemEnabled;


function p5_containerSize() 
{
  let containerDimensions = 
  {
    w: p5_container.offsetWidth,
    h: p5_container.offsetHeight
  };
  
  return containerDimensions;
}


function setup() 
{
  const canvas = createCanvas (p5_containerSize().w, p5_containerSize().h);
  canvas.parent("p5_container");

  colorBackground       = color (getComputedStyle(document.documentElement).getPropertyValue("--colorBackground"));
  colorBackgroundMiddle = color (getComputedStyle(document.documentElement).getPropertyValue("--colorBackgroundMiddle"));
  colorMiddle           = color (getComputedStyle(document.documentElement).getPropertyValue("--colorMiddle"));
  colorForegroundMiddle = color (getComputedStyle(document.documentElement).getPropertyValue("--colorForegroundMiddle"));
  colorForeground       = color (getComputedStyle(document.documentElement).getPropertyValue("--colorForeground"));

  loadSystem();
}


function loadSystem() 
{
  loadSystemEnabled = true;
}


function draw() 
{
  if (loadSystemEnabled) 
  {
    loadSystemEnabled = false;

    runAndGetPerceptronVizCalculations();
    //updateDOMinputRange();
    //createPerceptronVizIterations();
  }

  //background (colorBackground);
  //drawLayout();
  //drawPerceptronVizIterations (document.getElementsByTagName("input")[0].value);
  noLoop();
}


function drawLayout() 
{
  push();
    translate (width/2, height/2);
    
      strokeWeight (2);
      stroke (colorBackgroundMiddle);
      noFill();
       
      beginShape();
        vertex (-width/2, -height/2);
        vertex (       0, -height/2);
        vertex (       0,  height/2);
        vertex (-width/2,  height/2);
      endShape (CLOSE);
      
      beginShape();
        vertex (       0, -height/2);
        vertex ( width/2, -height/2);
        vertex ( width/2,  height/2);
        vertex (       0,  height/2);
      endShape (CLOSE);
  pop();
}

