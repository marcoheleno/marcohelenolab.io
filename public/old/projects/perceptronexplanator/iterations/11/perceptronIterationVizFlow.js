

const perceptronActivationFunction = function (sumWeightedInputs) 
{
  // Here you can define the 
  // Perceptron's Activation Function

  // The below result is the sign of the sum, -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}





let perceptronIterationsCalculations, perceptronNumIterations, perceptronIterationsViz;


function runAndGetPerceptronVizCalculations() 
{
  const dataPoints = getData (20, p5_containerSize().w); // See data.js

  const transparentPerceptronInstance = new TransparentPerceptron (dataPoints[0].inputs.length, perceptronActivationFunction, 0.0005);


  perceptronIterationsCalculations = new Array (dataPoints.length);

  for (let x=0; x<perceptronIterationsCalculations.length; x++) 
  {
    perceptronIterationsCalculations[x] = new Array (0);
  }

  let perceptronTrainedOnDataPointNum = 0;
  let x = 0;
  do 
  {
    transparentPerceptronInstance.feedForward (dataPoints[x].inputs);
    transparentPerceptronInstance.train (dataPoints[x].goal);

    perceptronIterationsCalculations[x].push( transparentPerceptronInstance.getAllTransparentPerceptronCalculations() );

    if( perceptronIterationsCalculations[x][perceptronIterationsCalculations[x].length-1].trainingError === 0 ) 
    {
      perceptronTrainedOnDataPointNum++;
    }

    if (x < perceptronIterationsCalculations.length-1) x++;
    else 
    {
      x = 0;
      if (perceptronTrainedOnDataPointNum < perceptronIterationsCalculations.length) perceptronTrainedOnDataPointNum = 0;
    }
  }
  while (perceptronTrainedOnDataPointNum < dataPoints.length);
  
  

  for (let x=0; x<perceptronIterationsCalculations.length; x++) 
  {
    console.log( perceptronIterationsCalculations[x].length );
  }
}


function updateDOMinputRange()
{
  //document.getElementsByTagName("datalist")[0].style.visibility = "hidden";
  document.getElementsByTagName("input")[0].style.visibility = "hidden";
  document.getElementsByTagName("input")[0].setAttribute ("max",   0);
  document.getElementsByTagName("input")[0].value = 0;
  
  if (perceptronIterationsCalculations[0].length > 1) 
  {
    document.getElementsByTagName("input")[0].setAttribute ("max", perceptronIterationsCalculations[0].length-1);
    document.getElementsByTagName("input")[0].value = perceptronIterationsCalculations[0].length-1;

    /*
    for (let i=0; i<perceptronIterationsCalculations.length; i++) 
    {
      document.getElementsByTagName("datalist")[0].appendChild (document.createElement("option"));
      document.getElementsByTagName("option")[i].setAttribute ("value", i);
    }
    document.getElementsByTagName("datalist")[0].style.visibility = "visible";
    */

    document.getElementsByTagName("input")[0].style.visibility = "visible";
  }
}


function createPerceptronVizIterations() 
{
  perceptronIterationsViz = new Array (perceptronIterationsCalculations.length);

  for (let x=0; x<perceptronIterationsViz.length; x++) 
  {
    perceptronIterationsViz[x] = new Array (perceptronIterationsCalculations[x].length);
  }

  for (let x=0; x<perceptronIterationsViz.length; x++) 
  {
    for (let y=0; y<perceptronIterationsViz[x].length; y++) 
    {
      perceptronIterationsViz[x][y] = 
        new PerceptronIterationViz 
        (
          perceptronIterationsCalculations[x][y], 
          y, 
          perceptronIterationsCalculations[x].length, 
          p5_containerSize().w, 
          p5_containerSize().h
        );
    }
  }

}


function drawPerceptronVizIterations (DOMinputRangeValue) 
{
  let selectedIteration = false;

  for (let x=0; x<perceptronIterationsViz.length; x++) 
  {
    for (let i=0; i<perceptronIterationsViz[x].length; i++) 
    {
      if (i === int(DOMinputRangeValue)) selectedIteration = true;
      else selectedIteration = false;

      let y = (height/perceptronIterationsViz.length) * x;

      perceptronIterationsViz[x][i].drawPerceptronIterationViz (selectedIteration, y);

      /*
      if (i<perceptronIterationsViz.length-1) 
      {
        drawDashedLine 
        (
          perceptronIterationsViz[i].getPerceptronIterationX(), 
          perceptronIterationsViz[i+1].getPerceptronIterationX(), 
          perceptronIterationsViz[i].getPerceptronIterationY()
        );
      }
      */

    }
  }
}

/*
function drawDashedLine (posX, nextposX, posY) 
{
  strokeWeight (1);
  stroke (colorMiddle);
  
  let dashWidth = 2;
  let i = 0;

  if (posX < nextposX) 
  {
    for (let x=posX; x<nextposX; x+=dashWidth)
    {
      if (i%2 === 0) line (x, posY, x+dashWidth, posY);
      i++;
    }
  }
  else 
  {
    for (let x=posX; x>nextposX; x-=dashWidth)
    {
      if (i%2 === 0) line (x, posY, x-dashWidth, posY);
      i++;
    }
  }
  
}
*/