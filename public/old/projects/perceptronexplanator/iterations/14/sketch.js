

let p5ColorBackground, p5ColorBackgroundMiddle, p5ColorMiddle, p5ColorForegroundMiddle, p5ColorForeground;
let loadSystemEnabled;


function setup() 
{
  //setAttributes("antialias", true);
  const canvas = createCanvas (p5_containerSize.w, p5_containerSize.h);//, WEBGL); // See crossBrowserDOM.js
  canvas.parent("p5_container");

  // See crossBrowserDOM.js
  p5ColorBackground       = color (colorBackground);
  p5ColorBackgroundMiddle = color (colorBackgroundMiddle);
  p5ColorMiddle           = color (colorMiddle);
  p5ColorForegroundMiddle = color (colorForegroundMiddle);
  p5ColorForeground       = color (colorForeground);

  loadSystem();
}


function loadSystem() 
{
  loadSystemEnabled = true;
}


function draw() 
{
  if (loadSystemEnabled) 
  {
    loadSystemEnabled = false;

    createPtronScript();
    updateDOMinputRange();
  }
  
  background (p5ColorBackground);
  drawLayout();
  
  drawPtronStoryboard (document.getElementsByTagName("input")[0].value);
}


function drawLayout() 
{
  push();
    translate (width/2, height/2);
    
      strokeWeight (2);
      stroke (p5ColorBackgroundMiddle);
      noFill();
       
      beginShape();
        vertex (-width/2, -height/2);
        vertex (       0, -height/2);
        vertex (       0,  height/2);
        vertex (-width/2,  height/2);
      endShape (CLOSE);
      
      beginShape();
        vertex (       0, -height/2);
        vertex ( width/2, -height/2);
        vertex ( width/2,  height/2);
        vertex (       0,  height/2);
      endShape (CLOSE);
  pop();
}


function drawPtronVizBoardwithP5 (x, y, e, s)
{
  /*
  if (e === 0) fill(p5ColorForegroundMiddle);
  else noFill();
  */
  noFill();

  if (s) stroke (p5ColorForeground);
  else stroke (p5ColorMiddle);

  strokeWeight (1);
  ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);
}



function drawPtronStoryboardRoutewithP5 (start, end, y) 
{
  strokeWeight (1);
  stroke (colorMiddle);
  
  let dashWidth = 2;
  let i = 0;

  if (start < end) 
  {
    for (let x=start; x<end; x+=dashWidth)
    {
      if (i%2 === 0) line (x, y, x+dashWidth, y);
      i++;
    }
  }
  else 
  {
    for (let x=start; x>end; x-=dashWidth)
    {
      if (i%2 === 0) line (x, y, x-dashWidth, y);
      i++;
    }
  }
  
}