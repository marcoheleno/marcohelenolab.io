//
// This function returns random data points, 
// with random horizontal positions
//
function getRandomData (numDataPoints, lowerBound, upperBound, goal) 
{
  const dataPoints = new Array (numDataPoints);
  
  for (let i=0; i<dataPoints.length; i++) 
  {
    const horizontalPositionWithFloatValue = Math.random() * (upperBound - (lowerBound)) + (lowerBound);
    const x = Math.trunc (horizontalPositionWithFloatValue); // float to int
    
    dataPoints[i] = 
    {
      inputs: [x, 1], //1 is the Bias value. If by chance(random) the value of x is 0 
      goal: goal
    };
  }

  return dataPoints;
}
