

const dataPoints = new Array (0);


//
// This function returns random data points, 
// with random horizontal positions
//
function getRandomData (numDataPoints, goal, lowerBound, upperBound) 
{
  if (dataPoints.length > 0) dataPoints.length = 0;

  let x;
  
  for (let i=0; i<numDataPoints; i++) 
  {
    x = Math.random() * (upperBound - (lowerBound)) + (lowerBound);
    
    dataPoints.push 
    (
      {
        inputs: [x, 1], //1 is the Bias value. Because if by chance the random returns a value of 0 for x
        goal: goal
      }
    );
  }

  return dataPoints;
}
