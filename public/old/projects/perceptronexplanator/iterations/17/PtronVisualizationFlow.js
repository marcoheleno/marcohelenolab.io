

//
// In this function you are able to define
// the Perceptron's Activation Function
//
const ptronActivationFunction = function (sumWeightedInputs) 
{
  // The result is the sign of the sum, 
  // returns -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}





const ptronScript = new Array (0);


//
// This function runs the Perceptron (feedForward + train)
// with the selected data
// and saves all iterations calaculations.
// The name "... Script" is an analogy to an animation script
//
function createPtronScript (dataPoints) 
{
  if (dataPoints.length > 0) 
  {
    // See transparentPerceptron.js
    const transparentPerceptronInstance = new TransparentPerceptron (dataPoints[0].inputs.length, ptronActivationFunction, 0.0005);


    // Creates a two-dimensional array.
    // For each dataPoint a collection of the Perceptron's calculations.
    // The second dimension's length is number of iterations 
    // the Perceptron took until trainingError of 0.
    // The name "... Script" is an analogy to an animation script.
    for (let i=0; i<dataPoints.length; i++) 
    {
      ptronScript.push( new Array (0) );
    }


    let i = 0;
    let numDataPointsPerceptronTrained  = 0;
    let highestNumOfIterations = 0;
    do 
    {
      // For each dataPoint...
      // Check if the Perceptron is not on it's first iteration and
      // if it has a trainingError of 0 on current (i) dataPoint
      if (ptronScript[i].length > 0 && 
          ptronScript[i][ptronScript[i].length-1].trainingError === 0) 
      {
        // Count the number of dataPoints that the Perceptron is trained on 
        numDataPointsPerceptronTrained++;
      }

      // For each dataPoint...
      // If the Perceptron on this iteration does not have a trainingError of 0
      // we FeedForward the inputs
      // and Train the Perceptron
      else
      {
        transparentPerceptronInstance.feedForward (dataPoints[i].inputs);
        transparentPerceptronInstance.train (dataPoints[i].goal);

        // We add to the second dimension of the array ptronScript
        // the Perceptron's calculations of this iteration
        ptronScript[i].push( transparentPerceptronInstance.getAllTransparentPerceptronCalculations() ); 
      }

      // Iterate through all the dataPoints
      if (i < ptronScript.length-1) i++;

      // If allready iterated through all the dataPoints
      else 
      {
        i = 0; // Restart iteration counter
        
        // If the Perceptron has not trained successfully on all dataPoints
        // also Restart the numDataPointsPerceptronTrained counter
        if (numDataPointsPerceptronTrained < ptronScript.length) numDataPointsPerceptronTrained = 0;
      }

      // Save the highest number of Iterations the Perceptron performed
      if (highestNumOfIterations < ptronScript[i].length) 
      {
        highestNumOfIterations = ptronScript[i].length;
      }

      // Repeat while the Perceptron has not successfully trained on all dataPoints
    }
    while (numDataPointsPerceptronTrained < dataPoints.length);

    //console.log(highestNumOfIterations);
    return highestNumOfIterations;
  }
}





//
// This function draws the Perceptron's Storyboard for Visualization.
// It drawns on the previously created ptronScript two-dimensional array values.
// For each dataPoint it is drawPtronStoryboard with all the iterations (drawPtronVizBoardwithP5) 
// the Perceptron went through.
//
function drawPtronStoryboard (dataPoints, DOMinputRangeValue) 
{
  let verticalSpaceBetweenDataPoints = p5_containerSize.h / (dataPoints.length+1);
  let xUser, xPtron, y;
  let s = false;

  for (let i=0; i<dataPoints.length; i++) 
  {
    xUser = dataPoints[i].inputs[0];
    y = verticalSpaceBetweenDataPoints * (i+1);
    drawPtronVizBoardwithP5 (xUser, y, s, true);
  }

  if (ptronScript.length > 0) 
  {
    let xLeft, yLeft, xLeftControl, yLeftControl, xRight, yRight, xRightControl, yRightControl, distance;
    let start, end;

    // c = column
    for (let c=0; c<ptronScript.length; c++) 
    {
      // l = line
      for (let l=0; l<ptronScript[c].length; l++) 
      {
        xPtron = p5_containerSize.w/2 + ptronScript[c][l].feedForwardCalculation;
        y = verticalSpaceBetweenDataPoints * (c+1);

        // Iteration selected by DOMinputRangeValue?
        if( l === Math.trunc(DOMinputRangeValue) ) s = true; 
        else s = false;
        drawPtronVizBoardwithP5 (xPtron, y, s, false);


        if (c===0 && l===0) 
        {
          distance = abs(xUser - xPtron);

          if (xUser < xPtron) 
          {
            xLeft = xUser;
            yLeft = y;
            xLeftControl = xUser + distance/6;
            yLeftControl = y - distance;
            xRight = xPtron;
            yRight = y;
            xRightControl = xPtron - distance/6;
            yRightControl = y - distance;
          }

          else 
          {
            xLeft = xPtron;
            yLeft = y;
            xLeftControl = xPtron + distance/6;
            yLeftControl = y - distance;
            xRight = xUser;
            yRight = y;
            xRightControl = xUser - distance/6;
            yRightControl = y - distance;
          }
        }

        if (s && ptronScript[c].length>1) 
        {
          start = xPtron;
          end = p5_containerSize.w/2;
          drawPtronStoryboardRoutewithP5 (start, end, y);
        }

      }
    }


    strokeWeight (1.5);
    stroke (p5colorForeground);
    noFill;
    //bezier(xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight);
    
    let steps = distance/3;
    for (let i=0; i <= steps; i++) 
    {
      let t = i / steps;
      let x = bezierPoint(xLeft, xLeftControl, xRightControl, xRight, t);
      let y = bezierPoint(yLeft, yLeftControl, yRightControl, yRight, t);
      point (x, y);
    }
  } 

}


