

let p5colorBackground, p5colorBackgroundMiddle, p5colorMiddle, p5colorForegroundMiddle, p5colorForeground;
let userPickXPosInterfaceEnabled, userPickGoalInterfaceEnabled, loadSystemEnabled, drawStoryboard;
let dataset, datasetIndex, userPickedGoal;

function setup() 
{
  const canvas = createCanvas (p5_containerSize.w, p5_containerSize.h); // See crossBrowserDOM.js
  canvas.parent("p5_container");

  // See crossBrowserDOM.js
  p5colorBackground       = color (colorBackground);
  p5colorBackgroundMiddle = color (colorBackgroundMiddle);
  p5colorMiddle           = color (colorMiddle);
  p5colorForegroundMiddle = color (colorForegroundMiddle);
  p5colorForeground       = color (colorForeground);

  loadSystem();
}


function loadSystem() 
{
  // See data.js
  // dataset = getRandomData (1, -1, -p5_containerSize.w/2, p5_containerSize.w/2);
     dataset = addUserPickedData (1, p5_containerSize.w/2);
     datasetIndex = 0;

  userPickXPosInterfaceEnabled = true;

  userPickedGoal = 0;
  userPickGoalInterfaceEnabled = false;
  
  ptronScript.length = 0;
  loadSystemEnabled = false;
}


function draw() 
{
  background (p5colorBackground);
  drawLayout (userPickedGoal);
  
  if (userPickXPosInterfaceEnabled) 
  {
    dataset[datasetIndex].inputs[0] = mouseX;
  }

  if (userPickGoalInterfaceEnabled) 
  {
    if (mouseX < p5_containerSize.w/2) userPickedGoal = -1;
    else userPickedGoal = 1;
  }

  if (loadSystemEnabled) 
  {
    loadSystemEnabled = false;

    const highestNumOfIterations = createPtronScript (dataset); // See data.js
    updateDOMinputRange (highestNumOfIterations);
  }
  
  drawPtronStoryboard (dataset, document.getElementsByTagName("input")[0].value);
}


function mouseReleased() 
{
  if (mouseX>0 && mouseX<p5_containerSize.w && mouseY>0 && mouseY<p5_containerSize.h) 
  {

    if (userPickXPosInterfaceEnabled) 
    {
      if (datasetIndex < dataset.length-1) datasetIndex++;
      else 
      {
        userPickXPosInterfaceEnabled = false;
        userPickGoalInterfaceEnabled = true;
      }
    }
    
    
    else if (userPickGoalInterfaceEnabled) 
    {
      for (let i=0; i<dataset.length; i++) 
      {
        dataset[i].goal = userPickedGoal;
      }
      userPickGoalInterfaceEnabled = false;
      loadSystemEnabled = true;
    }

  }
}


function drawLayout (userPickedGoal) 
{
  push();
    translate (p5_containerSize.w/2, p5_containerSize.h/2);
    
      strokeWeight (1);
      stroke (p5colorMiddle);

      if (userPickedGoal < 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (-p5_containerSize.w/2 + 1, -p5_containerSize.h/2 + 1);
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
        vertex (-p5_containerSize.w/2 + 1,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
      
      if (userPickedGoal > 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1,  p5_containerSize.h/2 - 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
  pop();
}


function drawPtronVizBoardwithP5 (x, y, s, u)
{
  if (u) 
  {
    stroke (p5colorMiddle);
    strokeWeight (1);
    fill (colorBackgroundMiddle);
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);

    stroke (p5colorBackground);
    strokeWeight (5);
    point (x, y);
  }

  else 
  {
    if (s) stroke (p5colorForeground);
    else stroke (p5colorMiddle);

    noFill();
    strokeWeight (1);
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);

    strokeWeight (5);
    //point (x, y);
  }
}



function drawPtronStoryboardRoutewithP5 (start, end, y) 
{
  strokeWeight (1);
  stroke (p5colorForeground);
  
  let dashWidth = 4;
  let i = 0;

  if (start < end) 
  {
    for (let x=start; x<end; x+=dashWidth)
    {
      point (x, y);
      i++;
    }
  }
  else 
  {
    for (let x=start; x>end; x-=dashWidth)
    {
      point (x, y);
      i++;
    }
  }
  
}