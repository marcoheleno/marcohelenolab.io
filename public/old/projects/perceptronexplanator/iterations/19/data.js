//
// This function returns random data points, 
// with random horizontal positions
//
function getRandomData (numDataPoints, goal, lowerBound, upperBound) 
{
  const dataPoints = new Array (numDataPoints);

  for (let i=0; i<dataPoints.length; i++) 
  {
    let x = Math.random() * (upperBound - (lowerBound)) + (lowerBound);
    
    dataPoints[i] = 
    {//inputs: [x, bias] Bias has the value of 1, because if by chance the x value is 0, we need the bias to untie
       inputs: [x, 1], 
       goal: goal
    };
  }

  return dataPoints;
}


function addUserPickedData (numMaxDataPoints, x) 
{
  const dataPoints = new Array (numMaxDataPoints);

  for (let i=0; i<numMaxDataPoints; i++) 
  {
    dataPoints[i] = 
    {//inputs: [x, bias] Bias has the value of 1, because if by chance the x value is 0, we need the bias to untie
       inputs: [x, 1], 
       goal: 0
    };
  }

  return dataPoints;
}
