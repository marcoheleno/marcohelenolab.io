

let p5colorBackground, p5colorBackgroundMiddle, p5colorMiddle, p5colorForegroundMiddle, p5colorForeground;
let userPickXPosInterfaceEnabled, userPickGoalInterfaceEnabled, startPtronEnabled;
let dataset, datasetIndex, userPickedGoal;


function setup() 
{
  const canvas = createCanvas (p5_containerSize.w, p5_containerSize.h); // See crossBrowserDOM.js
  canvas.parent("p5_container");

  // See crossBrowserDOM.js
  p5colorBackground       = color (colorBackground);
  p5colorBackgroundMiddle = color (colorBackgroundMiddle);
  p5colorMiddle           = color (colorMiddle);
  p5colorForegroundMiddle = color (colorForegroundMiddle);
  p5colorForeground       = color (colorForeground);

  re_startSystem();
}


function re_startSystem() 
{
  // See data.js
  // dataset = getRandomData (1, -1, -p5_containerSize.w/2, p5_containerSize.w/2);
  
  dataset = addUserPickedData (1, p5_containerSize.w/2);
  datasetIndex = 0;
  userPickXPosInterfaceEnabled = true;

  userPickedGoal = 0;
  userPickGoalInterfaceEnabled = false;
  
  ptronScript.length = 0;
  startPtronEnabled = false;
}


function draw() 
{
  background (p5colorBackground);
  drawPtronGoalPanelsWithP5 (userPickedGoal);
  
  if (userPickXPosInterfaceEnabled) 
  {
    dataset[datasetIndex].inputs[datasetIndex] = mouseX;
  }

  else 

  if (userPickGoalInterfaceEnabled) 
  {
    if (mouseX < p5_containerSize.w/2) userPickedGoal = -1;
    else userPickedGoal = 1;

    for (let i=0; i<dataset.length; i++) 
    {
      dataset[i].goal = userPickedGoal;
    }
  }

  else 

  if (startPtronEnabled) 
  {
    startPtronEnabled = false;

    const highestNumOfIterations = createPtronScript (dataset); // See data.js
    updateDOMinputRange (highestNumOfIterations);
  }
  
  let DOMinputRangeValue = document.getElementsByTagName("input")[0].value;
  drawPtronStoryboard (dataset, DOMinputRangeValue);

  drawPtronGoalPanelsWithP5 (0);
}


function mouseReleased() 
{
  if (mouseX>0 && mouseX<p5_containerSize.w && mouseY>0 && mouseY<p5_containerSize.h) 
  {

    if (userPickXPosInterfaceEnabled) 
    {
      if (datasetIndex < dataset.length-1) datasetIndex++;
      else 
      {
        userPickXPosInterfaceEnabled = false;
        userPickGoalInterfaceEnabled = true;
      }
    }
    
    else 
    
    if (userPickGoalInterfaceEnabled) 
    {
      userPickGoalInterfaceEnabled = false;
      startPtronEnabled = true;
    }

  }
}


function drawPtronGoalPanelsWithP5 (goal) 
{
  push();
    translate (p5_containerSize.w/2, p5_containerSize.h/2);
    
      strokeWeight (1);
      stroke (colorForegroundMiddle);

      if (goal < 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (-p5_containerSize.w/2 + 1, -p5_containerSize.h/2 + 1);
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
        vertex (-p5_containerSize.w/2 + 1,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
      
      if (goal > 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1,  p5_containerSize.h/2 - 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
  pop();
}


function drawPtronStoryboardElementWithP5 (x, y, selectedWithDOMinputRangeValue, pickedByUser)
{
  
  if (pickedByUser) 
  {
    noStroke();
    fill (p5colorMiddle);
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);

    stroke (p5colorBackground);
    strokeWeight (5);
    point (x, y);
  }

  else 
  {
    if (selectedWithDOMinputRangeValue) stroke (p5colorForeground);
    else stroke (colorForegroundMiddle);

    strokeWeight (1);
    noFill();
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);
    
    if (selectedWithDOMinputRangeValue) 
    {
      strokeWeight (5);
      point (x, y);
    }
  }

}


function drawPtronStoryboardRouteWithP5 (beginning, end, y) 
{
  const xPositions = [beginning, end];
  if (beginning > end) xPositions.reverse();

  strokeWeight (1);
  stroke (p5colorForeground);
  
  const dashWidth = 4;
  for (let x=xPositions[0]; x<xPositions[1]; x+=dashWidth)
  {
    point (x, y);
  }
}
