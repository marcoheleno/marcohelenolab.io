

let p5colorBackground, p5colorBackgroundMiddle, p5colorMiddle, p5colorForegroundMiddle, p5colorForeground;
let userPickXPosInterfaceEnabled, userPickGoalInterfaceEnabled, startPtronEnabled;
let addHTMLelementOnceEnabled, dataset, datasetIndex, userPickedGoal, ptronIterationRun, ptronIterationTrain;
let forRunPtron, forTrainPtron, highestNumOfIterations;


function setup() 
{
  const canvas = createCanvas (p5_containerSize.w, p5_containerSize.h); // See crossBrowserDOM.js
  canvas.parent("p5_container");

  // See crossBrowserDOM.js
  p5colorBackground       = color (colorBackground);
  p5colorBackgroundMiddle = color (colorBackgroundMiddle);
  p5colorMiddle           = color (colorMiddle);
  p5colorForegroundMiddle = color (colorForegroundMiddle);
  p5colorForeground       = color (colorForeground);

  re_startSystem();
}


function re_startSystem() 
{
  addHTMLelementOnceEnabled = true;
  
  // See data.js
  // dataset = getRandomData (1, -1, -p5_containerSize.w/2, p5_containerSize.w/2);
  
  dataset = addUserPickedData (1, p5_containerSize.w/2);
  datasetIndex = 0;
  userPickXPosInterfaceEnabled = true;

  userPickedGoal = 0;
  userPickGoalInterfaceEnabled = false;
  
  ptronScript.length = 0;
  ptronIterationRun = -1;
  ptronIterationTrain = -1;
  startPtronEnabled = false;
}


function draw() 
{
  background (p5colorBackground);
  drawPtronGoalPanelsWithp5 (userPickedGoal);
  
  if (userPickXPosInterfaceEnabled) 
  {
    if (addHTMLelementOnceEnabled) 
    {
      addHTMLelementOnceEnabled = false;
      document.getElementById("p5_interchangeableTooltip2ButtonContainer").appendChild (document.createElement("p"));
      document.getElementsByTagName("p")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
      document.getElementsByTagName("p")[0].innerHTML = "Pick the initial Datapoint position";
    }
    
    dataset[datasetIndex].inputs[0] = mouseX;
  }

  else 

  if (userPickGoalInterfaceEnabled) 
  {
    if (addHTMLelementOnceEnabled) 
    {
      addHTMLelementOnceEnabled = false;
      document.getElementById("p5_interchangeableTooltip2ButtonContainer").appendChild (document.createElement("p"));
      document.getElementsByTagName("p")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
      document.getElementsByTagName("p")[0].innerHTML = "Pick panel side to define system's goal";
    }
    
    if (mouseX < p5_containerSize.w/2) userPickedGoal = -1;
    else userPickedGoal = 1;

    for (let i=0; i<dataset.length; i++) 
    {
      dataset[i].goal = userPickedGoal;
    }
  }

  else 

  if (startPtronEnabled) 
  {
    startPtronEnabled = false;
    highestNumOfIterations = createPtronScript (dataset); // See data.js

    /*
    ptronIterationTrain = highestNumOfIterations;
    addHTMLelementOnceEnabled = true;
    forRunPtron = false;
    */
  }
  
  drawUserPickedStoryboard (dataset);

  if (addHTMLelementOnceEnabled && forRunPtron) 
  {
    addHTMLelementOnceEnabled = false;
    forRunPtron = false;
    document.getElementById("p5_interchangeableTooltip2Button").remove();
    document.getElementById("p5_interchangeableTooltip2ButtonContainer").appendChild (document.createElement("button"));
    document.getElementsByTagName("button")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
    document.getElementsByTagName("button")[0].setAttribute ("type", "button");
    document.getElementsByTagName("button")[0].setAttribute ("onclick", "runPtron()");
    document.getElementsByTagName("button")[0].innerHTML = "Run Perceptron";
  }

  else

  if (addHTMLelementOnceEnabled && forTrainPtron) 
  {
    addHTMLelementOnceEnabled = false;
    forTrainPtron = false;
    document.getElementById("p5_interchangeableTooltip2Button").remove();
    document.getElementById("p5_interchangeableTooltip2ButtonContainer").appendChild (document.createElement("button"));
    document.getElementsByTagName("button")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
    document.getElementsByTagName("button")[0].setAttribute ("type", "button");
    document.getElementsByTagName("button")[0].setAttribute ("onclick", "trainPtron()");
    document.getElementsByTagName("button")[0].innerHTML = "Train Perceptron";
  }

  

  if (ptronIterationTrain === highestNumOfIterations) 
  {
    console.log ("final");
    if (addHTMLelementOnceEnabled) 
    {
      addHTMLelementOnceEnabled = false;
      updateDOMinputRange (highestNumOfIterations);
    }

    let DOMinputRangeValue = document.getElementsByTagName("input")[0].value;
    drawPtronStoryboard (dataset, DOMinputRangeValue);
    drawRouteFromSelectedWithDOMinputRangeValue2TrainedPtronStoryboard (dataset, DOMinputRangeValue);
    drawRouteFromUserPicked2InitialPtronStoryboard (dataset);
  }

  else if (ptronIterationRun > -1 && forRunPtron) 
  {
    drawPtronStoryboard (dataset, ptronIterationRun);
    drawRouteFromUserPicked2InitialPtronStoryboard (dataset);
  }

  else if (ptronIterationTrain > -1 && forTrainPtron) 
  {
    drawRouteFromSelectedWithDOMinputRangeValue2TrainedPtronStoryboard (dataset, ptronIterationTrain);
  }

  drawPtronGoalPanelsWithp5 (0);
}


function runPtron () 
{
  console.log("run");
  addHTMLelementOnceEnabled = true;
  forTrainPtron = true;
  ptronIterationRun++;
}


function trainPtron () 
{
  console.log("train");
  addHTMLelementOnceEnabled = true;
  forRunPtron = true;
  ptronIterationTrain++;
}


function mouseReleased() 
{
  if (mouseX>0 && mouseX<p5_containerSize.w && mouseY>0 && mouseY<p5_containerSize.h) 
  {

    if (userPickXPosInterfaceEnabled) 
    {
      if (datasetIndex < dataset.length-1) datasetIndex++;
      else 
      {
        userPickXPosInterfaceEnabled = false;
        userPickGoalInterfaceEnabled = true;

        document.getElementById("p5_interchangeableTooltip2Button").remove();
        addHTMLelementOnceEnabled = true;
      }
    }
    
    else 
    
    if (userPickGoalInterfaceEnabled) 
    {
      userPickGoalInterfaceEnabled = false;
      startPtronEnabled = true;
      addHTMLelementOnceEnabled = true;
      forRunPtron = true;
    }

  }
}


function drawPtronGoalPanelsWithp5 (goal) 
{
  push();
    translate (p5_containerSize.w/2, p5_containerSize.h/2);
    
      strokeWeight (1);
      stroke (colorForegroundMiddle);

      if (goal < 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (-p5_containerSize.w/2 + 1, -p5_containerSize.h/2 + 1);
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
        vertex (-p5_containerSize.w/2 + 1,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
      
      if (goal > 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1,  p5_containerSize.h/2 - 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
  pop();
}


function drawPtronStoryboardElementWithp5 (x, y, selectedWithDOMinputRangeValue, pickedByUser)
{
  
  if (pickedByUser) 
  {
    noStroke();
    fill (p5colorMiddle);
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);

    stroke (p5colorBackground);
    strokeWeight (5);
    point (x, y);
  }

  else 
  {
    if (selectedWithDOMinputRangeValue) stroke (p5colorForeground);
    else stroke (colorForegroundMiddle);

    strokeWeight (1);
    noFill();
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);
    
    if (selectedWithDOMinputRangeValue) 
    {
      strokeWeight (5);
      point (x, y);
    }
  }

}


function drawPtronStoryboardRouteWithp5 (beginning, end, y) 
{
  const xPositions = [beginning, end];
  if (beginning > end) xPositions.reverse();

  strokeWeight (1);
  stroke (p5colorForeground);
  
  const dashWidth = 4;
  for (let x=xPositions[0]; x<xPositions[1]; x+=dashWidth)
  {
    point (x, y);
  }
}


function drawRouteFromUserPicked2InitialPtronStoryboardWithp5 (xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight, distance, userPickedXonLeftPanel) 
{
  strokeWeight (1.4);
  stroke (p5colorForeground);
  noFill;
  //bezier(xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight);

  let dashWidth = distance/3;
  for (let d=0; d<=dashWidth; d++) 
  {
    if (userPickedXonLeftPanel) stroke( map(d, 0, dashWidth+1, 100, 250) );
    else stroke( map(d, 0, dashWidth+1, 250, 100) );

    let t = d/dashWidth;
    let x = bezierPoint (xLeft, xLeftControl, xRightControl, xRight, t);
    let y = bezierPoint (yLeft, yLeftControl, yRightControl, yRight, t);

    point (x, y);
  }
}
