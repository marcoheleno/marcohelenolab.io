

let whiteColor, lightGrayColor, mediumGrayColor, darkGrayColor, blackColor;
let bottomStatusPanelX, bottomStatusPanelY, statusPanelsWidth, statusPanelsHeight;
let perceptronIterationPanelX, perceptronIterationPanelY;
let perceptronIterationPanelWidth, perceptronIterationPanelHeight;
let secondsBetweenDrawAndShow, secondsBetweenPanelSelect;


function preferences() 
{
  whiteColor      = color (250);
  lightGrayColor  = color (240);
  mediumGrayColor = color (210);
  darkGrayColor   = color (150);
  blackColor      = color (50);

  statusPanelsWidth  = width;
  statusPanelsHeight = 30;
  topStatusPanelX    = 0;
  topStatusPanelY    = -height/2 + statusPanelsHeight/2;
  bottomStatusPanelX = 0;
  bottomStatusPanelY = height/2 - statusPanelsHeight/2;

  perceptronIterationPanelX = 0;
  perceptronIterationPanelY = 0;

  if (height > width) 
  {
    perceptronIterationPanelWidth  = percentage2Pixels (45, width);
    perceptronIterationPanelHeight = percentage2Pixels (55, height);
  }

  else 
  {
    perceptronIterationPanelWidth  = percentage2Pixels (30, width);
    perceptronIterationPanelHeight = percentage2Pixels (70, height); 
  }

  secondsBetweenDrawAndShow = 5 * 1000; // milliseconds
  secondsBetweenPanelSelect = 1 * 1000;
}


function percentage2Pixels (percentage, numPixels)
{
  let percentage2Pixels = int( (percentage*numPixels) / 100 );
  if (percentage2Pixels%2 != 0) percentage2Pixels++;
  return percentage2Pixels;
}
