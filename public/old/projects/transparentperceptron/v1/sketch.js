

let fontLight, fontRegular, fontMedium;
let startSys, drawTime, tempMenu;


function preload() 
{
  fontLight   = loadFont ("IBM_Plex_Mono/IBMPlexMono-Light.ttf");
  fontRegular = loadFont ("IBM_Plex_Mono/IBMPlexMono-Regular.ttf");
  fontMedium  = loadFont ("IBM_Plex_Mono/IBMPlexMono-Medium.ttf");
}


function setup() 
{
  setAttributes("antialias", true);
  createCanvas (windowWidth, windowHeight, WEBGL);
  startSys = true;
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;

    preferences();
    background (whiteColor);
    createStatusPanels();
    createPerceptronIterationRepPanel();

    runPerceptron();
    createPerceptronVizIterations();
  }
  
  if (!perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() ) 
  {
    renderPerceptronVizIterations();
  }

  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() && 
     !perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() ) 
  {
    drawPerceptronVizIterations();
    drawTime = millis();
  }

  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() && 
      (millis()-drawTime) > secondsBetweenDrawAndShow) 
  {
    background (whiteColor);
    revealPerceptronVizIterations();
    updatePerceptronIterationRepPanel (perceptronIterationsCalculations[0]);
    drawPerceptronIterationRepPanel (perceptronIterationPanelWidth/2, 0, 0);
  }

  drawTopStatusPanel();
  drawBottomStatusPanel();
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  startSys = true;
}
