

function percentage2Pixels (percentage, numPixels)
{
  let percentage2Pixels = int( (percentage*numPixels) / 100 );
  if (percentage2Pixels%2 != 0) percentage2Pixels++;
  return percentage2Pixels;
}


function domRotatedSliderTranslate (x, y, sliderWidth) 
{
  // see style.css
  let domRotatedSliderTranslate = 
  {
    x:  width/2 - sliderWidth/2 + x, 
    y: height/2 + sliderWidth/2 + y
  };

  return domRotatedSliderTranslate;
}