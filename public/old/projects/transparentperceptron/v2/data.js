

function getData (panelwidth) 
{
  let x = int( random(-panelwidth/2, panelwidth/2) );
  let b = 1; //Bias

  let g = 1; //Goal
  if (x < 0) g = -1;
  
  let randomDataPoint = 
  {
    inputs: [x, b],
    goal: g
  };

  return randomDataPoint;
}