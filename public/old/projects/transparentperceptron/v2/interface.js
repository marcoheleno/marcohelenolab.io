

let topStatusPanel, bottomStatusPanel, perceptronIterationRepPanel;
let repPanGridColumn, repPanelGridLine;
let perceptronFlowEllipseDiameter;
let leftXweightSlider, rightBiasWeightSlider;


function createStatusPanels() 
{
  topStatusPanel    = createGraphics (statusPanelsWidth, statusPanelsHeight, P2D);

  bottomStatusPanel = createGraphics (statusPanelsWidth, statusPanelsHeight, P2D);
  bottomStatusPanel.push();
    bottomStatusPanel.translate(statusPanelsWidth/2, statusPanelsHeight/2 - 2); // 2D coordinate system

      bottomStatusPanel.background (whiteColor);
      bottomStatusPanel.fill (darkGrayColor);
      bottomStatusPanel.noStroke();
      bottomStatusPanel.textFont ("IBM Plex Mono");
      bottomStatusPanel.textAlign (CENTER, CENTER);
      bottomStatusPanel.textSize (10);
      bottomStatusPanel.text ("TRANSPARENT PERCEPTRON v0.2", 0, 0);

  bottomStatusPanel.pop();
}


function updateTopStatusPanel (status) 
{
  topStatusPanel.push();
    topStatusPanel.translate(statusPanelsWidth/2, statusPanelsHeight/2 + 2); // 2D coordinate system

      topStatusPanel.background (whiteColor);
      topStatusPanel.fill (darkGrayColor);
      topStatusPanel.noStroke();
      topStatusPanel.textFont ("IBM Plex Mono");
      topStatusPanel.textAlign (CENTER, CENTER);
      topStatusPanel.textSize (10);
      topStatusPanel.text (status, 0, 0);

  topStatusPanel.pop();
}


function drawTopStatusPanel() 
{
  push();
    translate (topStatusPanelX, topStatusPanelY, 0);
      
      textureMode (NORMAL);
      texture (topStatusPanel);
      noStroke();
        
      beginShape();
        vertex (-statusPanelsWidth/2, -statusPanelsHeight/2, 0, 0, 0);
        vertex ( statusPanelsWidth/2, -statusPanelsHeight/2, 0, 1, 0);
        vertex ( statusPanelsWidth/2,  statusPanelsHeight/2, 0, 1, 1);
        vertex (-statusPanelsWidth/2,  statusPanelsHeight/2, 0, 0, 1);
      endShape (CLOSE);
    pop();
}


function drawBottomStatusPanel() 
{
  push();
    translate (bottomStatusPanelX, bottomStatusPanelY, 0);
      
      textureMode (NORMAL);
      texture (bottomStatusPanel);
      noStroke();
        
      beginShape();
        vertex (-statusPanelsWidth/2, -statusPanelsHeight/2, 0, 0, 0);
        vertex ( statusPanelsWidth/2, -statusPanelsHeight/2, 0, 1, 0);
        vertex ( statusPanelsWidth/2,  statusPanelsHeight/2, 0, 1, 1);
        vertex (-statusPanelsWidth/2,  statusPanelsHeight/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}
