

let startSys, drawTime;


function setup() 
{
  setAttributes("antialias", true);
  createCanvas (windowWidth, windowHeight, WEBGL);
  startSys = true;
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;

    preferences();
    background (whiteColor);
    createStatusPanels();

    runPerceptron();
    createPerceptronVizIterations();
  }
  
  if (!perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() ) 
  {
    renderPerceptronVizIterations();
  }

  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() && 
     !perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() ) 
  {
    drawPerceptronVizIterations();
    drawTime = millis();
  }

  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() && 
      perceptronIterationsViz.length > 1 && 
      (millis()-drawTime) > millisBetweenDrawLayersAndSeparateLayers) 
  {
    background (whiteColor);
    revealPerceptronVizIterations();
  }

  drawTopStatusPanel();
  drawBottomStatusPanel();
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  startSys = true;
}
