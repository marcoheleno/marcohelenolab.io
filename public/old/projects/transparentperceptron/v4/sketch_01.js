

let sketch01 = function(p01) {

p01.setup = function() 
{
  p01.createCanvas (p01.windowWidth/3, p01.windowHeight);
};


p01.draw = function() 
{
  p01.background(255);

  /*
  p01.strokeWeight(1);
  p01.stroke(0);
  let x1 = p01.map (xmin, xmin, xmax, 0, p01.width);
  let y1 = p01.map (f(xmin), ymin, ymax, p01.height, 0);
  let x2 = p01.map (xmax, xmin, xmax, 0, p01.width);
  let y2 = p01.map (f(xmax), ymin, ymax, p01.height, 0);
  p01.line (x1, y1, x2, y2);
  */
  
  

  for (let i=0; i<data.length; i++) 
  {
    p01.strokeWeight(1);
    p01.stroke(100);

    if (data[i].output > 0) p01.fill(255);
    else p01.fill(100);

    let x = p01.map(data[i].input[0], xmin, xmax, 0, p01.width);
    let y = p01.map(data[i].input[1], ymin, ymax, p01.height, 0);

    p01.ellipse(x, y, 6, 6);
  };
  
  p01.strokeWeight(1);
  p01.stroke(0);
  p01.noFill();
  p01.rect(0, 0, p01.width-1, p01.height-1);
};

p01.windowResized = function() 
{
  p01.resizeCanvas (p01.windowWidth/3, p01.windowHeight);
};

};

let myp501 = new p5(sketch01);