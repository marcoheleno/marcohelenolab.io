

let sketch03 = function(p03) {

p03.setup = function() 
{
  p03.createCanvas (p03.windowWidth/3, p03.windowHeight);
};


p03.draw = function() 
{
  /*
  p03.strokeWeight(1);
  p03.stroke(0);
  let x1 = p03.map (xmin, xmin, xmax, 0, p03.width);
  let y1 = p03.map (f(xmin), ymin, ymax, p03.height, 0);
  let x2 = p03.map (xmax, xmin, xmax, 0, p03.width);
  let y2 = p03.map (f(xmax), ymin, ymax, p03.height, 0);
  p03.line (x1, y1, x2, y2);
  */
  

  
  p03.strokeWeight(1);
  p03.stroke(0, 30);
  let weights = ptron.getTransformedWeights();

  x1 = xmin;
  y1 = (-weights[2] - weights[0] * x1) / weights[1];
  x2 = xmax;
  y2 = (-weights[2] - weights[0] * x2) / weights[1];

  x1 = p03.map (x1, xmin, xmax, 0, p03.width);
  y1 = p03.map (y1, ymin, ymax, p03.height, 0);
  x2 = p03.map (x2, xmin, xmax, 0, p03.width);
  y2 = p03.map (y2, ymin, ymax, p03.height, 0);
  p03.line (x1, y1, x2, y2);


  p03.strokeWeight(1);
  p03.stroke(0);
  p03.noFill();
  p03.rect(0, 0, p03.width-1, p03.height-1);
};

p03.windowResized = function() 
{
  p03.resizeCanvas (p03.windowWidth/3, p03.windowHeight);
};

};

let myp503 = new p5(sketch03);