

let transparentPerceptronVizFlow = [0, 0, 0];
let transparentPerceptronIterationsVisualizations = [];
let iterationsCounter, x=0;


function runPerceptron() 
{
  const randomDataPoint = getData(); // See data.js

  window.alert ("Goal: " + randomDataPoint.goal);

  let transparentPerceptronInstance = new TransparentPerceptron 
  (
    randomDataPoint.inputs.length, // See data.js
    perceptronActivationFunction,  // See perceptronActivationFunction.js
    preferences.perceptronLearningRate // See preferences.js
  );

  let i = 0;
  do 
  {
    // See transparentPerceptron.js
    let weights = transparentPerceptronInstance.getWeights();
    let sumWeightedInputs = transparentPerceptronInstance.sumOfWeightedInputs (randomDataPoint.inputs);
    let activationResult = transparentPerceptronInstance.runActivationFunction();
    let guessError = transparentPerceptronInstance.calculateError (randomDataPoint.goal);

    transparentPerceptronIterationsVisualizations.push
    (
      new PerceptronIterationViz
      (
        weights,
        sumWeightedInputs,
        activationResult,
        guessError
      )
    );

    transparentPerceptronInstance.adjustWeights (randomDataPoint.inputs);

    i++;
  }
  while (transparentPerceptronIterationsVisualizations[i-1].guessError != 0);

  if (transparentPerceptronIterationsVisualizations[transparentPerceptronIterationsVisualizations.length-1].guessError === 0) 
  {
    transparentPerceptronVizFlow[0] = 1;
  }
}


function renderPerceptronVizIterations() 
{
  for (let i=0; i<transparentPerceptronIterationsVisualizations.length; i++) 
  {
    transparentPerceptronIterationsVisualizations[i].render();
  }

  if (transparentPerceptronIterationsVisualizations[transparentPerceptronIterationsVisualizations.length-1].isRendered) 
  {
    transparentPerceptronVizFlow[1] = 1;
    iterationsCounter = 0;
  }
}


function drawPerceptronVizIterations() 
{
  let y = -height/2+(preferences.gridCellH*iterationsCounter);

  transparentPerceptronIterationsVisualizations[iterationsCounter].draw (x, y, 0);
  
  if (iterationsCounter >= transparentPerceptronIterationsVisualizations.length-1 && 
      transparentPerceptronIterationsVisualizations[transparentPerceptronIterationsVisualizations.length-1].isDrawn) 
  {
    transparentPerceptronVizFlow[2] = 1;
    iterationsCounter = 0;
  }
  else 
  {
    iterationsCounter++;
    x += (preferences.gridCellW/2) * transparentPerceptronIterationsVisualizations[iterationsCounter-1].activationResult;
  }
}
