

const preferences = 
{
  colorLayer1: "rgb(250, 250, 250)",
  colorLayer2: "rgb(240, 240, 240)",
  colorLayer3: "rgb(150, 150, 150)",
  colorLayer4: "rgb(50, 50, 50)",

  gridCellW: percentage2Pixels (5, window.innerWidth),
  gridCellH: 40,

  perceptronLearningRate: 0.0005
};
