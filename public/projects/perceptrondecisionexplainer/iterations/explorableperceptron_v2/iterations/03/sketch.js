let perceptronInst;
let desiredOutput, points, prevIterationCoordinate;
let interationNum, interationSectionWidth, interationSectionHeight;
let font, fontBold, titleHeight;
let systemStarted, x, sliderX, sliderXweight, sliderBias, buttonStart, buttonReStart, labelWidth;


function preload() 
{
  font = loadFont ("IBM_Plex_Mono/IBMPlexMono-Regular.ttf");
  fontBold = loadFont ("IBM_Plex_Mono/IBMPlexMono-Bold.ttf");
}


function setup() 
{
  createCanvas (windowWidth, windowHeight);
  //noLoop();
  noSmooth();
  drawTitleBar();
}


function drawTitleBar() 
{
  labelWidth = 110;
  let x = 12;
  let y = 12;

  sliderX = createSlider (-width/2, width/2, random(-width/2, width/2), 1);
  sliderX.position (x+labelWidth, y);
  sliderX.style ("width", width-(x*2)-(labelWidth*2)+"px");

  sliderXweight = createSlider (-1, 1, random(-1, 1), 0.1);
  sliderXweight.position (x+labelWidth, y*3);
  sliderXweight.style ("width", width-(x*2)-(labelWidth*2)+"px");

  sliderBias = createSlider (-1, 1, random(-1, 1), 0.1);
  sliderBias.position (x+labelWidth, y*5);
  sliderBias.style ("width", width-(x*2)-(labelWidth*2)+"px");

  buttonStart = createButton ("Start");
  buttonStart.style ("width", "100px");
  buttonStart.position (width-100-x, (130/5)*4);
  buttonStart.mousePressed (startSystem);
  
  buttonReStart = createButton ("Restart / Randomize");
  buttonReStart.style("width", "150px");
  buttonReStart.position(width-250-(x*2), (130/5)*4);
  buttonReStart.mousePressed(reStartSystem);

  reStartSystem();
}


function reStartSystem() 
{
  background (30);
  systemStarted = false;
  x = 0;
  sliderX.value (random(-width/2, width/2));
  sliderXweight.value (random(-1, 1));
  sliderBias.value    (random(-1, 1));
  loop();
}


function startSystem () 
{
  // To keep track of the number of interations.
  interationNum = 0;
  // Controlling the "frameRate()" for interation visualization.
  frameRate (1);
  
  prevIterationCoordinate = 
  {
    x: 0, 
    y: (interationSectionHeight)/4
  };
  
  systemStarted = true;
}


function draw() 
{
  if (!systemStarted) 
  {
    titleHeight = 130;

    noStroke();   
    fill (100);
    rect (0, 0, width, titleHeight);

    // Defining "interationSection" Width and Height.
    interationSectionWidth  = width/2;
    interationSectionHeight = 60;

    
    // Generate random "x" coordinate.
    //x = 100;//random (-width/2, width/2);
    x = sliderX.value();
    let y = 0;
    
    // Defining the "bias" for the point.
    let bias = 1;
    
    
    // Defining "desiredOutput" for the point
    // This means that this perceptronInst is going to be 
    // trained with Supervised Learning.
    desiredOutput = 1;
    if (x < 0) desiredOutput = -1;


    // Creating a point object.
    // Each point instance has:
    // inputs: [horizontal coordinate, bias].
    // output: desiredOutput for Supervised Learning.
    // y: vertical coordinate.
    // s: point's size.
    points = 
    {
      inputs: [x, bias],
      output: desiredOutput,
      y: y,
      s: interationSectionHeight/4
    };


    // "perceptronInst" receives 2 inputs:
    // 1º input: Number of weights, they are "x" and "bias".
    // 2º input: "learningRate", lower means slower.
    //perceptronInst = new Perceptron (2, 0.0005);
    perceptronInst = new Perceptron (sliderXweight.value(), sliderBias.value(), 0.0005);

    
    fill (0, 255, 0);
    textFont (fontBold);

    textSize (13);
    textAlign (LEFT, CENTER);
    text ("X COORDINATE", 12, (12+6.5)*1);
    text ("X WEIGHT",     12, (12+2.5)*3);
    text ("BIAS WEIGHT",  12, (12+1.5)*5);
    textAlign (RIGHT, CENTER);
    text (sliderX.value(),       width-12-55, (12+6.5)*1);
    text (sliderXweight.value(), width-12-55, (12+2.5)*3);
    text (sliderBias.value(),    width-12-55, (12+1.5)*5);

    textSize (14);
    textAlign (LEFT, CENTER);
    let desiredOutputPanel = "";
    if (desiredOutput < 0) desiredOutputPanel = "LEFT";
    else desiredOutputPanel = "RIGHT";

    let  desiredOutputText =  "GOAL: GO TO " + desiredOutputPanel + " PANEL";
    text (desiredOutputText, 12, (titleHeight/5)*4);
    
    interationNum = 0;
  }
  
  
  push();
      // (0, 0) at the center of the canvas.
      translate (width/2, titleHeight + interationSectionHeight*interationNum + interationSectionHeight/2);
      

      // "feedForward()" the point's input to the Perceptron
      // for it to guess or predict 
      // towards the "desiredOutput",
      // left or Right Section, 
      // -1 or +1.
      perceptronInst.feedForward (points.inputs);

      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = perceptronInst.getActivationResult();
      print ("Perceptron's Guess: " + neuronGuessing);


      // Keeping track of the 
      // tranformations/calculations 
      // of each point and iteration
      // Aka the horizontal coordinate of the perceptronInst's Guess.
      x = perceptronInst.getFeedForwardGuess();
      // Simplifying the vertical coordinate.
      let y = (interationSectionHeight)/4 + points.y;


      guessedOutputPanel = "";
      if (neuronGuessing < 0) guessedOutputPanel = "LEFT";
      else guessedOutputPanel = "RIGHT";


      // Coloring the iteration graph background based on the point's "desiredOutput".
      let backgroundFirstQuad =  color (255);
      let backgroundSecondQuad = color (255);
      
      if (points.output < 0) 
      {
        if (points.output == neuronGuessing) backgroundFirstQuad =  color (120, 230, 120);
        else backgroundFirstQuad = color (255);
        backgroundSecondQuad = color (200);
      }
      else 
      {
        backgroundFirstQuad =  color (200);
        if (points.output == neuronGuessing) backgroundSecondQuad =  color (120, 230, 120);
        else backgroundSecondQuad = color (255);
      }
      
      noStroke();
      fill (backgroundFirstQuad);
      rect (-width/2, -interationSectionHeight/2, interationSectionWidth, interationSectionHeight);
      
      fill (backgroundSecondQuad);
      rect (0,        -interationSectionHeight/2, interationSectionWidth, interationSectionHeight);
      print ("Desired Output: " + points.output);

      
      if (systemStarted) 
      {
      // Keeping track of the number of interations.
      print ("Interation Number: " + (interationNum+1) );


      // "train()" the perceptronInst based on
      // the difference between the 
      // "desiredOutput" and "neuronGuessing".
      // This will result on a deviation, the guessing error.
      // This deviation/error is used to adjust the weight and bias.
      perceptronInst.train (points.output, neuronGuessing);
      
      
      // Drawing the number of the iteration.
      fill (30);
      textSize (13);
      textFont (font);
      textAlign (LEFT, CENTER);

      // Stopping iteration once the perceptronInst is trained
      let trainingStatus = "";
      if (neuronGuessing == points.output) 
      {
        noLoop();
        
        if (interationNum==0) 
        {
          trainingStatus = "INITIAL WEIGHTS:      " + perceptronInst.getSumWeights() + " (PERCEPTRON GUESSES " + guessedOutputPanel + " PANEL)";
        }
        else 
        {
          trainingStatus = "ALGORITHM OPTIMIZED! " + perceptronInst.getSumWeights() + " (GOAL REACHED)";
        }
      }
      
      else
      {
        if (interationNum==0) 
        {
          trainingStatus = "INITIAL WEIGHTS:      " + perceptronInst.getSumWeights() + " (PERCEPTRON GUESSES " + guessedOutputPanel + " PANEL)";
        }
        else 
        {
          trainingStatus = "OPTIMIZING ALGORITHM: " + perceptronInst.getSumWeights();
        }
      }
      
      text ((interationNum+1) +" - "+ trainingStatus, -interationSectionWidth + 12, (interationSectionHeight/4)*-1-2);
      print("Optimizing: " + perceptronInst.getSumWeights());


      // Drawing a graphical representation 
      // of the connection between iterations.
      // A representation of the continuous 
      // transformations the perceptronInst is apply to the data.
      stroke (30);
      strokeWeight (1.5);
      line (prevIterationCoordinate.x, prevIterationCoordinate.y, 
                                    x, prevIterationCoordinate.y);

      // Keeping track of the previous 
      // iteration coordinates.
      prevIterationCoordinate = 
      {
        x: x, 
        y: (interationSectionHeight)/4
      };


      interationNum++;
      }


      // Drawing a graphical representation 
      // of the perceptronInst's guessing.
      stroke (30);
      strokeWeight (points.s/3);print(x);
      point (x, y);
      
      strokeWeight (1.5);
      noFill();
      ellipse (x, y, points.s, points.s);
      
    pop();
    
    print ("--------------------------");
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);

  // Restart system
  startSystem();
}

