


class Perceptron 
{

  constructor (num_weights) 
  {
    this.num_weights = num_weights + 1; //bias weight
    this.weights = new Array (this.num_weights);
    this.learning_rate = 0.00005;
    this.startWithRandomWeights();
  }



  startWithRandomWeights() 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = random (-1, 1);
    }
  }



  getTransformedWeights() 
  {
    return this.weights;
  }


  
  runFeedForwardWith (inputs)
  {
    this.sum_weighted_inputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs += inputs[i] * this.weights[i];
    }

    this.runActivationFunction();
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs > 0) this.activation_result =  1;
    else                              this.activation_result = -1;
  }



  getActivationResult() 
  {
    return this.activation_result;
  }



  getTrainingErrorWith (goal) 
  {
    this.training_error = goal - this.activation_result;
  }



  runTrainingWith (inputs) 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * inputs[i];
    }
  }
  
}
