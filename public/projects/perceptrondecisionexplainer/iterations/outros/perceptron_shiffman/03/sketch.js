

let data = new Array(100);
let ptron;
let count = 0;


let y = [];


function setup() 
{
  createCanvas (windowWidth, windowHeight);

  ptron = new Perceptron (2);
  
  for (let i=0; i<data.length; i++) 
  {
    let x = random (width);
        y[i] = random (height);
    let answer = 0;

    if (x < width/2) answer = -1;
    else answer = 1;

    data[i] = 
    {
      input: [x, 1],
      output: answer
    };
  }
}


function draw() 
{
  background(255);

  line (width/2, 0, width/2, height);
  //let weights = ptron.getTransformedWeights();


  for (let i=0; i<data.length; i++) 
  {
    ptron.runFeedForwardWith(data[i].input);
    const guess = ptron.getActivationResult();

    if (guess > 0) fill(255);
    else fill(0);

    ellipse (data[i].input[0], y[i], 6, 6);

    ptron.getTrainingErrorWith (data[i].output);
    ptron.runTrainingWith (data[i].input);
  }
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}

