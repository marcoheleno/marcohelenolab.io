

let data = new Array(1000);
let ptron;

let x = [];
let y = [];


function setup() 
{
  createCanvas (windowWidth, windowHeight);

  ptron = new Perceptron (2);
  
  for (let i=0; i<data.length; i++) 
  {
    x[i] = random (width);
    y[i] = random (height);

    const random_picker = int(random(0, 2));
    let p = 0;
    if (random_picker === 0) p = 10;
    else
    if (random_picker === 1) p = 30;

    let answer = 0;
    if (p < 20) answer = -1;
    else answer = 1;

    data[i] = 
    {
      input: [p, 1],
      output: answer
    };
  }
}


function draw() 
{
  background(255);

  for (let i=0; i<data.length; i++) 
  {
    ptron.runFeedForwardWith (data[i].input);

    if (ptron.getActivationResult() > 0) fill(255);
    else fill(0);

    ellipse (x[i], y[i], data[i].input[0]);

    ptron.getTrainingErrorWith (data[i].output);
    ptron.runTrainingWith (data[i].input);
  }
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}

