

let data = new Array(100);
let ptron;

let c = [];
let x = [];
let y = [];


function setup() 
{
  createCanvas (windowWidth, windowHeight);

  ptron = new Perceptron (2);
  
  for (let i=0; i<data.length; i++) 
  {
    x[i] = random (width);
    y[i] = random (height);
    c[i] = 255;

    const random_picker = int(random(0, 2));
    let p = 0;
    if (random_picker === 0) p = 10;
    else
    if (random_picker === 1) p = 30;

    let answer = 0;
    if (p < 20) answer = -1;
    else answer = 1;

    data[i] = 
    {
      input: [p, 1],
      output: answer
    };
  }
}


function draw() 
{
  background(255);

  for (let i=0; i<data.length; i++) 
  {
    fill (c[i]);
    ellipse (x[i], y[i], data[i].input[0]);
  }
}


function mousePressed() 
{
  for (let i=0; i<data.length; i++) 
  {
    ptron.runFeedForwardWith (data[i].input);

    if (ptron.getActivationResult() > 0) c[i] = 255;
    else c[i] = 0;

    ptron.getTrainingErrorWith (data[i].output);
    ptron.runTrainingWith (data[i].input);
  }
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}

