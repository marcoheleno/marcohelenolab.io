


function generateRandomDataSet (num_data_points, lower_bound, upper_bound) 
{
  const threshold = 0;
  const data_set = new Array (num_data_points);

  const ten_percente_pos = upper_bound * 0.5;
  const ten_percente_neg = lower_bound * 0.5;
  // For explainability reasons.
  // Do not want users to wait long for a decision.
  
  for (let i=0; i<data_set.length; i++) 
  {
    let x = 0;
    while (x>ten_percente_neg && x<ten_percente_pos) 
    {
      x = Math.random() * (upper_bound - (lower_bound)) + (lower_bound);
    }

    let goal = -1;
    if (x > threshold) goal = 1;
    
    data_set[i] = 
    {
      index: i, 
      inputs: [x, 1], 
      goal: goal
    };
  }

  return data_set;
}
