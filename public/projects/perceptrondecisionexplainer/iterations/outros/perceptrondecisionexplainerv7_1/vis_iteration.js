


let iteration_graphics_layer_1, iteration_graphics_layer_2;



function createEachIterationGraphics (stage_w, stage_h) 
{
  if (iteration_graphics_layer_1 == null) iteration_graphics_layer_1 = new Array(0);
  if (iteration_graphics_layer_2 == null) iteration_graphics_layer_2 = new Array(0);

  iteration_graphics_layer_1.push( createGraphics (stage_w, stage_h) );
  iteration_graphics_layer_2.push( createGraphics (stage_w, stage_h) );
}



function addGoalStageToEachIterationGraphics (i, data_g) 
{
  const gradiente_width = (iteration_graphics_layer_1[i].width/4)*3;

  iteration_graphics_layer_1[i].clear();

  iteration_graphics_layer_1[i].strokeWeight (1);
  iteration_graphics_layer_1[i].strokeCap (SQUARE);

  for (let gs=iteration_graphics_layer_1[i].width/2; gs>(iteration_graphics_layer_1[i].width/2-gradiente_width); gs--) 
  {
    const transparency = map (gs, iteration_graphics_layer_1[i].width/2, iteration_graphics_layer_1[i].width/2-gradiente_width, 200, 0);
    iteration_graphics_layer_1[i].stroke (0, transparency);
    iteration_graphics_layer_1[i].line (gs, 0, gs, iteration_graphics_layer_1[i].height);
  }

  for (let gs=iteration_graphics_layer_1[i].width/2; gs<(iteration_graphics_layer_1[i].width/2+gradiente_width); gs++) 
  {
    const transparency = map (gs, iteration_graphics_layer_1[i].width/2, iteration_graphics_layer_1[i].width/2+gradiente_width, 200, 0);
    iteration_graphics_layer_1[i].stroke (255, transparency);
    iteration_graphics_layer_1[i].line (gs, 0, gs, iteration_graphics_layer_1[i].height);
  }

  /*
  iteration_graphics_layer_1[i].noFill();

  if (data_g === -1) 
  {
    iteration_graphics_layer_1[i].stroke (goal_color);
    iteration_graphics_layer_1[i].rect (0, 0, iteration_graphics_layer_1[i].width/2, iteration_graphics_layer_1[i].height);
    iteration_graphics_layer_1[i].stroke (color_pos_1);
    iteration_graphics_layer_1[i].rect (iteration_graphics_layer_1[i].width/2, 0, iteration_graphics_layer_1[i].width/2, iteration_graphics_layer_1[i].height);
  }

  else 

  if (data_g === 1) 
  {
    iteration_graphics_layer_1[i].stroke (color_pos_1);
    iteration_graphics_layer_1[i].rect (0, 0, iteration_graphics_layer_1[i].width/2, iteration_graphics_layer_1[i].height);
    iteration_graphics_layer_1[i].stroke (goal_color);
    iteration_graphics_layer_1[i].rect (iteration_graphics_layer_1[i].width/2, 0, iteration_graphics_layer_1[i].width/2, iteration_graphics_layer_1[i].height);
  }
  */
}


 
function addDataPointToEachIterationGraphics (i, data_x) 
{
  /*
  const x = map(data_x, -width/2, width/2, 0, iteration_graphics_layer_1[i].width);

  iteration_graphics_layer_1[i].noFill();
  iteration_graphics_layer_1[i].strokeCap (SQUARE);
  iteration_graphics_layer_1[i].stroke (color_neg_2);

  iteration_graphics_layer_1[i].strokeWeight (1);
  iteration_graphics_layer_1[i].circle (x, iteration_graphics_layer_1[i].height/2, iteration_graphics_layer_1[i].height-(iteration_graphics_layer_1[i].height/4));

  iteration_graphics_layer_1[i].strokeWeight (5);
  iteration_graphics_layer_1[i].point  (x, iteration_graphics_layer_1[i].height/2);
  */
}



function drawEachGoalStageAndDataPointIterationGraphics (i, stage_x, stage_y, stage_rY) 
{
  push();
    translate (stage_x, stage_y, 0);
    rotateY( radians(stage_rY) );

      noStroke();

      textureMode (NORMAL);
      texture (iteration_graphics_layer_1[i]);

      beginShape();
        vertex (-iteration_graphics_layer_1[i].width/2, -iteration_graphics_layer_1[i].height/2, 0, 0, 0);
        vertex ( iteration_graphics_layer_1[i].width/2, -iteration_graphics_layer_1[i].height/2, 0, 1, 0);
        vertex ( iteration_graphics_layer_1[i].width/2,  iteration_graphics_layer_1[i].height/2, 0, 1, 1);
        vertex (-iteration_graphics_layer_1[i].width/2,  iteration_graphics_layer_1[i].height/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}



function clearEachPerceptronDecisionIterationGraphics (i) 
{
  iteration_graphics_layer_2[i].clear();
  iteration_graphics_layer_2[i].background(200, 110, 110, 1);
}



function addPerceptronDecisionToEachIterationGraphics (i, decision_x, training_error, selected, data_g) 
{
  const x = map(decision_x, -width/2, width/2, 0, iteration_graphics_layer_2[i].width);

  iteration_graphics_layer_2[i].noFill();
  iteration_graphics_layer_2[i].strokeCap (SQUARE);

  if (data_g === -1) 
  {
    iteration_graphics_layer_2[i].stroke (0);
  }
  else if (data_g === 1)
  {
    iteration_graphics_layer_2[i].stroke (255);
  }

  iteration_graphics_layer_2[i].strokeWeight (1);
  iteration_graphics_layer_2[i].line (x, iteration_graphics_layer_2[i].height/2-(iteration_graphics_layer_2[i].height/2), 
                                      x, iteration_graphics_layer_2[i].height/2+(iteration_graphics_layer_2[i].height/2) );

  
  if (selected) 
  {
    iteration_graphics_layer_2[i].strokeWeight (10);
  }
  else 
  {
    iteration_graphics_layer_2[i].strokeWeight (5);
  }
  
  if (training_error === -2 || training_error === 2) 
  {
    iteration_graphics_layer_2[i].stroke (non_goal_color);
  }
  else if (training_error === 0)
  {
    iteration_graphics_layer_2[i].stroke (goal_color);
  }

  iteration_graphics_layer_2[i].point  (x, iteration_graphics_layer_2[i].height/2);
}



function drawEachPerceptronDecisionIterationGraphics (i, stage_x, stage_y, stage_rY) 
{
  push();
    translate (stage_x, stage_y, 0);
    rotateY( radians(stage_rY) );

      noStroke();

      textureMode (NORMAL);
      texture (iteration_graphics_layer_2[i]);

      beginShape();
        vertex (-iteration_graphics_layer_2[i].width/2, -iteration_graphics_layer_2[i].height/2, 0, 0, 0);
        vertex ( iteration_graphics_layer_2[i].width/2, -iteration_graphics_layer_2[i].height/2, 0, 1, 0);
        vertex ( iteration_graphics_layer_2[i].width/2,  iteration_graphics_layer_2[i].height/2, 0, 1, 1);
        vertex (-iteration_graphics_layer_2[i].width/2,  iteration_graphics_layer_2[i].height/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}