


let visualization_moment;
let data_set, iteration_set;
let perceptron, perceptron_trained_with_data_point;
let input_range_weight_x_value, input_range_weight_bias_value;
let previous_input_range_weight_x_value, previous_input_range_weight_bias_value;
let input_range_weights_disabled;



function setResponsive() 
{
  const inner_window_dimensions = 
  {
    w: window.innerWidth, 
    h: window.innerHeight
  };

  document.body.style.width  = inner_window_dimensions.w + "px";
  document.body.style.height = inner_window_dimensions.h + "px";


  if (inner_window_dimensions.w <= 1024 && inner_window_dimensions.w > inner_window_dimensions.h || 
      inner_window_dimensions.w <= 1024 && deviceOrientation === "landscape") 
  {
    //document.getElementsByTagName("main")[0].remove();
    //Adiciona um overlayer com a informação de por o dispositivo em modo portrait
  }

  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth + "px";

  const canvas_container_dimensions = 
  {
    w: document.getElementById("p5_canvas_container").offsetWidth, 
    h: document.getElementById("p5_canvas_container").offsetHeight
  };

  return canvas_container_dimensions
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();
  visualization_moment = 1;
  perceptron_trained_with_data_point = 0;
  previous_input_range_weight_x_value = 0;
  previous_input_range_weight_bias_value = 0;
  input_range_weights_disabled = false;
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas_container_dimensions = setResponsive();
  const canvas = createCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h, WEBGL);
  canvas.parent("p5_canvas_container");
  colorMode(RGB, 255, 255, 255, 100);
}



function windowResized() 
{
  location.reload();
}



function draw() 
{

  if (visualization_moment>1)
  {
    if (perceptron_trained_with_data_point < data_set.length)
    {
      input_range_weight_x_value    = Number(document.getElementById("input_range_weight_x").value);
      input_range_weight_bias_value = Number(document.getElementById("input_range_weight_bias").value);

      if (previous_input_range_weight_x_value != input_range_weight_x_value || 
          previous_input_range_weight_bias_value != input_range_weight_bias_value) 
      {
        visualization_moment = 2;
      }
    }
    
    else
    
    if (!input_range_weights_disabled) 
    {
      document.getElementById("input_range_weight_x").disabled = true;
      document.getElementById("input_range_weight_bias").disabled = true;

      document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_cursor", "default");
      document.querySelector(':root').style.setProperty("--input_range_weight_x_track_cursor", "default");
      document.querySelector(':root').style.setProperty("--input_range_weight_bias_thumb_cursor", "default");
      document.querySelector(':root').style.setProperty("--input_range_weight_bias_track_cursor", "default");
      
      input_range_weights_disabled = true;
      visualization_moment = 2;
    }
  }

  
  switch (visualization_moment) 
  {

    case 1:
      defineColorVariables();
      data_set = generateRandomDataSet (1, -width/2, width/2);

      for (let i=0; i<data_set.length; i++) 
      {
        createEachIterationGraphics (width, height/data_set.length);
        addGoalStageToEachIterationGraphics (i, data_set[i].goal);
        addDataPointToEachIterationGraphics (i, data_set[i].inputs[0]);
        clearEachPerceptronDecisionIterationGraphics (i);
      }

      perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);

      document.getElementById("input_range_weight_x").setAttribute    ("step", perceptron.getLearningRate()*10);
      document.getElementById("input_range_weight_bias").setAttribute ("step", perceptron.getLearningRate());

      document.getElementById("input_range_weight_x").value    = parseFloat(perceptron.getWeightsValues()[0]).toFixed(6);
      document.getElementById("input_range_weight_bias").value = parseFloat(perceptron.getWeightsValues()[1]).toFixed(6);

      visualization_moment = 2;
      break;


    case 2:
      background (color_neg_2);
      
      if (perceptron_trained_with_data_point < data_set.length) 
      {
        if (mouseIsPressed) 
        {
          for (let i=0; i<data_set.length; i++) 
          {
            perceptron.runWithHumanTraining (data_set[i], data_set.length, input_range_weight_x_value, input_range_weight_bias_value);

            const data_point_w = width;
            const data_point_h = height/data_set.length;
            const data_point_x = 0;
            const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
            const data_point_ry = map(perceptron.getIteration().feedforward_result, -data_point_w/2, data_point_w/2, -90, 90);

            drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
            drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
          }
        }

        else

        if (!mouseIsPressed) 
        {
          for (let i=0; i<data_set.length; i++) 
          {
            perceptron.runWithHumanTraining (data_set[i], data_set.length, input_range_weight_x_value, input_range_weight_bias_value);
            perceptron.saveIteration();

            /*
            console.log (perceptron.getIteration().data_point_index);
            console.log (perceptron.getIteration().weight_x);
            console.log (perceptron.getIteration().weight_bias);
            console.log (perceptron.getIteration().feedforward_result);
            console.log (perceptron.getIteration().activation_result);
            console.log (perceptron.getIteration().training_error);
            console.log ("-----");
            */

            if (perceptron.getIteration().training_error === 0) 
            {
              perceptron_trained_with_data_point++;
            }

            if (i === data_set.length-1 && perceptron_trained_with_data_point < data_set.length)
            {
              perceptron_trained_with_data_point = 0;
            }

            const data_point_w = width;
            const data_point_h = height/data_set.length;
            const data_point_x = 0;
            const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
            const data_point_ry = map(perceptron.getIteration().feedforward_result, -data_point_w/2, data_point_w/2, -90, 90);

            drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
            drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
          }

          previous_input_range_weight_x_value = input_range_weight_x_value;
          previous_input_range_weight_bias_value = input_range_weight_bias_value;
        }

        iteration_set = perceptron.getIterationsSet();
      }

      visualization_moment = 3;
      break;


    case 3:
      background (color_neg_2);

      const num_iterations_per_data_point = (iteration_set.length/data_set.length)-1;
      let num_data_points_perceptron_trained_in_this_iteration = 0;
      
      for (let i=0; i<data_set.length; i++) 
      {
        const data_point_w = width;
        const data_point_h = height/data_set.length;
        const data_point_x = 0;
        const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
        const data_point_ry = map(iteration_set[((iteration_set.length-1)-(data_set.length-1))+i].feedforward_result, -data_point_w/2, data_point_w/2, -90, 90);

        clearEachPerceptronDecisionIterationGraphics (i);

        for (let j=0; j<iteration_set.length; j++) 
        {
          if (i === iteration_set[j].data_point_index) 
          {
            /*
            console.log (iteration_set[i].data_point_index);
            console.log (iteration_set[i].weight_x);
            console.log (iteration_set[i].weight_bias);
            console.log (iteration_set[i].feedforward_result);
            console.log (iteration_set[i].activation_result);
            console.log (iteration_set[i].training_error);
            console.log ("-----");
            */
          
            addPerceptronDecisionToEachIterationGraphics (i, iteration_set[j].feedforward_result, iteration_set[j].training_error, false, data_set[i].goal);
          }

          if ( j === ((num_iterations_per_data_point*data_set.length)+i) && iteration_set[j].training_error === 0) 
          {
            num_data_points_perceptron_trained_in_this_iteration++;
          }
        }
        
        drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
        drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);

        if (num_data_points_perceptron_trained_in_this_iteration === data_set.length) 
        {
          document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_color", "rgb("+red(goal_color)+","+green(goal_color)+","+blue(goal_color)+")");
          document.querySelector(':root').style.setProperty("--input_range_weight_bias_thumb_color", "rgb("+red(goal_color)+","+green(goal_color)+","+blue(goal_color)+")");
        }
        else 
        {
          document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_color", "rgb("+red(non_goal_color)+","+green(non_goal_color)+","+blue(non_goal_color)+")");
          document.querySelector(':root').style.setProperty("--input_range_weight_bias_thumb_color", "rgb("+red(non_goal_color)+","+green(non_goal_color)+","+blue(non_goal_color)+")");
        }
      }
      
      break;

    default: console.log ("Switch conditional out of case.");
  }

  //console.log (frameRate());
}


/*
function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
*/
