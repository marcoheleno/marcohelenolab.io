

function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");
}


function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);
}


function draw() 
{
  fill (255, 0, 0);
  rect (0, 0, width, height);
}

