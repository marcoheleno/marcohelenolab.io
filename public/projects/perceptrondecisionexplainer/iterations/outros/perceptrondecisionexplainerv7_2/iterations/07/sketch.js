


function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  clear();

  if (device_orientation === "p") 
    portraitOrientationInterface();
}


