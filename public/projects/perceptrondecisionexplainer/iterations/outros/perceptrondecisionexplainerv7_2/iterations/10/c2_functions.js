


let world;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, 0, width, height));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const constForce = new c2.ConstForce (0, 0.5);
  world.addForce (constForce);
}



function addParticles2PhysicsWorldWith (data_set) 
{
  for (let i=0; i<data_set.length; i++) 
  {
    const p = new c2.Particle (random(width), 0);
    p.radius = data_set[i].input[0];
    p.color = 255;

    world.addParticle(p);
  }
}



function drawRotatingFunnelAngledAt (a) 
{
  push();
    translate (width/2, height/2);
  
    angleMode(DEGREES);
    rotate (a);
  
      const p1 = screenPosition (-width/2, 0);
      const p2 = screenPosition (-30, 0);
      const p3 = screenPosition ( 30, 0);
      const p4 = screenPosition (width/2, 0);

  pop();

  stroke (255);
  strokeWeight (1);
  
  beginShape();
    vertex (p1.x, p1.y);
    vertex (p2.x, p2.y);
  endShape();

  beginShape();
    vertex (p3.x, p3.y);
    vertex (p4.x, p4.y);
  endShape();
  


  let polygon_1Constraint, polygon_2Constraint;

  if (polygon_1Constraint != undefined) world.removeConstraint (polygon_1Constraint);
  if (polygon_2Constraint != undefined) world.removeConstraint (polygon_2Constraint);
  
  const polygon_1 = new c2.Polygon ([
      new c2.Point (p1.x, p1.y), 
      new c2.Point (p2.x, p2.y)
  ]);
  
  const polygon_2 = new c2.Polygon ([
      new c2.Point (p3.x, p3.y), 
      new c2.Point (p4.x, p4.y)
  ]);
  
  polygon_1Constraint = new c2.PolygonConstraint (polygon_1);
  world.addConstraint (polygon_1Constraint);
  
  polygon_2Constraint = new c2.PolygonConstraint (polygon_2);
  world.addConstraint (polygon_2Constraint);
}



function drawParticles() 
{
  world.update();

  ellipseMode(RADIUS);
  strokeWeight (1);
  noFill();
  
  for (let i=0; i<world.particles.length; i++) 
  {
    stroke  (world.particles[i].color);
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);
  }
}


