


function addRandomDataSetWith (data_set_length) 
{
  const data_set = new Array (data_set_length);

  for (let i=0; i<data_set.length; i++) 
  {
    const particle_min_radius = 10;
    const particle_max_radius = 20;
    const random_radius_picker = int(random(2));
    let particle_radius = 0;
    if (random_radius_picker === 0) particle_radius = particle_min_radius;
    else
    if (random_radius_picker === 1) particle_radius = particle_max_radius;



    const ptron_training_threshold = particle_min_radius + ((particle_max_radius-particle_min_radius)/2);
    let ptron_training_solution = 0;
    if (particle_radius < ptron_training_threshold) ptron_training_solution = -1;
    else ptron_training_solution = 1;



    const data_bias = 1;
    data_set[i] = 
    {
      input: [particle_radius, data_bias],
      output: ptron_training_solution
    };
  }

  return data_set;
}


