


let world;
let funnel_left__margin, funnel_left__center, funnel_right_center, funnel_right_margin;
let divider_h;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, -data_max_radius*10, width, height+data_max_radius*10));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const constForce = new c2.ConstForce (0, 0.5); //0.2
  world.addForce (constForce);
}



function addLineField2PhysicsWorld() 
{
  const lineField = new c2.LineField (new c2.Line (width/2, 0, width/2, height-divider_h-data_max_radius*2), 0.15);
  world.addForce(lineField);
}



function drawRotatingFunnelAngledAt (a) 
{
  const x = width/2;
  const y = height/5;
  const margin = data_min_radius/2;

  push();
    translate (x, y);
  
    angleMode(DEGREES);
    rotate (a);
  
      funnel_left__margin = screenPosition (-x+margin, -y);
      funnel_left__center = screenPosition (-(data_max_radius+2),  0);
      funnel_left__neck = screenPosition (-(data_max_radius+2),  50);

      funnel_right_neck = screenPosition ( data_max_radius+2,  50);
      funnel_right_center = screenPosition ( data_max_radius+2,  0);
      funnel_right_margin = screenPosition ( x-margin, -y);
  pop();

  stroke (255);
  strokeWeight (1);
  
  beginShape();
    vertex (funnel_left__margin.x, funnel_left__margin.y);
    vertex (funnel_left__center.x, funnel_left__center.y);
    vertex (funnel_left__neck.x, funnel_left__neck.y);
  endShape();

  beginShape();
    vertex (funnel_right_neck.x, funnel_right_neck.y);
    vertex (funnel_right_center.x, funnel_right_center.y);
    vertex (funnel_right_margin.x, funnel_right_margin.y);
  endShape();

  divider_h = height-230;
  const divider_top____center = {x:width/2, y:height-divider_h};
  const divider_bottom_center = {x:width/2, y:height};
  
  beginShape();
    vertex (divider_top____center.x, divider_top____center.y);
    vertex (divider_bottom_center.x, divider_bottom_center.y);
  endShape();



  let polygon_funnel_left____constraint, polygon_funnel_right___constraint, polygon_divider_center_constraint;

  if (polygon_funnel_left____constraint != undefined) world.removeConstraint (polygon_funnel_left____constraint);
  if (polygon_funnel_right___constraint != undefined) world.removeConstraint (polygon_funnel_right___constraint);
  if (polygon_divider_center_constraint != undefined) world.removeConstraint (polygon_divider_center_constraint);
  
  const polygon_funnel__left = new c2.Polygon ([
      new c2.Point (funnel_left__margin.x, funnel_left__margin.y), 
      new c2.Point (funnel_left__center.x, funnel_left__center.y),
      new c2.Point (funnel_left__neck.x, funnel_left__neck.y)
  ]);
  
  const polygon_funnel_right = new c2.Polygon ([
      new c2.Point (funnel_right_neck.x, funnel_right_neck.y),
      new c2.Point (funnel_right_center.x, funnel_right_center.y), 
      new c2.Point (funnel_right_margin.x, funnel_right_margin.y)
  ]);

  const polygon_divider_center = new c2.Polygon ([
    new c2.Point (divider_top____center.x, divider_top____center.y), 
    new c2.Point (divider_bottom_center.x, divider_bottom_center.y)
  ]);
  
  polygon_funnel_left____constraint = new c2.PolygonConstraint (polygon_funnel__left);
  world.addConstraint (polygon_funnel_left____constraint);
  
  polygon_funnel_right___constraint = new c2.PolygonConstraint (polygon_funnel_right);
  world.addConstraint (polygon_funnel_right___constraint);

  polygon_divider_center_constraint = new c2.PolygonConstraint (polygon_divider_center);
  world.addConstraint (polygon_divider_center_constraint);
}



function addParticles2PhysicsWorldWith() 
{

  if (world.particles.length < data_set.length) 
  {
    const p = new c2.Particle (width/2, -data_max_radius*2);
    p.radius = data_set[world.particles.length].input[0];
    //p.mass = map(data_set[i].input[0], data_min_radius, data_max_radius, 0.5, 0.2) + random(0.45);
    p.color = 255;

    world.addParticle(p);
  }

  /*
  for (let i=0; i<data_set.length; i++) 
  {
    const p = new c2.Particle (random(data_max_radius, width-data_max_radius), -data_max_radius*10);
    p.radius = data_set[i].input[0];
    //p.mass = map(data_set[i].input[0], data_min_radius, data_max_radius, 0.5, 0.2) + random(0.45);
    p.color = 255;

    world.addParticle(p);
  }
  */
}



function drawParticles() 
{
  world.update();

  ellipseMode(RADIUS);
  strokeWeight (1);
  noFill();

  let num_particles_below_funnel_neck = 0;
  console.log (world.particles.length, data_set.length);
  for (let i=0; i<world.particles.length; i++) 
  {
    stroke  (world.particles[i].color);
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);

    if (world.particles[i].position.y > height/2) num_particles_below_funnel_neck++;
  }

  if (num_particles_below_funnel_neck === world.particles.length) 
  {
    addParticles2PhysicsWorldWith();
  }
}


