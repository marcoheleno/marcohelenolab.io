


let data_set;



function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");


  
  // See getColorsFromCSS() in index.js
  color_darker   = color( red(color_darker),   green(color_darker),   blue(color_darker)   );
  color_dark     = color( red(color_dark),     green(color_dark),     blue(color_dark)     );
  color_neutral  = color( red(color_neutral),  green(color_neutral),  blue(color_neutral)  );
  color_bright   = color( red(color_bright),   green(color_bright),   blue(color_bright)   );
  color_brighter = color( red(color_brighter), green(color_brighter), blue(color_brighter) );
  
  

  data_set = new Array (10);
  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }


  
  addScreenPositionFunction();
  addPhysicsWorld();
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  background (color_dark);


  /*
  strokeWeight(1);
  noFill();
  stroke (255, 0, 0);
  rect (0, 0, width, height);
  */


  if (device_orientation === "p") portraitOrientationInterface();



  drawFunnel2PhysicsWorld();
  drawDivider2PhysicsWorldAngledAt (1);
  drawParticle2PhysicsWorld();
}


