


let world;
let funnel_height, funnel_left_polygon_constraint, funnel_right_polygon_constraint;
let divider_lower_margin, divider_height;
let polygon_divider_constraint, polygon_divider_floor_constraint;
let ptron_decision_attractor_x, ptron_decision_attractor;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, -data_set[0].max_radius, width, height+data_set[0].max_radius));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line (0, height, width, height), 1);
  world.addForce (floorForce);
}



function drawFunnel2PhysicsWorld() 
{
  funnel_height = data_set[0].max_radius * 2;
  const neck_h  = data_set[0].max_radius;

  const funnel_left_margin  = {x:0, y:0};
  const funnel_left_center  = {x:width/2 - data_set[0].max_radius, y:funnel_height - neck_h};
  const funnel_left_neck    = {x:width/2 - data_set[0].max_radius, y:funnel_height};

  const funnel_right_margin = {x:width, y:0};
  const funnel_right_center = {x:width/2 + data_set[0].max_radius, y:funnel_height - neck_h};
  const funnel_right_neck   = {x:width/2 + data_set[0].max_radius, y:funnel_height};



  if (funnel_left_polygon_constraint  != undefined) world.removeConstraint (funnel_left_polygon_constraint);
  if (funnel_right_polygon_constraint != undefined) world.removeConstraint (funnel_right_polygon_constraint);
  
  const funnel_left_polygon = new c2.Polygon 
  ([
    new c2.Point (funnel_left_margin.x, funnel_left_margin.y), 
    new c2.Point (funnel_left_center.x, funnel_left_center.y), 
    new c2.Point (funnel_left_neck.x,   funnel_left_neck.y)
  ]);

  const funnel_right_polygon = new c2.Polygon 
  ([
    new c2.Point (funnel_right_margin.x, funnel_right_margin.y), 
    new c2.Point (funnel_right_center.x, funnel_right_center.y), 
    new c2.Point (funnel_right_neck.x,   funnel_right_neck.y)
  ]);

  funnel_left_polygon_constraint  = new c2.PolygonConstraint (funnel_left_polygon);
  world.addConstraint (funnel_left_polygon_constraint);
  
  funnel_right_polygon_constraint = new c2.PolygonConstraint (funnel_right_polygon);
  world.addConstraint (funnel_right_polygon_constraint);



  noFill();
  stroke (color_bright);
  strokeWeight (2);
  
  beginShape();
    vertex (funnel_left_margin.x, funnel_left_margin.y);
    vertex (funnel_left_center.x, funnel_left_center.y);
    vertex (funnel_left_neck.x,   funnel_left_neck.y);
  endShape();

  beginShape();
    vertex (funnel_right_margin.x, funnel_right_margin.y);
    vertex (funnel_right_center.x, funnel_right_center.y);
    vertex (funnel_right_neck.x,   funnel_right_neck.y);
  endShape();
}



function drawDivider2PhysicsWorldAngledAt (ang) 
{
  divider_lower_margin = 15;
  divider_height = height - divider_lower_margin - funnel_height - data_set[0].max_radius*2;

  const divider_upper = {x:width/2, y:height-divider_lower_margin-divider_height};
  const divider_lower = {x:width/2, y:height-divider_lower_margin};

  push();
    translate (divider_lower.x, divider_lower.y);
  
    angleMode (DEGREES);
    rotate (ang);

    const divider_floor_left  = screenPosition (-width/2, 0);
    const divider_floor_right = screenPosition ( width/2, 0);
  pop();



  if (polygon_divider_constraint       != undefined) world.removeConstraint (polygon_divider_constraint);
  if (polygon_divider_floor_constraint != undefined) world.removeConstraint (polygon_divider_floor_constraint);

  const polygon_divider = new c2.Polygon 
  ([
    new c2.Point (divider_upper.x, divider_upper.y), 
    new c2.Point (divider_lower.x, divider_lower.y)
  ]);

  const polygon_divider_floor = new c2.Polygon 
  ([
    new c2.Point (divider_floor_left.x,  divider_floor_left.y), 
    new c2.Point (divider_floor_right.x, divider_floor_right.y)
  ]);

  polygon_divider_constraint = new c2.PolygonConstraint (polygon_divider);
  world.addConstraint (polygon_divider_constraint);

  polygon_divider_floor_constraint = new c2.PolygonConstraint (polygon_divider_floor);
  world.addConstraint (polygon_divider_floor_constraint);



  stroke (color_bright);
  strokeWeight (2);
  noFill();

  push();
    translate (divider_lower.x, divider_lower.y);
  
    angleMode(DEGREES);
    rotate (ang);
    
    beginShape();
      vertex (0, -divider_height);
      vertex (0,  0);
    endShape();
  pop();


  push();
    translate (0, 0);
    
    beginShape();
      vertex (divider_floor_left.x,  divider_floor_left.y);
      vertex (divider_floor_right.x, divider_floor_right.y);
    endShape();
  pop();
}



function drawParticle2PhysicsWorld() 
{
  world.update();

  ellipseMode (RADIUS);
  noStroke();
  fill (color_bright);

  let num_particles_below_funnel_neck = 0;

  for (let i=0; i<world.particles.length; i++) 
  {
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);

    if (world.particles[i].position.y > height - divider_lower_margin - divider_height) 
      num_particles_below_funnel_neck++;
  }

  

  if (world.particles.length === num_particles_below_funnel_neck &&
      world.particles.length < data_set.length && 
      ptron_decisions_attractor_x[world.particles.length] != undefined) 
    addParticle2PhysicsWorld();



  strokeWeight(1);
  stroke (255, 0, 0);
  line (0, height-divider_lower_margin-divider_height, width, height-divider_lower_margin-divider_height);

  stroke (0, 255, 0);
  line (ptron_decision_attractor_x, height-divider_lower_margin-divider_height, ptron_decision_attractor_x, height-divider_lower_margin);
}



function addParticle2PhysicsWorld() 
{
  addPtronDecisionAttractor2PhysicsWorldAt (ptron_decisions_attractor_x[world.particles.length]);
  /*
  if (data_set[world.particles.length].inputs[0] < data_set[world.particles.length].ptron_training_threshold) 
    addPtronDecisionAttractor2PhysicsWorldAt ( (width/4)*1 );
  else
    addPtronDecisionAttractor2PhysicsWorldAt ( (width/4)*3 );
  */

  
  const p  = new c2.Particle (width/2, -data_set[0].max_radius);
  p.radius = data_set[world.particles.length].inputs[0];
  world.addParticle (p);
}



function addPtronDecisionAttractor2PhysicsWorldAt (pos) 
{
  ptron_decision_attractor_x = pos
  const y = height-divider_lower_margin;
  const h = divider_height;

  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = 0;
  }

  ptron_decision_attractor = new c2.LineField (new c2.Line (ptron_decision_attractor_x, y-h, ptron_decision_attractor_x, y), 0.02);
  world.addForce (ptron_decision_attractor);
}



function removeParticle2PhysicsWorld() 
{
  //console.log ("-" + world.particles.length);

  for (let i=0; i<world.particles.length; i++) 
  {
    world.removeParticle (world.particles[i]);
    i--;
  }

  //console.log ("--" + world.particles.length);
}