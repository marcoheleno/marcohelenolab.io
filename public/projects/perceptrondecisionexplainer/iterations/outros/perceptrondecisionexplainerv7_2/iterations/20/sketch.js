


let data_set;
let ptron, ptron_training_i, ptron_training_iteration_weight_values, ptron_training_iteration_decisions;



function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");

  // See getColorsFromCSS() in index.js
  color_darker   = color( red(color_darker),   green(color_darker),   blue(color_darker)   );
  color_dark     = color( red(color_dark),     green(color_dark),     blue(color_dark)     );
  color_neutral  = color( red(color_neutral),  green(color_neutral),  blue(color_neutral)  );
  color_bright   = color( red(color_bright),   green(color_bright),   blue(color_bright)   );
  color_brighter = color( red(color_brighter), green(color_brighter), blue(color_brighter) );
  
  addScreenPositionFunction();
  re_StartVisualization();
}



function re_StartVisualization() 
{
  console.clear();

  data_set = new Array (5);
  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }

  ptron = new Perceptron (2);

  ptron_training_i = 0;
  buildPtronTrainingIterationSet();

  addPhysicsWorld();
  removeParticle2PhysicsWorld(); // only makes sense on the restart
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);

  re_StartVisualization();
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  //if (device_orientation === "p") portraitOrientationInterface();

  background (color_dark);

  strokeWeight(1);
  noFill();
  stroke (255, 0, 0);
  rect (0, 0, width, height);
  
  drawFunnel2PhysicsWorld();

  const ptron_weight_to_angle = map( ptron_training_iteration_weight_values[clique_at_iteration[ptron_training_i]], -1, 1, 2, -2);
  drawDivider2PhysicsWorldAngledAt (ptron_weight_to_angle);
  drawParticles2PhysicsWorldWith (ptron_training_iteration_decisions[clique_at_iteration[ptron_training_i]], ptron_weight_to_angle, data_set.length);
}



function mouseReleased() 
{
  if (ptron_training_i < clique_at_iteration.length-1) 
  {
    removeParticle2PhysicsWorld();
    ptron_training_i++;
  }
}


