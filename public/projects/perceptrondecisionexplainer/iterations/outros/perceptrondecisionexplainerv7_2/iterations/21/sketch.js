


let data_set;
let iteration_i;
let ptron, ptron_moments;
let ptron_weight_value_to_angle, ptron_training_iteration_weight_values;
let ptron_training_iteration_decisions, ptron_training_iteration_errors;



function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");

  // See getColorsFromCSS() in index.js
  color_darker   = color( red(color_darker),   green(color_darker),   blue(color_darker)   );
  color_dark     = color( red(color_dark),     green(color_dark),     blue(color_dark)     );
  color_neutral  = color( red(color_neutral),  green(color_neutral),  blue(color_neutral)  );
  color_bright   = color( red(color_bright),   green(color_bright),   blue(color_bright)   );
  color_brighter = color( red(color_brighter), green(color_brighter), blue(color_brighter) );
  
  addScreenPositionFunction();
  re_StartVisualization();
}



function re_StartVisualization() 
{
  //console.clear(); //------------- eliminar

  data_set = new Array (5);
  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }

  ptron = new Perceptron (2);
  ptron_weight_value_to_angle = 0;
  ptron_moments = 0;

  iteration_i = 0;
  buildPtronTrainingIterationSet();

  addPhysicsWorld();
  removeParticle2PhysicsWorld(); // only usefull on the restart
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);
  
  re_StartVisualization();
}



function sceneManager (story_section) 
{
  //console.log (story_section);
}



function draw() 
{
  //if (device_orientation === "p") portraitOrientationInterface();

  background (color_dark);

  /*
  strokeWeight(1);
  noFill();
  stroke (255, 0, 0);
  rect (0, 0, width, height);
  */
  
  drawFunnel2PhysicsWorld();

  if (ptron_training_iteration_weight_values.length > 0 &&
      ptron_training_iteration_weight_values[0] != undefined ||
      ptron_training_iteration_weight_values[0] != null)
  {
    
    if (iteration_i < saved_ptron_training_iterations_at.length-1)
      ptron_weight_value_to_angle = map(ptron_training_iteration_weight_values[saved_ptron_training_iterations_at[iteration_i]], -1, 1, 3, -3);



    // 0, mostra as particulas a serem categorizadas
    if (ptron_moments === 0) 
    {
      drawDivider2PhysicsWorldAngledAt (ptron_weight_value_to_angle, color_bright);
      drawParticles2PhysicsWorldWith 
      (
        ptron_training_iteration_errors[saved_ptron_training_iterations_at[iteration_i]],
        ptron_training_iteration_decisions[saved_ptron_training_iterations_at[iteration_i]], 
        ptron_weight_value_to_angle, 
        data_set.length
      );

      fill (255);
      textSize (13);
      textAlign(CENTER, TOP);
      text("O Ptron está a classificar os dados/circulos.", width/2, 20);
      text("Quando os 5 dados/circulos acabarem de cair,", width/2, 40);
      text("click para que o Perceptron possa retirar os correctos", width/2, 60);
      text("e apreender com os incorrectos.", width/2, 80);
    }


    else


    // 1, apaga as particulas correctas e realça as erradas
    if (ptron_moments === 1) 
    {
      drawDivider2PhysicsWorldAngledAt (ptron_weight_value_to_angle, color_bright);

      if (iteration_i < saved_ptron_training_iterations_at.length-1)
        removeWrongParticle2PhysicsWorld (ptron_training_iteration_errors[saved_ptron_training_iterations_at[iteration_i]]);
      
      drawParticles2PhysicsWorldWith 
      (
        ptron_training_iteration_errors[saved_ptron_training_iterations_at[iteration_i]],
        ptron_training_iteration_decisions[saved_ptron_training_iterations_at[iteration_i]], 
        ptron_weight_value_to_angle, 
        world.particles.length
      );

      fill (255);
      textSize (13);
      textAlign(CENTER, TOP);
      text("Click novamente para que Ptron corrigir a sua intrepetação dos dados", width/2, 20);
    }


    else


    // 2, mostra as particulas erradas e corrige o peso
    if (ptron_moments === 2) 
    {
      let ptron_training_color = color_bright;
      
      /*
      if (world.particles.length === data_set.length) 
      {
        */
        let ptron_trained_in = 0;

        for (let i=0; i<ptron_training_iteration_errors[saved_ptron_training_iterations_at[iteration_i-1]].length; i++) 
        {
          if (ptron_training_iteration_errors[saved_ptron_training_iterations_at[iteration_i-1]][i] === 0)
            ptron_trained_in++;
        }

        if (ptron_trained_in === 5) 
          ptron_training_color = color (0, 255, 0);
        else 
          ptron_training_color = color (255, 0, 0);
      /*
      }
      else 
      {
        ptron_training_color = color (255, 0, 0);
      }
      */

      drawDivider2PhysicsWorldAngledAt (ptron_weight_value_to_angle, ptron_training_color);
      drawParticles2PhysicsWorldWith 
      (
        ptron_training_iteration_errors[saved_ptron_training_iterations_at[iteration_i-1]],
        ptron_training_iteration_decisions[saved_ptron_training_iterations_at[iteration_i-1]], 
        ptron_weight_value_to_angle, 
        world.particles.length
      );

      fill (255);
      textSize (13);
      textAlign(CENTER, TOP);
      text("Click novamente para que o Ptron possa realizar", width/2, 20);
      text("uma nova classificação dos dados", width/2, 40);
    }

  }
}



function mouseReleased() 
{
  if (iteration_i < saved_ptron_training_iterations_at.length) 
  {
    if (ptron_moments < 2) 
      ptron_moments++;
    else 
      ptron_moments = 0;

    if (ptron_moments === 2) 
      iteration_i++;

    if (ptron_moments === 0) 
      removeParticle2PhysicsWorld();
  }

  //console.log (ptron_moments, iteration_i, saved_ptron_training_iterations_at.length );
}


