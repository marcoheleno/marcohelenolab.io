

let vis_section_size;


window.onload = function() 
{
  adjustSections();
}


window.onresize = function() 
{
  adjustSections();
}


function adjustSections() 
{
  const body_w = document.getElementsByTagName("body")[0].offsetWidth;
  const body_h = document.getElementsByTagName("body")[0].offsetHeight;


  const nav_x = 0;
  const nav_y = 0;
  const nav_w = 35;
  const nav_h = body_h;
  document.getElementsByTagName("nav")[0].style.position = "fixed";
  document.getElementsByTagName("nav")[0].style.left     = nav_x + "px";
  document.getElementsByTagName("nav")[0].style.top      = nav_y + "px";
  document.getElementsByTagName("nav")[0].style.width    = nav_w + "px";
  document.getElementsByTagName("nav")[0].style.height   = nav_h + "px";
  
  
  const txt_section_x = nav_w;
  const txt_section_y = 0;
  let   txt_section_w = body_w - nav_w - body_h;
  const txt_section_h = 100;//%

  if (body_w < body_h || txt_section_w < 440) txt_section_w = ((body_w - nav_w)/8)*3;
  
  document.getElementById("txt_section").style.marginLeft = txt_section_x + "px";
  document.getElementById("txt_section").style.marginTop  = txt_section_y + "px";
  document.getElementById("txt_section").style.width      = txt_section_w + "px";
  document.getElementById("txt_section").style.height     = txt_section_h + "%";


  const vis_section_x = nav_w + txt_section_w;
  const vis_section_y = 0;
  let   vis_section_w = body_h;
  const vis_section_h = body_h;

  if (body_w < body_h) vis_section_w = ((body_w - nav_w)/8)*5;

  document.getElementById("vis_section").style.position = "fixed";
  document.getElementById("vis_section").style.left     = vis_section_x + "px";
  document.getElementById("vis_section").style.top      = vis_section_y + "px";
  document.getElementById("vis_section").style.width    = vis_section_w + "px";
  document.getElementById("vis_section").style.height   = vis_section_h + "px";

  vis_section_size = 
  {
    w: vis_section_w,
    h: vis_section_w
  };
}



// IntersectionObserver

const options = 
{
  root: document.getElementById("txt_section"), 
  rootMargin: "0px", 
  threshold: 1.0
}

const callback = (entries, observer) => 
{
  entries.forEach((entry) => 
  {
    // Each entry describes an intersection change for one observed target element:
    //   entry.boundingClientRect
    //   entry.intersectionRatio
    //   entry.intersectionRect
    //   entry.isIntersecting
    //   entry.rootBounds
    //	 entry.target
    //   entry.time
	
	  if (entry.isIntersecting) console.log (entry.target.getAttribute('id'));
  });
};

const observer = new IntersectionObserver (callback, options);

observer.observe (document.getElementById("target-container_1"));
observer.observe (document.getElementById("target-container_2"));
observer.observe (document.getElementById("target-container_3"));


