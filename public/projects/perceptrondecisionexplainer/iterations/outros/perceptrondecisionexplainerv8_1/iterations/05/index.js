


let vis_section_size;



window.onload = function() 
{
  window.scrollTo (0, 0);
  
  adjustSections();
}



window.onresize = function() 
{
  adjustSections();
}



function adjustSections() 
{
  const body_w = document.getElementsByTagName("body")[0].offsetWidth;
  const body_h = document.getElementsByTagName("body")[0].offsetHeight;
  
  if (body_h > body_w) 
    adjustSectionsPortrait  (body_w, body_h);
  else
    adjustSectionsLandscape (body_w, body_h);
}



function adjustSectionsPortrait (body_w, body_h) 
{
  const nav_w = body_w;
  const nav_h = 35;
  const nav_x = 0;
  const nav_y = body_h - nav_h;
  document.getElementsByTagName("nav")[0].style.position = "fixed";
  document.getElementsByTagName("nav")[0].style.left     = nav_x + "px";
  document.getElementsByTagName("nav")[0].style.top      = nav_y + "px";
  document.getElementsByTagName("nav")[0].style.width    = nav_w + "px";
  document.getElementsByTagName("nav")[0].style.height   = nav_h + "px";



  const vis_section_x = 0;
  const vis_section_y = 0;
  const vis_section_w = body_w;
  let   vis_section_h = body_w; //body_w, because of square canvas

  // if square canvas fills the whole browser window, change vis_section_w
  const vis_section_h_limit = ((body_h - nav_h)/8)*5.5;
  if (vis_section_h > vis_section_h_limit) vis_section_h = vis_section_h_limit;

  document.getElementById("vis_section").style.position = "fixed";
  document.getElementById("vis_section").style.left     = vis_section_x + "px";
  document.getElementById("vis_section").style.top      = vis_section_y + "px";
  document.getElementById("vis_section").style.width    = vis_section_w + "px";
  document.getElementById("vis_section").style.height   = vis_section_h + "px";

  vis_section_size = 
  {
    w: vis_section_h,
    h: vis_section_h
  };



  const txt_sections_container_x = 0;
  const txt_sections_container_y = vis_section_h; 
  const txt_sections_container_w = body_w;
  const txt_sections_container_h = body_h - vis_section_h - nav_h;  
  document.getElementById("txt_sections_container").style.position = "fixed";
  document.getElementById("txt_sections_container").style.left     = txt_sections_container_x + "px";
  document.getElementById("txt_sections_container").style.top      = txt_sections_container_y + "px";
  document.getElementById("txt_sections_container").style.width    = txt_sections_container_w + "px";
  document.getElementById("txt_sections_container").style.height   = txt_sections_container_h + "px";
}



function adjustSectionsLandscape (body_w, body_h) 
{
  const nav_x = 0;
  const nav_y = 0;
  const nav_w = 35;
  const nav_h = body_h;
  document.getElementsByTagName("nav")[0].style.position = "fixed";
  document.getElementsByTagName("nav")[0].style.left     = nav_x + "px";
  document.getElementsByTagName("nav")[0].style.top      = nav_y + "px";
  document.getElementsByTagName("nav")[0].style.width    = nav_w + "px";
  document.getElementsByTagName("nav")[0].style.height   = nav_h + "px";
  

  
  const txt_sections_container_x = nav_w;
  const txt_sections_container_y = 0; 
  let   txt_sections_container_w = body_w - nav_w - body_h; //body_h, because of square canvas
  const txt_sections_container_h = body_h;

  // if square canvas fills the whole browser window, change txt_sections_container_w
  const txt_sections_container_w_limit = 440;
  if (txt_sections_container_w < txt_sections_container_w_limit) txt_sections_container_w = ((body_w - nav_w)/8)*3;
  
  document.getElementById("txt_sections_container").style.position = "fixed";
  document.getElementById("txt_sections_container").style.left     = txt_sections_container_x + "px";
  document.getElementById("txt_sections_container").style.top      = txt_sections_container_y + "px";
  document.getElementById("txt_sections_container").style.width    = txt_sections_container_w + "px";
  document.getElementById("txt_sections_container").style.height   = txt_sections_container_h + "px";



  const vis_section_x = nav_w + txt_sections_container_w;
  const vis_section_y = 0;
  let   vis_section_w = body_h; //body_h because of square canvas
  const vis_section_h = body_h;

  // if square canvas fills the whole browser window, change vis_section_w
  if (txt_sections_container_w < txt_sections_container_w_limit) vis_section_w = ((body_w - nav_w)/8)*5;

  document.getElementById("vis_section").style.position = "fixed";
  document.getElementById("vis_section").style.left     = vis_section_x + "px";
  document.getElementById("vis_section").style.top      = vis_section_y + "px";
  document.getElementById("vis_section").style.width    = vis_section_w + "px";
  document.getElementById("vis_section").style.height   = vis_section_h + "px";

  vis_section_size = 
  {
    w: vis_section_w,
    h: vis_section_w
  };
}



// IntersectionObserver

const options = 
{
  root: document.getElementById("txt_sections_container"), 
  rootMargin: "0px", 
  threshold: 1.0
}

const callback = (entries, observer) => 
{
  entries.forEach((entry) => 
  {
    // Each entry describes an intersection change for one observed target element:
    //   entry.boundingClientRect
    //   entry.intersectionRatio
    //   entry.intersectionRect
    //   entry.isIntersecting
    //   entry.rootBounds
    //	 entry.target
    //   entry.time
	
	  if (entry.isIntersecting) sceneManager (entry.target.getAttribute('id'));
  });
};

const observer = new IntersectionObserver (callback, options);

observer.observe (document.getElementById("story_section_1"));
observer.observe (document.getElementById("story_section_2"));
observer.observe (document.getElementById("story_section_3"));


