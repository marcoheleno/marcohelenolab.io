


function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  rect (10, 10, width-20, height-20);
}



function mouseWheel (event) 
{
  document.getElementById("txt_sections_container").scrollBy 
    ({
      top: event.delta,
      left: 0,
      behavior: 'smooth'
    });

    return false;
}


