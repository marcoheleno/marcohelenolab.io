


let world;
let polygon_1, polygon_2;
let p1, p2, p3, p4;
let polygon_1Constraint, polygon_2Constraint, polygon_3Constraint;
let max_particle_radius = 20;



function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h, WEBGL);
  canvas.parent ("vis_section");

  addScreenPositionFunction();

  world = new c2.World (new c2.Rect(-width/2, (-height/2)-(max_particle_radius*2), width, height+(max_particle_radius*2)));

  for (let i=0; i<50; i++) 
  {
    const p = new c2.Particle (random(-width/2, width/2), (-height/2)-(max_particle_radius*2));
    p.radius = random (10, max_particle_radius);
    
    world.addParticle(p);
  }

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const constForce = new c2.ConstForce (0, 0.5);
  world.addForce (constForce);
  
  criaRodaPolignos (0);
}



function criaRodaPolignos (a) 
{
  push();
    translate (0, 0);
  
    angleMode(DEGREES);
    rotateZ (a);
  
      p1 = screenPosition (-width/2, 0);
      p2 = screenPosition (-30, 0);
      p3 = screenPosition ( 30, 0);
      p4 = screenPosition (width/2, 0);

      beginShape();
        vertex (p1.x, p1.y);
        vertex (p2.x, p2.y);
      endShape(CLOSE);

      beginShape();
        vertex (p3.x, p3.y);
        vertex (p4.x, p4.y);
      endShape(CLOSE);
  
      beginShape();
        vertex (p2.x, p2.y);
        vertex (p3.x, p3.y);
      endShape(CLOSE);
  
  pop();
  
  definePolignos();
}



function definePolignos() 
{
  if (polygon_1Constraint != undefined) world.removeConstraint (polygon_1Constraint);
  if (polygon_2Constraint != undefined) world.removeConstraint (polygon_2Constraint);
  
  polygon_1 = new c2.Polygon ([
      new c2.Point (p1.x, p1.y), 
      new c2.Point (p2.x, p2.y)
  ]);
  
  polygon_2 = new c2.Polygon ([
      new c2.Point (p3.x, p3.y), 
      new c2.Point (p4.x, p4.y)
  ]);
  
  polygon_3 = new c2.Polygon ([
      new c2.Point (p2.x, p2.y), 
      new c2.Point (p3.x, p3.y)
  ]);

  polygon_1Constraint = new c2.PolygonConstraint (polygon_1);
  world.addConstraint (polygon_1Constraint);
  
  polygon_2Constraint = new c2.PolygonConstraint (polygon_2);
  world.addConstraint (polygon_2Constraint);
  
  polygon_3Constraint = new c2.PolygonConstraint (polygon_3);
  world.addConstraint (polygon_3Constraint);
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  world.update();

  background (50, 50, 50);

  if (device_orientation === "p") 
    portraitOrientationInterface();

  noFill();
  stroke (255);
  strokeWeight (1);

  line (polygon_1.vertices[0].x, polygon_1.vertices[0].y, polygon_1.vertices[1].x, polygon_1.vertices[1].y);
  line (polygon_2.vertices[0].x, polygon_2.vertices[0].y, polygon_2.vertices[1].x, polygon_2.vertices[1].y);
  line (polygon_3.vertices[0].x, polygon_3.vertices[0].y, polygon_3.vertices[1].x, polygon_3.vertices[1].y);

  strokeWeight(0.2);
  for (let i=0; i<world.particles.length; i++) 
  {
    push();
      translate (world.particles[i].position.x, world.particles[i].position.y);
        sphere (world.particles[i].radius);
    pop();
  }
}



function keyPressed()
{
  criaRodaPolignos (random(-20, 20));
}



function mousePressed()
{
  world.removeConstraint (polygon_3Constraint);
}


