


let world, lineField;
let funnel_h;
let funnel_left_margin,  funnel_left_center,  funnel_left_neck;
let funnel_right_center, funnel_right_margin, funnel_right_neck;
let divider_h, divider_top, divider_bottom;
let divider_floor_left, divider_floor_right;
let margin;




function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, -data_max_radius, width, height+data_max_radius));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const constForce = new c2.ConstForce (0, 1); //0.2
  world.addForce (constForce);

  /*
  const floorField = new c2.LineField (new c2.Line (0, height, width, height), 1);
  world.addForce (floorField);
  */
}



function addLineField2PhysicsWorld (x) 
{
  console.log (x);
  lineField = new c2.LineField (new c2.Line (x, height-margin-divider_h, x, height-margin), 0.01);
  world.addForce (lineField);
}

function removeLineFieldFromPhysicsWorld() 
{
  world.removeForce (lineField);
  lineField = 0;
}



function addFunnel2PhysicsWorld() 
{
  funnel_h = height/8;
  const neck_h   = 50;

  funnel_left_margin  = {x:0, y:0};
  funnel_left_center  = {x:width/2 - data_max_radius, y:funnel_h - neck_h};
  funnel_left_neck    = {x:width/2 - data_max_radius, y:funnel_h};

  funnel_right_margin = {x:width, y:0};
  funnel_right_center = {x:width/2 + data_max_radius, y:funnel_h - neck_h};
  funnel_right_neck   = {x:width/2 + data_max_radius, y:funnel_h};
  
  const polygon_funnel_left = new c2.Polygon 
  ([
    new c2.Point (funnel_left_margin.x, funnel_left_margin.y), 
    new c2.Point (funnel_left_center.x, funnel_left_center.y), 
    new c2.Point (funnel_left_neck.x,   funnel_left_neck.y)
  ]);

  const polygon_funnel_right = new c2.Polygon 
  ([
    new c2.Point (funnel_right_margin.x, funnel_right_margin.y), 
    new c2.Point (funnel_right_center.x, funnel_right_center.y), 
    new c2.Point (funnel_right_neck.x,   funnel_right_neck.y)
  ]);

  const polygon_funnel_left_constraint  = new c2.PolygonConstraint (polygon_funnel_left);
  world.addConstraint (polygon_funnel_left_constraint);
  
  const polygon_funnel_right_constraint = new c2.PolygonConstraint (polygon_funnel_right);
  world.addConstraint (polygon_funnel_right_constraint);
}



function drawFunnel() 
{
  noFill();
  stroke (255);
  strokeWeight (1);
  
  beginShape();
    vertex (funnel_left_margin.x, funnel_left_margin.y);
    vertex (funnel_left_center.x, funnel_left_center.y);
    vertex (funnel_left_neck.x,   funnel_left_neck.y);
  endShape();

  beginShape();
    vertex (funnel_right_margin.x, funnel_right_margin.y);
    vertex (funnel_right_center.x, funnel_right_center.y);
    vertex (funnel_right_neck.x,   funnel_right_neck.y);
  endShape();
}



function addDivider2PhysicsWorldAngledAt (a) 
{
  margin = 10;
  const x = width/2;
  const y = height - margin;
  divider_h = height - margin - funnel_h - data_max_radius*2;

  push();
    translate (x, y);
  
    angleMode(DEGREES);
    rotate (a);

    noFill();
    stroke (255);
    strokeWeight (1);
    
    beginShape();
      vertex (0, -divider_h);
      vertex (0,  0);
    endShape();

    beginShape();
      vertex (-width/2, 0);
      vertex ( width/2, 0);
    endShape();

    /*
    divider_top    = screenPosition (0, -divider_h);
    divider_bottom = screenPosition (0,  0);
    */

    divider_floor_left  = screenPosition (-width/2, 0);
    divider_floor_right = screenPosition ( width/2, 0);
  pop();

  let polygon_divider_constraint, polygon_divider_floor_constraint;

  if (polygon_divider_constraint       != undefined) world.removeConstraint (polygon_divider_constraint);
  if (polygon_divider_floor_constraint != undefined) world.removeConstraint (polygon_divider_floor_constraint);


  divider_top    = {x:x, y:y-divider_h};
  divider_bottom = {x:x, y:y};

  const polygon_divider = new c2.Polygon 
  ([
    new c2.Point (divider_top.x,    divider_top.y), 
    new c2.Point (divider_bottom.x, divider_bottom.y)
  ]);

  polygon_divider_constraint = new c2.PolygonConstraint (polygon_divider);
  world.addConstraint (polygon_divider_constraint);



  const polygon_divider_floor = new c2.Polygon 
  ([
    new c2.Point (divider_floor_left.x,  divider_floor_left.y), 
    new c2.Point (divider_floor_right.x, divider_floor_right.y)
  ]);

  polygon_divider_floor_constraint = new c2.PolygonConstraint (polygon_divider_floor);
  world.addConstraint (polygon_divider_floor_constraint);
}


/*
function drawDivider() 
{
  noFill();
  stroke (255);
  strokeWeight (1);
  
  beginShape();
    vertex (divider_top.x,    divider_top.y);
    vertex (divider_bottom.x, divider_bottom.y);
  endShape();
}
*/


function addParticle2PhysicsWorld() 
{
  if (world.particles.length < data_set.length) 
  {
    if (data_set[world.particles.length].input[0] < ptron_training_threshold) addLineField2PhysicsWorld (0);
    else 
    if (data_set[world.particles.length].input[0] > ptron_training_threshold) addLineField2PhysicsWorld (width);

    const p = new c2.Particle (width/2, -data_max_radius*2);
    p.radius = data_set[world.particles.length].input[0];
    //p.mass = map(data_set[world.particles.length].input[0], data_min_radius, data_max_radius, 0.5, 0.2);
    p.color = 255;
    world.addParticle(p);
  }
}



function drawParticles() 
{
  world.update();

  ellipseMode(RADIUS);
  //strokeWeight (2);
  noStroke();

  let num_particles_below_funnel_neck = 0;
  console.log (world.particles.length);
  for (let i=0; i<world.particles.length; i++) 
  {
    

    fill  (world.particles[i].color);
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);
    fill(0);
    text (i, world.particles[i].position.x, world.particles[i].position.y);

    stroke(255, 0, 0);
    line (0, height-divider_h-margin, width, height-divider_h-margin);

    if (world.particles[i].position.y > height-divider_h-margin) 
    {
      num_particles_below_funnel_neck++;
    }
  }

  if (num_particles_below_funnel_neck > 1 &&
      num_particles_below_funnel_neck === world.particles.length) 
  {
    removeLineFieldFromPhysicsWorld();
    addParticle2PhysicsWorld();
  }
}


