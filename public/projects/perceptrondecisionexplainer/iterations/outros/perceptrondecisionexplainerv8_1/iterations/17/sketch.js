


let data_set, ptron, ptron_decisions_attractor_x, ang;



function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");


  
  // See getColorsFromCSS() in index.js
  color_darker   = color( red(color_darker),   green(color_darker),   blue(color_darker)   );
  color_dark     = color( red(color_dark),     green(color_dark),     blue(color_dark)     );
  color_neutral  = color( red(color_neutral),  green(color_neutral),  blue(color_neutral)  );
  color_bright   = color( red(color_bright),   green(color_bright),   blue(color_bright)   );
  color_brighter = color( red(color_brighter), green(color_brighter), blue(color_brighter) );
  


  addScreenPositionFunction();
  re_StartVisualization();
}



function re_StartVisualization() 
{
  console.clear();
  ang = 0;



  data_set = new Array (5);
  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }

  ptron_decisions_attractor_x = new Array (data_set.length);

  ptron = new Perceptron (2);



  addPhysicsWorld();
  removeParticle2PhysicsWorld(); // only makes sense on the restart
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);

  re_StartVisualization();
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  background (color_dark);


  
  strokeWeight(1);
  noFill();
  stroke (255, 0, 0);
  rect (0, 0, width, height);
  


  if (device_orientation === "p") portraitOrientationInterface();



  drawFunnel2PhysicsWorld();
  drawDivider2PhysicsWorldAngledAt (ang);
  drawParticle2PhysicsWorld();
}



function mousePressed() 
{
  removeParticle2PhysicsWorld();

  for (let d=0; d<10; d++) 
  {
    const weights_value = ptron.getTransformedWeights();
    ang = map( weights_value[0], -1, 1, 2, -2);

    for (let i=0; i<data_set.length; i++) 
    {
      ptron.runFeedForwardWith (data_set[i].inputs);

      if (ptron.getActivationResult() < 0) 
        ptron_decisions_attractor_x[i] = constrain( map(ang, -2, 0, 10, width/2-10), 10, width/2-10);
      else 
        ptron_decisions_attractor_x[i] = constrain( map(ang, 0, 2, width/2+10, width-10), width/2+10, width-10);

      /*
      if (ptron.getActivationResult() < 0) 
        ptron_decisions_attractor_x[i] = (width/4)*1;
      else 
        ptron_decisions_attractor_x[i] = (width/4)*3;
      */
      ptron.runTrainingErrorWith (data_set[i].output);

      console.log (ptron.getTrainingError());

      ptron.runTrainingWith (data_set[i].inputs);
    }
  }

  console.log ("----------");
}