


let data_set, ptron, ang;
let training_iterations, ptron_intertaions, data_training_done, num_cliques, total_num_cliques;
let iteration_num_clique;



function setup() 
{
  const canvas = createCanvas (vis_section_size.w, vis_section_size.h);
  canvas.parent ("vis_section");


  
  // See getColorsFromCSS() in index.js
  color_darker   = color( red(color_darker),   green(color_darker),   blue(color_darker)   );
  color_dark     = color( red(color_dark),     green(color_dark),     blue(color_dark)     );
  color_neutral  = color( red(color_neutral),  green(color_neutral),  blue(color_neutral)  );
  color_bright   = color( red(color_bright),   green(color_bright),   blue(color_bright)   );
  color_brighter = color( red(color_brighter), green(color_brighter), blue(color_brighter) );
  


  addScreenPositionFunction();
  re_StartVisualization();
}



function re_StartVisualization() 
{
  console.clear();
  ang = 0;
  training_iterations = 0;
  ptron_intertaions = [];
  data_training_done = false;
  num_cliques = 0;
  iteration_num_clique = [];



  data_set = new Array (5);
  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }

  ptron = new Perceptron (2);



  addPhysicsWorld();
  removeParticle2PhysicsWorld(); // only makes sense on the restart
}



function windowResized() 
{
  resizeCanvas (vis_section_size.w, vis_section_size.h);

  re_StartVisualization();
}



function sceneManager (story_section) 
{
  console.log (story_section);
}



function draw() 
{
  background (color_dark);


  
  strokeWeight(1);
  noFill();
  stroke (255, 0, 0);
  rect (0, 0, width, height);
  


  if (device_orientation === "p") portraitOrientationInterface();


  while (data_training_done === false)
    trainPtron();

  drawFunnel2PhysicsWorld();
  drawDivider2PhysicsWorldAngledAt (ang);

  if (num_cliques > 0) 
  {
    console.log(iteration_num_clique[num_cliques]);
    drawParticle2PhysicsWorldAt (iteration_num_clique[num_cliques]);
  }
}


function mouseReleased() 
{
  console.log ("--");

  if (num_cliques < total_num_cliques) 
  {
    removeParticle2PhysicsWorld();
    console.log (total_num_cliques);
    console.log (num_cliques);
    num_cliques++;
  }
  console.log ("----------");
}



function trainPtron() 
{
  let data_trained_on = 0;
  let ptron_decisions_attractor_x = new Array (data_set.length);

  while (data_trained_on < data_set.length-1) 
  {
    const weights_value = ptron.getTransformedWeights();
    ang = map( weights_value[0], -1, 1, 2, -2);

    for (let i=0; i<data_set.length; i++) 
    {
      ptron.runFeedForwardWith (data_set[i].inputs);

      if (ptron.getActivationResult() < 0) 
        ptron_decisions_attractor_x[i] = constrain( map(ang, -2, 0, 10, width/2-10), 10, width/2-10);
      else 
        ptron_decisions_attractor_x[i] = constrain( map(ang, 0, 2, width/2+10, width-10), width/2+10, width-10);

      ptron.runTrainingErrorWith (data_set[i].output);

      if (ptron.getTrainingError() === 0) 
        data_trained_on++;

      //console.log (ptron.getTrainingError());
      ptron.runTrainingWith (data_set[i].inputs);
    }

    //console.log ("-------------");

    ptron_intertaions.push(ptron_decisions_attractor_x);
    training_iterations++;
    
    if (data_trained_on < data_set.length) 
      data_trained_on = 0;
    else 
      data_training_done = true;
  }

  console.log (data_training_done);
  //console.log (data_trained_on);
  console.log (training_iterations);
  console.log (ptron_intertaions.length);

  const num_iterations_jump = floor(training_iterations/3);
  console.log (num_iterations_jump);

  const rounded_num_cliques = floor(training_iterations/num_iterations_jump);
  console.log (rounded_num_cliques);

  for (let i=0; i<=rounded_num_cliques; i++)
  {
    iteration_num_clique[i] = num_iterations_jump*i;
  }

  //console.log (num_iterations_jump*rounded_num_cliques);
  //console.log (training_iterations - num_iterations_jump*3);

  total_num_cliques = rounded_num_cliques + (training_iterations - num_iterations_jump*3);
  console.log (total_num_cliques);
  for (let i=rounded_num_cliques+1; i<=total_num_cliques; i++)
  {
    iteration_num_clique[i] = iteration_num_clique[i-1]+1;
  }

  console.log ("-------------");
  for (let i=0; i<iteration_num_clique.length; i++)
  {
    console.log(iteration_num_clique[i]);
  }

  console.log ("-------------");
}