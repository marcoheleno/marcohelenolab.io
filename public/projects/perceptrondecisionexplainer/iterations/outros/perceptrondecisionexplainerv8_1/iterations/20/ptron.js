


function buildPtronTrainingIterationSet() 
{
  let i = 0;
  ptron_training_iteration_weight_values = [];
  ptron_training_iteration_decisions = [];
  let ptron_trained_on_num_data_points = 0;

  while (ptron_trained_on_num_data_points < data_set.length) 
  {
    ptron_training_iteration_weight_values[i] = ptron.getCalculatedWeights()[0];
    ptron_training_iteration_decisions[i] = new Array (data_set.length);
    
    for (let d=0; d<data_set.length; d++) 
    {
      ptron.runFeedForwardWith (data_set[d].inputs);

      ptron_training_iteration_decisions[i][d] = ptron.getActivationResult();

      ptron.calculateTrainingErrorWith (data_set[d].output);

      if (ptron.getTrainingError() === 0) 
        ptron_trained_on_num_data_points++;

      ptron.runTrainingWith (data_set[d].inputs);
    }

    i++;
    
    if (ptron_trained_on_num_data_points < data_set.length-1) 
      ptron_trained_on_num_data_points = 0; 
  }

  console.log (ptron_training_iteration_weight_values.length);

  const num_iterations_jump = floor( (ptron_training_iteration_weight_values.length-1)/3);
  console.log (num_iterations_jump);

  clique_at_iteration = [];

  const rounded_num_cliques = floor( (ptron_training_iteration_weight_values.length-1)/num_iterations_jump);
  console.log (rounded_num_cliques);
  for (let i=0; i<=rounded_num_cliques; i++) 
  {
    clique_at_iteration [i] = num_iterations_jump*i;
  }

  const remaining_num_cliques = rounded_num_cliques + ( (ptron_training_iteration_weight_values.length-1) - (num_iterations_jump*3) );
  console.log (remaining_num_cliques);
  for (let i=rounded_num_cliques+1; i<=remaining_num_cliques; i++) 
  {
    clique_at_iteration[i] = clique_at_iteration[i-1]+1;
  }

  console.log ("-------------");
  for (let i=0; i<clique_at_iteration.length; i++)
  {
    console.log(clique_at_iteration[i]);
  }
  console.log ("-------------");
}


