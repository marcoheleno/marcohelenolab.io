


function buildPtronTrainingIterationSet() 
{
  let i = 0;
  ptron_training_iteration_weight_values = [];
  ptron_training_iteration_decisions = [];
  ptron_training_iteration_errors = [];
  let ptron_trained_on_num_data_points = 0;

  while (ptron_trained_on_num_data_points < data_set.length) 
  {
    ptron_training_iteration_weight_values[i] = ptron.getCalculatedWeights()[0];
    ptron_training_iteration_decisions[i] = new Array (data_set.length);
    ptron_training_iteration_errors[i]    = new Array (data_set.length);
    
    for (let d=0; d<data_set.length; d++) 
    {
      ptron.runFeedForwardWith (data_set[d].inputs);

      ptron_training_iteration_decisions[i][d] = ptron.getActivationResult();

      ptron.calculateTrainingErrorWith (data_set[d].output);

      ptron_training_iteration_errors[i][d] = ptron.getTrainingError();
      if (ptron_training_iteration_errors[i][d] === 0) 
        ptron_trained_on_num_data_points++;

      ptron.runTrainingWith (data_set[d].inputs);
    }

    i++;
    
    if (ptron_trained_on_num_data_points < data_set.length-1) 
      ptron_trained_on_num_data_points = 0; 
  }

  //console.log (ptron_training_iteration_weight_values.length);

  const num_iterations_jump = floor( (ptron_training_iteration_weight_values.length-1)/3);
  //console.log (num_iterations_jump);

  saved_ptron_training_iterations_at = [];

  const rounded_num_clicks = floor( (ptron_training_iteration_weight_values.length-1)/num_iterations_jump);
  //console.log (rounded_num_clicks);
  for (let i=0; i<=rounded_num_clicks; i++) 
  {
    saved_ptron_training_iterations_at [i] = num_iterations_jump*i;
  }

  const remaining_num_clicks = rounded_num_clicks + ((ptron_training_iteration_weight_values.length-1) - (num_iterations_jump*3));
  //console.log (remaining_num_clicks);
  for (let i=rounded_num_clicks+1; i<=remaining_num_clicks; i++) 
  {
    saved_ptron_training_iterations_at[i] = saved_ptron_training_iterations_at[i-1]+1;
  }

  /*
  console.log ("-------------");
  for (let i=0; i<saved_ptron_training_iterations_at.length; i++)
  {
    console.log(saved_ptron_training_iterations_at[i]);
  }
  console.log ("-------------");
  */
}


