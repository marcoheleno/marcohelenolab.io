


class DataElement 
{
  constructor() 
  {
    if (device_orientation === "p") 
    {
      this.min_radius = int(height/19);
      this.max_radius = int(height/13);
    }
    else
    if (device_orientation === "l") 
    {
      this.min_radius = int(width/19);
      this.max_radius = int(width/13);
    }

    this.runRandomDataPicker();
    this.runPtronTrainingSolutionProvider();
    this.buildDataElement();
  }

  

  runRandomDataPicker()
  {
    const random_radius_picker = int(random(2));

    this.radius = 0;
    if (random_radius_picker === 0) this.radius = this.min_radius;
    else
    if (random_radius_picker === 1) this.radius = this.max_radius;
  }



  runPtronTrainingSolutionProvider()
  {
    this.ptron_training_threshold = this.min_radius + ( (this.max_radius - this.min_radius)/2 );
    
    this.solution = 0;
    if (this.radius < this.ptron_training_threshold) this.solution = -1;
    else this.solution = 1;
  }



  buildDataElement() 
  {
    const data_bias = 1;
    this.inputs = [this.radius, data_bias];
    this.output = this.solution;
  }
}


