


let world;
let funnel_height, funnel_left_polygon_constraint, funnel_right_polygon_constraint;
let divider_lower_margin, divider_height;
let polygon_divider_constraint, polygon_divider_floor_constraint;
let ptron_decision_attractor;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, -data_set[0].max_radius, width, height+data_set[0].max_radius));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line (0, height, width, height), 1);
  world.addForce (floorForce);
}



function drawFunnel2PhysicsWorld() 
{
  funnel_height = data_set[0].max_radius * 2;
  const neck_h  = data_set[0].max_radius;

  const funnel_left_margin  = {x:0, y:0};
  const funnel_left_center  = {x:width/2 - data_set[0].max_radius, y:funnel_height - neck_h};
  const funnel_left_neck    = {x:width/2 - data_set[0].max_radius, y:funnel_height};

  const funnel_right_margin = {x:width, y:0};
  const funnel_right_center = {x:width/2 + data_set[0].max_radius, y:funnel_height - neck_h};
  const funnel_right_neck   = {x:width/2 + data_set[0].max_radius, y:funnel_height};



  if (funnel_left_polygon_constraint  != undefined) world.removeConstraint (funnel_left_polygon_constraint);
  if (funnel_right_polygon_constraint != undefined) world.removeConstraint (funnel_right_polygon_constraint);
  
  const funnel_left_polygon = new c2.Polygon 
  ([
    new c2.Point (funnel_left_margin.x, funnel_left_margin.y), 
    new c2.Point (funnel_left_center.x, funnel_left_center.y), 
    new c2.Point (funnel_left_neck.x,   funnel_left_neck.y)
  ]);

  const funnel_right_polygon = new c2.Polygon 
  ([
    new c2.Point (funnel_right_margin.x, funnel_right_margin.y), 
    new c2.Point (funnel_right_center.x, funnel_right_center.y), 
    new c2.Point (funnel_right_neck.x,   funnel_right_neck.y)
  ]);

  funnel_left_polygon_constraint  = new c2.PolygonConstraint (funnel_left_polygon);
  world.addConstraint (funnel_left_polygon_constraint);
  
  funnel_right_polygon_constraint = new c2.PolygonConstraint (funnel_right_polygon);
  world.addConstraint (funnel_right_polygon_constraint);



  noFill();
  stroke (color_bright);
  strokeWeight (2);
  
  beginShape();
    vertex (funnel_left_margin.x, funnel_left_margin.y);
    vertex (funnel_left_center.x, funnel_left_center.y);
    vertex (funnel_left_neck.x,   funnel_left_neck.y);
  endShape();

  beginShape();
    vertex (funnel_right_margin.x, funnel_right_margin.y);
    vertex (funnel_right_center.x, funnel_right_center.y);
    vertex (funnel_right_neck.x,   funnel_right_neck.y);
  endShape();


  /*
  strokeWeight(1);
  stroke (255, 0, 0);
  line (0, height-divider_lower_margin-divider_height, width, height-divider_lower_margin-divider_height);
  */
}



function drawDivider2PhysicsWorldAngledAt (tilting_angle, stroke_color) 
{
  divider_lower_margin = 15;
  divider_height = height - divider_lower_margin - funnel_height - data_set[0].max_radius*2;

  const divider_upper = {x:width/2, y:height-divider_lower_margin-divider_height};
  const divider_lower = {x:width/2, y:height-divider_lower_margin};

  push();
    translate (divider_lower.x, divider_lower.y);
  
    angleMode (DEGREES);
    rotate (tilting_angle);

    const divider_floor_left  = screenPosition (-width/2, 0);
    const divider_floor_right = screenPosition ( width/2, 0);
  pop();



  if (polygon_divider_constraint       != undefined) world.removeConstraint (polygon_divider_constraint);
  if (polygon_divider_floor_constraint != undefined) world.removeConstraint (polygon_divider_floor_constraint);

  const polygon_divider = new c2.Polygon 
  ([
    new c2.Point (divider_upper.x, divider_upper.y), 
    new c2.Point (divider_lower.x, divider_lower.y)
  ]);

  const polygon_divider_floor = new c2.Polygon 
  ([
    new c2.Point (divider_floor_left.x,  divider_floor_left.y), 
    new c2.Point (divider_floor_right.x, divider_floor_right.y)
  ]);

  polygon_divider_constraint = new c2.PolygonConstraint (polygon_divider);
  world.addConstraint (polygon_divider_constraint);

  polygon_divider_floor_constraint = new c2.PolygonConstraint (polygon_divider_floor);
  world.addConstraint (polygon_divider_floor_constraint);



  stroke (stroke_color);
  strokeWeight (2);
  noFill();

  push();
    translate (divider_lower.x, divider_lower.y);
  
    angleMode(DEGREES);
    rotate (tilting_angle);
    
    beginShape();
      vertex (0, -divider_height);
      vertex (0,  0);
    endShape();
  pop();


  push();
    translate (0, 0);
    
    beginShape();
      vertex (divider_floor_left.x,  divider_floor_left.y);
      vertex (divider_floor_right.x, divider_floor_right.y);
    endShape();
  pop();
}



function drawParticles2PhysicsWorldWith (with_ptron_training_iteration_errors, ptron_training_iteration_errors, ptron_training_decisions, ptron_weight_value_to_angle, num_data_points_to_draw) 
{
  world.update();

  ellipseMode (RADIUS);
  noStroke();
  
  let num_particles_below_funnel_neck = 0;

  for (let i=0; i<world.particles.length; i++) 
  {
    if (with_ptron_training_iteration_errors === false) 
      fill (color_bright);

    else 

    if (with_ptron_training_iteration_errors === true) 
    {
      if (ptron_training_iteration_errors[i] === 0) 
        fill (0, 255, 0);
      else 
        fill (255, 0, 0);
    }

    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);

    if (world.particles[i].position.y > height - divider_lower_margin - divider_height)
        num_particles_below_funnel_neck++;
  }

  
  /*
  for (let i=0; i<world.particles.length; i++) 
  {
    if (world.particles[i].position.y > height - divider_lower_margin - divider_height)
    {
      num_particles_below_funnel_neck++;
      if (ptron_training_iteration_errors[i] === 0) 
        fill (0, 255, 0);
      else 
        fill (255, 0, 0);
    }
    else 
    {
      fill (color_bright);
    }

    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);
  }
  */

  if (world.particles.length === num_particles_below_funnel_neck &&
      world.particles.length < num_data_points_to_draw) 
  {
    addPtronDecisionAttractor2PhysicsWorldWith (ptron_training_decisions, ptron_weight_value_to_angle);
    addParticle2PhysicsWorld();
  }
}



function addParticle2PhysicsWorld() 
{
  const p  = new c2.Particle (width/2, -data_set[0].max_radius);
  p.radius = data_set[world.particles.length].inputs[0];
  world.addParticle (p);
}



function addPtronDecisionAttractor2PhysicsWorldWith (ptron_training_decisions, ptron_weight_value_to_angle) 
{
  let x = 0;
  if (ptron_training_decisions[world.particles.length] < 0) 
    x = constrain( map(ptron_weight_value_to_angle, -2, 0, 10, width/2-10), 10, width/2-10);
  else 
    x = constrain( map(ptron_weight_value_to_angle,  0, 2, width/2+10, width-10), width/2+10, width-10);
  
  const y = height-divider_lower_margin;
  const h = divider_height;

  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = 0;
  }

  ptron_decision_attractor = new c2.LineField (new c2.Line (x, y-h, x, y), 0.02);
  world.addForce (ptron_decision_attractor);


  /*
  stroke (0, 255, 0);
  line (x, height-divider_lower_margin-divider_height, x, height-divider_lower_margin);
  */
}



function removeParticle2PhysicsWorld() 
{
  //console.log ("-" + world.particles.length);

  for (let i=0; i<world.particles.length; i++) 
  {
    world.removeParticle (world.particles[i]);
    i--;
  }

  //console.log ("--" + world.particles.length);
}



function removeWrongParticle2PhysicsWorld (ptron_training_iteration_errors) 
{
  for (let i=0; i<world.particles.length; i++) 
  {
    if (ptron_training_iteration_errors[i] === 0) 
    {
      world.removeParticle (world.particles[i]);
      ptron_training_iteration_errors.splice(i, 1);
      i--;
    }
  }
}