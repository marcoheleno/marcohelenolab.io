


let table_row_i;



function setup() 
{
  const canvas = createCanvas (windowWidth, windowHeight);
  canvas.parent ("vis_section");

  createColorsFromCSS();
  addScreenPositionFunction();

  createDataSetWith (1);
  startPerceptronWith();
  runPerceptronWith();
  //printPerceptronIterationsTableLog();

  table_row_i = 0;

  re_StartVisualization();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  re_StartVisualization();
}



function re_StartVisualization() 
{
  addPhysicsWorld();
}



function draw() 
{
  clear();

  const temp_data_id =                          ptron_iterations_table_log.getNum (table_row_i, "data_id");
  const temp_data_input_0_value =               ptron_iterations_table_log.getNum (table_row_i, "data_input_0_value");
  const temp_data_input_1_bias =                ptron_iterations_table_log.getNum (table_row_i, "data_input_1_bias");
  const temp_data_solution_value =              ptron_iterations_table_log.getNum (table_row_i, "data_solution_value");
  const temp_ptron_learning_rate_value =        ptron_iterations_table_log.getNum (table_row_i, "ptron_learning_rate_value");
  const temp_ptron_weight_0_value =             ptron_iterations_table_log.getNum (table_row_i, "ptron_weight_0_value");
  const temp_ptron_weight_1_value =             ptron_iterations_table_log.getNum (table_row_i, "ptron_weight_1_value");
  const temp_ptron_sum_weighted_inputs_result = ptron_iterations_table_log.getNum (table_row_i, "ptron_sum_weighted_inputs_result");
  const temp_ptron_activation_result =          ptron_iterations_table_log.getNum (table_row_i, "ptron_activation_result");
  const temp_ptron_training_error =             ptron_iterations_table_log.getNum (table_row_i, "ptron_training_error");

  drawFunnel2PhysicsWorldWith (temp_ptron_weight_0_value, true, temp_ptron_training_error);
  drawDivider2PhysicsWorld();
  drawParticles2PhysicsWorldWith (true, temp_ptron_training_error, temp_ptron_activation_result, temp_ptron_weight_0_value);

  if (physics_world_in_anim === false) 
  {

  }
}



function keyPressed() 
{
  removeWrongParticle2PhysicsWorld ( ptron_iterations_table_log.getNum (table_row_i, "ptron_training_error") );
}


