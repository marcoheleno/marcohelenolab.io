


let world;
let funnel_h;
let funnel_left_polygon_constraint, funnel_right_polygon_constraint;
let divider_margin, divider_h;
let polygon_divider_constraint, polygon_divider_floor_constraint;
let ptron_decision_attractor;
let attractor_x, attractor_y, attractor_h;
let physics_world_in_anim;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, -data_set[0].max_radius, width, height+data_set[0].max_radius));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line (0, height, width, height), 1);
  world.addForce (floorForce);

  if (physics_world_in_anim === undefined) 
    physics_world_in_anim = false;
    
}



function drawFunnel2PhysicsWorldWith (ptron_weight_0_value, ptron_checking_error, ptron_training_error) 
{
  const funnel_w = width + width/4;
        funnel_h = data_set[0].max_radius * 4;
  const neck_h   = data_set[0].max_radius;

  push();
    translate (width/2, 0);
  
    angleMode (DEGREES);
    rotate ( map(ptron_weight_0_value, -1, 1, 15, -15) );

    const funnel_left_margin  = screenPosition (-funnel_w/2, 0);
    const funnel_left_center  = screenPosition (-data_set[0].max_radius, funnel_h - neck_h);
    const funnel_left_neck    = screenPosition (-data_set[0].max_radius, funnel_h);

    const funnel_right_margin = screenPosition ( funnel_w/2, 0);
    const funnel_right_center = screenPosition ( data_set[0].max_radius, funnel_h - neck_h);
    const funnel_right_neck   = screenPosition ( data_set[0].max_radius, funnel_h);
  pop();



  if (funnel_left_polygon_constraint  != undefined) world.removeConstraint (funnel_left_polygon_constraint);
  if (funnel_right_polygon_constraint != undefined) world.removeConstraint (funnel_right_polygon_constraint);
  
  const funnel_left_polygon = new c2.Polygon 
  ([
    new c2.Point (funnel_left_margin.x, funnel_left_margin.y), 
    new c2.Point (funnel_left_center.x, funnel_left_center.y), 
    new c2.Point (funnel_left_neck.x,   funnel_left_neck.y)
  ]);

  const funnel_right_polygon = new c2.Polygon 
  ([
    new c2.Point (funnel_right_margin.x, funnel_right_margin.y), 
    new c2.Point (funnel_right_center.x, funnel_right_center.y), 
    new c2.Point (funnel_right_neck.x,   funnel_right_neck.y)
  ]);

  funnel_left_polygon_constraint  = new c2.PolygonConstraint (funnel_left_polygon);
  world.addConstraint (funnel_left_polygon_constraint);
  
  funnel_right_polygon_constraint = new c2.PolygonConstraint (funnel_right_polygon);
  world.addConstraint (funnel_right_polygon_constraint);



  noFill();
  strokeWeight (1);

  if (ptron_checking_error === false) stroke (color_neutral);
  else 
  {
    if (ptron_training_error === 0) stroke (0, 255, 0);
    else stroke (255, 0, 0);
  }
  
  beginShape();
    vertex (funnel_left_margin.x, funnel_left_margin.y);
    vertex (funnel_left_center.x, funnel_left_center.y);
    vertex (funnel_left_neck.x,   funnel_left_neck.y);
  endShape();

  beginShape();
    vertex (funnel_right_margin.x, funnel_right_margin.y);
    vertex (funnel_right_center.x, funnel_right_center.y);
    vertex (funnel_right_neck.x,   funnel_right_neck.y);
  endShape();
}



function drawDivider2PhysicsWorld() 
{
        divider_margin = 30;
        divider_h = height - divider_margin - funnel_h - data_set[0].max_radius*2;
  const divider_w = width - divider_margin*2;

  const divider_wall_upper  = {x: width/2, y: height - divider_margin - divider_h};
  const divider_wall_lower  = {x: width/2, y: height - divider_margin};

  const divider_floor_left  = {x: width/2 - divider_w/2, y: height - divider_margin};
  const divider_floor_right = {x: width/2 + divider_w/2, y: height - divider_margin};



  if (polygon_divider_constraint       != undefined) world.removeConstraint (polygon_divider_constraint);
  if (polygon_divider_floor_constraint != undefined) world.removeConstraint (polygon_divider_floor_constraint);

  const polygon_divider = new c2.Polygon 
  ([
    new c2.Point (divider_wall_upper.x, divider_wall_upper.y), 
    new c2.Point (divider_wall_lower.x, divider_wall_lower.y)
  ]);

  const polygon_divider_floor = new c2.Polygon 
  ([
    new c2.Point (divider_floor_left.x,  divider_floor_left.y), 
    new c2.Point (divider_floor_right.x, divider_floor_right.y)
  ]);

  polygon_divider_constraint = new c2.PolygonConstraint (polygon_divider);
  world.addConstraint (polygon_divider_constraint);

  polygon_divider_floor_constraint = new c2.PolygonConstraint (polygon_divider_floor);
  world.addConstraint (polygon_divider_floor_constraint);



  stroke (color_neutral);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (divider_wall_upper.x, divider_wall_upper.y);
    vertex (divider_wall_lower.x, divider_wall_lower.y);
  endShape();
  
  beginShape();
    vertex (divider_floor_left.x,  divider_floor_left.y);
    vertex (divider_floor_right.x, divider_floor_right.y);
  endShape();



  const chart_legend_min_radius_pos = {x:     0 + divider_margin + data_set[0].min_radius, y: divider_wall_upper.y};
  const chart_legend_max_radius_pos = {x: width - divider_margin - data_set[0].max_radius, y: divider_wall_upper.y};

  noStroke();
  fill (color_dark);
  ellipse (chart_legend_min_radius_pos.x, chart_legend_min_radius_pos.y, data_set[0].min_radius, data_set[0].min_radius);
  ellipse (chart_legend_max_radius_pos.x, chart_legend_max_radius_pos.y, data_set[0].max_radius, data_set[0].max_radius);

  stroke (color_darker);
  strokeWeight (1.2);
  line 
  (
    chart_legend_min_radius_pos.x, 
    chart_legend_min_radius_pos.y - data_set[0].min_radius/6, 
    chart_legend_min_radius_pos.x, 
    chart_legend_min_radius_pos.y + data_set[0].min_radius/6
  );

  line 
  (
    chart_legend_min_radius_pos.x - data_set[0].min_radius/6, 
    chart_legend_min_radius_pos.y, 
    chart_legend_min_radius_pos.x + data_set[0].min_radius/6, 
    chart_legend_min_radius_pos.y
  );

  line 
  (
    chart_legend_max_radius_pos.x, 
    chart_legend_max_radius_pos.y - data_set[0].max_radius/6, 
    chart_legend_max_radius_pos.x, 
    chart_legend_max_radius_pos.y + data_set[0].max_radius/6
  );

  line 
  (
    chart_legend_max_radius_pos.x - data_set[0].max_radius/6, 
    chart_legend_max_radius_pos.y, 
    chart_legend_max_radius_pos.x + data_set[0].max_radius/6, 
    chart_legend_max_radius_pos.y
  );

  stroke (color_dark);
  strokeWeight (4);
  strokeCap(SQUARE);
  point (chart_legend_min_radius_pos.x, chart_legend_min_radius_pos.y);
  point (chart_legend_max_radius_pos.x, chart_legend_max_radius_pos.y);
  strokeCap(ROUND);
}



function drawParticles2PhysicsWorldWith (ptron_checking_error, ptron_training_error, ptron_activation_result, ptron_weight_0_value) 
{
  world.update();

  ellipseMode (RADIUS);

  let num_particles_below_funnel_neck = 0;

  for (let i=0; i<world.particles.length; i++) 
  {
    noStroke();

    if (ptron_checking_error === false) fill (color_bright);
    else 
    {
      if (ptron_training_error === 0) fill (0, 255, 0);
      else fill (255, 0, 0);
    }
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);



    stroke (color_darker);
    strokeWeight (1.2);
    line 
    (
      world.particles[i].position.x, 
      world.particles[i].position.y - world.particles[i].radius/6, 
      world.particles[i].position.x, 
      world.particles[i].position.y + world.particles[i].radius/6
    );

    line 
    (
      world.particles[i].position.x - world.particles[i].radius/6, 
      world.particles[i].position.y, 
      world.particles[i].position.x + world.particles[i].radius/6, 
      world.particles[i].position.y
    );

    

    if (ptron_checking_error === false) stroke (color_bright);
    else 
    {
      if (ptron_training_error === 0) stroke (0, 255, 0);
      else stroke (255, 0, 0);
    }
    strokeWeight (4);
    strokeCap(SQUARE);
    point (world.particles[i].position.x, world.particles[i].position.y);
    strokeCap(ROUND);



    if (world.particles[i].position.y > height - divider_margin - divider_h) 
    {
      num_particles_below_funnel_neck++;
    }

    if (world.particles[i].position.x >= attractor_x - 5 &&
        world.particles[i].position.x <= attractor_x + 5) 
    {
      removeAttractor2PhysicsWorld();
      physics_world_in_anim = false;
    }
  }



  if (world.particles.length === num_particles_below_funnel_neck &&
      world.particles.length < data_set.length) 
  {
    addParticle2PhysicsWorld (ptron_activation_result, ptron_weight_0_value);
  }
}



function addParticle2PhysicsWorld (ptron_activation_result, ptron_weight_0_value) 
{
  physics_world_in_anim = true;

  const temp_particle  = new c2.Particle (width/2, -data_set[0].max_radius);
  temp_particle.radius = data_set[world.particles.length].inputs[0];
  world.addParticle (temp_particle);

  addAttractor2PhysicsWorldWith (ptron_activation_result, ptron_weight_0_value);
}



function addAttractor2PhysicsWorldWith (ptron_activation_result, ptron_weight_0_value) 
{
  const outer_bound_left  = width/2 - (data_set[0].radius)*1;
  const inner_bound_left  = width/2 - (data_set[0].radius)/2;

  const inner_bound_right = width/2 + (data_set[0].radius)/2;
  const outer_bound_right = width/2 + (data_set[0].radius)*1;

  attractor_x = 0;
  if (ptron_activation_result < 0) 
    attractor_x = constrain( map(ptron_weight_0_value, -1, 0, outer_bound_left,  inner_bound_left),  outer_bound_left,  inner_bound_left);
  else 
    attractor_x = constrain( map(ptron_weight_0_value,  0, 1, inner_bound_right, outer_bound_right), inner_bound_right, outer_bound_right);
  
  attractor_y = height - divider_margin - divider_h;
  attractor_h = divider_h;

  ptron_decision_attractor = new c2.LineField (new c2.Line (attractor_x, attractor_y, attractor_x, attractor_y+attractor_h), 0.9);
  world.addForce (ptron_decision_attractor);
}



function removeAttractor2PhysicsWorld() 
{
  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = undefined;
  }
}



function removeParticle2PhysicsWorld() 
{
  //console.log ("-" + world.particles.length);

  for (let i=0; i<world.particles.length; i++) 
  {
    world.removeParticle (world.particles[i]);
    i--;
  }

  //console.log ("--" + world.particles.length);
}



function removeWrongParticle2PhysicsWorld (ptron_training_error) 
{
  for (let i=0; i<world.particles.length; i++) 
  {
    if (ptron_training_error === 0) 
    {
      world.removeParticle (world.particles[i]);
      i--;
    }
  }
}



function seePerceptronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    stroke (255, 0, 255);
    strokeWeight (1);
    line (attractor_x, attractor_y, attractor_x, attractor_y+attractor_h);
  }
}


