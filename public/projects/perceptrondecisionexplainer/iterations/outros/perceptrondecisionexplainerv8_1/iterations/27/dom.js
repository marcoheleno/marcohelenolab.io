


let color_darker, color_dark, color_neutral, color_bright, color_brighter;



function createColorsFromCSS()
{
    // Defined in index.css
    color_darker   = getComputedStyle(document.documentElement).getPropertyValue("--color_darker");
    color_dark     = getComputedStyle(document.documentElement).getPropertyValue("--color_dark");
    color_neutral  = getComputedStyle(document.documentElement).getPropertyValue("--color_neutral");
    color_bright   = getComputedStyle(document.documentElement).getPropertyValue("--color_bright");
    color_brighter = getComputedStyle(document.documentElement).getPropertyValue("--color_brighter");

    color_darker   = color( red(color_darker),   green(color_darker),   blue(color_darker)   );
    color_dark     = color( red(color_dark),     green(color_dark),     blue(color_dark)     );
    color_neutral  = color( red(color_neutral),  green(color_neutral),  blue(color_neutral)  );
    color_bright   = color( red(color_bright),   green(color_bright),   blue(color_bright)   );
    color_brighter = color( red(color_brighter), green(color_brighter), blue(color_brighter) );
}


