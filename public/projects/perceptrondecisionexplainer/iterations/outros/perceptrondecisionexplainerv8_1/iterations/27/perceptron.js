


let ptron, ptron_iterations_table_log;



function startPerceptronWith()
{
  ptron = 0;
  ptron = new Perceptron (data_set[0].inputs.length);

  createPerceptronIterationsTableLog (data_set[0]);
}



class Perceptron 
{

  constructor (num_weights) 
  {
    this.learning_rate = 0.0005;

    this.weights = new Array (num_weights); 
    this.startWithRandomWeights();
  }



  startWithRandomWeights() 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = random (-1, 1);
    }
  }


  
  runFeedForwardWith (inputs)
  {
    this.sum_weighted_inputs_result = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs_result += inputs[i] * this.weights[i];
    }

    this.runActivationFunction();
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs_result < 0) 
      this.activation_result = -1;
    else
      this.activation_result =  1;
  }



  calculateTrainingErrorWith (goal) 
  {
    this.training_error = goal - this.activation_result;
  }



  runTrainingWith (inputs) 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * inputs[i];
    }
  }
  
}



function createPerceptronIterationsTableLog (data_sample) 
{
  ptron_iterations_table_log = 0;
  ptron_iterations_table_log = new p5.Table([0]);



  // data log

  ptron_iterations_table_log.addColumn ("data_id");

  for (let i=0; i<data_sample.inputs.length; i++) 
  {
    if (data_sample.inputs[i] != 1) 
      ptron_iterations_table_log.addColumn("data_input_" + i + "_value");
    else 
      ptron_iterations_table_log.addColumn("data_input_" + i + "_bias");
  }

  ptron_iterations_table_log.addColumn("data_solution_value");



  // ptron log

  ptron_iterations_table_log.addColumn("ptron_iteration_id");

  ptron_iterations_table_log.addColumn("ptron_learning_rate_value");

  for (let i=0; i<data_sample.inputs.length; i++) 
    ptron_iterations_table_log.addColumn("ptron_weight_" + i + "_value");
  
  ptron_iterations_table_log.addColumn("ptron_sum_weighted_inputs_result");

  ptron_iterations_table_log.addColumn("ptron_activation_result");

  ptron_iterations_table_log.addColumn("ptron_training_error");

  //console.log (ptron_iterations_table_log.columns);
  /*
  0: "data_id"
  1: "data_input_0_value"
  2: "data_input_1_bias"
  3: "data_solution_value"
  4: "ptron_iteration_id"
  5: "ptron_learning_rate_value"
  6: "ptron_weight_0_value"
  7: "ptron_weight_1_value"
  8: "ptron_sum_weighted_inputs_result"
  9: "ptron_activation_result"
 10: "ptron_training_error"
  */
}



function runPerceptronWith() 
{
  let ptron_trained_on_num_data_elements = 0;
  let ptron_iteration_i = 0;

  while (ptron_trained_on_num_data_elements < data_set.length) 
  {

    for (let d=0; d<data_set.length; d++) 
    {
      ptron_iterations_table_log.addRow();
      const table_i = ptron_iterations_table_log.rows.length-1;



      // data log

      ptron_iterations_table_log.setNum (table_i, "data_id", d);

      for (let i=0; i<data_set[d].inputs.length; i++) 
      {
        if (data_set[d].inputs[i] != 1) 
          ptron_iterations_table_log.setNum (table_i, "data_input_" + i + "_value",  data_set[d].inputs[i]);
        else 
          ptron_iterations_table_log.setNum (table_i, "data_input_" + i + "_bias",   data_set[d].inputs[i]);
      }
      
      ptron_iterations_table_log.setNum (table_i, "data_solution_value", data_set[d].solution);



      // ptron log

      ptron_iterations_table_log.setNum (table_i, "ptron_iteration_id", ptron_iteration_i);

      ptron_iterations_table_log.setNum (table_i, "ptron_learning_rate_value", ptron.learning_rate);
      ptron_iterations_table_log.setNum (table_i, "ptron_weight_0_value", ptron.weights[0]);
      ptron_iterations_table_log.setNum (table_i, "ptron_weight_1_value", ptron.weights[1]);

      ptron.runFeedForwardWith (data_set[d].inputs);
      ptron_iterations_table_log.setNum (table_i, "ptron_sum_weighted_inputs_result", ptron.sum_weighted_inputs_result);
      ptron_iterations_table_log.setNum (table_i, "ptron_activation_result", ptron.activation_result);

      ptron.calculateTrainingErrorWith (data_set[d].solution);
      ptron_iterations_table_log.setNum (table_i, "ptron_training_error", ptron.training_error);
      if (ptron.training_error === 0) ptron_trained_on_num_data_elements++;

      if (ptron.training_error === 0 && ptron_iteration_i === 0) 
        re_StartVisualization();

      ptron.runTrainingWith (data_set[d].inputs);
    }

    ptron_iteration_i++;

    if (ptron_trained_on_num_data_elements < data_set.length-1) 
      ptron_trained_on_num_data_elements = 0; 
  }
}



function printPerceptronIterationsTableLog() 
{
  saveTable (ptron_iterations_table_log, "ptron_iterations_table_log", "csv");
}


