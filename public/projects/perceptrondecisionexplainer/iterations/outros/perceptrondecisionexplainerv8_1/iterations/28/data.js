


let data_set;



function createDataSetWith (num_data_elements)
{
  data_set = new Array (num_data_elements);

  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }
}



class DataElement 
{
  constructor() 
  {
    this.min_radius = int(height/19);
    this.max_radius = int(height/13);
    
    this.runRandomDataDefiner();
    this.runSolutionProviderForPtronTraining();
    this.buildDataElement();
  }

  

  runRandomDataDefiner()
  {
    const random_radius_definer = int(random(2));

    this.radius = 0;
    if (random_radius_definer === 0) this.radius = this.min_radius;
    else
    if (random_radius_definer === 1) this.radius = this.max_radius;
  }



  runSolutionProviderForPtronTraining()
  {
    this.threshold_for_ptron_training = this.min_radius + ( (this.max_radius - this.min_radius)/2 );
    
    this.solution = 0;
    if (this.radius < this.threshold_for_ptron_training) this.solution = -1;
    else this.solution = 1;
  }



  buildDataElement() 
  {
    const data_bias = 1;
    this.inputs = [this.radius, data_bias];
    this.solution;
  }
}


