


let world;
let funnel_left_polygon_constraint, funnel_right_polygon_constraint;
let divider_margin, divider_h;
let polygon_divider_constraint, polygon_divider_floor_constraint;
let ptron_decision_attractor;
let attractor_x, attractor_y, attractor_h;
let physics_world_in_anim;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect(0, -data_set[0].max_radius, width, height+data_set[0].max_radius));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line (0, height, width, height), 1);
  world.addForce (floorForce);

  if (physics_world_in_anim === undefined) 
    physics_world_in_anim = false;
}






function drawBalanceSeparator2PhysicsWorldWith (ptron_weight_0_value, is_ptron_checking_error, ptron_training_error_value) 
{
  const funnel_w = width/6;
  const lixo1 = map (ptron_weight_0_value, -1, 1, 15, -funnel_w/2.2);
  const lixo2 = map (ptron_weight_0_value, -1, 1, -15, 14); //14
  const lixo3 = 0;//map (data_set[0].radius, data_set[0].min_radius, data_set[0].max_radius, 0, 4);

  push();
    translate (width/2, height/4);

      rotate (lixo2 + lixo3);

      const balance_separator_left_edge  = screenPosition (0 + lixo1, 0);
      const balance_separator_right_edge = screenPosition (funnel_w + lixo1, 0);
  pop();



  if (funnel_left_polygon_constraint  != undefined) world.removeConstraint (funnel_left_polygon_constraint);
  
  const funnel_left_polygon = new c2.Polygon 
  ([
    new c2.Point (balance_separator_left_edge.x, balance_separator_left_edge.y), 
    new c2.Point (balance_separator_right_edge.x, balance_separator_right_edge.y)
  ]);

  funnel_left_polygon_constraint  = new c2.PolygonConstraint (funnel_left_polygon);
  world.addConstraint (funnel_left_polygon_constraint);



  noFill();
  strokeWeight (1);

  if (is_ptron_checking_error === false) stroke (color_neutral);
  else 
  {
    if (ptron_training_error_value === 0) stroke (0, 255, 0);
    else stroke (255, 0, 0);
  }

  beginShape();
    vertex (balance_separator_left_edge.x, balance_separator_left_edge.y);
    vertex (balance_separator_right_edge.x, balance_separator_right_edge.y);
  endShape();

  circle (balance_separator_left_edge.x, balance_separator_left_edge.y, 5);
}



function drawDivider2PhysicsWorld() 
{
        divider_margin = 30;
        divider_h = height/2;
  const divider_w = width - divider_margin*2;

  const divider_wall_upper  = {x: width/2, y: height - divider_margin - divider_h};
  const divider_wall_lower  = {x: width/2, y: height - divider_margin};

  const divider_floor_left  = {x: width/2 - divider_w/2, y: height - divider_margin};
  const divider_floor_right = {x: width/2 + divider_w/2, y: height - divider_margin};



  if (polygon_divider_constraint       != undefined) world.removeConstraint (polygon_divider_constraint);
  if (polygon_divider_floor_constraint != undefined) world.removeConstraint (polygon_divider_floor_constraint);

  const polygon_divider = new c2.Polygon 
  ([
    new c2.Point (divider_wall_upper.x, divider_wall_upper.y), 
    new c2.Point (divider_wall_lower.x, divider_wall_lower.y)
  ]);

  const polygon_divider_floor = new c2.Polygon 
  ([
    new c2.Point (divider_floor_left.x,  divider_floor_left.y), 
    new c2.Point (divider_floor_right.x, divider_floor_right.y)
  ]);

  polygon_divider_constraint = new c2.PolygonConstraint (polygon_divider);
  world.addConstraint (polygon_divider_constraint);

  polygon_divider_floor_constraint = new c2.PolygonConstraint (polygon_divider_floor);
  world.addConstraint (polygon_divider_floor_constraint);


  
  stroke (color_neutral);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (divider_wall_upper.x, divider_wall_upper.y);
    vertex (divider_wall_lower.x, divider_wall_lower.y);
  endShape();
  
  beginShape();
    vertex (divider_floor_left.x,  divider_floor_left.y);
    vertex (divider_floor_right.x, divider_floor_right.y);
  endShape();



  /*
  const chart_legend_min_radius_pos = {x:     0 + divider_margin + data_set[0].min_radius, y: divider_wall_upper.y};
  const chart_legend_max_radius_pos = {x: width - divider_margin - data_set[0].max_radius, y: divider_wall_upper.y};

  noStroke();
  fill (color_dark);
  ellipse (chart_legend_min_radius_pos.x, chart_legend_min_radius_pos.y, data_set[0].min_radius, data_set[0].min_radius);
  ellipse (chart_legend_max_radius_pos.x, chart_legend_max_radius_pos.y, data_set[0].max_radius, data_set[0].max_radius);

  stroke (color_darker);
  strokeWeight (1.2);
  line 
  (
    chart_legend_min_radius_pos.x, 
    chart_legend_min_radius_pos.y - data_set[0].min_radius/6, 
    chart_legend_min_radius_pos.x, 
    chart_legend_min_radius_pos.y + data_set[0].min_radius/6
  );

  line 
  (
    chart_legend_min_radius_pos.x - data_set[0].min_radius/6, 
    chart_legend_min_radius_pos.y, 
    chart_legend_min_radius_pos.x + data_set[0].min_radius/6, 
    chart_legend_min_radius_pos.y
  );

  line 
  (
    chart_legend_max_radius_pos.x, 
    chart_legend_max_radius_pos.y - data_set[0].max_radius/6, 
    chart_legend_max_radius_pos.x, 
    chart_legend_max_radius_pos.y + data_set[0].max_radius/6
  );

  line 
  (
    chart_legend_max_radius_pos.x - data_set[0].max_radius/6, 
    chart_legend_max_radius_pos.y, 
    chart_legend_max_radius_pos.x + data_set[0].max_radius/6, 
    chart_legend_max_radius_pos.y
  );

  stroke (color_dark);
  strokeWeight (4);
  strokeCap(SQUARE);
  point (chart_legend_min_radius_pos.x, chart_legend_min_radius_pos.y);
  point (chart_legend_max_radius_pos.x, chart_legend_max_radius_pos.y);
  strokeCap(ROUND);
  */
}






function drawParticles2PhysicsWorldWith (is_ptron_checking_error, ptron_training_error_value, ptron_activation_result, ptron_weight_0_value) 
{
  world.update();

  let num_particles_below_funnel_neck = 0;

  for (let i=0; i<world.particles.length; i++) 
  {
    noStroke();

    if (is_ptron_checking_error === false) fill (color_bright);
    else 
    {
      if (ptron_training_error_value === 0) fill (0, 255, 0);
      else fill (255, 0, 0);
    }
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);


    /*
    stroke (color_darker);
    strokeWeight (1.2);
    line 
    (
      world.particles[i].position.x, 
      world.particles[i].position.y - world.particles[i].radius/6, 
      world.particles[i].position.x, 
      world.particles[i].position.y + world.particles[i].radius/6
    );

    line 
    (
      world.particles[i].position.x - world.particles[i].radius/6, 
      world.particles[i].position.y, 
      world.particles[i].position.x + world.particles[i].radius/6, 
      world.particles[i].position.y
    );
    */

    
    /*
    if (is_ptron_checking_error === false) stroke (color_bright);
    else 
    {
      if (ptron_training_error_value === 0) stroke (0, 255, 0);
      else stroke (255, 0, 0);
    }
    strokeWeight (4);
    strokeCap(SQUARE);
    point (world.particles[i].position.x, world.particles[i].position.y);
    strokeCap(ROUND);
    */



    if (world.particles[i].position.y > height - divider_margin - divider_h) 
    {
      num_particles_below_funnel_neck++;
      physics_world_in_anim = false;
    }

    if (world.particles[i].position.x >= attractor_x - 5 &&
        world.particles[i].position.x <= attractor_x + 5) 
    {
      removeAttractor2PhysicsWorld();
    }
  }

  if (world.particles.length === num_particles_below_funnel_neck &&
      world.particles.length < data_set.length) 
  {
    addParticle2PhysicsWorld (ptron_activation_result, ptron_weight_0_value);
  }
}



function addParticle2PhysicsWorld (ptron_activation_result, ptron_weight_0_value) 
{
  physics_world_in_anim = true;

  const temp_particle  = new c2.Particle (width/2, -data_set[0].max_radius);
  temp_particle.radius = data_set[world.particles.length].inputs[0];
  world.addParticle (temp_particle);

  addAttractor2PhysicsWorldWith (ptron_activation_result, ptron_weight_0_value);
}



function removeParticle2PhysicsWorld() 
{
  world.removeParticle (world.particles[0]);
}






function addAttractor2PhysicsWorldWith (ptron_activation_result, ptron_weight_0_value) 
{
  const outer_bound_left  = width/2 - (data_set[0].radius)*7;
  const inner_bound_left  = width/2 - (data_set[0].radius)/2;

  const inner_bound_right = width/2 + (data_set[0].radius)/2;
  const outer_bound_right = width/2 + (data_set[0].radius)*7;

  attractor_x = 0;
  if (ptron_activation_result < 0) 
    attractor_x = outer_bound_left;//constrain( map(ptron_weight_0_value, -1, 0, outer_bound_left,  inner_bound_left),  outer_bound_left,  inner_bound_left);
  else 
    attractor_x = outer_bound_right;//constrain( map(ptron_weight_0_value,  0, 1, inner_bound_right, outer_bound_right), inner_bound_right, outer_bound_right);
  
  attractor_y = height - divider_margin - divider_h;
  attractor_h = divider_h;

  ptron_decision_attractor = new c2.LineField (new c2.Line (attractor_x, attractor_y, attractor_x, attractor_y+attractor_h), 0.9);
  world.addForce (ptron_decision_attractor);
}



function removeAttractor2PhysicsWorld() 
{
  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = undefined;
  }
}



function seePerceptronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    stroke (255, 0, 255);
    strokeWeight (1);
    line (attractor_x, attractor_y, attractor_x, attractor_y+attractor_h);
  }
}


