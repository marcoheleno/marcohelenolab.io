


let temp_data_id, temp_data_input_0_value, temp_data_input_1_bias, temp_data_solution_value;
let temp_ptron_learning_rate_value, temp_ptron_weight_0_value, temp_ptron_weight_1_value;
let temp_ptron_sum_weighted_inputs_result, temp_ptron_activation_result, temp_ptron_training_error;
let table_row_i, vis_moment;
let remove_particle;
let auto_vis;



function setup() 
{
  const canvas = createCanvas (windowWidth, windowHeight);
  canvas.parent ("vis_section");

  angleMode (DEGREES);
  ellipseMode(RADIUS);

  createColorsFromCSS();
  addScreenPositionFunction();
  re_StartVisualization();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  re_StartVisualization();
}



function re_StartVisualization() 
{
  createDataSetWith (1);
  startPerceptronWith();
  runPerceptronWith();
  //printPerceptronIterationsTableLog();
  addPhysicsWorld();

  table_row_i = 0;
  vis_moment  = 0;
  remove_particle = false;
}



function draw() 
{
  clear();

  temp_data_id =                          ptron_iterations_table_log.getNum (table_row_i, "data_id");
  temp_data_input_0_value =               ptron_iterations_table_log.getNum (table_row_i, "data_input_0_value");
  temp_data_input_1_bias =                ptron_iterations_table_log.getNum (table_row_i, "data_input_1_bias");
  temp_data_solution_value =              ptron_iterations_table_log.getNum (table_row_i, "data_solution_value");
  temp_ptron_learning_rate_value =        ptron_iterations_table_log.getNum (table_row_i, "ptron_learning_rate_value");
  temp_ptron_weight_0_value =             ptron_iterations_table_log.getNum (table_row_i, "ptron_weight_0_value");
  temp_ptron_weight_1_value =             ptron_iterations_table_log.getNum (table_row_i, "ptron_weight_1_value");
  temp_ptron_sum_weighted_inputs_result = ptron_iterations_table_log.getNum (table_row_i, "ptron_sum_weighted_inputs_result");
  temp_ptron_activation_result =          ptron_iterations_table_log.getNum (table_row_i, "ptron_activation_result");
  temp_ptron_training_error =             ptron_iterations_table_log.getNum (table_row_i, "ptron_training_error");

  
  
  if (vis_moment === 0) 
  {
    drawBalanceSeparator2PhysicsWorldWith (0, false, 0);

    noStroke();
    fill (color_brighter);
    text ("O Ptron irá a classificar um dado/circulo,", 10, height/2 + 100 + 0);
    text ("escolhendo e projetando, ", 10, height/2 + 100 + 15);
    text ("com base no diametro de cada circulo,", 10, height/2 + 100 + 30);
    text ("para a esquerda ou para a direita.", 10, height/2 + 100 + 45);
    text ("Click para iniciar!", 10, height/2 + 100 + 75);
  }

  else 

  if (vis_moment === 1) 
  {
    if (table_row_i > 0 && remove_particle === true) 
    {
      removeParticle2PhysicsWorld();
      remove_particle = false;
    }

    drawBalanceSeparator2PhysicsWorldWith (temp_ptron_weight_0_value, false, temp_ptron_training_error);
    drawParticles2PhysicsWorldWith (false, temp_ptron_training_error, temp_ptron_activation_result, temp_ptron_weight_0_value);

    if (auto_vis === true) vis_moment++;

    noStroke();
    fill (color_brighter);
    text ("Click novamente para que o Ptron", 10, height/2 + 100 + 0);
    text ("possa verificar se a classificação está correcta.", 10, height/2 + 100 + 15);
  }

  else 

  if (vis_moment === 2) 
  {
    if (temp_ptron_training_error === 0) 
    {
      vis_moment++;
    }
    else 
    {
      drawBalanceSeparator2PhysicsWorldWith (temp_ptron_weight_0_value, false, temp_ptron_training_error);
      drawParticles2PhysicsWorldWith (true, temp_ptron_training_error, temp_ptron_activation_result, temp_ptron_weight_0_value);
      
      noStroke();
      fill (color_brighter);
      text ("O Ptron identificou que a classificação está incorrecta.", 10, height/2 + 100 + 0);
      text ("Click novamente para que o Ptron possa apreender, corrigindo a sua intrepetação.", 10, height/2 + 100 + 30);
    }
  }

  else 

  if (vis_moment === 3) 
  {
    remove_particle = true;
    if (temp_ptron_training_error === 0) 
    {
      vis_moment++;
    }
    else 
    {
      drawBalanceSeparator2PhysicsWorldWith (temp_ptron_weight_0_value, true, temp_ptron_training_error);
      drawParticles2PhysicsWorldWith (true, temp_ptron_training_error, temp_ptron_activation_result, temp_ptron_weight_0_value);
      
      noStroke();
      fill (color_brighter);
      text ("O Ptron corrigiu a sua intrepetação.", 10, height/2 + 100 + 0);
      text ("Click novamente para que o Ptron possa realizar uma nova classificação.", 10, height/2 + 100 + 30);
    }
  }
  
  

  if (vis_moment === 4) 
  {
    if (table_row_i > 0 && remove_particle === true) 
    {
      removeParticle2PhysicsWorld();
      remove_particle = false;
    }

    drawBalanceSeparator2PhysicsWorldWith (temp_ptron_weight_0_value, true, temp_ptron_training_error);
    drawParticles2PhysicsWorldWith (true, temp_ptron_training_error, temp_ptron_activation_result, temp_ptron_weight_0_value);
    
    noStroke();
    fill (color_brighter);
    text ("O Ptron identificou que a classificação está correcta,", 10, height/2 + 100 + 0);
    text ("logo não terá que corrigir a sua intrepetação.", 10, height/2 + 100 + 15);
    text ("O Ptron encontra-se treinado!", 10, height/2 + 100 + 45);
  }



  drawDivider2PhysicsWorld();
  seePerceptronDecisionAttractor();
  if (auto_vis === true) visualizationManager();
}



function visualizationManager() 
{
  if (physics_world_in_anim === false) 
  {
    if (vis_moment < 3) 
    {
      vis_moment++;
    }
    else 
    {
      if (temp_ptron_training_error != 0) 
        vis_moment = 1;
    }

    if (vis_moment === 3 && temp_ptron_training_error != 0) 
      table_row_i++;
  }
}



function mouseReleased() 
{
  visualizationManager();
}



function keyReleased() 
{
  if (key === "a") auto_vis = !auto_vis;
}


