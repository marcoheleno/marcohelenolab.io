

let pg;


function setup() 
{
  createCanvas (windowWidth, windowHeight, WEBGL);
}



function draw() 
{
  background (100);
  


  angleMode (DEGREES);

  push();
    translate (0, 0, 0);
  
    rotateX (0);
    rotateY (0);
  
    divisor (map(mouseX, 0, width, 0, 46));
  pop();

  angleMode (RADIANS);
}


  
function divisor (aperture_focus) 
{
  const x = 0;
  const y = 0;
  const w = 500;
  const h = 300;

  const aperture_diameter = 250;
  const stroke_thickness = 2;
  const stroke_c = color (255, 255, 255);
  const fill_c = color (0, 0, 0);
  

  
  push();
    translate (x, y);
  
    textureMode (NORMAL);
    texture
    (
      aperture 
      (
        w, 
        h, 
        aperture_diameter, 
        aperture_focus, 
        stroke_thickness, 
        stroke_c, 
        fill_c
      )
    );
  
    beginShape();
      vertex (-w/2, -h/2, 0, 0);
      vertex ( w/2, -h/2, 1, 0);
      vertex ( w/2,  h/2, 1, 1);
      vertex (-w/2,  h/2, 0, 1);
    endShape (CLOSE);
  
  pop();
}



function aperture (outer_w, outer_h, aperture_diameter, aperture_focus, stroke_thickness, stroke_c, fill_c) 
{
  if (pg === undefined) pg = createGraphics (500, 300, P2D);

  pg.clear();

  apertureBlades 
  (
    outer_w/2, 
    outer_h/2, 
    aperture_diameter/1.5, 
    aperture_focus, 
    stroke_thickness, 
    stroke_c, 
    fill_c, 
    pg
  );



  pg.strokeWeight (stroke_thickness);
  pg.stroke (stroke_c);
  pg.fill (fill_c);
  
  pg.push();
    pg.translate (outer_w/2, outer_h/2)

    pg.beginShape();
      pg.vertex(-outer_w/2+stroke_thickness/2, -outer_h/2+stroke_thickness/2);
      pg.vertex( outer_w/2-stroke_thickness/2, -outer_h/2+stroke_thickness/2);
      pg.vertex( outer_w/2-stroke_thickness/2,  outer_h/2-stroke_thickness/2);
      pg.vertex(-outer_w/2+stroke_thickness/2,  outer_h/2-stroke_thickness/2);
  
      pg.beginContour();
        pg.vertex (-aperture_diameter/2, 0);
  
        pg.bezierVertex 
        (
          -aperture_diameter/2, 
           aperture_diameter/3.6, 
          -aperture_diameter/3.6, 
           aperture_diameter/2, 
           0, 
           aperture_diameter/2
        );

        pg.bezierVertex 
        (
          aperture_diameter/3.6, 
          aperture_diameter/2, 
          aperture_diameter/2, 
          aperture_diameter/3.6, 
          aperture_diameter/2, 
          0
        );

        pg.bezierVertex 
        (
           aperture_diameter/2, 
          -aperture_diameter/3.6, 
           aperture_diameter/3.6, 
          -aperture_diameter/2, 
           0, 
          -aperture_diameter/2
        );

        pg.bezierVertex 
        (
          -aperture_diameter/3.6, 
          -aperture_diameter/2, 
          -aperture_diameter/2, 
          -aperture_diameter/3.6, 
          -aperture_diameter/2, 
           0
        );
      pg.endContour();
  
    pg.endShape (CLOSE);
  pg.pop();

  return pg;
}



function apertureBlades (x, y, radius, focus, stroke_thickness, stroke_c, fill_c, pg) 
{
  const num_of_blades = 6;
  const blade_rotation_interval = 360/num_of_blades;
  
  angleMode (DEGREES);

  for (let b=0; b<num_of_blades; b++) 
  {
    pg.push();
      pg.translate (x, y);
      pg.rotate (blade_rotation_interval * b);

      apertureBlade (radius, focus, stroke_thickness, stroke_c, fill_c, pg);
    pg.pop();
  }
  
  angleMode (RADIANS);
}



function apertureBlade (radius, focus, stroke_thickness, stroke_c, fill_c, pg) 
{
  const blade_one_third_aperture = 3;
  
  

  angleMode (RADIANS);

  const bezier_edges_pos = [];
  
  for (let ang=0; ang<360; ang+=(360/blade_one_third_aperture)) 
  {
    const temp_border_pos = 
    {
      x: 0 + radius * cos(radians(ang)), 
      y: 0 + radius * sin(radians(ang))
    }
    
    bezier_edges_pos.push (temp_border_pos);

    /*
    stroke (0, 255, 255);
    strokeWeight (6);
    line (0, 0, temp_border_pos.x, temp_border_pos.y);
    */
  }



  const bezier_ctrls_pos = [];
  const bezier_ctrls_dist_center = radius*1.25;
  
  for (let ang=0; ang<360; ang+=360/(blade_one_third_aperture*6)) 
  {
    const temp_border_pos = 
    {
      x: 0 + bezier_ctrls_dist_center * cos(radians(ang)), 
      y: 0 + bezier_ctrls_dist_center * sin(radians(ang))
    }
    
    bezier_ctrls_pos.push (temp_border_pos);
    
    /*
    stroke (255, 255, 0);
    strokeWeight (3);
    line (0, 0, temp_border_pos.x, temp_border_pos.y);
    */
  }
  
  
  
  const bezier_ctrls_2_pos = [];
  const bezier_ctrls_2_dist_center = radius*2.7;
  
  for (let ang=0; ang<360; ang+=360/(blade_one_third_aperture*6)) 
  {
    const temp_border_pos = 
    {
      x: 0 + bezier_ctrls_2_dist_center * cos(radians(ang)), 
      y: 0 + bezier_ctrls_2_dist_center * sin(radians(ang))
    }
    
    bezier_ctrls_2_pos.push (temp_border_pos);
    
    /*
    stroke (150, 150, 150);
    strokeWeight (1);
    line (0, 0, temp_border_pos.x, temp_border_pos.y);
    */
  }


  
  pg.push();
    pg.translate (0, 0);

    pg.push();
      pg.translate (bezier_edges_pos[0].x, bezier_edges_pos[0].y);

      pg.angleMode (DEGREES);
      pg.rotate (focus);

      //pg.strokeWeight (stroke_thickness);
      //pg.stroke (stroke_c);
      pg.fill (fill_c);


      
      const edge_1 = {x:0, y:0};

      const ctrl_1 = 
      {
        x:abs(bezier_edges_pos[0].x - bezier_ctrls_pos[1].x)*-1, 
        y:abs(bezier_edges_pos[0].y - bezier_ctrls_pos[1].y)*1
      };

      const ctrl_2 = 
      {
        x:abs(bezier_edges_pos[0].x - bezier_ctrls_pos[5].x)*-1, 
        y:abs(bezier_edges_pos[0].y - bezier_ctrls_pos[5].y)*1
      };

      const edge_2 = 
      {
        x:abs(bezier_edges_pos[0].x - bezier_edges_pos[1].x)*-1, 
        y:abs(bezier_edges_pos[0].y - bezier_edges_pos[1].y)*1
      };
      
      
      
      const edge_3 = {x:0, y:0};

      const ctrl_3 = 
      {
        x:abs(bezier_edges_pos[0].x - bezier_ctrls_2_pos[2].x)*1, 
        y:abs(bezier_edges_pos[0].y - bezier_ctrls_2_pos[2].y)*1
      };

      const ctrl_4 = 
      {
        x:abs(bezier_edges_pos[0].x - bezier_ctrls_2_pos[4].x)*-1, 
        y:abs(bezier_edges_pos[0].y - bezier_ctrls_2_pos[4].y)*1
      };

      const edge_4 = 
      {
        x:abs(bezier_edges_pos[0].x - bezier_edges_pos[1].x)*-1, 
        y:abs(bezier_edges_pos[0].y - bezier_edges_pos[1].y)*1
      };


      
      pg.beginShape();

        pg.vertex (edge_1.x, edge_1.y);

        pg.bezierVertex 
        (
          ctrl_1.x, ctrl_1.y, 
          ctrl_2.x, ctrl_2.y, 
          edge_2.x, edge_2.y
        );

        pg.bezierVertex 
        (
          ctrl_4.x, ctrl_4.y, 
          ctrl_3.x, ctrl_3.y, 
          edge_3.x, edge_3.y
        );

      pg.endShape();

    pg.pop();

  pg.pop();


  
  angleMode (RADIANS);
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}


