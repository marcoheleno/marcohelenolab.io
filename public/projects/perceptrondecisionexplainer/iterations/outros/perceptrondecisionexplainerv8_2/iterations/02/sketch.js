


let pg_activation_floor;



function setup() 
{
  setAttributes('antialias', true);
  createCanvas (windowWidth, windowHeight, WEBGL);
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  pg_activation_floor.remove();
  pg_activation_floor = 0;
}



function draw() 
{
  background (250);


  
  push();
    translate (0, 0, 0);
  
    angleMode (DEGREES);
    rotateX (0); //65
    rotateY (0); //0.5
    rotateZ (0);

    activationSeparator (0, -200, 0);
  pop();

  angleMode (RADIANS);
}
  


function activationSeparator (x, y, z) 
{
  const w = 400;
  const h = 400;

  const hole_d = h/2.3;
  
  push();
    translate (x, y, z);
  
    angleMode (DEGREES);
    rotateX (0); //65
    rotateY (0); //0.5
    rotateZ (0);

    activationBlades (0, 0, 0, hole_d/1.3, map(mouseX, 0, width, 0, 46) );

    activationFloor (0, 0, 0, w, h, hole_d);
  pop();

  angleMode (RADIANS);
}



function activationFloor (x, y, z, w, h, hole_d) 
{
  const stroke_thickness = 1;
  const stroke_c = color (50);
  const fill_c = color (50);
  
  if (pg_activation_floor === undefined || pg_activation_floor === 0) 
  {
    pg_activation_floor = createGraphics (w, h, P2D);
  }

  pg_activation_floor.clear();

  pg_activation_floor.strokeWeight (stroke_thickness);
  pg_activation_floor.stroke (stroke_c);
  pg_activation_floor.fill (fill_c);
  
  pg_activation_floor.push();
    pg_activation_floor.translate (w/2, h/2)

    pg_activation_floor.beginShape();
      pg_activation_floor.vertex (-w/2+stroke_thickness/2, -h/2+stroke_thickness/2);
      pg_activation_floor.vertex ( w/2-stroke_thickness/2, -h/2+stroke_thickness/2);
      pg_activation_floor.vertex ( w/2-stroke_thickness/2,  h/2-stroke_thickness/2);
      pg_activation_floor.vertex (-w/2+stroke_thickness/2,  h/2-stroke_thickness/2);
  
      pg_activation_floor.beginContour();
      pg_activation_floor.vertex (-hole_d/2, 0);
  
      pg_activation_floor.bezierVertex 
      (
        -hole_d/2, 
         hole_d/3.6, 
        -hole_d/3.6, 
         hole_d/2, 
         0, 
         hole_d/2
      );

      pg_activation_floor.bezierVertex 
      (
        hole_d/3.6, 
        hole_d/2, 
        hole_d/2, 
        hole_d/3.6, 
        hole_d/2, 
        0
      );

      pg_activation_floor.bezierVertex 
      (
         hole_d/2, 
        -hole_d/3.6, 
         hole_d/3.6, 
        -hole_d/2, 
         0, 
        -hole_d/2
      );

      pg_activation_floor.bezierVertex 
      (
        -hole_d/3.6, 
        -hole_d/2, 
        -hole_d/2, 
        -hole_d/3.6, 
        -hole_d/2, 
          0
      );
      pg_activation_floor.endContour();
  
    pg_activation_floor.endShape (CLOSE);
  pg_activation_floor.pop();



  push();
    translate (x, y, z);
  
    textureMode (NORMAL);
    texture (pg_activation_floor);
  
    beginShape();
      vertex (-w/2, -h/2, 0, 0, 0);
      vertex ( w/2, -h/2, 0, 1, 0);
      vertex ( w/2,  h/2, 0, 1, 1);
      vertex (-w/2,  h/2, 0, 0, 1);
    endShape (CLOSE);
  
  pop();
}



function activationBlades (x, y, z, aperture_diameter, aperture_focus) 
{
  const stroke_thickness = 2;
  const stroke_c = color (40);
  const fill_c = color (50);

  const num_of_blades = 6;
  const blade_rotation_interval = 360/num_of_blades;
  
  for (let b=0; b<num_of_blades; b++) 
  {
    push();
      translate (x, y, z);

      angleMode (DEGREES);
      rotateZ (blade_rotation_interval * b);

      strokeWeight (stroke_thickness);
      stroke (stroke_c);
      fill (fill_c);

      blade (aperture_diameter, aperture_focus);
    pop();
  }
  
  angleMode (RADIANS);
}



function blade (aperture_diameter, aperture_focus) 
{
  const blade_one_third_of_circle = 3;
  
  angleMode (RADIANS);



  const bezier_edges_pos = [];
  const bezier_edges_dist_center = aperture_diameter;
  
  for (let ang=0; ang<360; ang+=(360/blade_one_third_of_circle)) 
  {
    const temp_border_pos = 
    {
      x: 0 + bezier_edges_dist_center * cos(radians(ang)), 
      y: 0 + bezier_edges_dist_center * sin(radians(ang))
    }
    
    bezier_edges_pos.push (temp_border_pos);
    /*
    stroke (255, 255, 0);
    strokeWeight (9);
    line (0, 0, temp_border_pos.x, temp_border_pos.y);
    */
  }



  const bezier_ctrls_1_pos = [];
  const bezier_ctrls_1_dist_center = aperture_diameter*1.25;
  
  for (let ang=0; ang<360; ang+=360/(blade_one_third_of_circle*6)) 
  {
    const temp_border_pos = 
    {
      x: 0 + bezier_ctrls_1_dist_center * cos(radians(ang)), 
      y: 0 + bezier_ctrls_1_dist_center * sin(radians(ang))
    }
    
    bezier_ctrls_1_pos.push (temp_border_pos);
    /*
    stroke (0, 255, 255);
    strokeWeight (3);
    line (0, 0, temp_border_pos.x, temp_border_pos.y);
    */
  }
  
  
  
  const bezier_ctrls_2_pos = [];
  const bezier_ctrls_2_dist_center = aperture_diameter*1.95;
  
  for (let ang=0; ang<360; ang+=360/(blade_one_third_of_circle*6)) 
  {
    const temp_border_pos = 
    {
      x: 0 + bezier_ctrls_2_dist_center * cos(radians(ang)), 
      y: 0 + bezier_ctrls_2_dist_center * sin(radians(ang))
    }
    
    bezier_ctrls_2_pos.push (temp_border_pos);
    /*
    stroke (150, 150, 150);
    strokeWeight (1);
    line (0, 0, temp_border_pos.x, temp_border_pos.y);
    */
  }



  const edge_1 = {x:0, y:0};

  const ctrl_1 = 
  {
    x:abs(bezier_edges_pos[0].x - bezier_ctrls_1_pos[1].x)*-1, 
    y:abs(bezier_edges_pos[0].y - bezier_ctrls_1_pos[1].y)*1
  };

  const ctrl_2 = 
  {
    x:abs(bezier_edges_pos[0].x - bezier_ctrls_1_pos[5].x)*-1, 
    y:abs(bezier_edges_pos[0].y - bezier_ctrls_1_pos[5].y)*1
  };

  const edge_2 = 
  {
    x:abs(bezier_edges_pos[0].x - bezier_edges_pos[1].x)*-1, 
    y:abs(bezier_edges_pos[0].y - bezier_edges_pos[1].y)*1
  };
      
      
      
  const edge_3 = {x:0, y:0};

  const ctrl_3 = 
  {
    x:abs(bezier_edges_pos[0].x - bezier_ctrls_2_pos[2].x)*1, 
    y:abs(bezier_edges_pos[0].y - bezier_ctrls_2_pos[2].y)*1
  };

  const ctrl_4 = 
  {
    x:abs(bezier_edges_pos[0].x - bezier_ctrls_2_pos[4].x)*-1, 
    y:abs(bezier_edges_pos[0].y - bezier_ctrls_2_pos[4].y)*1
  };

  const edge_4 = 
  {
    x:abs(bezier_edges_pos[0].x - bezier_edges_pos[1].x)*-1, 
    y:abs(bezier_edges_pos[0].y - bezier_edges_pos[1].y)*1
  };


  
  push();
    translate (0, 0, 0);
    
    push();
      translate (bezier_edges_pos[0].x, bezier_edges_pos[0].y, 0);
      
      angleMode (DEGREES);
      rotateX (-2);
      rotateY (-2);
      rotateZ (aperture_focus);
      

      beginShape();
      
        vertex (edge_1.x, edge_1.y, 0);
        
        bezierVertex 
        (
          ctrl_1.x, ctrl_1.y, 0, 
          ctrl_2.x, ctrl_2.y, 0, 
          edge_2.x, edge_2.y, 0
        );
        
        bezierVertex 
        (
          ctrl_4.x, ctrl_4.y, 0, 
          ctrl_3.x, ctrl_3.y, 0, 
          edge_3.x, edge_3.y, 0
        );
        
      endShape (CLOSE);

    pop();

  pop();

  angleMode (RADIANS);
}


