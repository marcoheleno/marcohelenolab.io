


let pg_visual_activation_platform;



function visualActivationPlatform (x, y, z, w, h, hole_d) 
{
  const stroke_thickness = 1;
  const stroke_c = color (100);
  const fill_c = color (100);
  
  if (pg_visual_activation_platform === undefined || 
      pg_visual_activation_platform === 0) 
  {
    pg_visual_activation_platform = createGraphics (w, h, P2D);
  }

  pg_visual_activation_platform.clear();

  pg_visual_activation_platform.strokeWeight (stroke_thickness);
  pg_visual_activation_platform.stroke (stroke_c);
  pg_visual_activation_platform.fill (fill_c);
  
  pg_visual_activation_platform.push();
    pg_visual_activation_platform.translate (w/2, h/2);

    pg_visual_activation_platform.beginShape();
      pg_visual_activation_platform.vertex (-w/2+stroke_thickness/2, -h/2+stroke_thickness/2);
      pg_visual_activation_platform.vertex ( w/2-stroke_thickness/2, -h/2+stroke_thickness/2);
      pg_visual_activation_platform.vertex ( w/2-stroke_thickness/2,  h/2-stroke_thickness/2);
      pg_visual_activation_platform.vertex (-w/2+stroke_thickness/2,  h/2-stroke_thickness/2);
  
      pg_visual_activation_platform.beginContour();
      pg_visual_activation_platform.vertex (-hole_d/2, 0);
  
      pg_visual_activation_platform.bezierVertex 
      (
        -hole_d/2, 
         hole_d/3.6, 
        -hole_d/3.6, 
         hole_d/2, 
         0, 
         hole_d/2
      );

      pg_visual_activation_platform.bezierVertex 
      (
        hole_d/3.6, 
        hole_d/2, 
        hole_d/2, 
        hole_d/3.6, 
        hole_d/2, 
        0
      );

      pg_visual_activation_platform.bezierVertex 
      (
         hole_d/2, 
        -hole_d/3.6, 
         hole_d/3.6, 
        -hole_d/2, 
         0, 
        -hole_d/2
      );

      pg_visual_activation_platform.bezierVertex 
      (
        -hole_d/3.6, 
        -hole_d/2, 
        -hole_d/2, 
        -hole_d/3.6, 
        -hole_d/2, 
          0
      );
      pg_visual_activation_platform.endContour();
  
    pg_visual_activation_platform.endShape (CLOSE);
  pg_visual_activation_platform.pop();



  push();
    translate (x, y, z);

    angleMode (DEGREES);
    rotateX (0);
    rotateY (2);
    rotateZ (0);
  
    textureMode (NORMAL);
    texture (pg_visual_activation_platform);
  
    beginShape();
      vertex (-w/2, -h/2, 0, 0, 0);
      vertex ( w/2, -h/2, 0, 1, 0);
      vertex ( w/2,  h/2, 0, 1, 1);
      vertex (-w/2,  h/2, 0, 0, 1);
    endShape (CLOSE);
  pop();

  angleMode (RADIANS);
}


