


let pg_visual_error_ramp_left_inner;



function visualErrorRampLeftInner (x, y, z, w, h, d, hole_d) 
{
  const stroke_thickness = 1;
  const stroke_c = color (100);
  const fill_c = color (100);

  if (pg_visual_error_ramp_left_inner === undefined || 
      pg_visual_error_ramp_left_inner === 0) 
  {
    pg_visual_error_ramp_left_inner = createGraphics (w, h, P2D);
  }

  pg_visual_error_ramp_left_inner.clear();

  pg_visual_error_ramp_left_inner.strokeWeight (stroke_thickness);
  pg_visual_error_ramp_left_inner.stroke (stroke_c);
  pg_visual_error_ramp_left_inner.fill (fill_c);

  pg_visual_error_ramp_left_inner.push();
    pg_visual_error_ramp_left_inner.translate (w/2, h/2);

    pg_visual_error_ramp_left_inner.beginShape();
      pg_visual_error_ramp_left_inner.vertex (-w/2+stroke_thickness/2, -h/2+stroke_thickness/2);
      pg_visual_error_ramp_left_inner.vertex ( w/2-stroke_thickness/2, -h/2+stroke_thickness/2);
      pg_visual_error_ramp_left_inner.vertex ( w/2-stroke_thickness/2,  h/2-stroke_thickness/2);
      pg_visual_error_ramp_left_inner.vertex (-w/2+stroke_thickness/2,  h/2-stroke_thickness/2);

      pg_visual_error_ramp_left_inner.beginContour();
      pg_visual_error_ramp_left_inner.vertex (-hole_d/2, 0);

      pg_visual_error_ramp_left_inner.bezierVertex 
      (
        -hole_d/2, 
         hole_d/3.6, 
        -hole_d/3.6, 
         hole_d/2, 
         0, 
         hole_d/2
      );

      pg_visual_error_ramp_left_inner.bezierVertex 
      (
        hole_d/3.6, 
        hole_d/2, 
        hole_d/2, 
        hole_d/3.6, 
        hole_d/2, 
        0
      );

      pg_visual_error_ramp_left_inner.bezierVertex 
      (
         hole_d/2, 
        -hole_d/3.6, 
         hole_d/3.6, 
        -hole_d/2, 
         0, 
        -hole_d/2
      );

      pg_visual_error_ramp_left_inner.bezierVertex 
      (
        -hole_d/3.6, 
        -hole_d/2, 
        -hole_d/2, 
        -hole_d/3.6, 
        -hole_d/2, 
         0
      );
      pg_visual_error_ramp_left_inner.endContour();

    pg_visual_error_ramp_left_inner.endShape (CLOSE);

  pg_visual_error_ramp_left_inner.pop();



  push();
    translate (x, y, z);
  
    angleMode (DEGREES);
    rotateX (0);
    rotateY (90);
    rotateZ (0);

    textureMode (NORMAL);
    texture (pg_visual_error_ramp_left_inner);

    /*
    beginShape();
      vertex (-w/2, -h/2, 0, 0, 0);
      vertex ( w/2, -h/2, 0, 1, 0);
      vertex ( w/2,  h/2, 0, 1, 1);
      vertex (-w/2,  h/2, 0, 0, 1);
    endShape (CLOSE);
    */
    
    beginShape();
      vertex (0, -h/2,  0, 0, 0);
      vertex (0,  h/2,  0, 1, 0);
      vertex (0,  h/2, -d, 1, 1);
      vertex (0, -h/2, -d, 0, 1);
    endShape (CLOSE);
  pop();

  angleMode (RADIANS);
}


