


let pg_visual_error_ramp_left_inner;


/*
function visualErrorRampLeftInner (x, y, z, w, h, d) 
{
  const stroke_thickness = 1;
  const stroke_c = color (100);
  const fill_c = color (100);

  push();
    translate (x, y, z);
  
    angleMode (DEGREES);
    rotateX (0);
    rotateY (90);
    rotateZ (0);

    strokeWeight (stroke_thickness);
    stroke (stroke_c);
    fill (fill_c);
    
    beginShape();
      vertex (0, -h/2,  0);
      vertex (0,  h/2,  0);
      vertex (0,  h/2, -d);
      vertex (0, -h/2, -d);
    endShape (CLOSE);
  pop();

  angleMode (RADIANS);
}
*/



function visualErrorRampRightInner (x, y, z, w, h, d) 
{
  const stroke_thickness = 1;
  const stroke_c = color (100);
  const fill_c = color (100);

  push();
    translate (x, y, z);
  
    angleMode (DEGREES);
    rotateX (0);
    rotateY (-65);
    rotateZ (0);

    strokeWeight (stroke_thickness);
    stroke (stroke_c);
    fill (fill_c);
    
    beginShape();
      vertex (0, -h/2,  0);
      vertex (0,  h/2,  0);
      vertex (0,  h/2, -d);
      vertex (0, -h/2, -d);
    endShape (CLOSE);
  pop();

  angleMode (RADIANS);
}
  

