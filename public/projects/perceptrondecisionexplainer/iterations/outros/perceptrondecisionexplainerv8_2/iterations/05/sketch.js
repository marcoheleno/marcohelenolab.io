


function setup() 
{
  setAttributes('antialias', true);
  createCanvas (windowWidth, windowHeight, WEBGL);
}



function reStartSys() 
{
  pg_visual_activation_platform.remove();
  pg_visual_activation_platform = 0;
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  reStartSys();
}



function draw() 
{
  background (200);

  

  //orbitControl();

  /*
  line (-width/2, -100, 0, width/2, -100, 0);
  line (-width/2,  100, 0, width/2,  100, 0);
  
  line (-width/2, 0, 0, width/2, 0, 0);
  line (0, -height/2, 0, 0, height/2, 0);
  */
  
  push();
    translate (0, 0, 0);
  
    angleMode (DEGREES);
    rotateX (70); //70
    rotateY (0);
    rotateZ (0);

  //visualErrorRampLeftInner (x, y, z, w, h, d, hole_d) 
    visualErrorRampLeftInner (0, 0, 0, 400, 350, 300, 60);

    //visualErrorRampRightInner (50, 0, -100, 400, 400, 300);

    /*
    const hole_d = 400/2.3;
    visualWeightBlades       (-75, -35, 98.5, hole_d/1.3, map(mouseX, 0, width, 0, 46));
    visualActivationPlatform (-75,   0,  100,        400, 400, hole_d);
    */
  pop();

  angleMode (RADIANS);


  strokeWeight (50);
  point (mouseX-width/2, mouseY-height/2);
  strokeWeight (1);
}


