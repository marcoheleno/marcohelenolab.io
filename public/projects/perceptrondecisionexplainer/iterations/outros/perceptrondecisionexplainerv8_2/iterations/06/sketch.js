


function setup() 
{
  createCanvas (windowWidth, windowHeight);
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}



function draw() 
{
  background (30);



  stroke (255);
  strokeWeight (1);
  line (width*0.5, height*0.5, width*0.5, height);

  const small = 100;
  const large = 200;
  line (width*0.20, height, width*0.5-small, height*0.75);
  line (width*0.80, height, width*0.5+small, height*0.75);



  stroke (255, 0, 0);
  strokeWeight (2);
  line (0, height-2, width*0.20, height-2);
  line (width/2, height-2, width*0.80, height-2);



  stroke (0, 255, 0);
  strokeWeight (2);
  line (width*0.20, height-2, width*0.5, height-2);
  line (width*0.80, height-2, width, height-2);



  push();
    translate (width*0.5, height*0.30);

    angleMode (DEGREES);
    rotate (35);

    stroke(255);
    strokeWeight(1);

    const w = width*0.22;
    const gap = map( mouseX, 0, width, small/2, large/2);

    line (-w*0.5, 0, -gap, 0);
    line (-gap, 0, -gap, 100);

    line ( w*0.5, 0, gap, 0);
    line ( gap, 0, gap, 100);
  pop();
}


