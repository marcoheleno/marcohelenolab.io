


let iteration_i;



function setup() 
{
  const canvas = createCanvas (windowWidth, windowHeight);
  canvas.parent ("vis_section");

  createColorsFromCSS();
  addScreenPositionFunction();
  re_StartSystem();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  re_StartSystem();
}



function re_StartSystem() 
{
  createDataSetWith (1);
  startPerceptronWith();
  runPerceptron();
  //printPerceptronIterationsTableLog();

  iteration_i = 0;

  addPhysicsWorld();
}



function draw() 
{
  clear();

  /*
  fill(255, 0, 0);
  ellipseMode (RADIUS);
  ellipse (width*0.5, height*0.3, data_set[0].min_radius);
  */

  drawPtronWeightAnalogy2PhysicsWorldWith 
  (
    getPerceptronIterationsRowLogAt(iteration_i).ptron_weight_0_value, 
    true, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error
  );

  drawPtronActivationAnalogy2PhysicsWorld();
  drawPtronTrainingAnalogy2PhysicsWorld();

  if ( getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error != 0) 
    iteration_i++;




  const small = 100;
  const large = 200;
  strokeWeight (1);
  line (width*0.20, height, width*0.5-small, height*0.75);
  line (width*0.80, height, width*0.5+small, height*0.75);



  stroke (255, 0, 0);
  strokeWeight (2);
  line (0, height-2, width*0.20, height-2);
  line (width/2, height-2, width*0.80, height-2);



  stroke (0, 255, 0);
  strokeWeight (2);
  line (width*0.20, height-2, width*0.5, height-2);
  line (width*0.80, height-2, width, height-2);
  
}


