


let world;
let ptron_weight_left_constraint_polygon, ptron_weight_right_constraint_polygon;
let divider_margin, divider_h;
let ptron_activation_constraint_polygon;
let ptron_decision_attractor;
let attractor_x, attractor_y, attractor_h;
let physics_world_in_anim;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect (0, -data_set[0].max_radius, width, height + data_set[0].max_radius*2) );

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line (0, height + data_set[0].max_radius, width, height + data_set[0].max_radius), 1);
  world.addForce (floorForce);
}






function drawPtronWeightAnalogy2PhysicsWorldWith (ptron_weight_0_value, ptron_checking_error, ptron_training_error) 
{
  const x = width*0.5;
  const y = height*0.30;
  const ptron_weight_w = width*0.22;

  //ptron_weight_openness
  const ptron_weight_o = map(ptron_weight_0_value, -1, 1, 0, data_set[0].max_radius*2);
  //const ptron_weight_o = map(mouseX, 0, width, 0, data_set[0].max_radius*2);

  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (45);

    const ptron_weight_left_margin  = screenPosition (-ptron_weight_w, 0);
    const ptron_weight_left_center  = screenPosition (-ptron_weight_o,     0);

    const ptron_weight_right_center = screenPosition ( 0, 0);
    //const ptron_weight_right_margin = screenPosition ( ptron_weight_w*0.5, 0);

    const ptron_weight_right_neck = screenPosition (0, ptron_weight_o*0.2);
  pop();



  if (ptron_weight_left_constraint_polygon  != undefined) world.removeConstraint (ptron_weight_left_constraint_polygon);
  if (ptron_weight_right_constraint_polygon != undefined) world.removeConstraint (ptron_weight_right_constraint_polygon);
  
  const ptron_weight_left_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_weight_left_margin.x, ptron_weight_left_margin.y), 
    new c2.Point (ptron_weight_left_center.x, ptron_weight_left_center.y)
  ]);

  const ptron_weight_right_polygon = new c2.Polygon 
  ([
    //new c2.Point (ptron_weight_right_margin.x, ptron_weight_right_margin.y), 
    new c2.Point (ptron_weight_right_center.x, ptron_weight_right_center.y), 
    new c2.Point (ptron_weight_right_neck.x, ptron_weight_right_neck.y)
  ]);

  ptron_weight_left_constraint_polygon  = new c2.PolygonConstraint (ptron_weight_left_polygon);
  world.addConstraint (ptron_weight_left_constraint_polygon);
  
  ptron_weight_right_constraint_polygon = new c2.PolygonConstraint (ptron_weight_right_polygon);
  world.addConstraint (ptron_weight_right_constraint_polygon);



  noFill();
  strokeWeight (1);

  if (ptron_checking_error === false) stroke (color_neutral);
  else 
  {
    if (ptron_training_error === 0) stroke (0, 255, 0);
    else stroke (255, 0, 0);
  }
  
  beginShape();
    vertex (ptron_weight_left_margin.x, ptron_weight_left_margin.y);
    vertex (ptron_weight_left_center.x, ptron_weight_left_center.y);
  endShape();

  beginShape();
    //vertex (ptron_weight_right_margin.x, ptron_weight_right_margin.y);
    vertex (ptron_weight_right_center.x, ptron_weight_right_center.y);
    vertex (ptron_weight_right_neck.x,   ptron_weight_right_neck.y);
  endShape();
}



function drawPtronActivationAnalogy2PhysicsWorld() 
{
  const x = width*0.5;
  const y = height*0.5;
  const h = height*0.5;

  const ptron_activation_upper  = {x: x, y: y};
  const ptron_activation_lower  = {x: x, y: y+h};
  
  if (ptron_activation_constraint_polygon != undefined) world.removeConstraint (ptron_activation_constraint_polygon);

  const ptron_activation_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_activation_upper.x, ptron_activation_upper.y), 
    new c2.Point (ptron_activation_lower.x, ptron_activation_lower.y)
  ]);

  ptron_activation_constraint_polygon = new c2.PolygonConstraint (ptron_activation_polygon);
  world.addConstraint (ptron_activation_constraint_polygon);



  stroke (color_neutral);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (ptron_activation_upper.x, ptron_activation_upper.y);
    vertex (ptron_activation_lower.x, ptron_activation_lower.y);
  endShape();
}



function drawPtronTrainingAnalogy2PhysicsWorld() 
{

}



function drawParticles2PhysicsWorldWith (ptron_checking_error, ptron_training_error, ptron_activation_result, ptron_weight_0_value) 
{
  world.update();

  ellipseMode (RADIUS);

  let num_particles_below_funnel_neck = 0;

  for (let i=0; i<world.particles.length; i++) 
  {
    noStroke();

    if (ptron_checking_error === false) fill (color_bright);
    else 
    {
      if (ptron_training_error === 0) fill (0, 255, 0);
      else fill (255, 0, 0);
    }
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);



    stroke (color_darker);
    strokeWeight (1.2);
    line 
    (
      world.particles[i].position.x, 
      world.particles[i].position.y - world.particles[i].radius/6, 
      world.particles[i].position.x, 
      world.particles[i].position.y + world.particles[i].radius/6
    );

    line 
    (
      world.particles[i].position.x - world.particles[i].radius/6, 
      world.particles[i].position.y, 
      world.particles[i].position.x + world.particles[i].radius/6, 
      world.particles[i].position.y
    );

    

    if (ptron_checking_error === false) stroke (color_bright);
    else 
    {
      if (ptron_training_error === 0) stroke (0, 255, 0);
      else stroke (255, 0, 0);
    }
    strokeWeight (4);
    strokeCap(SQUARE);
    point (world.particles[i].position.x, world.particles[i].position.y);
    strokeCap(ROUND);



    if (world.particles[i].position.y > height - divider_margin - divider_h) 
    {
      num_particles_below_funnel_neck++;
      physics_world_in_anim = false;
    }

    if (world.particles[i].position.x >= attractor_x - 5 &&
        world.particles[i].position.x <= attractor_x + 5) 
    {
      removeAttractor2PhysicsWorld();
    }
  }

  if (world.particles.length === num_particles_below_funnel_neck &&
      world.particles.length < data_set.length) 
  {
    addParticle2PhysicsWorld (ptron_activation_result, ptron_weight_0_value);
  }
}



function addParticle2PhysicsWorld (ptron_activation_result, ptron_weight_0_value) 
{
  physics_world_in_anim = true;

  const temp_particle  = new c2.Particle (width/2, -data_set[0].max_radius);
  temp_particle.radius = data_set[world.particles.length].inputs[0];
  world.addParticle (temp_particle);

  addAttractor2PhysicsWorldWith (ptron_activation_result, ptron_weight_0_value);
}



function removeParticle2PhysicsWorld() 
{
  world.removeParticle (world.particles[0]);
}






function addAttractor2PhysicsWorldWith (ptron_activation_result, ptron_weight_0_value) 
{
  const outer_bound_left  = width/2 - (data_set[0].radius)*1;
  const inner_bound_left  = width/2 - (data_set[0].radius)/2;

  const inner_bound_right = width/2 + (data_set[0].radius)/2;
  const outer_bound_right = width/2 + (data_set[0].radius)*1;

  attractor_x = 0;
  if (ptron_activation_result < 0) 
    attractor_x = constrain( map(ptron_weight_0_value, -1, 0, outer_bound_left,  inner_bound_left),  outer_bound_left,  inner_bound_left);
  else 
    attractor_x = constrain( map(ptron_weight_0_value,  0, 1, inner_bound_right, outer_bound_right), inner_bound_right, outer_bound_right);
  
  attractor_y = height - divider_margin - divider_h;
  attractor_h = divider_h;

  ptron_decision_attractor = new c2.LineField (new c2.Line (attractor_x, attractor_y, attractor_x, attractor_y+attractor_h), 0.9);
  world.addForce (ptron_decision_attractor);
}



function removeAttractor2PhysicsWorld() 
{
  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = undefined;
  }
}



function seePerceptronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    stroke (255, 0, 255);
    strokeWeight (1);
    line (attractor_x, attractor_y, attractor_x, attractor_y+attractor_h);
  }
}


