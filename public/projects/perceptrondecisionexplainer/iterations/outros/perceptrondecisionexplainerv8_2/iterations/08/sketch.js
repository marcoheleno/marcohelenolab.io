


let iteration_i;



function setup() 
{
  const canvas = createCanvas (windowWidth, windowHeight);
  canvas.parent ("vis_section");

  createColorsFromCSS();
  addScreenPositionFunction();
  re_StartSystem();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  re_StartSystem();
}



function re_StartSystem() 
{
  createDataSetWith (1);
  startPerceptronWith();
  runPerceptron();
  //printPerceptronIterationsTableLog();

  iteration_i = 0;

  addPhysicsWorld();
}



function draw() 
{
  clear();

  //console.log (getPerceptronIterationsRowLogAt(iteration_i).ptron_sum_weighted_inputs_result);

  drawPtronWeightAnalogy2PhysicsWorldWith 
  (
    getPerceptronIterationsRowLogAt(iteration_i).ptron_sum_weighted_inputs_result, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_activation_result, 
    true, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error
  );

  drawPtronActivationAnalogy2PhysicsWorld();
  drawPtronTrainingAnalogy2PhysicsWorld();

  drawParticles2PhysicsWorldWith 
  (
    true, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_activation_result
  );

  seePerceptronDecisionAttractor();

  
  if (physics_world_in_anim === false) 
  {
    if ( getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error != 0) 
      iteration_i++;
  }
}


