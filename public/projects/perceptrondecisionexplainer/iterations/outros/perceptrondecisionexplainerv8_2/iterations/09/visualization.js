


let world;
let ptron_weight_left_constraint_polygon, ptron_weight_right_constraint_polygon;
let ptron_error_slope_left_constraint_polygon, ptron_error_slope_right_constraint_polygon;
let ptron_data_separator_left_constraint_polygon, ptron_data_separator_right_constraint_polygon;
let ptron_activation_constraint_polygon;
let ptron_decision_wall_attractor, ptron_decision_floor_attractor;
let ptron_decision_floor_attractor_x, ptron_decision_floor_attractor_y, attractor_h;
let physics_world_in_anim;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect (0, -data_set[0].max_radius, width, height + data_set[0].max_radius*3) );

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line (0, height + data_set[0].max_radius, width, height + data_set[0].max_radius), 1);
  world.addForce (floorForce);

  if (physics_world_in_anim === undefined) 
    physics_world_in_anim = false;
}






function drawPtronWeightAnalogy2PhysicsWorldWith (ptron_weight_0_value, activation, ptron_checking_error, ptron_training_error) 
{
  const x = width*0.5;
  const y = height*0.30;
  const ptron_weight_w = data_set[0].max_radius*4;



  
  //ptron_weight_openness
  //const ptron_weight_o = map( constrain(ptron_weight_0_value, -1, 0), -1, 0, data_set[0].max_radius*4, 0);
  //const ptron_weight_o = map(mouseX-width/2, -width/2, width/2, 0, ptron_weight_w);
  //console.log (mouseX-width/2);

  // !!! console.log ((ptron_weight_0_value*2 + data_set[0].threshold_for_ptron_training*2) );
  // !!! const ptron_weight_o = ptron_weight_w - (ptron_weight_0_value*2 + data_set[0].threshold_for_ptron_training*2) + data_set[0].radius/2.5;//(ptron_weight_0_value*2 + data_set[0].threshold_for_ptron_training*2 - 30)*-1;
  
  /*
  const ptron_weight_o = map 
  (
    ptron_weight_0_value, 
    -data_set[0].max_radius, 
    0, 
    data_set[0].max_radius*3, 
    data_set[0].max_radius*2-6
  );
  */

  const ptron_weight_o = data_set[0].threshold_for_ptron_training*2 - ptron_weight_0_value*2 -(data_set[0].radius/3)*activation - 10;
  //console.log (-activation * -3);

  //console.log (ptron_weight_o, data_set[0].max_radius*2);

  

  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (45);
    
    const ptron_weight_left_margin  = screenPosition (-ptron_weight_w, 0);
    const ptron_weight_left_center  = screenPosition (-ptron_weight_o, 0);

    const ptron_weight_right_center = screenPosition (0, 0);
    const ptron_weight_right_neck   = screenPosition (0, 100);
  pop();



  if (ptron_weight_left_constraint_polygon  != undefined) world.removeConstraint (ptron_weight_left_constraint_polygon);
  if (ptron_weight_right_constraint_polygon != undefined) world.removeConstraint (ptron_weight_right_constraint_polygon);
  
  const ptron_weight_left_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_weight_left_margin.x, ptron_weight_left_margin.y), 
    new c2.Point (ptron_weight_left_center.x, ptron_weight_left_center.y)
  ]);

  const ptron_weight_right_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_weight_right_center.x, ptron_weight_right_center.y), 
    new c2.Point (ptron_weight_right_neck.x,   ptron_weight_right_neck.y)
  ]);

  ptron_weight_left_constraint_polygon  = new c2.PolygonConstraint (ptron_weight_left_polygon);
  world.addConstraint (ptron_weight_left_constraint_polygon);
  
  ptron_weight_right_constraint_polygon = new c2.PolygonConstraint (ptron_weight_right_polygon);
  world.addConstraint (ptron_weight_right_constraint_polygon);



  noFill();
  strokeWeight (1);

  if (ptron_checking_error === false) stroke (color_bright);
  else 
  {
    if (ptron_training_error === 0) stroke (0, 255, 0);
    else stroke (255, 0, 0);
  }
  
  beginShape();
    vertex (ptron_weight_left_margin.x, ptron_weight_left_margin.y);
    vertex (ptron_weight_left_center.x, ptron_weight_left_center.y);
  endShape();

  beginShape();
    vertex (ptron_weight_right_center.x, ptron_weight_right_center.y);
    vertex (ptron_weight_right_neck.x,   ptron_weight_right_neck.y);
  endShape();
}



function drawPtronActivationAnalogy2PhysicsWorld() 
{
  const x = width*0.5;
  const y = height*0.5;
  const h = height*0.5;

  const ptron_activation_upper = {x: x, y: y};
  const ptron_activation_lower = {x: x, y: y+h};
  
  if (ptron_activation_constraint_polygon != undefined) world.removeConstraint (ptron_activation_constraint_polygon);

  const ptron_activation_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_activation_upper.x, ptron_activation_upper.y), 
    new c2.Point (ptron_activation_lower.x, ptron_activation_lower.y)
  ]);

  ptron_activation_constraint_polygon = new c2.PolygonConstraint (ptron_activation_polygon);
  world.addConstraint (ptron_activation_constraint_polygon);



  stroke (color_bright);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (ptron_activation_upper.x, ptron_activation_upper.y);
    vertex (ptron_activation_lower.x, ptron_activation_lower.y);
  endShape();
}



function drawPtronTrainingAnalogy2PhysicsWorld() 
{
  const x = width*0.5;
  const y = height*0.70;

  const ptron_error_slope_w = width*0.05;

  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (-45);

    const ptron_error_slope_left_center = screenPosition (                   0, 0);
    const ptron_error_slope_left_margin = screenPosition (-ptron_error_slope_w, 0);

    const ptron_data_separator_left_upper_y = screenPosition (-ptron_error_slope_w + -data_set[0].min_radius*2, 0);
  pop();

  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (45);

    const ptron_error_slope_right_center = screenPosition (                   0, 0);
    const ptron_error_slope_right_margin = screenPosition ( ptron_error_slope_w, 0);

    const ptron_data_separator_right_upper_y = screenPosition ( ptron_error_slope_w + data_set[0].min_radius*2, 0);
  pop();

  if (ptron_error_slope_left_constraint_polygon  != undefined) world.removeConstraint (ptron_error_slope_left_constraint_polygon);
  if (ptron_error_slope_right_constraint_polygon != undefined) world.removeConstraint (ptron_error_slope_right_constraint_polygon);
  
  const ptron_error_slope_left_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_error_slope_left_center.x, ptron_error_slope_left_center.y), 
    new c2.Point (ptron_error_slope_left_margin.x, ptron_error_slope_left_margin.y)
  ]);

  const ptron_error_slope_right_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_error_slope_right_center.x, ptron_error_slope_right_center.y), 
    new c2.Point (ptron_error_slope_right_margin.x,   ptron_error_slope_right_margin.y)
  ]);

  ptron_error_slope_left_constraint_polygon  = new c2.PolygonConstraint (ptron_error_slope_left_polygon);
  world.addConstraint (ptron_error_slope_left_constraint_polygon);
  
  ptron_error_slope_right_constraint_polygon = new c2.PolygonConstraint (ptron_error_slope_right_polygon);
  world.addConstraint (ptron_error_slope_right_constraint_polygon);

  stroke (color_bright);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (ptron_error_slope_left_center.x, ptron_error_slope_left_center.y);
    vertex (ptron_error_slope_left_margin.x, ptron_error_slope_left_margin.y);
  endShape();

  beginShape();
    vertex (ptron_error_slope_right_center.x, ptron_error_slope_right_center.y);
    vertex (ptron_error_slope_right_margin.x, ptron_error_slope_right_margin.y);
  endShape();


  
  const ptron_data_separator_left_upper = {x: ptron_error_slope_left_margin.x + -data_set[0].min_radius*2, y: ptron_data_separator_left_upper_y.y};
  const ptron_data_separator_left_lower = {x: ptron_error_slope_left_margin.x + -data_set[0].min_radius*2, y: height};
  
  if (ptron_data_separator_left_constraint_polygon != undefined) world.removeConstraint (ptron_data_separator_left_constraint_polygon);
  
  const ptron_data_separator_left_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_data_separator_left_upper.x, ptron_data_separator_left_upper.y), 
    new c2.Point (ptron_data_separator_left_lower.x, ptron_data_separator_left_lower.y)
  ]);

  ptron_data_separator_left_constraint_polygon = new c2.PolygonConstraint (ptron_data_separator_left_polygon);
  world.addConstraint (ptron_data_separator_left_constraint_polygon);

  stroke (color_bright);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (ptron_data_separator_left_upper.x, ptron_data_separator_left_upper.y);
    vertex (ptron_data_separator_left_lower.x, ptron_data_separator_left_lower.y);
  endShape();



  
  const ptron_data_separator_right_upper = {x: ptron_error_slope_right_margin.x + data_set[0].min_radius*2, y: ptron_data_separator_right_upper_y.y};
  const ptron_data_separator_right_lower = {x: ptron_error_slope_right_margin.x + data_set[0].min_radius*2, y: height};
  
  if (ptron_data_separator_right_constraint_polygon != undefined) world.removeConstraint (ptron_data_separator_right_constraint_polygon);
  
  const ptron_data_separator_right_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_data_separator_right_upper.x, ptron_data_separator_right_upper.y), 
    new c2.Point (ptron_data_separator_right_lower.x, ptron_data_separator_right_lower.y)
  ]);

  ptron_data_separator_right_constraint_polygon = new c2.PolygonConstraint (ptron_data_separator_right_polygon);
  world.addConstraint (ptron_data_separator_right_constraint_polygon);

  stroke (color_bright);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (ptron_data_separator_right_upper.x, ptron_data_separator_right_upper.y);
    vertex (ptron_data_separator_right_lower.x, ptron_data_separator_right_lower.y);
  endShape();



  stroke (0, 255, 0);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (x, height-10);
    vertex (ptron_data_separator_left_lower.x, height-10);
  endShape();

  beginShape();
    vertex (ptron_data_separator_right_lower.x, height-10);
    vertex (width, height-10);
  endShape();



  stroke (255, 0, 0);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (0, height-10);
    vertex (ptron_data_separator_left_lower.x, height-10);
  endShape();

  beginShape();
    vertex (x, height-10);
    vertex (ptron_data_separator_right_lower.x, height-10);
  endShape();
}



function drawParticles2PhysicsWorldWith (ptron_checking_error, ptron_training_error, ptron_activation_result) 
{
  if (world.particles.length < data_set.length) 
  {
    addParticle2PhysicsWorld (ptron_activation_result);
  }

  world.update();

  

  for (let i=0; i<world.particles.length; i++) 
  {
    noStroke();

    if (ptron_checking_error === false) fill (color_bright);
    else 
    {
      if (ptron_training_error === 0) fill (0, 255, 0);
      else fill (255, 0, 0);
    }

    ellipseMode (RADIUS);
    ellipse (world.particles[i].position.x, world.particles[i].position.y, world.particles[i].radius);
    ellipseMode (CORNER);

    if (world.particles[i].position.y > height/2) 
    {
      physics_world_in_anim = false;
      removeParticle2PhysicsWorld();
      removeAttractor2PhysicsWorld();
    }
  }
}



function addParticle2PhysicsWorld (ptron_activation_result) 
{
  physics_world_in_anim = true;

  const temp_particle  = new c2.Particle (width/2 - data_set[0].min_radius*2, -data_set[0].max_radius);
  temp_particle.radius = data_set[world.particles.length].inputs[0];
  world.addParticle (temp_particle);

  addAttractor2PhysicsWorldWith (ptron_activation_result);
}



function removeParticle2PhysicsWorld() 
{
  world.removeParticle (world.particles[0]);
}






function addAttractor2PhysicsWorldWith (ptron_activation_result) 
{
  ptron_decision_wall_attractor_x = width*0.5;
  ptron_decision_wall_attractor_y = height*0.9;
  ptron_decision_wall_attractor_h = height*0.1;

  ptron_decision_floor_attractor_x = width*0.5;
  ptron_decision_floor_attractor_y = height-5;
  ptron_decision_floor_attractor_w = 0;


  
  if (ptron_activation_result < 0) 
  {
    ptron_decision_wall_attractor_x = 1;
    /*
    ptron_decision_floor_attractor_x =  width*0.5 - data_set[0].min_radius*2;
    ptron_decision_floor_attractor_w = (width*0.5 - data_set[0].min_radius*6) * -1;
    
    ptron_decision_floor_attractor_x =  width*0.5 - data_set[0].min_radius*2;
    ptron_decision_floor_attractor_w = (width*0.5 - data_set[0].min_radius*2) * -1;
    */
  }
  else 
  {
    ptron_decision_wall_attractor_x = width-1;
    /*
    ptron_decision_floor_attractor_x =  width*0.5 + data_set[0].min_radius*2;
    ptron_decision_floor_attractor_w = (width*0.5 - data_set[0].min_radius*6) *  1;
    
    ptron_decision_floor_attractor_x =  width*0.5 + data_set[0].min_radius*2;
    ptron_decision_floor_attractor_w = (width*0.5 - data_set[0].min_radius*2) *  1;
    */
  }

  ptron_decision_floor_attractor_x = width*0.5 - data_set[0].min_radius*2;
  ptron_decision_floor_attractor_w = data_set[0].min_radius*4;



  //if (data_set[0].radius > data_set[0].threshold_for_ptron_training) 
  {
    
    ptron_decision_wall_attractor = new c2.LineField (new c2.Line 
    (
      ptron_decision_wall_attractor_x, 
      ptron_decision_wall_attractor_y, 
      ptron_decision_wall_attractor_x, 
      ptron_decision_wall_attractor_y + ptron_decision_wall_attractor_h), 
      0.9
    );
    world.addForce (ptron_decision_wall_attractor);
  }


  
  ptron_decision_floor_attractor = new c2.LineField (new c2.Line 
  (
    ptron_decision_floor_attractor_x, 
    ptron_decision_floor_attractor_y, 
    ptron_decision_floor_attractor_x + ptron_decision_floor_attractor_w, 
    ptron_decision_floor_attractor_y), 
    0.1
  );
  world.addForce (ptron_decision_floor_attractor);
}



function removeAttractor2PhysicsWorld() 
{
  if (ptron_decision_wall_attractor != undefined) 
  {
    world.removeForce (ptron_decision_wall_attractor);
    ptron_decision_wall_attractor = undefined;
  }

  if (ptron_decision_floor_attractor != undefined) 
  {
    world.removeForce (ptron_decision_floor_attractor);
    ptron_decision_floor_attractor = undefined;
  }
}



function seePerceptronDecisionAttractor() 
{
  if (ptron_decision_wall_attractor != undefined) 
  {
    stroke (255, 255, 0);
    strokeWeight (1);
    line 
    (
      ptron_decision_wall_attractor_x, 
      ptron_decision_wall_attractor_y, 
      ptron_decision_wall_attractor_x, 
      ptron_decision_wall_attractor_y + ptron_decision_wall_attractor_h
    );
  }

  if (ptron_decision_floor_attractor != undefined) 
  {
    stroke (255, 255, 0);
    strokeWeight (1);
    line 
    (
      ptron_decision_floor_attractor_x, 
      ptron_decision_floor_attractor_y, 
      ptron_decision_floor_attractor_x + ptron_decision_floor_attractor_w, 
      ptron_decision_floor_attractor_y
    )
  }
}


