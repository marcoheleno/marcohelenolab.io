


let iteration_i;



function setup() 
{
  const canvas = createCanvas (windowWidth, windowHeight);
  canvas.parent ("vis_section");

  createColorsFromCSS();
  addScreenPositionFunction();
  re_StartSystem();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  re_StartSystem();
}



function re_StartSystem() 
{
  createDataSetWith (1);

  startPerceptron();
  runPerceptron();
  //printPerceptronIterationsTableLog();

  iteration_i = 0;

  addPhysicsWorld();
}



function draw() 
{
  clear();

  drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
  (
    getPerceptronIterationsRowLogAt(iteration_i).ptron_sum_weighted_inputs_result, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_activation_result, 
    true, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error
  );

  drawPtronActivationAnalogy2PhysicsWorld();

  drawParticles2PhysicsWorldWith 
  (
    getPerceptronIterationsRowLogAt(iteration_i).ptron_activation_result, 
    true, 
    getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error
  );

  showPerceptronDecisionAttractor();

  
  if (is_physics_world_in_anim === false) 
  {
    if (getPerceptronIterationsRowLogAt(iteration_i).ptron_training_error != 0) 
      iteration_i++;
  }
}


