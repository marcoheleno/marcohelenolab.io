


let world;
let ptron_sum_weighted_inputs_left_constraint_polygon, ptron_sum_weighted_inputs_right_constraint_polygon;
let ptron_activation_constraint_polygon;
let ptron_decision_attractor_x, ptron_decision_attractor_y, ptron_decision_attractor_h;





function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect (0, -data_set[0].max_radius, width, height + data_set[0].max_radius*3) );

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line 
  (
    0, 
    height + data_set[0].max_radius*2, 
    width, 
    height + data_set[0].max_radius*2), 
    1
  );
  world.addForce (floorForce);
}





function drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
(
  ptron_sum_weighted_inputs_value, 
  ptron_activation_value, 
  is_ptron_checking_error, 
  ptron_training_error_value
) 
{
  const x = width*0.5;
  const y = height*0.30;
  const ptron_sum_weighted_inputs_w = data_set[0].max_radius*4;

  // ptron_sum_weighted_inputs_openness
  const ptron_sum_weighted_inputs_o = data_set[0].threshold_for_ptron_training*2 - ptron_sum_weighted_inputs_value*2 -(data_set[0].radius/3)*ptron_activation_value - 5;

  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (45);
    
    const ptron_sum_weighted_inputs_left_margin  = screenPosition (-ptron_sum_weighted_inputs_w, 0);
    const ptron_sum_weighted_inputs_left_center  = screenPosition (-ptron_sum_weighted_inputs_o, 0);

    const ptron_sum_weighted_inputs_right_center = screenPosition (0, 0);
    const ptron_sum_weighted_inputs_right_neck   = screenPosition (0, ptron_sum_weighted_inputs_o*0.5);
  pop();



  if (ptron_sum_weighted_inputs_left_constraint_polygon  != undefined) 
    world.removeConstraint (ptron_sum_weighted_inputs_left_constraint_polygon);

  if (ptron_sum_weighted_inputs_right_constraint_polygon != undefined) 
    world.removeConstraint (ptron_sum_weighted_inputs_right_constraint_polygon);
  
  const ptron_sum_weighted_inputs_left_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y), 
    new c2.Point (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y)
  ]);

  const ptron_sum_weighted_inputs_right_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_sum_weighted_inputs_right_center.x, ptron_sum_weighted_inputs_right_center.y), 
    new c2.Point (ptron_sum_weighted_inputs_right_neck.x,   ptron_sum_weighted_inputs_right_neck.y)
  ]);

  ptron_sum_weighted_inputs_left_constraint_polygon  = new c2.PolygonConstraint (ptron_sum_weighted_inputs_left_polygon);
  world.addConstraint (ptron_sum_weighted_inputs_left_constraint_polygon);
  
  ptron_sum_weighted_inputs_right_constraint_polygon = new c2.PolygonConstraint (ptron_sum_weighted_inputs_right_polygon);
  world.addConstraint (ptron_sum_weighted_inputs_right_constraint_polygon);



  noFill();
  strokeWeight (1);

  if (is_ptron_checking_error === false) stroke (color_level_2);
  else 
  {
    if (ptron_training_error_value === 0) stroke (color_highlight_pos);
    else stroke (color_highlight_neg);
  }
  
  beginShape();
    vertex (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y);
    vertex (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y);
  endShape();

  beginShape();
    vertex (ptron_sum_weighted_inputs_right_center.x, ptron_sum_weighted_inputs_right_center.y);
    vertex (ptron_sum_weighted_inputs_right_neck.x,   ptron_sum_weighted_inputs_right_neck.y);
  endShape();
}





function drawPtronActivationAnalogy2PhysicsWorld() 
{
  const x = width*0.5;
  const y = height*0.5;
  const h = height*0.25;

  const ptron_activation_upper = {x:x, y:y};
  const ptron_activation_lower = {x:x, y:y+h};
  
  if (ptron_activation_constraint_polygon != undefined) 
    world.removeConstraint (ptron_activation_constraint_polygon);

  const ptron_activation_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_activation_upper.x, ptron_activation_upper.y), 
    new c2.Point (ptron_activation_lower.x, ptron_activation_lower.y)
  ]);

  ptron_activation_constraint_polygon = new c2.PolygonConstraint (ptron_activation_polygon);
  world.addConstraint (ptron_activation_constraint_polygon);



  stroke (color_level_2);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (ptron_activation_upper.x, ptron_activation_upper.y);
    vertex (ptron_activation_lower.x, ptron_activation_lower.y);
  endShape();
}





function drawParticles2PhysicsWorldWith 
(
  data_id, 
  ptron_activation_value, 
  is_ptron_checking_error, 
  ptron_training_error_value
) 
{
  if (world.particles.length < 1) 
    addParticle2PhysicsWorld (data_id, ptron_activation_value);

  world.update();

  

  for (let i=0; i<world.particles.length; i++) 
  {
    noStroke();

    if (is_ptron_checking_error === false)  fill (color_level_2);
    else 
    {
      if (ptron_training_error_value === 0) fill (color_highlight_pos);
      else fill (color_highlight_neg);
    }

    ellipseMode (RADIUS);
    ellipse 
    (
      world.particles[i].position.x, 
      world.particles[i].position.y, 
      world.particles[i].radius
    );
    ellipseMode (CORNER);

    if (world.particles[i].position.y > height*0.5) 
    {
      removeAttractor2PhysicsWorld();
    }

    if (world.particles[i].position.y > height) 
    {
      removeParticle2PhysicsWorld();
    }
  }
}



function addParticle2PhysicsWorld (data_id, ptron_activation_value) 
{
  const temp_particle  = new c2.Particle 
  (
    width*0.5 - data_set[0].radius*2, 
    -data_set[0].radius
  );

  temp_particle.radius = data_set[data_id].inputs[0];

  world.addParticle (temp_particle);

  addAttractor2PhysicsWorldWith (ptron_activation_value);
}



function removeParticle2PhysicsWorld() 
{
  world.removeParticle (world.particles[0]);
}






function addAttractor2PhysicsWorldWith (ptron_activation_value) 
{
  ptron_decision_attractor_x = width*0.5;

  if (ptron_activation_value < 0) 
  {
    ptron_decision_attractor_x = 10;
  }
  else 
  {
    ptron_decision_attractor_x = width-10;
  }

  ptron_decision_attractor_y = height*0.9;
  ptron_decision_attractor_h = height*0.1;
  
  ptron_decision_attractor = new c2.LineField (new c2.Line 
  (
    ptron_decision_attractor_x, 
    ptron_decision_attractor_y, 
    ptron_decision_attractor_x, 
    ptron_decision_attractor_y + ptron_decision_attractor_h), 
    0.9
  );
  world.addForce (ptron_decision_attractor);
}



function removeAttractor2PhysicsWorld() 
{
  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = undefined;
  }
}



function showPerceptronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    stroke (255, 255, 0);
    strokeWeight (1);
    noFill();

    line 
    (
      ptron_decision_attractor_x, 
      ptron_decision_attractor_y, 
      ptron_decision_attractor_x, 
      ptron_decision_attractor_y + ptron_decision_attractor_h
    );
  }
}


