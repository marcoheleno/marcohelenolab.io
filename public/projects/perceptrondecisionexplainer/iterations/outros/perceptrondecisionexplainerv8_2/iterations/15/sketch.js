


let ptron_log_iteration_i;
let vis_moment_i;



function setup() 
{
  const canvas = createCanvas (windowWidth, windowHeight);
  canvas.parent ("vis_section");

  createColorsFromCSS();
  addScreenPositionFunction();
  re_StartSystem();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  re_StartSystem();
}



function re_StartSystem() 
{
  createDataSetWith (2);
  startPerceptron();
  runPerceptron();

  ptron_log_iteration_i = 0;
  vis_moment_i = 0;

  addPhysicsWorld();
}



function draw() 
{
  clear();


  /*
  if (vis_moment_i === 0) 
  {
    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_sum_weighted_inputs_result, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      false, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );
    
    noStroke();
    fill (color_level_4);
    text ("O Ptron irá a classificar um dado/circulo,", 10, height/2 + 100 + 0);
    text ("tentando colocar o circulo, ", 10, height/2 + 100 + 15);
    text ("com base no seu diametro,", 10, height/2 + 100 + 30);
    text ("do lado esquerdo ou do lado direito.", 10, height/2 + 100 + 45);
    text ("Click para iniciar!", 10, height/2 + 100 + 75);
  }


  else 


  if (vis_moment_i === 1) 
  {
    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_sum_weighted_inputs_result, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      false, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );

    drawParticles2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).data_id, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      false, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );

    if (ptron_log_iteration_i === 0) 
    {
      for (let i=0; i<world.particles.length; i++) 
      {
        if (world.particles[i].position.y >= height*0.5 - 4 &&
            world.particles[i].position.y <= height*0.5 + 4) 
        {
          noLoop();
        }
      }
    }
    
    noStroke();
    fill (color_level_4);
    text ("Click novamente para que o Ptron", 10, height/2 + 100 + 0);
    text ("possa verificar se a classificação está correcta.", 10, height/2 + 100 + 15);
  }


  else 


  if (vis_moment_i === 2) 
  {
    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_sum_weighted_inputs_result, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      false, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );

    drawParticles2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).data_id, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      true, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );
    
    noStroke();
    fill (color_level_4);
    text ("O Ptron identificou que a classificação está incorrecta.", 10, height/2 + 100 + 0);
    text ("Click novamente para que o Ptron possa apreender, corrigindo a sua intrepetação.", 10, height/2 + 100 + 30);
  }


  else 


  if (vis_moment_i === 3) 
  {
    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_sum_weighted_inputs_result, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      true, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );

    drawParticles2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).data_id, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      false, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );
    
    noStroke();
    fill (color_level_4);
    text ("O Ptron corrigiu a sua intrepetação.", 10, height/2 + 100 + 0);
    text ("Click novamente para que o Ptron possa realizar uma nova classificação.", 10, height/2 + 100 + 30);
  }


  else 


  if (vis_moment_i === 4) 
  {
    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_sum_weighted_inputs_result, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      true, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );

    drawParticles2PhysicsWorldWith 
    (
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).data_id, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
      true, 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
    );
    
    noStroke();
    fill (color_level_4);
    text ("O Ptron identificou que a classificação está correcta,", 10, height/2 + 100 + 0);
    text ("logo não terá que corrigir a sua intrepetação.", 10, height/2 + 100 + 15);
    text ("O Ptron encontra-se treinado!", 10, height/2 + 100 + 45);
  }
  */



  //console.log (getPerceptronIterationsRowLogAt(ptron_log_iteration_i).data_id);
  console.log (ptron_log_iteration_i);

  drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
  (
    getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_sum_weighted_inputs_result, 
    getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
    true, 
    getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
  );

  drawParticles2PhysicsWorldWith 
  (
    getPerceptronIterationsRowLogAt(ptron_log_iteration_i).data_id, 
    getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_activation_result, 
    true, 
    getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error
  );

  drawPtronActivationAnalogy2PhysicsWorld();
  showPerceptronDecisionAttractor();

  if (world.particles.length === 0) 
  {
    if (ptron_log_iteration_i < ptron_iterations_log_table.getRowCount()-1) 
      ptron_log_iteration_i++;
    else 
      noLoop();
  }
}



function visMomentManager() 
{
  if (vis_moment_i < 3) 
  {
    if (vis_moment_i === 1 && ptron_log_iteration_i === 0) 
    {
      loop();
    }

    vis_moment_i++;
  }

  else 
  {
    if (getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error != 0) 
    {
      vis_moment_i = 1;
    }
  }

  if (vis_moment_i === 3 && 
      getPerceptronIterationsRowLogAt(ptron_log_iteration_i).ptron_training_error != 0) 
  {
    ptron_log_iteration_i++;
  }
}



function mouseReleased() 
{
  visMomentManager();
}


