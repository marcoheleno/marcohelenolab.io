


let color_level_1, color_level_2, color_level_3, color_level_4;
let color_highlight_neg, color_highlight_pos;



function createColorsFromCSS()
{
    // Defined in index.css
    color_level_1 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_1");
    color_level_2 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_2");
    color_level_3 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_3");
    color_level_4 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_4");
    color_highlight_neg = getComputedStyle(document.documentElement).getPropertyValue("--color_highlight_neg");
    color_highlight_pos = getComputedStyle(document.documentElement).getPropertyValue("--color_highlight_pos");

    color_level_1 = color( red(color_level_1), green(color_level_1), blue(color_level_1) );
    color_level_2 = color( red(color_level_2), green(color_level_2), blue(color_level_2) );
    color_level_3 = color( red(color_level_3), green(color_level_3), blue(color_level_3) );
    color_level_4 = color( red(color_level_4), green(color_level_4), blue(color_level_4) );
    color_highlight_neg = color( red(color_highlight_neg), green(color_highlight_neg), blue(color_highlight_neg) );
    color_highlight_pos = color( red(color_highlight_pos), green(color_highlight_pos), blue(color_highlight_pos) );
}


