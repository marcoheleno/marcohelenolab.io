

let startSys, drawTime, separateLayersTime, interfaceDrawn;
let millisBetweenDrawLayersAndSeparateLayers;


function setup() 
{
  clear();
  setAttributes("antialias", true);
  createCanvas (windowWidth, windowHeight);
  startSys = true;
}


function draw() 
{
  //console.log (frameRate());

  
  if (startSys) 
  {
    startSys = false;
    interfaceDrawn = true;
    interfaceDrawn = false;

    preferences();
    background (colorLayer1);
    createStatusPanels();

    runPerceptron();
    createPerceptronVizIterations();
  }


  push();
    translate (width/2, height/2);
    
      stroke (colorLayer2);
      strokeWeight (1);
      fill(0, 0, 0, 0);
      
      beginShape();
        vertex (-perceptronIterationPanelWidth/2, -perceptronIterationPanelHeight/2, 0);
        vertex (                               0, -perceptronIterationPanelHeight/2, 0);
        vertex (                               0,  perceptronIterationPanelHeight/2, 0);
        vertex (-perceptronIterationPanelWidth/2,  perceptronIterationPanelHeight/2, 0);
      endShape (CLOSE);

      beginShape();
        vertex (                               0, -perceptronIterationPanelHeight/2, 0);
        vertex ( perceptronIterationPanelWidth/2, -perceptronIterationPanelHeight/2, 0);
        vertex ( perceptronIterationPanelWidth/2,  perceptronIterationPanelHeight/2, 0);
        vertex (                               0,  perceptronIterationPanelHeight/2, 0);
      endShape (CLOSE);
  pop();


  drawPerceptronVizIterations();

  if (!interfaceDrawn) 
  {
    updateTopStatusPanel ('<a href="javascript:window.location.reload()">CLICK TO RUN THE SYSTEM AGAIN</a>');

    interfaceDrawn = true;

    noLoop();
  }
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  startSys = true;
}
