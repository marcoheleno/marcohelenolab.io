

class PerceptronIterationViz 
{

  constructor (ptronIterationVals, ptronIterationIndex, ptronNumIterations, vizPanelWidth, vizPanelHeight) 
  {
    this.feedForwardCalculation = ptronIterationVals.feedForwardCalculation;
    //this.transformedWeights = ptronIterationVals.transformedWeights;
    //this.activationResult = ptronIterationVals.activationResult;
    this.trainingError = ptronIterationVals.trainingError;

    this.ptronIterationIndex = ptronIterationIndex;
    this.ptronNumIterations = ptronNumIterations;
    
    this.vizPanelWidth  = vizPanelWidth;
    this.vizPanelHeight = vizPanelHeight;
  }

  

  drawPerceptronIterationViz()
  {
    this.x = width/2 + this.feedForwardCalculation;
    this.y = height/2;
    
    if (this.trainingError != 0) stroke (map(this.ptronIterationIndex, 0, this.ptronNumIterations, 250, 50));
    else stroke (colorLayer4);

    strokeWeight (5);
    point (this.x, this.y);
  }
  
}
