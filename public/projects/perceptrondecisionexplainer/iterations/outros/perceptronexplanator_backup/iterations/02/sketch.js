

let p5CanvasContainerWidth, p5CanvasContainerHeight;
let colorLayer1, colorLayer2, colorLayer3, colorLayer4;
let startSys;


function setup() 
{
  p5CanvasContainerWidth  = p5CanvasContainer.offsetWidth;
  p5CanvasContainerHeight = p5CanvasContainer.offsetHeight;

  const canvas = createCanvas (p5CanvasContainerWidth, p5CanvasContainerHeight);
  canvas.parent("p5CanvasContainer");

  colorLayer1 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer1'));
  colorLayer2 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer2'));
  colorLayer3 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer3'));
  colorLayer4 = color (getComputedStyle(document.documentElement).getPropertyValue('--colorLayer4'));

  startSys = true;
  
  const inputContainer = document.getElementsByTagName("div")[1];
  inputContainer.getElementsByTagName("input")[0].setAttribute("min", "0");
  inputContainer.getElementsByTagName("input")[0].setAttribute("max", "100");
  inputContainer.getElementsByTagName("input")[0].setAttribute("step", "10");
  inputContainer.getElementsByTagName("input")[0].setAttribute("value", "50");
  inputContainer.getElementsByTagName("input")[0].style.transform = "rotate(90deg)";
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;
    runPerceptron();
    createPerceptronVizIterations();
  }


  push();
    translate (width/2, height/2);
    
      stroke (colorLayer2);
      strokeWeight (1);
      fill(0, 0, 0, 0);
      
      beginShape();
        vertex (-width/2, -height/2, 0);
        vertex (       0, -height/2, 0);
        vertex (       0,  height/2, 0);
        vertex (-width/2,  height/2, 0);
      endShape (CLOSE);

      beginShape();
        vertex (       0, -height/2, 0);
        vertex ( width/2, -height/2, 0);
        vertex ( width/2,  height/2, 0);
        vertex (       0,  height/2, 0);
      endShape (CLOSE);
  pop();


  drawPerceptronVizIterations();
}

function windowResized() 
{
  p5CanvasContainerWidth  = p5CanvasContainer.offsetWidth;
  p5CanvasContainerHeight = p5CanvasContainer.offsetHeight;

  resizeCanvas (p5CanvasContainerWidth, p5CanvasContainerHeight);

  startSys = true;
}
