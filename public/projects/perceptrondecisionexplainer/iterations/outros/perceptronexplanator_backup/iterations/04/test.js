
const vw3 = Math.max(document.documentElement.clientWidth  || 0, window.innerWidth  || 0);
const vh3 = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

document.body.style.width = vw3;
document.body.style.height = vh3;