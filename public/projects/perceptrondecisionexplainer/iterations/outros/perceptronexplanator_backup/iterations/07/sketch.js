

let colorBackground, colorBackgroundMiddle, colorMiddle, colorForegroundMiddle, colorForeground;
let loadSystemEnabled;


function p5_containerSize() 
{
  let containerDimensions = 
  {
    w: p5_container.offsetWidth,
    h: p5_container.offsetHeight
  };
  
  return containerDimensions;
}


function setup() 
{
  const canvas = createCanvas (p5_containerSize().w, p5_containerSize().h);
  canvas.parent("p5_container");

  colorBackground       = color (getComputedStyle(document.documentElement).getPropertyValue("--colorBackground"));
  colorBackgroundMiddle = color (getComputedStyle(document.documentElement).getPropertyValue("--colorBackgroundMiddle"));
  colorMiddle           = color (getComputedStyle(document.documentElement).getPropertyValue("--colorMiddle"));
  colorForegroundMiddle = color (getComputedStyle(document.documentElement).getPropertyValue("--colorForegroundMiddle"));
  colorForeground       = color (getComputedStyle(document.documentElement).getPropertyValue("--colorForeground"));

  loadSystem();
}


function loadSystem() 
{
  loadSystemEnabled = true;
}


function draw() 
{
  if (loadSystemEnabled) 
  {
    loadSystemEnabled = false;

    runAndGetPerceptronVizCalculations();
    updateDOMinputRange();
    createPerceptronVizIterations();
  }

  background (colorBackground);
  drawLayout();
  drawPerceptronVizIterations (document.getElementsByTagName("input")[0].value);
}


function drawLayout() 
{
  push();
    translate (width/2, height/2);
    
      strokeWeight (3);
      stroke (colorBackgroundMiddle);
      noFill();
       
      beginShape();
        vertex (-width/2, -height/2);
        vertex (       0, -height/2);
        vertex (       0,  height/2);
        vertex (-width/2,  height/2);
      endShape (CLOSE);
      
      beginShape();
        vertex (       0, -height/2);
        vertex ( width/2, -height/2);
        vertex ( width/2,  height/2);
        vertex (       0,  height/2);
      endShape (CLOSE);



      strokeWeight (1);
      stroke (colorForeground);

      if (randomDataPoint.goal === -1) 
      {
        beginShape();
          vertex (-width/2, -height/2);
          vertex (       0, -height/2);
          vertex (       0,  height/2);
          vertex (-width/2,  height/2);
        endShape (CLOSE);
      }

      else if (randomDataPoint.goal === 1) 
      {
        beginShape();
          vertex (       0, -height/2);
          vertex ( width/2, -height/2);
          vertex ( width/2,  height/2);
          vertex (       0,  height/2);
        endShape (CLOSE);
      }
  pop();
}


function perceptronIterationVizGraphics (ptronX, ptronY, ptronTrainingError, ptronIterationIndex, ptronNumIterations)
{
  let c = colorForeground;

  if (ptronTrainingError != 0) 
  {
    c = lerpColor(colorMiddle, colorForeground, map(ptronIterationIndex, 0, ptronNumIterations, 0, 1));
  }
  
  stroke (c);
  strokeWeight (10);
  point (ptronX, ptronY);
}


function perceptronIterationVizLine (ptronX, ptronY, ptronTrainingError)
{
  if (ptronTrainingError != 0) 
  {
    strokeWeight (2);
    stroke (colorForeground);

    let dash = 3;
    let i = 0;

    if (ptronX < width/2) 
    {
      for (let x=ptronX; x<width/2; x+=dash)
      {
        if (i%2 === 0) line (x, ptronY, x+dash, ptronY);
        i++;
      }
    }
    else 
    {
      for (let x=ptronX; x>width/2; x-=dash)
      {
        if (i%2 === 0) line (x, ptronY, x-dash, ptronY);
        i++;
      }
    }

  }
}