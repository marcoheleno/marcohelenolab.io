

//
// In this function you are able to define
// the Perceptron's Activation Function
//
const ptronActivationFunction = function (sumWeightedInputs) 
{
  // The result is the sign of the sum, 
  // returns -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}





let ptronScript, ptronStoryboard;
let highestNumberOfIterations;


//
// This function runs the Perceptron (feedForward + train)
// with the selected data
// and saves all iterations calaculations.
// The name "... Script" is an analogy to an animation script
//
function createPtronScript() 
{
   // See data.js
  const dataPoints = getRandomData (1, -p5_containerSize.w/2, p5_containerSize.w/2, -1);
  
   // See transparentPerceptron.js
  const transparentPerceptronInstance = new TransparentPerceptron (dataPoints[0].inputs.length, ptronActivationFunction, 0.0005);


  // Creates a two-dimensional array.
  // For each dataPoint a collection of the Perceptron's calculations.
  // The second dimension's length is number of iterations 
  // the Perceptron took until trainingError of 0.
  // The name "... Script" is an analogy to an animation script.
  ptronScript = new Array (dataPoints.length);

  for (let i=0; i<ptronScript.length; i++) 
  {
    ptronScript[i] = new Array (0);
  }


  let i = 0;
  let numDataPointsPerceptronTrained  = 0;
  highestNumberOfIterations = 0;
  do 
  {
    // For each dataPoint...
    // Check if the Perceptron is not on it's first iteration and
    // if it has a trainingError of 0 on current (i) dataPoint
    if (ptronScript[i].length > 0 && 
        ptronScript[i][ptronScript[i].length-1].trainingError === 0) 
    {
      // Count the number of dataPoints that the Perceptron is trained on 
      numDataPointsPerceptronTrained++;
    }

    // For each dataPoint...
    // If the Perceptron on this iteration does not have a trainingError of 0
    // we FeedForward the inputs
    // and Train the Perceptron
    else
    {
      transparentPerceptronInstance.feedForward (dataPoints[i].inputs);
      transparentPerceptronInstance.train (dataPoints[i].goal);

      // We add to the second dimension of the array ptronScript
      // the Perceptron's calculations of this iteration
      ptronScript[i].push( transparentPerceptronInstance.getAllTransparentPerceptronCalculations() ); 
    }

    // Iterate through all the dataPoints
    if (i < ptronScript.length-1) i++;

    // If allready iterated through all the dataPoints
    else 
    {
      i = 0; // Restart iteration counter
      
      // If the Perceptron has not trained successfully on all dataPoints
      // also Restart the numDataPointsPerceptronTrained counter
      if (numDataPointsPerceptronTrained < ptronScript.length) numDataPointsPerceptronTrained = 0;
    }

    // Save the highest number of Iterations the Perceptron performed
    if (highestNumberOfIterations < ptronScript[i].length-1) 
    {
      highestNumberOfIterations = ptronScript[i].length-1;
    }

    // Repeat while the Perceptron has not successfully trained on all dataPoints
  }
  while (numDataPointsPerceptronTrained < dataPoints.length);

  // console.log(highestNumberOfIterations+1);
}





//
// This function draws the Perceptron's Storyboard for Visualization.
// It drawns on the previously created ptronScript two-dimensional array values.
// For each dataPoint it is drawPtronStoryboard with all the iterations (drawPtronVizBoardwithP5) 
// the Perceptron went through.
//
function drawPtronStoryboard (DOMinputRangeValue) 
{
  let verticalSpaceBetweenDataPoints = p5_containerSize.h / (ptronScript.length+1);
  let x, y;
  let s = false;
  let start, end;

  // c = column
  for (let c=0; c<ptronScript.length; c++) 
  {
    // l = line
    for (let l=0; l<ptronScript[c].length; l++) 
    {
      x = p5_containerSize.w/2 + ptronScript[c][l].feedForwardCalculation;
      y = verticalSpaceBetweenDataPoints * (c+1);
      e = ptronScript[c][l].trainingError;

      // Iteration selected by DOMinputRangeValue?
      if ( l === Math.trunc(DOMinputRangeValue) ) s = true; 
      else s = false;

      drawPtronVizBoardwithP5 (x, y, e, s);

      if (l < ptronScript[c].length-1) 
      {
        start = x;
        end = p5_containerSize.w/2 + ptronScript[c][l+1].feedForwardCalculation;
        drawPtronStoryboardRoutewithP5 (start, end, y);
      }

    }
  }

}


