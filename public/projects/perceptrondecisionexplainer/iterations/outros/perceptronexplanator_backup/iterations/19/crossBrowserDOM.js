

//
// Cross Browser/Device
// DOM functions
//


let p5_containerSize;
let colorBackground, colorBackgroundMiddle, colorMiddle, colorForegroundMiddle, colorForeground;


window.onload = function() 
{
    setBodyDOMsize2innerBrowserWindowSize();
    get_p5_containerSize();
    getColorsFromCSSFile();

    /*
    const dataset = getRandomData (1, -1, -p5_containerSize.w/2, p5_containerSize.w/2);
    const highestNumOfIterations = createPtronScript (dataset); // See data.js
    updateDOMinputRange (highestNumOfIterations);
    
    let DOMinputRangeValue = document.getElementsByTagName("input")[0].value;
    drawPtronStoryboard (dataset, DOMinputRangeValue);
    */
}


//
// Set Body Width & Height
// to Inner window Width & Height
//
function setBodyDOMsize2innerBrowserWindowSize() 
{
    document.body.style.width  = window.innerWidth +"px";
    document.body.style.height = window.innerHeight+"px";
}


//
// Get a define DOM container Width & Height
//
function get_p5_containerSize() 
{
    p5_containerSize = 
    {
        w: document.getElementById("p5_container").offsetWidth,
        h: document.getElementById("p5_container").offsetHeight
    };
}


//
// If the browser is Resized 
// or Orientation is changed
// Reload DOM
//
window.addEventListener ("resize", function reloadDOM() 
{
    location.reload();
});


//
// Update DOM Input Range Slider
// with with 
//
function updateDOMinputRange (numIterations)
{
    document.getElementsByTagName("input")[0].value = 0;

    if (numIterations > 1) 
    {
        //console.log(numIterations);

        document.getElementsByTagName("input")[0].setAttribute ("max", numIterations-1);
        document.getElementsByTagName("input")[0].style.visibility = "visible";
    }

    else 
    {
        document.getElementsByTagName("input")[0].style.visibility = "hidden";
        document.getElementsByTagName("input")[0].setAttribute ("max",   0);
    }
}


//
// Get colors from style.css
//
function getColorsFromCSSFile()
{
    // See style.css
    colorBackground       = getComputedStyle(document.documentElement).getPropertyValue("--colorBackground");
    colorBackgroundMiddle = getComputedStyle(document.documentElement).getPropertyValue("--colorBackgroundMiddle");
    colorMiddle           = getComputedStyle(document.documentElement).getPropertyValue("--colorMiddle");
    colorForegroundMiddle = getComputedStyle(document.documentElement).getPropertyValue("--colorForegroundMiddle");
    colorForeground       = getComputedStyle(document.documentElement).getPropertyValue("--colorForeground");
}
