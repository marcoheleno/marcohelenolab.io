//
// Cross Browser/Device
// DOM functions
//



let p5_containerSize;
let colorBackground, colorBackgroundMiddle, colorMiddle, colorForegroundMiddle, colorForeground;



window.onload = function() 
{
    setExplainableVisualizationDOMdivHeight();
    get_p5_containerSize();
    getColorsFromCSSFile();
}



function setExplainableVisualizationDOMdivHeight()
{
    let DOMheaderWidth  = document.getElementsByTagName("header")[0].offsetWidth;
    let DOMheaderHeight = document.getElementsByTagName("header")[0].offsetHeight;
    //console.log (DOMheaderHeight);

    let DOMexplainable_visualizationDivHeight = 0;

    if (DOMheaderWidth>481 && DOMheaderWidth<767) DOMexplainable_visualizationDivHeight = 340;
    else DOMexplainable_visualizationDivHeight = window.innerHeight - DOMheaderHeight;
    //console.log (DOMexplainable_visualizationDivHeight);

    document.getElementById("explainable_visualization").style.height = DOMexplainable_visualizationDivHeight + "px";
}



//
// Get DOM p5_container Width & Height
//
function get_p5_containerSize() 
{
    p5_containerSize = 
    {
        w: document.getElementById("p5_container").offsetWidth-1,
        h: document.getElementById("p5_container").offsetHeight-1
    };
}



window.onscroll = function() 
{
    if (window.scrollY >= 50) 
    {
        document.getElementById("read_more_button").style.visibility = "hidden";
    }
    else 
    {
        document.getElementById("read_more_button").style.visibility = "visible";
    }
}



//
// If the browser orientation is changed
// Reload DOM
//
window.onorientationchange = function(event) 
{
    location.reload();
    window.scroll (0, 0);
};



//
// Get colors from style.css
//
function getColorsFromCSSFile()
{
    // See style.css
    colorBackground       = getComputedStyle(document.documentElement).getPropertyValue("--colorBackground");
    colorBackgroundMiddle = getComputedStyle(document.documentElement).getPropertyValue("--colorBackgroundMiddle");
    colorMiddle           = getComputedStyle(document.documentElement).getPropertyValue("--colorMiddle");
    colorForegroundMiddle = getComputedStyle(document.documentElement).getPropertyValue("--colorForegroundMiddle");
    colorForeground       = getComputedStyle(document.documentElement).getPropertyValue("--colorForeground");
}



//
// Update DOM Input Range Slider
// with with 
//
function updateDOMinputRange (numIterations, goal)
{
    document.getElementsByTagName("input")[0].value = 0;

    if (numIterations > 1) 
    {
        //console.log(numIterations);

        document.getElementsByTagName("input")[0].setAttribute ("max", numIterations-1);
        document.getElementsByTagName("input")[0].style.visibility = "visible";

        if (goal === -1) 
        document.getElementsByTagName("input")[0].style.transform = "rotate(180deg)";
    }

    else 
    {
        document.getElementsByTagName("input")[0].style.visibility = "hidden";
        document.getElementsByTagName("input")[0].setAttribute ("max",   0);
    }
}



function addDOMTooltip()
{
    document.getElementById("p5_interchangeable_tooltip_2_button_container").appendChild (document.createElement("p"));
    document.getElementsByTagName("p")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
}


function updateDOMTooltipWith (message)
{
    document.getElementsByTagName("p")[0].innerHTML = message;
}



function addDOMButton()
{
    document.getElementById("p5_interchangeable_tooltip_2_button_container").appendChild (document.createElement("button"));
    document.getElementsByTagName("button")[1].setAttribute ("id", "p5_interchangeableTooltip2Button");
    document.getElementsByTagName("button")[1].setAttribute ("type", "button");
}


function updateDOMButtonWith (action, label)
{
    document.getElementsByTagName("button")[1].setAttribute ("onclick", action);
    document.getElementsByTagName("button")[1].innerHTML = label;
}

