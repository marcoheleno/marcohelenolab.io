

let p5colorMinus2, p5colorMinus1, p5colorZero, p5colorPlus1, p5colorPlus2;
let userIterationEnabled, goalPickerEnabled, runPtronAndGetIterationsEnabled, updateDOMenabled;
let vizPtronIterationsEnabled, updateDOMButtonForRunPtronEnabled, updateDOMButtonForTrainPtronEnabled;
let showRunPtronAndhideTrainPtronIterationEnabled;
let dataset, datasetIndex, userPickedGoal, highestNumOfIterations, runPtronIterationCounter, trainPtronIterationCounter;
let timeCounter, timeUntilRestartSystem;


function setup() 
{
  const canvas = createCanvas (p5_containerSize.w, p5_containerSize.h); // See crossBrowserDOM.js
  canvas.parent("p5_container");

  // See crossBrowserDOM.js
  p5colorMinus2       = color (colorBackground);
  p5colorMinus1 = color (colorBackgroundMiddle);
  p5colorZero           = color (colorMiddle);
  p5colorPlus1 = color (colorForegroundMiddle);
  p5colorPlus2       = color (colorForeground);

  re_startSystem();
}


function re_startSystem() 
{
  if (p5_interchangeableTooltip2ButtonContainer.firstChild) 
  {
    document.getElementById("p5_interchangeableTooltip2Button").remove();
  }

  highestNumOfIterations = 0;
  userPickedGoal = 0;
  updateDOMinputRange (highestNumOfIterations, userPickedGoal);

  userIterationEnabled = true;
  updateDOMenabled = true;

  goalPickerEnabled = false;
  
  runPtronAndGetIterationsEnabled = false;
  ptronIterationsCalculus.length = 0;
  
  vizPtronIterationsEnabled = false;
  updateDOMButtonForRunPtronEnabled = false;
  updateDOMButtonForTrainPtronEnabled = false;
  runPtronIterationCounter = -1;
  trainPtronIterationCounter = -1;
  showRunPtronAndhideTrainPtronIterationEnabled = false;

  timeCounter = millis();
  timeUntilRestartSystem = 1000*(60*4);
}


function draw() 
{
  if (pwinMouseX != winMouseX || pwinMouseY != winMouseY) 
  {
    timeCounter = millis();
  }

  if (millis()-timeCounter >= timeUntilRestartSystem) 
  {
    //location.href="index.html";
  }

  background (p5colorMinus2);
  drawPanels (userPickedGoal);
  

  if (userIterationEnabled) 
  {
    if (updateDOMenabled) 
    {
      updateDOMenabled = false;
      addDOMTooltip();
      updateDOMTooltipWith ("Drag to choose a starting position");

      dataset = addUserPickedData (1, p5_containerSize.w/2); // See data.js
      datasetIndex = 0;
    }
    
    dataset[datasetIndex].inputs[0] = mouseX;
  }

  else 

  if (goalPickerEnabled) 
  {
    if (updateDOMenabled) 
    {
      updateDOMenabled = false;
      updateDOMTooltipWith ("Select left or right panel to set system goal");
    }
    
    if (mouseX < p5_containerSize.w/2) userPickedGoal = -1;
    else userPickedGoal = 1;

    for (let i=0; i<dataset.length; i++) 
    {
      dataset[i].goal = userPickedGoal;
    }
  }

  else 

  if (runPtronAndGetIterationsEnabled) 
  {
    runPtronAndGetIterationsEnabled = false;
    highestNumOfIterations = runPtronAndGetIterations (dataset);
  }

  else 

  if (vizPtronIterationsEnabled) 
  {
    if (updateDOMenabled) 
    {
      updateDOMenabled = false;
      addDOMButton();
    }

    if (updateDOMButtonForRunPtronEnabled && runPtronIterationCounter<highestNumOfIterations-1) 
    {
      updateDOMButtonForRunPtronEnabled = false;
      updateDOMButtonWith ("runPtron()", "Press to iterate Perceptron");
    }

    else 

    if (updateDOMButtonForTrainPtronEnabled && trainPtronIterationCounter<highestNumOfIterations-1) 
    {
      updateDOMButtonForTrainPtronEnabled = false;

      if (userPickedGoal === -1) updateDOMButtonWith ("trainPtron()", "Press to compare with left goal");
      else  updateDOMButtonWith ("trainPtron()", "Press to compare with right goal");
    }
  }


  if (userIterationEnabled || goalPickerEnabled || runPtronAndGetIterationsEnabled || vizPtronIterationsEnabled) 
  {
    drawUserIteration (dataset);
  }


  if (trainPtronIterationCounter < highestNumOfIterations-1) 
  {
    if (runPtronIterationCounter >= 0) 
    {
      drawPtronIteration (dataset, runPtronIterationCounter);
      if (runPtronIterationCounter === 0) drawRouteFromUser2PtronIteration (dataset);
    }
    if (trainPtronIterationCounter >= 0 && !showRunPtronAndhideTrainPtronIterationEnabled) 
    {
      drawRouteFromSelected2TrainedIteration (dataset, trainPtronIterationCounter);
    }
  }
  
  else if (trainPtronIterationCounter === highestNumOfIterations-1) 
  {
    if (updateDOMenabled) 
    {
      updateDOMenabled = false;
      document.getElementById("p5_interchangeableTooltip2Button").remove();

      addDOMTooltip();
      if (highestNumOfIterations > 1) 
      {
        updateDOMTooltipWith ("Perceptron Trained.<br>Drag slider to skim over iterations");
        updateDOMinputRange (highestNumOfIterations, userPickedGoal);
      }

      else updateDOMTooltipWith ("Perceptron Trained");
    }

    drawUserIteration (dataset);
    let DOMinputRangeValue = document.getElementById("iterations_slider").value;
    //let DOMinputRangeValue = document.getElementsByTagName("input")[0].value;
    drawAllPtronIterations (dataset, DOMinputRangeValue);
    drawRouteFromSelected2TrainedIteration (dataset, DOMinputRangeValue);
    drawRouteFromUser2PtronIteration (dataset);
  }
  
  drawPanels (0);
}


function runPtron () 
{
  if (runPtronIterationCounter < highestNumOfIterations-1) runPtronIterationCounter++;
  updateDOMButtonForTrainPtronEnabled = true;
  showRunPtronAndhideTrainPtronIterationEnabled = true;
}


function trainPtron () 
{
  if (trainPtronIterationCounter < highestNumOfIterations-2) 
  {
    trainPtronIterationCounter++;
    updateDOMButtonForRunPtronEnabled = true;
    showRunPtronAndhideTrainPtronIterationEnabled = false;
  }
  else 
  {
    trainPtronIterationCounter++;
    updateDOMenabled = true;
    vizPtronIterationsEnabled = false;
  }
}


function mouseReleased() 
{
  timeCounter = millis();

  if (mouseX>0 && mouseX<p5_containerSize.w && mouseY>0 && mouseY<p5_containerSize.h) 
  {

    if (userIterationEnabled) 
    {
      if (datasetIndex < dataset.length-1) datasetIndex++;
      else 
      {
        userIterationEnabled = false;
        goalPickerEnabled = true;
        updateDOMenabled = true;
      }
    }
    
    else 
    
    if (goalPickerEnabled) 
    {
      goalPickerEnabled = false;
      runPtronAndGetIterationsEnabled = true;
      vizPtronIterationsEnabled = true;
      updateDOMenabled = true;
      updateDOMButtonForRunPtronEnabled = true;
      document.getElementById("p5_interchangeableTooltip2Button").remove();
    }

  }
}


function drawPanels (goal) 
{
  push();
    translate (p5_containerSize.w/2, p5_containerSize.h/2);
    
      strokeWeight (1);
      stroke (colorForegroundMiddle);

      if (goal < 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (-p5_containerSize.w/2 + 1, -p5_containerSize.h/2 + 1);
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
        vertex (-p5_containerSize.w/2 + 1,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
      
      if (goal > 0) fill (colorBackgroundMiddle);
      else noFill();
      beginShape();
        vertex (                        0, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1, -p5_containerSize.h/2 + 1);
        vertex ( p5_containerSize.w/2 - 1,  p5_containerSize.h/2 - 1);
        vertex (                        0,  p5_containerSize.h/2 - 1);
      endShape (CLOSE);
  pop();
}


function drawPtronStoryboardElementWithp5 (x, y, selectedWithDOMinputRangeValue, pickedByUser)
{
  
  if (pickedByUser) 
  {
    noStroke();
    fill (p5colorZero);
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);

    stroke (p5colorMinus2);
    strokeWeight (5);
    point (x, y);
  }

  else 
  {
    if (selectedWithDOMinputRangeValue) stroke (p5colorPlus2);
    else stroke (colorForegroundMiddle);

    strokeWeight (1);
    noFill();
    ellipse (x, y, p5_containerSize.w/2, p5_containerSize.w/2);
    
    strokeWeight (5);
    point (x, y);
  }

}


function drawPtronStoryboardRouteWithp5 (beginning, end, y) 
{
  const xPositions = [beginning, end];
  if (beginning > end) xPositions.reverse();

  strokeWeight (1.6);
  stroke (p5colorPlus2);
  
  const dashWidth = 4;
  for (let x=xPositions[0]; x<xPositions[1]; x+=dashWidth)
  {
    point (x, y);
  }
}


function drawRouteFromUserPicked2InitialPtronStoryboardWithp5 (xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight, distance, userPickedXonLeftPanel) 
{
  strokeWeight (1.6);
  stroke (p5colorPlus2);
  noFill;
  //bezier(xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight);

  let dashWidth = distance/3;
  for (let d=0; d<=dashWidth; d++) 
  {
    if (userPickedXonLeftPanel) stroke( map(d, 0, dashWidth+1, 100, 250) );
    else stroke( map(d, 0, dashWidth+1, 250, 100) );

    let t = d/dashWidth;
    let x = bezierPoint (xLeft, xLeftControl, xRightControl, xRight, t);
    let y = bezierPoint (yLeft, yLeftControl, yRightControl, yRight, t);

    point (x, y);
  }
}
