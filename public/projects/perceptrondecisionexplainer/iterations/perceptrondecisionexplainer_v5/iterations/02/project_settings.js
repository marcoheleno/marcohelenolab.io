

let p5_color_minus_2, p5_color_minus_1, p5_color_zero, p5_color_plus_1, p5_color_plus_2, p5_color_trained, p5_color_untrained;
let dataset, game_board_size;
let DOM_functions_done, perceptron_started;
let visualization_moment, user_select_goal, num_iterations, perceptron_instance, perceptron_instance_erro;
let iterations_data_input_x_position_weight_value, iterations_data_input_bias_weight_value, iterations_data_x_pos, iterations_data_color;
let iterations_gameboard_color, iteration_weights_sum;


function re_startSystem() 
{
  // See getColorsFromCSSFile() in DOM_functions.js
  p5_color_minus_2 = { r:red(color_minus_2), g:green(color_minus_2), b:blue(color_minus_2) };
  p5_color_minus_1 = { r:red(color_minus_1), g:green(color_minus_1), b:blue(color_minus_1) };
  p5_color_zero    = { r:red(color_zero),    g:green(color_zero),    b:blue(color_zero)    };
  p5_color_plus_1  = { r:red(color_plus_1),  g:green(color_plus_1),  b:blue(color_plus_1)  };
  p5_color_plus_2  = { r:red(color_plus_2),  g:green(color_plus_2),  b:blue(color_plus_2)  };

  p5_color_trained   = { r:red(color_trained), g:green(color_trained), b:blue(color_trained) };
  p5_color_untrained = { r:red(color_untrained), g:green(color_untrained), b:blue(color_untrained) };

  dataset = 
  {
    inputs: [0, 1], 
    goal: 0
  };

  // Game board size - w (width) and h (height)
  game_board_size = 
  {
    w:(width/8)*4, 
    h:(height/4)*3
  };

  visualization_moment = 1;

  DOM_functions_done = false;

  user_select_goal = 0;
  num_iterations = 0;
  updateIterationsDOMinputRangeSlider (num_iterations, user_select_goal);

  perceptron_instance = 0;
  perceptron_started = false;
  perceptron_instance_erro = 0;

  iterations_data_input_x_position_weight_value = [];
  iterations_data_input_bias_weight_value = [];
  iterations_data_x_pos = [];
  iterations_data_color = [];
  iterations_gameboard_color = [];
  iteration_weights_sum = [];

  updateXposWeightDOMinputRangeSlider (0);
  updateBiasWeightDOMinputRangeSlider (0);
  



  



  if (tooltip_or_button_container.firstChild) 
  {
    document.getElementById("tooltip_or_button_element").remove();
  }

  

  updateDOMenabled = true;

  goalPickerEnabled = false;
  
  runPtronAndGetIterationsEnabled = false;
  //ptronIterationsCalculus.length = 0;
  
  vizPtronIterationsEnabled = false;
  updateDOMButtonForRunPtronEnabled = false;
  updateDOMButtonForTrainPtronEnabled = false;
  runPtronIterationCounter = -1;
  trainPtronIterationCounter = -1;
  showRunPtronAndhideTrainPtronIterationEnabled = false;

  timeCounter = millis();
  timeUntilRestartSystem = 1000*(60*4);
}

