


let goalPickerEnabled, runPtronAndGetIterationsEnabled, updateDOMenabled;
let vizPtronIterationsEnabled, updateDOMButtonForRunPtronEnabled, updateDOMButtonForTrainPtronEnabled;
let showRunPtronAndhideTrainPtronIterationEnabled;
let datasetIndex, runPtronIterationCounter, trainPtronIterationCounter;
let timeCounter, timeUntilRestartSystem;



let user_select_x;


function setup() 
{
  setAttributes("antialias", true);
  const canvas = createCanvas (p5_container_size.w, p5_container_size.h, WEBGL); // See DOM_functions.js
  canvas.parent("p5_container");
  colorMode (RGB, 255, 255, 255, 100);

  re_startSystem();
}


function draw() 
{
  // falta o timer

  background (p5_color_minus_2.r, p5_color_minus_2.g, p5_color_minus_2.b);


  switch (visualization_moment) 
  {
    case 1:
      positionSelectMoment(); // visualization_moments.js
      break;

    case 2:
      goalSelectMoment();
      break;

    case 3:
      iterationPerceptronSelectMoment();
      break;

    case 4:
      iterationPerceptronSelectResult();
      break;

    default: console.log ("erro"); // corrigir...
  }


  drawUserPickedDataPosition (user_select_x);

  for (let i=0; i<iterations_data_x_pos.length; i++)
  {
    if (i === getIterationsDOMinputRangeSliderValue()) 
    {
      drawPerceptronDataPosition (iterations_data_x_pos[i], iterations_data_color[i], true);
    }
    else 
    {
      drawPerceptronDataPosition (iterations_data_x_pos[i], iterations_data_color[i], false);
    }
  }

  
  if (num_iterations > 0) 
  {    
    if (perceptron_instance.getTrainingError() === 0) 
    {
      let i = getIterationsDOMinputRangeSliderValue();
      updateXposWeightDOMinputRangeSlider (iterations_data_input_x_position_weight_value[i]);
      updateBiasWeightDOMinputRangeSlider (iterations_data_input_bias_weight_value[i]);
      drawGameBoardWith (user_select_goal, iterations_data_x_pos[i], iterations_gameboard_color[i], true);
    }
    else
    {
      updateXposWeightDOMinputRangeSlider (iterations_data_input_x_position_weight_value[num_iterations-1]);
      updateBiasWeightDOMinputRangeSlider (iterations_data_input_bias_weight_value[num_iterations-1]);
      drawGameBoardWith (user_select_goal, iterations_data_x_pos[num_iterations-1], iterations_gameboard_color[num_iterations-1], true);
    }
  }
  else 
  {
    drawGameBoardWith (user_select_goal, 0, color(0, 0, 0), false);
  }
}



function mouseReleased() 
{

  switch (visualization_moment) 
  {
    case 1:
      positionSelectResult();
      break;

    case 2:
      goalSelectResult();
      break;

    case 3:
      break;

    case 4:
      break;

    default: console.log ("erro"); // corrigir...
  }

}



function runPtron () 
{
  if (runPtronIterationCounter < num_iterations-1) runPtronIterationCounter++;
  updateDOMButtonForTrainPtronEnabled = true;
  showRunPtronAndhideTrainPtronIterationEnabled = true;
}


function trainPtron () 
{
  if (trainPtronIterationCounter < num_iterations-2) 
  {
    trainPtronIterationCounter++;
    updateDOMButtonForRunPtronEnabled = true;
    showRunPtronAndhideTrainPtronIterationEnabled = false;
  }
  else 
  {
    trainPtronIterationCounter++;
    updateDOMenabled = true;
    vizPtronIterationsEnabled = false;
  }
}











function drawPtronStoryboardRouteWithp5 (beginning, end, y) 
{
  const xPositions = [beginning, end];
  if (beginning > end) xPositions.reverse();

  strokeWeight (1.6);
  stroke (p5_color_plus_2.r, p5_color_plus_2.g, p5_color_plus_2.b);
  
  const dashWidth = 4;
  for (let x=xPositions[0]; x<xPositions[1]; x+=dashWidth)
  {
    point (x, y);
  }
}


function drawRouteFromUserPicked2InitialPtronStoryboardWithp5 (xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight, distance, userPickedXonLeftPanel) 
{
  strokeWeight (1.6);
  stroke (p5_color_plus_2.r, p5_color_plus_2.g, p5_color_plus_2.b);
  noFill;
  //bezier(xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight);

  let dashWidth = distance/3;
  for (let d=0; d<=dashWidth; d++) 
  {
    if (userPickedXonLeftPanel) stroke( map(d, 0, dashWidth+1, 100, 250) );
    else stroke( map(d, 0, dashWidth+1, 250, 100) );

    let t = d/dashWidth;
    let x = bezierPoint (xLeft, xLeftControl, xRightControl, xRight, t);
    let y = bezierPoint (yLeft, yLeftControl, yRightControl, yRight, t);

    point (x, y);
  }
}
