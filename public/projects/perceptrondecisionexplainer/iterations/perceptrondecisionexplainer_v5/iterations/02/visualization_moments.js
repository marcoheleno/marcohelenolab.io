


function positionSelectMoment() 
{
  if (!DOM_functions_done) 
  {
    DOM_functions_done = true;
    addDOMTooltip();
    updateDOMtooltipORbuttonWith ("Drag to choose a starting position");
  }

  if (mouseX-width/2 >= -game_board_size.w/2 && 
      mouseX-width/2 <=  game_board_size.w/2) 
  {
    user_select_x = mouseX-width/2;
  }
}

function positionSelectResult() 
{
  dataset.inputs[0] = user_select_x;
  visualization_moment = 2;
  DOM_functions_done = false;
}



function goalSelectMoment() 
{
  if (!DOM_functions_done) 
  {
    DOM_functions_done = true;
    updateDOMtooltipORbuttonWith ("Select left or right panel to set system goal");
  }

  if (mouseX-width/2 < 0) user_select_goal = -1;
  else                    user_select_goal =  1;
}

function goalSelectResult() 
{
  dataset.goal = user_select_goal;
  visualization_moment = 3;
  DOM_functions_done = false;
}




function iterationPerceptronSelectMoment() 
{
  if (!DOM_functions_done) 
  {
    DOM_functions_done = true;
    
    if (!perceptron_started) 
    {
      addDOMButton();
      updateDOMtooltipORbuttonWith ("Press to start Perceptron with random guess");
    }
    else 
    {
      updateDOMtooltipORbuttonWith ("Press to iterate Perceptron");
    }

  }
}

function iteratePerceptron() 
{
  if (dataset.goal != 0) 
  {
    if (!perceptron_started) 
    {
      perceptron_started = true;
      DOM_functions_done = false;
      perceptron_instance = new Perceptron (dataset.inputs.length, 0.0005); // See Perceptron.js
    }

    perceptron_instance.feedForward (dataset.inputs);
    iterations_data_x_pos[num_iterations] = perceptron_instance.getFeedForwardCalculation();

    perceptron_instance.activationFunction();

    perceptron_instance.train (dataset.goal);

    iterations_data_input_x_position_weight_value[num_iterations] = perceptron_instance.getIndividualWeightValue()[0];
    iterations_data_input_bias_weight_value[num_iterations] = perceptron_instance.getIndividualWeightValue()[1];
    iteration_weights_sum[num_iterations] = perceptron_instance.getSumWeightsValue();

    if (perceptron_instance.getTrainingError() === 0) 
    {
      iterations_data_color[num_iterations] = p5_color_trained;
      iterations_gameboard_color[num_iterations] = p5_color_trained;
    }
    else 
    {
      iterations_data_color[num_iterations] = p5_color_untrained;
      iterations_gameboard_color[num_iterations] = p5_color_untrained;
    }

    num_iterations++;

    if (perceptron_instance.getTrainingError()===0 && num_iterations<=1) 
    {
      perceptron_started = false;
      num_iterations = 0;
      iteratePerceptron();
    }
    else 
    if (perceptron_instance.getTrainingError()===0 && num_iterations>1) 
    {
      iterations_data_input_x_position_weight_value[num_iterations] = perceptron_instance.getIndividualWeightValue()[0];
      iterations_data_input_bias_weight_value[num_iterations] = perceptron_instance.getIndividualWeightValue()[1];
      iteration_weights_sum[num_iterations] = perceptron_instance.getSumWeightsValue();

      visualization_moment = 4;
      DOM_functions_done = false;
    }

  }
}

function iterationPerceptronSelectResult() 
{
  if (!DOM_functions_done) 
  {
    DOM_functions_done = true;
    addDOMTooltip();

    if (num_iterations > 1) 
    {
      updateDOMtooltipORbuttonWith ("Perceptron Trained!<br>Drag slider to skim over iterations");
      updateIterationsDOMinputRangeSlider (num_iterations, user_select_goal);
    }

    else updateDOMtooltipORbuttonWith ("Perceptron Trained!");
  }
}
