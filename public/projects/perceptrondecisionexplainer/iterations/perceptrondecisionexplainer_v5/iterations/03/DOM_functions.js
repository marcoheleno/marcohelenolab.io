

//
// DOM functions
//


let color_minus_2, color_minus_1, color_zero, color_plus_1, color_plus_2;
let color_trained, color_untrained;
let p5_container_size;


//
// Initial functions to load as soon the page is loaded
//
window.onload = function() 
{
    setBodyDOMsize2innerBrowserWindowSize();
    getP5ContainerSize();
    getColorsFromCSSFile();
    disableUserInteractionInWeightsDOMinputRangeSliders();
}



//
// Set Body Width & Height
// to Inner browser Width & Height
//
function setBodyDOMsize2innerBrowserWindowSize() 
{
    document.body.style.width  = window.innerWidth +"px";
    document.body.style.height = window.innerHeight+"px";
}



//
// Get p5js container Width & Height
// used to later define the p5js canvas Width & Height.
// The Width & Height of this container are calculated in percentage 
// relative to the browser size and is defined inside style.css
//
function getP5ContainerSize() 
{
    if (p5_container_size === undefined) 
    {
        p5_container_size = 
        {
            w: document.getElementById("p5_container").offsetWidth,
            h: document.getElementById("p5_container").offsetHeight
        };
    }
    
    return p5_container_size;
}



//
// If the browser is Resized 
// or Orientation is changed
// Reload DOM
//
window.addEventListener ("resize", function reloadDOM() 
{
    location.reload();
});



//
// Get colors from style.css
//
function getColorsFromCSSFile()
{
    // See style.css
    color_minus_2 = getComputedStyle(document.documentElement).getPropertyValue("--color_minus_2");
    color_minus_1 = getComputedStyle(document.documentElement).getPropertyValue("--color_minus_1");
    color_zero    = getComputedStyle(document.documentElement).getPropertyValue("--color_zero");
    color_plus_1  = getComputedStyle(document.documentElement).getPropertyValue("--color_plus_1");
    color_plus_2  = getComputedStyle(document.documentElement).getPropertyValue("--color_plus_2");

    color_trained    = getComputedStyle(document.documentElement).getPropertyValue("--color_trained");
    color_untrained  = getComputedStyle(document.documentElement).getPropertyValue("--color_untrained");
}



//
// Update Iterations DOM Input Range Slider Values
//
function updateIterationsDOMinputRangeSlider (num_iterations, goal) 
{
    if (num_iterations > 1) 
    {
        //console.log(num_iterations);

        document.getElementById("iterations_DOM_input_range_slider").setAttribute ("max", num_iterations-1);
        document.getElementById("iterations_DOM_input_range_slider").value = num_iterations-1;

        if (goal === -1) 
        document.getElementById("iterations_DOM_input_range_slider").style.transform = "rotate(180deg)";
        else 
        if (goal === 1) 
        document.getElementById("iterations_DOM_input_range_slider").style.transform = "rotate(0deg)";

        document.getElementById("iterations_DOM_input_range_slider").style.visibility = "visible";
    }

    else 
    {
        document.getElementById("iterations_DOM_input_range_slider").setAttribute ("max",   0);
        document.getElementById("iterations_DOM_input_range_slider").value = 0;
        document.getElementById("iterations_DOM_input_range_slider").style.visibility = "hidden";
    }
}

//
// Get Iterations DOM Input Range Slider Value
//
function getIterationsDOMinputRangeSliderValue() 
{
    return Number(document.getElementById("iterations_DOM_input_range_slider").value);
}



//
// Disable User Interaction in Weights DOM Input Range Sliders
//
function disableUserInteractionInWeightsDOMinputRangeSliders() 
{
    document.getElementById("xpos_weight_DOM_input_range_slider").disabled = true;
    document.getElementById("bias_weight_DOM_input_range_slider").disabled = true;
}

//
// Update X position & bias weights DOM Input Range Sliders Values
//
function updateXposWeightDOMinputRangeSlider (x_weight_value) 
{
    let rounded_x_weight_value = parseFloat(x_weight_value).toFixed(5); //rounded float with one decimal place
    
    document.getElementById("xpos_weight_DOM_input_range_slider").value = rounded_x_weight_value;
}

function updateBiasWeightDOMinputRangeSlider (bias_weight_value) 
{
    let rounded_bias_weight_value = parseFloat(bias_weight_value).toFixed(5); //rounded float with one decimal place
    
    document.getElementById("bias_weight_DOM_input_range_slider").value = rounded_bias_weight_value;
}



//
// A set of functions relating to a single DOM element.
// This element of the DOM assumes two main functions 
// (never simultaneously) that appear depending on the 
// interaction requirements of each moment of use. 
// Each of the two behaviors is graphically identified 
// for the best user orientation. 
// This choice aims to simplify and focus the user on a 
// single interactive element on most of the visualization.
//
function addDOMTooltip()
{
    const container_element = document.getElementById ("tooltip_or_button_container");

    if (container_element.firstChild &&
        container_element.firstChild.isEqualNode(container_element.getElementsByTagName("button")[0]) ) 
        {
            container_element.getElementsByTagName("button")[0].remove();
        }
    
    container_element.appendChild( document.createElement("p") );
    container_element.getElementsByTagName("p")[0].setAttribute ("id", "tooltip_or_button_element");
}

function addDOMButton()
{
    const container_element = document.getElementById ("tooltip_or_button_container");

    if (container_element.firstChild &&
        container_element.firstChild.isEqualNode(container_element.getElementsByTagName("p")[0]) ) 
        {
            container_element.getElementsByTagName("p")[0].remove();
        }
    
    container_element.appendChild( document.createElement("button") );
    container_element.getElementsByTagName("button")[0].setAttribute ("id", "tooltip_or_button_element");
    container_element.getElementsByTagName("button")[0].setAttribute ("type", "button");
    container_element.getElementsByTagName("button")[0].setAttribute ("onclick", "iteratePerceptron()");
}

function updateDOMtooltipORbuttonWith (label)
{
    document.getElementById("tooltip_or_button_element").innerHTML = label;
}