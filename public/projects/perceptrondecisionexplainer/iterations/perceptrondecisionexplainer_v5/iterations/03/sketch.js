


let goalPickerEnabled, runPtronAndGetIterationsEnabled, updateDOMenabled;
let vizPtronIterationsEnabled, updateDOMButtonForRunPtronEnabled, updateDOMButtonForTrainPtronEnabled;
let showRunPtronAndhideTrainPtronIterationEnabled;
let datasetIndex, runPtronIterationCounter, trainPtronIterationCounter;
let timeCounter, timeUntilRestartSystem;

let user_select_x;


function setup() 
{
  setAttributes("antialias", true);
  const canvas = createCanvas (p5_container_size.w, p5_container_size.h, WEBGL); // See DOM_functions.js
  canvas.parent("p5_container");
  colorMode (RGB, 255, 255, 255, 100);

  re_startSystem();
}


function draw() 
{
  // falta o timer

  background (p5_color_minus_2.r, p5_color_minus_2.g, p5_color_minus_2.b);


  switch (visualization_moment) 
  {
    case 1:
      positionSelectMoment(); // visualization_moments.js
      break;

    case 2:
      goalSelectMoment();
      break;

    case 3:
      iterationPerceptronSelectMoment();
      break;

    case 4:
      iterationPerceptronSelectResult();
      break;

    default: console.log ("erro"); // corrigir...
  }


  drawUserPickedDataPosition (user_select_x);

  for (let i=0; i<iterations_data_x_pos.length; i++)
  {
    if (i === getIterationsDOMinputRangeSliderValue()) 
    {
      drawPerceptronDataPosition (iterations_data_x_pos[i], iterations_data_color[i], true);
    }
    else 
    {
      drawPerceptronDataPosition (iterations_data_x_pos[i], iterations_data_color[i], false);
    }
  }

  
  if (num_iterations > 0) 
  {
    if (perceptron_instance.getTrainingError() === 0) 
    {
      let i = getIterationsDOMinputRangeSliderValue();
      updateXposWeightDOMinputRangeSlider (iterations_data_input_x_position_weight_value[i]);
      updateBiasWeightDOMinputRangeSlider (iterations_data_input_bias_weight_value[i]);
      drawGameBoardWith (user_select_goal, iterations_data_x_pos[i], iterations_gameboard_color[i], true);
    }
    else
    {
      updateXposWeightDOMinputRangeSlider (iterations_data_input_x_position_weight_value[num_iterations-1]);
      updateBiasWeightDOMinputRangeSlider (iterations_data_input_bias_weight_value[num_iterations-1]);
      drawGameBoardWith (user_select_goal, iterations_data_x_pos[num_iterations-1], iterations_gameboard_color[num_iterations-1], true);
    }

    const rangeX = document.getElementById ("xpos_weight_DOM_input_range_slider");
    const bubbleX = document.getElementById ("xpos_weight_DOM_input_range_slider_value");
    setBubble(rangeX, bubbleX);

    const rangeB = document.getElementById ("bias_weight_DOM_input_range_slider");
    const bubbleB = document.getElementById ("bias_weight_DOM_input_range_slider_value");
    setBubble(rangeB, bubbleB);
  }
  else 
  {
    drawGameBoardWith (user_select_goal, 0, color(0, 0, 0), false);
  }
}



function mouseReleased() 
{

  switch (visualization_moment) 
  {
    case 1:
      positionSelectResult();
      break;

    case 2:
      goalSelectResult();
      break;

    case 3:
      break;

    case 4:
      break;

    default: console.log ("erro"); // corrigir...
  }

}
