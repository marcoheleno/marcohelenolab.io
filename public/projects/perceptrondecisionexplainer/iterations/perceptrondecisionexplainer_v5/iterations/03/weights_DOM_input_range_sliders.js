/*
const allRanges = document.querySelectorAll(".weights_container");

allRanges.forEach(wrap => 
{
  const range  = wrap.querySelector(".weights_DOM_input_range_sliders");
  const bubble = wrap.querySelector(".weights_DOM_input_range_sliders_values");

  range.addEventListener("input", () => 
  {
    setBubble(range, bubble);
  });

  setBubble(range, bubble);
});
*/

function setBubble(range, bubble) {
  
  const val = range.value;
  const min = range.min ? range.min : 0;
  const max = range.max ? range.max : 100;
  const newVal = Number(((val - min) * 100) / (max - min));
  //bubble.innerHTML = val;
  
  // Sorta magic numbers based on size of the native UI thumb
  bubble.style.left = `calc(${newVal}% + (${newVal * 0.15}px))`;
}
