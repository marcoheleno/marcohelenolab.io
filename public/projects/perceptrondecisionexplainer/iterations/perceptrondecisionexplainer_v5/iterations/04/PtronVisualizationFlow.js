

//
// In this function you are able to define
// the Perceptron's Activation Function
//
/*
const ptronActivationFunction = function (sumWeightedInputs) 
{
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}
*/




let ptronIterationsCalculus = 0;


//
// This function runs the Perceptron (feedForward + train)
// with the selected data
// and saves all iterations calaculations.
//
function runPtronAndGetIterations (dataset) 
{
  ptronIterationsCalculus = 0;
  ptronIterationsCalculus = new Array (0);

  if (dataset.length > 0) 
  {
    const num_of_inputs = dataset[0].inputs.length;

    const perceptron_instance = new Perceptron (num_of_inputs, ptronActivationFunction, 0.0005); // See Perceptron.js


    // Creates a two-dimensional array.
    // For each dataPoint a collection of the Perceptron's iterations is saved.
    // The second dimension's length is number of iterations 
    // the Perceptron took until trainingError of 0.
    // The name "... Script" is an analogy to an animation script.
    for (let i=0; i<dataset.length; i++) 
    {
      ptronIterationsCalculus.push( new Array (0) );
    }


    let i = 0;
    let numDataPointsPerceptronTrained  = 0;
    let num_iterations = 0;
    do 
    {
      // For each dataPoint...
      // Check if the Perceptron is not on it's first iteration and
      // if it has a trainingError of 0 on current (i) dataPoint
      if (ptronIterationsCalculus[i].length > 0 && 
          ptronIterationsCalculus[i][ptronIterationsCalculus[i].length-1].trainingError === 0) 
      {
        // Count the number of dataset that the Perceptron is trained on 
        numDataPointsPerceptronTrained++;
      }

      // For each dataPoint...
      // If the Perceptron on this iteration does not have a trainingError of 0
      // we FeedForward the inputs
      // and Train the Perceptron
      else
      {
        perceptron_instance.feedForward (dataset[i].inputs);
        perceptron_instance.train (dataset[i].goal);

        // We add to the second dimension of the array ptronIterationsCalculus
        // the Perceptron's calculations of this iteration
        ptronIterationsCalculus[i].push( perceptron_instance.getAllTransparentPerceptronCalculations() ); 
      }

      // Iterate through all the dataset
      if (i < ptronIterationsCalculus.length-1) i++;

      // If allready iterated through all the dataset
      else 
      {
        i = 0; // Restart iteration counter
        
        // If the Perceptron has not trained successfully on all dataset
        // also Restart the numDataPointsPerceptronTrained counter
        if (numDataPointsPerceptronTrained < ptronIterationsCalculus.length) numDataPointsPerceptronTrained = 0;
      }

      // Save the highest number of Iterations the Perceptron performed
      if (num_iterations < ptronIterationsCalculus[i].length) 
      {
        num_iterations = ptronIterationsCalculus[i].length;
      }

      // Repeat while the Perceptron has not successfully trained on all dataset
    }
    while (numDataPointsPerceptronTrained < dataset.length);

    // For explanatory reason, we restart the Perceptron, 
    // so it does not reach the goal at the first iteration.
    if (num_iterations <= 1) 
    {
      runPtronAndGetIterationsEnabled = true;
      //console.log("vai reiniciar");
    }
    
    //console.log(num_iterations);
    return num_iterations;
  }
}





//
// This function draws the Perceptron's Storyboard for Visualization.
// It drawns on the previously created ptronIterationsCalculus two-dimensional array values.
// For each dataPoint it is drawAllPtronIterations with all the iterations (drawPerceptornHorizontalDataPointPosition) 
// the Perceptron went through.
//
function drawAllPtronIterations (dataset, DOM_input_range_slider_value) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let x, y;
    let DOM_input_range_slider_value = false;

    // c = column
    for (let c=0; c<ptronIterationsCalculus.length; c++) 
    {
      // l = line
      for (let l=0; l<ptronIterationsCalculus[c].length; l++) 
      {

        x = ptronIterationsCalculus[c][l].feedForwardCalculation;
        y = 0;

        // Iteration selected by DOM_input_range_slider_value?
        if( l === Math.trunc(DOM_input_range_slider_value) ) DOM_input_range_slider_value = true; 
        else DOM_input_range_slider_value = false;

        drawPerceptornHorizontalDataPointPosition (x, y, DOM_input_range_slider_value);
      }
    }

  }
}


function drawRouteFromSelected2TrainedIteration (dataset, DOM_input_range_slider_value) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let DOM_input_range_slider_value = false;
    let verticalSpaceBetweenDataPoints = 0;
    let beginning, end, y;

    // c = column
    for (let c=0; c<ptronIterationsCalculus.length; c++) 
    {
      // l = line
      for (let l=0; l<ptronIterationsCalculus[c].length; l++) 
      {
        if( l === Math.trunc(DOM_input_range_slider_value) ) DOM_input_range_slider_value = true; 
        else DOM_input_range_slider_value = false;

        if (DOM_input_range_slider_value && ptronIterationsCalculus[c].length>1) 
        {
          beginning = width/2 + ptronIterationsCalculus[c][l].feedForwardCalculation;
          end = width/2 + ptronIterationsCalculus[c][ptronIterationsCalculus[c].length-1].feedForwardCalculation;
          y = verticalSpaceBetweenDataPoints * (c+1);

          drawPtronStoryboardRouteWithp5 (beginning, end, y);
        }

      }
    }

  }
}


function drawRouteFromUser2PtronIteration (dataset) 
{
  if (ptronIterationsCalculus.length > 0) 
  {
    let xUser, xPtron, verticalSpaceBetweenDataPoints, y, distance, xPositions, userPickedXonLeftPanel;
    let xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight;

    for (let i=0; i<dataset.length; i++) 
    {
      xUser  = dataset[i].inputs[0];
      xPtron = width/2 + ptronIterationsCalculus[i][0].feedForwardCalculation;

      verticalSpaceBetweenDataPoints = 0;
      y = verticalSpaceBetweenDataPoints;

      distance = Math.abs(xUser - xPtron);

      xPositions = [xUser, xPtron];
      userPickedXonLeftPanel = true;
      if (xPtron > xUser) 
      {
        xPositions.reverse();
        userPickedXonLeftPanel = false;
      }
      xLeft  = xPositions[0];
      xRight = xPositions[1];

      yLeft  = y;
      yRight = y;
      xLeftControl = xLeft + distance/6;
      yLeftControl = yLeft - distance;
      xRightControl = xRight - distance/6;
      yRightControl = yRight - distance;

      drawRouteFromUserPicked2InitialPtronStoryboardWithp5 (xLeft, yLeft, xLeftControl, yLeftControl, xRightControl, yRightControl, xRight, yRight, distance, userPickedXonLeftPanel);
    }
  }
}

