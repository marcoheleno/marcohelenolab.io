


function drawGameBoardWith (goal, iteration_datapoint_x_pos, c, selected) 
{
  // position in X, Y and Z
  const x = 0;
  const y = 0;
  const z = 0;
  
  // rotation from 0 to 360 in X, Y and Z
  const rX = -15;
  const rY = 0;
  const rZ = map (iteration_datapoint_x_pos, -game_board_size.w/2, game_board_size.w/2, -40, 40);
  
  // transparency value
  const goalPanelAlphaValue = 80;
  const normPanelAlphaValue = 40;
  
  push();
    translate (x, y, z);
      
      rotateX ( radians(rX) );
      rotateY ( radians(rY) );
      rotateZ ( radians(rZ) );
  
      strokeWeight (1);
      stroke (p5_color_zero.r, p5_color_zero.g, p5_color_zero.b, p5_color_zero);
  

      if (goal < 0) emissiveMaterial (p5_color_minus_1.r, p5_color_minus_1.g, p5_color_minus_1.b, goalPanelAlphaValue);
      else          emissiveMaterial (p5_color_minus_1.r, p5_color_minus_1.g, p5_color_minus_1.b, normPanelAlphaValue);

      if (selected && goal < 0) emissiveMaterial (c.r, c.g, c.b, goalPanelAlphaValue);

      beginShape();
        vertex (-game_board_size.w/2, 0, -game_board_size.h/2);
        vertex (                   0, 0, -game_board_size.h/2);
        vertex (                   0, 0,  game_board_size.h/2);
        vertex (-game_board_size.w/2, 0,  game_board_size.h/2);
      endShape (CLOSE);
      
  
      if (goal > 0) emissiveMaterial (p5_color_minus_1.r, p5_color_minus_1.g, p5_color_minus_1.b, goalPanelAlphaValue);
      else          emissiveMaterial (p5_color_minus_1.r, p5_color_minus_1.g, p5_color_minus_1.b, normPanelAlphaValue);

      if (selected && goal > 0) emissiveMaterial (c.r, c.g, c.b, goalPanelAlphaValue);
  
      beginShape();
        vertex (                   0, 0, -game_board_size.h/2);
        vertex ( game_board_size.w/2, 0, -game_board_size.h/2);
        vertex ( game_board_size.w/2, 0,  game_board_size.h/2);
        vertex (                   0, 0,  game_board_size.h/2);
      endShape (CLOSE);
  
  pop();
}



function drawUserPickedDataPointPosition (x) 
{
  // position in Y and Z
  const y = 0;
  const z = 0;

  let datapoint_size;

  if (width < height) datapoint_size = width/20;
  else                datapoint_size = height/20;
  
  push();
    translate (x, y, z);
      
      noStroke();
      emissiveMaterial (p5_color_zero.r, p5_color_zero.g, p5_color_zero.b);
      sphere (datapoint_size);

  pop();
}



function drawPerceptronDataPointPosition (iteration_datapoint_x_pos, c, selected)
{
  // position in X,Y and Z
  const x = 0;
  const y = 0;
  const z = 0;

  // rotation from 0 to 360 in X, Y and Z
  const rX = 0;
  const rY = 0;
  const rZ = map (iteration_datapoint_x_pos, -game_board_size.w/2, game_board_size.w/2, -45, 45);

  if (selected) 
    emissiveMaterial (c.r, c.g, c.b, 100);
  else 
    emissiveMaterial (c.r, c.g, c.b, 60);

  let datapoint_size;

  if (width < height) datapoint_size = width/20;
  else                datapoint_size = height/20;
  

  push();
    translate (x, y, z);

    rotateX ( radians(rX) );
    rotateY ( radians(rY) );
    rotateZ ( radians(rZ) );
    
    push();
      translate (iteration_datapoint_x_pos, y, z);
        
      noStroke();
      sphere (datapoint_size);
    pop();

  pop();
}
