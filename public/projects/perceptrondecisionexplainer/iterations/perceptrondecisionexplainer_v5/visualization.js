







function setup() 
{
  setAttributes("antialias", true);
  const canvas = createCanvas (p5_container_size.w, p5_container_size.h, WEBGL); // See DOM_functions.js
  canvas.parent("p5_container");
  colorMode (RGB, 255, 255, 255, 100);

  re_startSystem();
}


function draw() 
{
  // falta o timer

  background (p5_color_minus_2.r, p5_color_minus_2.g, p5_color_minus_2.b);


  switch (visualization_moment) 
  {
    case 1:
      positionSelectMoment(); // visualization_moments.js
      break;

    case 2:
      goalSelectMoment();
      break;

    case 3:
      iterationPerceptronSelectMoment();
      break;

    case 4:
      iterationPerceptronSelectResult();
      break;

    default: console.log ("erro"); // corrigir...
  }


  drawUserPickedDataPointPosition (user_select_x);

  for (let i=0; i<iterations_datapoint_x_pos.length; i++)
  {
    if (i === getIterationsDOMinputRangeSliderValue()) 
    {
      drawPerceptronDataPointPosition (iterations_datapoint_x_pos[i], iterations_datapoint_color[i], true);
    }
    else 
    {
      drawPerceptronDataPointPosition (iterations_datapoint_x_pos[i], iterations_datapoint_color[i], false);
    }
  }

  
  if (num_iterations > 0) 
  {
    if (perceptron_instance.getTrainingError() === 0) 
    {
      let i = getIterationsDOMinputRangeSliderValue();
      updateXposWeightDOMinputRangeSlider (iterations_datapoint_input_x_position_weight_value[i]);
      updateBiasWeightDOMinputRangeSlider (iterations_datapoint_input_bias_weight_value[i]);
      drawGameBoardWith (user_select_goal, iterations_datapoint_x_pos[i], iterations_gameboard_color[i], true);

      if (iterations_gameboard_color[i].r === 157)
        document.querySelector(':root').style.setProperty('--color_DOM_input_range_slider_weights_thumb', 'rgb(157, 2, 8)');
      else 
      if (iterations_gameboard_color[i].r === 39)
        document.querySelector(':root').style.setProperty('--color_DOM_input_range_slider_weights_thumb', 'rgb(39, 163, 0)');
    }
    else
    {
      updateXposWeightDOMinputRangeSlider (iterations_datapoint_input_x_position_weight_value[num_iterations-1]);
      updateBiasWeightDOMinputRangeSlider (iterations_datapoint_input_bias_weight_value[num_iterations-1]);
      drawGameBoardWith (user_select_goal, iterations_datapoint_x_pos[num_iterations-1], iterations_gameboard_color[num_iterations-1], true);

      if (iterations_gameboard_color[num_iterations-1].r === 157)
        document.querySelector(':root').style.setProperty('--color_DOM_input_range_slider_weights_thumb', 'rgb(157, 2, 8)');
      else 
      if (iterations_gameboard_color[num_iterations-1].r === 39)
        document.querySelector(':root').style.setProperty('--color_DOM_input_range_slider_weights_thumb', 'rgb(39, 163, 0)');
    }
  }
  else 
  {
    drawGameBoardWith (user_select_goal, 0, color(0, 0, 0), false);
  }
}



function mouseReleased() 
{

  switch (visualization_moment) 
  {
    case 1:
      positionSelectResult();
      break;

    case 2:
      goalSelectResult();
      break;

    case 3:
      break;

    case 4:
      break;

    default: console.log ("erro"); // corrigir...
  }

}
