


class Perceptron 
{

  constructor (num_weights) 
  {
    this.weights = new Array (num_weights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = Math.random()*2 - 1;
      console.log (this.weights[i]);
    }

    this.learning_rate = 0.0005;
  }



  getLearningRate() 
  {
    return this.learning_rate;
  }



  getWeightsValues() 
  {
    return this.weights;
  }



  runFeedForwardWith (inputs)
  {
    this.inputs = inputs;
    this.sum_weighted_inputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs += this.inputs[i] * this.weights[i];
    }

    return this.sum_weighted_inputs;
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs > 0) this.activation_result =  1; // Right Quad
    else                              this.activation_result = -1; // Left  Quad

    return this.activation_result;
  }



  runTrainingWith (goal) 
  {
    this.training_error = goal - this.activation_result;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * this.inputs[i];
    }

    return this.training_error;
  }



  runWith (inputs, goal) 
  {
    const temp = this.getWeightsValues();
    this.iteration = new Map();
    this.iteration.set( "weights", temp);
    this.iteration.set( "feed_forward",   this.runFeedForwardWith (inputs) );
    this.iteration.set( "activation",     this.runActivationFunction()     );
    this.iteration.set( "training_error", this.runTrainingWith (goal)      );
  }



  getIterationValues() 
  {
    return this.iteration;
  }

}
