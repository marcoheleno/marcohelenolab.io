


let canvas_container_width, canvas_container_height;
let visualization_moment;
let data_set, iterations_set, perceptron_instance;
let iteration_index;



function setResponsive() 
{
  document.body.style.width  = window.innerWidth +"px";
  document.body.style.height = window.innerHeight+"px";
  
  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth+"px";
  
  canvas_container_width  = document.getElementById("p5_canvas_container").offsetWidth;
  canvas_container_height = document.getElementById("p5_canvas_container").offsetHeight;
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();

  visualization_moment = 1;
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas = createCanvas (canvas_container_width, canvas_container_height, WEBGL);
  canvas.parent("p5_canvas_container");
  colorMode (RGB, 255, 255, 255, 100);

  iteration_index = 0;
}



function windowResized() 
{
  setResponsive();
  resizeCanvas (canvas_container_width, canvas_container_height);
}



function draw() 
{
  defineColorVariables();
  background (color_neg_2);
  
  
  switch (visualization_moment) 
  {
    case 1:
      data_set = generateRandomDataSet (1, -width/2, width/2);
      perceptron_instance = new Perceptron (data_set[data_set.length-1].inputs.length); // See Perceptron.js

      iterations_set = new Array (data_set.length);
      for (let i=0; i<iterations_set.length; i++)
      {
        iterations_set[i] = new Array (0);
      }

      visualization_moment = 2;

      break;

    case 2:
      iterations_set[0][iteration_index] = new Array (4);
      iterations_set[0][iteration_index][0] = perceptron_instance.getWeightsValues();
      iterations_set[0][iteration_index][1] = perceptron_instance.runFeedForwardWith (data_set[0].inputs);
      iterations_set[0][iteration_index][2] = perceptron_instance.runActivationFunction();


      
      iterations_set[0][iteration_index][3] = perceptron_instance.runTrainingWith (data_set[0].goal);


      console.log ("--------------");
      console.log ("i " + iteration_index);
      console.log (iterations_set[0][0][0][0]);
      console.log (iterations_set[0][0][0][1]);

      if (iterations_set[0][iteration_index][3] === 0) visualization_moment = 3;
      else iteration_index++;

      break;

    default: console.log ("Switch conditional out of case."); // corrigir...
  }

}



function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
