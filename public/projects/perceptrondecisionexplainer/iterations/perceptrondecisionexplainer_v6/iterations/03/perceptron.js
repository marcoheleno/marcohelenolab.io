


class Perceptron 
{

  constructor (num_weights) 
  {
    this.weights = new Array (num_weights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = Math.random()*2 - 1;
    }

    this.learning_rate = 0.0005;
    this.iteration = new Array (0);

    this.weights[0] = 0.5;
    this.weights[1] = -0.3;
    console.log (this.weights[0]);
    console.log (this.weights[1]);
  }



  getLearningRate() 
  {
    return this.learning_rate;
  }



  runFeedForwardWith (data_point)
  {
    if (this.iteration.length === 0 || this.iteration[this.iteration.length-1][4] != 0) 
    {
      this.iteration.push (new Array(6));
    }

    this.iteration[this.iteration.length-1][0] = data_point.index;
    this.iteration[this.iteration.length-1][1] = this.weights[0];
    this.iteration[this.iteration.length-1][2] = this.weights[1];

    this.sum_weighted_inputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs += data_point.inputs[i] * this.weights[i];
    }

    this.iteration[this.iteration.length-1][3] = this.sum_weighted_inputs;
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs > 0) this.activation_result =  1;
    else                              this.activation_result = -1;

    this.iteration[this.iteration.length-1][4] = this.activation_result;
  }



  runTrainingWith (data_point) 
  {
    this.training_error = data_point.goal - this.activation_result;

    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * data_point.inputs[i];
    }

    this.iteration[this.iteration.length-1][5] = this.training_error;
  }



  getIteration()
  {
    return this.iteration[this.iteration.length-1];
  }

}
