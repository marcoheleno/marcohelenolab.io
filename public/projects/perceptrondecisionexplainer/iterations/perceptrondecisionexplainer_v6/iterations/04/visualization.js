


let canvas_container_width, canvas_container_height;
let visualization_moment;
let data_set, perceptron_instance;



function setResponsive() 
{
  document.body.style.width  = window.innerWidth +"px";
  document.body.style.height = window.innerHeight+"px";
  
  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth+"px";
  
  canvas_container_width  = document.getElementById("p5_canvas_container").offsetWidth;
  canvas_container_height = document.getElementById("p5_canvas_container").offsetHeight;
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();

  visualization_moment = 1;
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas = createCanvas (canvas_container_width, canvas_container_height, WEBGL);
  canvas.parent("p5_canvas_container");
  colorMode (RGB, 255, 255, 255, 100);
}



function windowResized() 
{
  setResponsive();
  resizeCanvas (canvas_container_width, canvas_container_height);
}



function draw() 
{
  defineColorVariables();
  background (color_neg_2);
  
  
  switch (visualization_moment) 
  {
    case 1:
      data_set = generateRandomDataSet (1, -width/2, width/2);
      perceptron_instance = new Perceptron (data_set[data_set.length-1].inputs.length);
      visualization_moment = 2;
      break;

    case 2:
      perceptron_instance.runWith (data_set[data_set.length-1]);
      
      console.log (perceptron_instance.getIteration().weight_x);
      console.log (perceptron_instance.getIteration().weight_bias);

      if (perceptron_instance.getIteration().training_error === 0) visualization_moment = 3;
      break;

    default: console.log ("Switch conditional out of case.");
  }

}



function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
