


class Perceptron 
{

  constructor (num_weights) 
  {
    this.weights = new Array (num_weights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = Math.random()*2 - 1;
    }

    this.learning_rate = 0.0005;
    this.iterations = new Array (0);
  }



  getLearningRate() 
  {
    return this.learning_rate;
  }



  runFeedForwardWith (inputs)
  {
    this.sum_weighted_inputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs += inputs[i] * this.weights[i];
    }

    return this.sum_weighted_inputs;
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs > 0) this.activation_result =  1;
    else                              this.activation_result = -1;

    return this.activation_result;
  }



  runTrainingWith (inputs, goal) 
  {
    this.training_error = goal - this.activation_result;

    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * inputs[i];
    }

    return this.training_error;
  }



  runWith (data_point) 
  {
    this.iteration_values = {};

    this.iteration_values.data_point_index = data_point.index;

    this.iteration_values.weight_x    = this.weights[0];
    this.iteration_values.weight_bias = this.weights[1];

    this.iteration_values.feed_forward_result = this.runFeedForwardWith (data_point.inputs);
    this.iteration_values.activation_result   = this.runActivationFunction();
    this.iteration_values.training_error      = this.runTrainingWith (data_point.inputs, data_point.goal);

    this.saveIteration();
  }



  saveIteration() 
  {
    if (
        this.iterations.length === 0               || 
        this.iteration_values.training_error != 0  || 
        this.iteration_values.training_error === 0 && 
        this.iterations[this.iterations.length-2] != 0
       ) 
    {
      this.iterations.push (this.iteration_values);
    }
  }



  getIteration()
  {
    return this.iterations[this.iterations.length-1];
  }

}
