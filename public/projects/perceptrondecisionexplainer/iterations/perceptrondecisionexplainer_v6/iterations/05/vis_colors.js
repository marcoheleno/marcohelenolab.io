


let themes_colors, switch_color_theme, active_color_theme, lerp_i;
let trained_color, untrained_color;
let color_neg_2, color_neg_1, color_zero, color_pos_1, color_pos_2;



function defineColorThemes() 
{
  themes_colors = new Array (2);

  // dark color theme
  themes_colors[0] = new Array(5);
  themes_colors[0][0] = color (0, 0, 0);       // color_neg_2
  themes_colors[0][1] = color (50, 50, 50);    // color_neg_1
  themes_colors[0][2] = color (115, 115, 115); // color_zero
  themes_colors[0][3] = color (210, 210, 210); 
  themes_colors[0][4] = color (250, 250, 250); // color_pos_2
  
  // light color theme
  themes_colors[1] = new Array(5);
  themes_colors[1][0] = color (250, 250, 250); // color_neg_2
  themes_colors[1][1] = color (210, 210, 210); // color_neg_1
  themes_colors[1][2] = color (190, 190, 190); // color_zero
  themes_colors[1][3] = color (50, 50, 50);    
  themes_colors[1][4] = color (0, 0, 0);       // color_pos_2
}



function defineColorVariables ()
{
  if (lerp_i == null) lerp_i = 0;
  if (switch_color_theme == null) switch_color_theme = true;
  if (active_color_theme == null) active_color_theme = false;

  if (lerp_i === 0 && switch_color_theme) active_color_theme = !active_color_theme;

  if (lerp_i < 1.01 && switch_color_theme) 
  {
    const lerp_speed = 0.04;


    trained_color   = color (0, 255, 0);
    untrained_color = color (255, 0, 0);

    color_neg_2 = lerpColor (themes_colors[int(!active_color_theme)][0], themes_colors[int(active_color_theme)][0], lerp_i);
    document.querySelector(':root').style.setProperty("--color_neg_2", "rgb("+red(color_neg_2)+","+green(color_neg_2)+","+blue(color_neg_2)+")");

    color_neg_1 = lerpColor (themes_colors[int(!active_color_theme)][1], themes_colors[int(active_color_theme)][1], lerp_i);
    document.querySelector(':root').style.setProperty("--color_neg_1", "rgb("+red(color_neg_1)+","+green(color_neg_1)+","+blue(color_neg_1)+")");

    color_zero = lerpColor(themes_colors[int(!active_color_theme)][2], themes_colors[int(active_color_theme)][2], lerp_i);
    document.querySelector(':root').style.setProperty("--color_zero", "rgb("+red(color_zero)+","+green(color_zero)+","+blue(color_zero)+")");

    color_pos_1 = lerpColor (themes_colors[int(!active_color_theme)][3], themes_colors[int(active_color_theme)][3], lerp_i);
    document.querySelector(':root').style.setProperty("--color_pos_1", "rgb("+red(color_pos_1)+","+green(color_pos_1)+","+blue(color_pos_1)+")");

    color_pos_2 = lerpColor (themes_colors[int(!active_color_theme)][4], themes_colors[int(active_color_theme)][4], lerp_i);
    document.querySelector(':root').style.setProperty("--color_pos_2", "rgb("+red(color_pos_2)+","+green(color_pos_2)+","+blue(color_pos_2)+")");
    

    if (lerp_i > 1) 
    {
      lerp_i = 0;
      switch_color_theme = !switch_color_theme;
    }
    else lerp_i += lerp_speed;
  }
}
