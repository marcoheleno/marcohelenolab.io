


//



function drawVizIteration (data_set, current_data_index, current_perceptron_instance_feed_forward_result) 
{
  const viz_stage_w = width;
  const viz_stage_h = height;

  const viz_iteration_stage_w = viz_stage_w;
  const viz_iteration_stage_h = viz_stage_h/data_set.length;
  const viz_iteration_stage_x = 0;
  const viz_iteration_stage_y = -viz_stage_h/2 + viz_iteration_stage_h*current_data_index + viz_iteration_stage_h/2;
  const viz_iteration_stage_rY = map(current_perceptron_instance_feed_forward_result, -viz_iteration_stage_w/2, viz_iteration_stage_w/2, -40, 40);

  drawEachVizStage (viz_iteration_stage_x, viz_iteration_stage_y, viz_iteration_stage_w, viz_iteration_stage_h, viz_iteration_stage_rY);
}



function drawEachVizStage (x, y, w, h, rY) 
{
  push();
    translate (x, y, 0);
    rotateY ( radians(rY) );

      beginShape();
        vertex (-w/2, -h/2, 0);
        vertex (   0, -h/2, 0);
        vertex (   0,  h/2, 0);
        vertex (-w/2,  h/2, 0);
      endShape (CLOSE);
  
      beginShape();
        vertex (   0, -h/2, 0);
        vertex ( w/2, -h/2, 0);
        vertex ( w/2,  h/2, 0);
        vertex (   0,  h/2, 0);
      endShape (CLOSE);
  
  pop();
}
