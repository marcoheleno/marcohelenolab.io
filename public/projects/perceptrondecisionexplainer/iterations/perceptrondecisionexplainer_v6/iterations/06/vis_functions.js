


function drawEachVizStage (stage_x, stage_y, stage_w, stage_h, stage_rY, data_g) 
{
  push();
    translate (stage_x, stage_y, 0);
    rotateY( radians(stage_rY) );

      stroke (color_neg_2);
      fill (color_zero);

      beginShape();
        vertex (-stage_w/2, -stage_h/2, 0);
        vertex (   0, -stage_h/2, 0);
        vertex (   0,  stage_h/2, 0);
        vertex (-stage_w/2,  stage_h/2, 0);
      endShape (CLOSE);
  
      beginShape();
        vertex (   0, -stage_h/2, 0);
        vertex ( stage_w/2, -stage_h/2, 0);
        vertex ( stage_w/2,  stage_h/2, 0);
        vertex (   0,  stage_h/2, 0);
      endShape (CLOSE);


      noStroke();
      if (data_g === -1) 
      {
        for (let i=0; i>-stage_w/8; i--) 
        {
          const transparency = map (i, 0, -stage_w/8, 100, 0);
          fill (red(trained_color), green(trained_color), blue(trained_color), transparency);
          
          push();
            translate (i, 0, 0);

              beginShape();
                vertex (-1, -stage_h/2, 0);
                vertex ( 0, -stage_h/2, 0);
                vertex ( 0,  stage_h/2, 0);
                vertex (-1,  stage_h/2, 0);
              endShape (CLOSE);

          pop();
        }

        for (let i=0; i<stage_w/8; i++) 
        {
          const transparency = map (i, 0, stage_w/8, 100, 0);
          fill (red(untrained_color), green(untrained_color), blue(untrained_color), transparency);
          
          push();
            translate (i, 0, 0);

              beginShape();
                vertex (0, -stage_h/2, 0);
                vertex (1, -stage_h/2, 0);
                vertex (1,  stage_h/2, 0);
                vertex (0,  stage_h/2, 0);
              endShape (CLOSE);
              
          pop();
        }
      }



      if (data_g === 1) 
      {
        for (let i=0; i>-stage_w/8; i--) 
        {
          const transparency = map (i, 0, -stage_w/8, 100, 0);
          fill (red(untrained_color), green(untrained_color), blue(untrained_color), transparency);
          
          push();
            translate (i, 0, 0);

              beginShape();
                vertex (-1, -stage_h/2, 0);
                vertex ( 0, -stage_h/2, 0);
                vertex ( 0,  stage_h/2, 0);
                vertex (-1,  stage_h/2, 0);
              endShape (CLOSE);

          pop();
        }

        for (let i=0; i<stage_w/8; i++) 
        {
          const transparency = map (i, 0, stage_w/8, 100, 0);
          fill (red(trained_color), green(trained_color), blue(trained_color), transparency);
          
          push();
            translate (i, 0, 0);

              beginShape();
                vertex (0, -stage_h/2, 0);
                vertex (1, -stage_h/2, 0);
                vertex (1,  stage_h/2, 0);
                vertex (0,  stage_h/2, 0);
              endShape (CLOSE);
              
          pop();
        }
      }
  
  pop();
}



function drawEachDataPoint (stage_x, stage_y, stage_h, stage_rY, data_x) 
{
  push();
    translate (stage_x, stage_y, 1);
    rotateY( radians(stage_rY) );

    push();
      translate (data_x, 0, 0);

        noFill();
        stroke (color_pos_1);

        strokeWeight (1);
        circle (0, 0, stage_h-(stage_h/4));

        strokeWeight (10);
        point (0, 0);
    pop();

  pop();
}



function drawEachDecisionIteration (stage_x, stage_y, stage_h, stage_rY, decision_x) 
{
  push();
    translate (stage_x, stage_y, 2);
    rotateY( radians(stage_rY) );

    push();
      translate (decision_x, 0, 0);

        noFill();
        stroke (color_pos_2);

        strokeWeight (1);
        circle (0, 0, stage_h-(stage_h/4));

        strokeWeight (10);
        point (0, 0);
    pop();

  pop();
}
