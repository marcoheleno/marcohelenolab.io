


let data_set;
let perceptron, perceptron_trained_with, perceptron_iteration_set;



function setResponsive() 
{
  document.body.style.width  = window.innerWidth +"px";
  document.body.style.height = window.innerHeight+"px";
  
  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth+"px";
  
  const canvas_container_dimensions = 
  {
    w: document.getElementById("p5_canvas_container").offsetWidth, 
    h: document.getElementById("p5_canvas_container").offsetHeight
  };
  
  return canvas_container_dimensions;
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  const canvas_container_dimensions = setResponsive();
  setAttributes("antialias", true);
  const canvas = createCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h, WEBGL);
  canvas.parent("p5_canvas_container");
  colorMode (RGB, 255, 255, 255, 100);

  frameRate(2);
}



function windowResized() 
{
  const canvas_container_dimensions = setResponsive();
  resizeCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h);
}



function draw() 
{
  defineColorVariables();

  background (color_neg_2);


  if (data_set == null || perceptron == null) 
  {
    data_set = generateRandomDataSet (10, -width/2, width/2);
    perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);
    perceptron_trained_with = 0;
  }
  
  
  for (let i=0; i<data_set.length; i++) 
  {

    if (perceptron_trained_with < data_set.length)
    {
      perceptron.runWith (data_set[i]);

      /*
      console.log (perceptron.getIteration().data_point_index);
      console.log (perceptron.getIteration().weight_x);
      console.log (perceptron.getIteration().weight_bias);
      console.log (perceptron.getIteration().feedforward_result);
      console.log (perceptron.getIteration().activation_result);
      console.log (perceptron.getIteration().training_error);
      console.log ("-----");
      */
      
      if (perceptron.getIteration().training_error === 0) 
      {
        perceptron_trained_with++;
      }

      if (i === data_set.length-1 && perceptron_trained_with < data_set.length)
      {
        perceptron_trained_with = 0;
      }
    }

    const iteration_stage_w = width;
    const iteration_stage_h = height/data_set.length;
    const iteration_stage_x = 0;
    const iteration_stage_y = -height/2 + iteration_stage_h*i + iteration_stage_h/2;

    const iteration_feedforward_result = perceptron.getIteration().feedforward_result;

    let iteration_stage_rY = map(iteration_feedforward_result, -iteration_stage_w/2, iteration_stage_w/2, -40, 40);
    if (perceptron.getIteration().training_error === 0) iteration_stage_rY = 0;

    drawEachVizStage  (iteration_stage_x, iteration_stage_y, iteration_stage_w, iteration_stage_h, iteration_stage_rY, data_set[i].goal);
    drawEachDataPoint (iteration_stage_x, iteration_stage_y, iteration_stage_h, iteration_stage_rY, data_set[i].inputs[0]);

    perceptron_iteration_set = perceptron.getIterationsSet();
  }


  for (let i=0; i<perceptron_iteration_set.length; i++) 
  {
    const data_point_index = perceptron_iteration_set[i].data_point_index;
    const iteration_feedforward_result = perceptron_iteration_set[i].feedforward_result;

    const iteration_stage_w = width;
    const iteration_stage_h = height/data_set.length;
    const iteration_stage_x = 0;
    const iteration_stage_y = -height/2 + iteration_stage_h*data_point_index + iteration_stage_h/2;

    let iteration_stage_rY = map(iteration_feedforward_result, -iteration_stage_w/2, iteration_stage_w/2, -40, 40);
    if (perceptron_trained_with === data_set.length) iteration_stage_rY = 0;

    drawEachDecisionIteration (iteration_stage_x, iteration_stage_y, iteration_stage_h, iteration_stage_rY, iteration_feedforward_result);
  }

}



function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
