


let stages_graphics, data_points_graphics, decision_iterations_graphics;



function createEachVizElement (stage_w, stage_h) 
{
  if (stages_graphics == null) stages_graphics = new Array(0);
  if (data_points_graphics == null) data_points_graphics = new Array(0);
  if (decision_iterations_graphics == null) decision_iterations_graphics = new Array(0);

  stages_graphics.push( createGraphics (stage_w, stage_h) );
  data_points_graphics.push( createGraphics (stage_w, stage_h) );
  decision_iterations_graphics.push( createGraphics (stage_w, stage_h) );
}



function drawEachVizStage (data_point_index, stage_x, stage_y, stage_rY, data_g) 
{
  const gradiente_width = stages_graphics[data_point_index].width/2;

  stages_graphics[data_point_index].clear();
  //stages_graphics[data_point_index].background(color_zero);
  stages_graphics[data_point_index].strokeWeight (1);
  stages_graphics[data_point_index].strokeCap (SQUARE);

  if (data_g === -1) 
  {
    for (let i=stages_graphics[data_point_index].width/2; i>(stages_graphics[data_point_index].width/2-gradiente_width); i--) 
    {
      const transparency = map (i, stages_graphics[data_point_index].width/2, stages_graphics[data_point_index].width/2-gradiente_width, 255, 0);
      stages_graphics[data_point_index].stroke (red(goal_color), green(goal_color), blue(goal_color), transparency);
      stages_graphics[data_point_index].line (i, 0, i, stages_graphics[data_point_index].height);
    }

    for (let i=stages_graphics[data_point_index].width/2; i<(stages_graphics[data_point_index].width/2+gradiente_width); gs++) 
    {
      const transparency = map (i, stages_graphics[data_point_index].width/2, stages_graphics[data_point_index].width/2+gradiente_width, 255, 0);
      stages_graphics[data_point_index].stroke (red(non_goal_color), green(non_goal_color), blue(non_goal_color), transparency);
      stages_graphics[data_point_index].line (i, 0, i, stages_graphics[data_point_index].height);
    }
  }


  if (data_g === 1) 
  {
    for (let i=stages_graphics[data_point_index].width/2; i>(stages_graphics[data_point_index].width/2-gradiente_width); i--) 
    {
      const transparency = map (i, stages_graphics[data_point_index].width/2, stages_graphics[data_point_index].width/2-gradiente_width, 255, 0);
      stages_graphics[data_point_index].stroke (red(non_goal_color), green(non_goal_color), blue(non_goal_color), transparency);
      stages_graphics[data_point_index].line (i, 0, i, stages_graphics[data_point_index].height);
    }

    for (let i=stages_graphics[data_point_index].width/2; i<(stages_graphics[data_point_index].width/2+gradiente_width); gs++) 
    {
      const transparency = map (i, stages_graphics[data_point_index].width/2, stages_graphics[data_point_index].width/2+gradiente_width, 255, 0);
      stages_graphics[data_point_index].stroke (red(goal_color), green(goal_color), blue(goal_color), transparency);
      stages_graphics[data_point_index].line (i, 0, i, stages_graphics[data_point_index].height);
    }
  }
  

  push();
    translate (stage_x, stage_y, 0);
    rotateY( radians(stage_rY) );

      stroke (color_neg_2);
      strokeWeight (1);

      textureMode (NORMAL);
      texture (stages_graphics[data_point_index]);

      beginShape();
        vertex (-stages_graphics[data_point_index].width/2, -stages_graphics[data_point_index].height/2, 0, 0, 0);
        vertex ( stages_graphics[data_point_index].width/2, -stages_graphics[data_point_index].height/2, 0, 1, 0);
        vertex ( stages_graphics[data_point_index].width/2,  stages_graphics[data_point_index].height/2, 0, 1, 1);
        vertex (-stages_graphics[data_point_index].width/2,  stages_graphics[data_point_index].height/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}



function drawEachDataPoint (data_point_index, stage_x, stage_y, stage_rY, data_x) 
{
  const x = map(data_x, -width/2, width/2, 0, data_points_graphics[data_point_index].width);

  data_points_graphics[data_point_index].clear();

  data_points_graphics[data_point_index].noFill();
  data_points_graphics[data_point_index].strokeCap (SQUARE);
  data_points_graphics[data_point_index].stroke (color_pos_1);

  data_points_graphics[data_point_index].strokeWeight (1);
  data_points_graphics[data_point_index].circle (x, data_points_graphics[data_point_index].height/2, data_points_graphics[data_point_index].height-(data_points_graphics[data_point_index].height/4));

  data_points_graphics[data_point_index].strokeWeight (10);
  data_points_graphics[data_point_index].point  (x, data_points_graphics[data_point_index].height/2);

  push();
    translate (stage_x, stage_y, 1);
    rotateY( radians(stage_rY) );

      noStroke();

      textureMode (NORMAL);
      texture (data_points_graphics[data_point_index]);

      beginShape();
        vertex (-data_points_graphics[data_point_index].width/2, -data_points_graphics[data_point_index].height/2, 0, 0, 0);
        vertex ( data_points_graphics[data_point_index].width/2, -data_points_graphics[data_point_index].height/2, 0, 1, 0);
        vertex ( data_points_graphics[data_point_index].width/2,  data_points_graphics[data_point_index].height/2, 0, 1, 1);
        vertex (-data_points_graphics[data_point_index].width/2,  data_points_graphics[data_point_index].height/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}



function drawEachDecisionIteration (data_point_index, stage_x, stage_y, stage_rY, decision_x) 
{
  const x = map(decision_x, -width/2, width/2, 0, decision_iterations_graphics[data_point_index].width);

  decision_iterations_graphics[data_point_index].clear();

  decision_iterations_graphics[data_point_index].noFill();
  decision_iterations_graphics[data_point_index].strokeCap (SQUARE);
  decision_iterations_graphics[data_point_index].stroke (color_pos_2);

  decision_iterations_graphics[data_point_index].strokeWeight (1);
  decision_iterations_graphics[data_point_index].circle (x, decision_iterations_graphics[data_point_index].height/2, decision_iterations_graphics[data_point_index].height-(decision_iterations_graphics[data_point_index].height/4));

  decision_iterations_graphics[data_point_index].strokeWeight (10);
  decision_iterations_graphics[data_point_index].point  (x, decision_iterations_graphics[data_point_index].height/2);

  push();
    translate (stage_x, stage_y, 2);
    rotateY( radians(stage_rY) );

      noStroke();

      textureMode (NORMAL);
      texture (decision_iterations_graphics[data_point_index]);

      beginShape();
        vertex (-decision_iterations_graphics[data_point_index].width/2, -decision_iterations_graphics[data_point_index].height/2, 0, 0, 0);
        vertex ( decision_iterations_graphics[data_point_index].width/2, -decision_iterations_graphics[data_point_index].height/2, 0, 1, 0);
        vertex ( decision_iterations_graphics[data_point_index].width/2,  decision_iterations_graphics[data_point_index].height/2, 0, 1, 1);
        vertex (-decision_iterations_graphics[data_point_index].width/2,  decision_iterations_graphics[data_point_index].height/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}
