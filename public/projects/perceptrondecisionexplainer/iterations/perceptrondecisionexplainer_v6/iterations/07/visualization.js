


let data_set;
let perceptron, perceptron_trained_with;



function setResponsive() 
{
  const inner_window_dimensions = 
  {
    w: window.innerWidth, 
    h: window.innerHeight
  };

  document.body.style.width  = inner_window_dimensions.w + "px";
  document.body.style.height = inner_window_dimensions.h + "px";


  if (inner_window_dimensions.w <= 1024 && inner_window_dimensions.w > inner_window_dimensions.h || 
      inner_window_dimensions.w <= 1024 && deviceOrientation === "landscape") 
  {
    //document.getElementsByTagName("main")[0].remove();
    //Adiciona um overlayer com a informação de por o dispositivo em modo portrait
  }

  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth + "px";

  const canvas_container_dimensions = 
  {
    w: document.getElementById("p5_canvas_container").offsetWidth, 
    h: document.getElementById("p5_canvas_container").offsetHeight
  };

  return canvas_container_dimensions
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas_container_dimensions = setResponsive();
  const canvas = createCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h, WEBGL);
  canvas.parent("p5_canvas_container");
}



function windowResized() 
{
  const canvas_container_dimensions = setResponsive();
  resizeCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h);
}



function draw() 
{
  defineColorVariables();

  background (color_neg_2);


  if (data_set == null || perceptron == null) 
  {
    data_set = generateRandomDataSet (8, -width/2, width/2);
    perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);
    perceptron_trained_with = 0;

    for (let i=0; i<data_set.length; i++) 
    {
      createEachVizElement (width, height/data_set.length);
    }
  }
  
  
  for (let i=0; i<data_set.length; i++) 
  {

    if (perceptron_trained_with < data_set.length)
    {
      perceptron.runWith (data_set[i]);

      /*
      console.log (perceptron.getIteration().data_point_index);
      console.log (perceptron.getIteration().weight_x);
      console.log (perceptron.getIteration().weight_bias);
      console.log (perceptron.getIteration().feedforward_result);
      console.log (perceptron.getIteration().activation_result);
      console.log (perceptron.getIteration().training_error);
      console.log ("-----");
      */
      
      if (perceptron.getIteration().training_error === 0) 
      {
        perceptron_trained_with++;
      }

      if (i === data_set.length-1 && perceptron_trained_with < data_set.length)
      {
        perceptron_trained_with = 0;
      }
    }

    const iteration_x = 0;
    const iteration_y = -height/2 + (height/data_set.length)*i + (height/data_set.length)/2;
    const iteration_rY = map(perceptron.getIteration().feedforward_result, -width/2, width/2, -40, 40);

    drawEachVizStage  (i, iteration_x, iteration_y, iteration_rY, data_set[i].goal);
    drawEachDataPoint (i, iteration_x, iteration_y, iteration_rY, data_set[i].inputs[0]);

    drawEachDecisionIteration (i, iteration_x, iteration_y, iteration_rY, perceptron.getIteration().feedforward_result);
  }


  const perceptron_iteration_set = perceptron.getIterationsSet();


  for (let i=0; i<perceptron_iteration_set.length; i++) 
  {
    const iteration_x = 0;
    const iteration_y = -height/2 + (height/data_set.length)*perceptron_iteration_set[i].data_point_index + (height/data_set.length)/2;
    const iteration_rY = map(perceptron_iteration_set[i].feedforward_result, -width/2, width/2, -40, 40);

    //drawEachDecisionIteration (perceptron_iteration_set[i].data_point_index, iteration_x, iteration_y, iteration_rY, perceptron_iteration_set[i].feedforward_result);
  }
    
}



function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
