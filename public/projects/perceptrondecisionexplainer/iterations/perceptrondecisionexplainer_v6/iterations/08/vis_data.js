


function generateRandomDataSet (num_data_points, lower_bound, upper_bound) 
{
  const threshold = 0;
  const data_set = new Array (num_data_points);

  for (let i=0; i<data_set.length; i++) 
  {
    const x = Math.random() * (upper_bound - (lower_bound)) + (lower_bound);

    let goal = -1;
    if (x > threshold) goal = 1;
    
    data_set[i] = 
    {
      index: i, 
      inputs: [x, 1], 
      goal: goal
    };
  }

  return data_set;
}
