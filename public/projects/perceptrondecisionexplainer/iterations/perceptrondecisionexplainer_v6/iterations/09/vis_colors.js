


let themes_colors, switch_color_theme, active_color_theme;
let goal_color, non_goal_color;
let color_neg_2, color_neg_1, color_zero, color_pos_1, color_pos_2;



function defineColorThemes() 
{
  themes_colors = new Array (2);

  // dark color theme
  themes_colors[0] = new Array(5);
  themes_colors[0][0] = color (0, 0, 0);       // color_neg_2
  themes_colors[0][1] = color (50, 50, 50);    // color_neg_1
  themes_colors[0][2] = color (115, 115, 115); // color_zero
  themes_colors[0][3] = color (150, 150, 150); // color_pos_1
  themes_colors[0][4] = color (250, 250, 250); // color_pos_2
  
  // light color theme
  themes_colors[1] = new Array(5);
  themes_colors[1][0] = color (250, 250, 250); // color_neg_2
  themes_colors[1][1] = color (210, 210, 210); // color_neg_1
  themes_colors[1][2] = color (190, 190, 190); // color_zero
  themes_colors[1][3] = color (120, 120, 120); // color_pos_1
  themes_colors[1][4] = color (0, 0, 0);       // color_pos_2

  switch_color_theme = true;
  active_color_theme = true;
}



function defineColorVariables ()
{
  if (switch_color_theme) active_color_theme = !active_color_theme;

  if (switch_color_theme) 
  {
    goal_color     = color (0, 255, 0);
    non_goal_color = color (255, 0, 0);

    color_neg_2 = themes_colors[int(active_color_theme)][0]
    document.querySelector(':root').style.setProperty("--color_neg_2", "rgb("+red(color_neg_2)+","+green(color_neg_2)+","+blue(color_neg_2)+")");

    color_neg_1 = themes_colors[int(active_color_theme)][1]
    document.querySelector(':root').style.setProperty("--color_neg_1", "rgb("+red(color_neg_1)+","+green(color_neg_1)+","+blue(color_neg_1)+")");

    color_zero = themes_colors[int(active_color_theme)][2]
    document.querySelector(':root').style.setProperty("--color_zero", "rgb("+red(color_zero)+","+green(color_zero)+","+blue(color_zero)+")");

    color_pos_1 = themes_colors[int(active_color_theme)][3]
    document.querySelector(':root').style.setProperty("--color_pos_1", "rgb("+red(color_pos_1)+","+green(color_pos_1)+","+blue(color_pos_1)+")");

    color_pos_2 = themes_colors[int(active_color_theme)][4]
    document.querySelector(':root').style.setProperty("--color_pos_2", "rgb("+red(color_pos_2)+","+green(color_pos_2)+","+blue(color_pos_2)+")");

    switch_color_theme = false;
  }

}
