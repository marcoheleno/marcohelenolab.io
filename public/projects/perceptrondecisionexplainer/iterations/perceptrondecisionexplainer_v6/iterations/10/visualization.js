


let data_set, iteration_set;
let perceptron, perceptron_trained_with;
let j = 0;


function setResponsive() 
{
  const inner_window_dimensions = 
  {
    w: window.innerWidth, 
    h: window.innerHeight
  };

  document.body.style.width  = inner_window_dimensions.w + "px";
  document.body.style.height = inner_window_dimensions.h + "px";


  if (inner_window_dimensions.w <= 1024 && inner_window_dimensions.w > inner_window_dimensions.h || 
      inner_window_dimensions.w <= 1024 && deviceOrientation === "landscape") 
  {
    //document.getElementsByTagName("main")[0].remove();
    //Adiciona um overlayer com a informação de por o dispositivo em modo portrait
  }

  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth + "px";

  const canvas_container_dimensions = 
  {
    w: document.getElementById("p5_canvas_container").offsetWidth, 
    h: document.getElementById("p5_canvas_container").offsetHeight
  };

  return canvas_container_dimensions
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas_container_dimensions = setResponsive();
  const canvas = createCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h, WEBGL);
  canvas.parent("p5_canvas_container");
}



function windowResized() 
{
  const canvas_container_dimensions = setResponsive();
  resizeCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h);
}



function draw() 
{
  defineColorVariables();

  background (color_neg_2);


  if (data_set == null || perceptron == null) 
  {
    data_set = generateRandomDataSet (8, -width/2, width/2);

    for (let i=0; i<data_set.length; i++) 
    {
      createEachIterationGraphics (width, height/data_set.length);
      addGoalStageToEachIterationGraphics (i, data_set[i].goal);
      addDataPointToEachIterationGraphics (i, data_set[i].inputs[0]);
    }

    perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);
    perceptron_trained_with = 0;
  }

  let i = 0;
  while (perceptron_trained_with < data_set.length) 
  {
    perceptron.runWith (data_set[i]);

    /*
    console.log (perceptron.getIteration().data_point_index);
    console.log (perceptron.getIteration().weight_x);
    console.log (perceptron.getIteration().weight_bias);
    console.log (perceptron.getIteration().feedforward_result);
    console.log (perceptron.getIteration().activation_result);
    console.log (perceptron.getIteration().training_error);
    console.log ("-----");
    */

    //addDecisionIterationToEachIterationGraphics (i, perceptron.getIteration().feedforward_result);

    if (perceptron.getIteration().training_error === 0) 
    {
      perceptron_trained_with++;
    }

    if (i === data_set.length-1 && perceptron_trained_with < data_set.length)
    {
      i = 0;
      perceptron_trained_with = 0;
    }
    else i++;
  }
  

  iteration_set = perceptron.getIterationsSet();
  /*
  console.log (iteration_set[i].data_point_index);
  console.log (iteration_set[i].weight_x);
  console.log (iteration_set[i].weight_bias);
  console.log (iteration_set[i].feedforward_result);
  console.log (iteration_set[i].activation_result);
  console.log (iteration_set[i].training_error);
  console.log ("-----");
  */

  let jump = iteration_set.length/data_set.length;
  console.log (jump);

  for (let i=0; i<data_set.length; i++) 
  {
    const iteration_w = width;
    const iteration_h = height/data_set.length;
    const iteration_x = 0;
    const iteration_y = -height/2 + iteration_h*i + iteration_h/2;

    if (iteration_set[j].data_point_index === i) 
    {
      console.log (iteration_set[j].data_point_index + " / " + j);

      addDecisionIterationToEachIterationGraphics (i, iteration_set[j].feedforward_result);
      const iteration_rY = map(iteration_set[j].feedforward_result, -iteration_w/2, iteration_w/2, -90, 90);
      drawEachIterationGraphics (i, iteration_x, iteration_y, iteration_rY);

      j++;
      if (j >= iteration_set.length) 
      {
        //j = 0;
        noLoop();
      }
    }
  }

  console.log (j/data_set.length);
  console.log ("-------");
  
}



function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
