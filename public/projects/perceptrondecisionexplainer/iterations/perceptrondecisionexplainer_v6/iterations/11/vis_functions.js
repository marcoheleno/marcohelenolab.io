


let iteration_graphics;



function createEachIterationGraphics (stage_w, stage_h) 
{
  if (iteration_graphics == null) iteration_graphics = new Array(0);

  iteration_graphics.push( createGraphics (stage_w, stage_h) );
}



function addGoalStageToEachIterationGraphics (i, data_g) 
{
  iteration_graphics[i].strokeWeight (1);
  iteration_graphics[i].stroke (color_neg_1);

  if (data_g === -1) 
  {
    iteration_graphics[i].fill (goal_color);
    iteration_graphics[i].rect (0, 0, iteration_graphics[i].width/2, iteration_graphics[i].height);

    iteration_graphics[i].fill (non_goal_color);
    iteration_graphics[i].rect (iteration_graphics[i].width/2, 0, iteration_graphics[i].width/2, iteration_graphics[i].height);
  }

  if (data_g === 1) 
  {
    iteration_graphics[i].fill (non_goal_color);
    iteration_graphics[i].rect (0, 0, iteration_graphics[i].width/2, iteration_graphics[i].height);

    iteration_graphics[i].fill (goal_color);
    iteration_graphics[i].rect (iteration_graphics[i].width/2, 0, iteration_graphics[i].width/2, iteration_graphics[i].height);
  }

}


 
function addDataPointToEachIterationGraphics (i, data_x) 
{
  const x = map(data_x, -width/2, width/2, 0, iteration_graphics[i].width);

  iteration_graphics[i].noFill();
  iteration_graphics[i].strokeCap (SQUARE);
  iteration_graphics[i].stroke (color_neg_2);

  iteration_graphics[i].strokeWeight (1);
  iteration_graphics[i].circle (x, iteration_graphics[i].height/2, iteration_graphics[i].height-(iteration_graphics[i].height/4));

  iteration_graphics[i].strokeWeight (5);
  iteration_graphics[i].point  (x, iteration_graphics[i].height/2);
}



function addDecisionIterationToEachIterationGraphics (i, decision_x) 
{
  const x = map(decision_x, -width/2, width/2, 0, iteration_graphics[i].width);

  iteration_graphics[i].noFill();
  iteration_graphics[i].strokeCap (SQUARE);
  iteration_graphics[i].stroke (color_neg_1);

  iteration_graphics[i].strokeWeight (0.1);
  iteration_graphics[i].circle (x, iteration_graphics[i].height/2, iteration_graphics[i].height-(iteration_graphics[i].height/4));

  iteration_graphics[i].strokeWeight (5);
  iteration_graphics[i].point  (x, iteration_graphics[i].height/2);
}



function drawEachIterationGraphics (i, stage_x, stage_y, stage_rY) 
{
  push();
    translate (stage_x, stage_y, 0);
    rotateY( radians(stage_rY) );

      noStroke();

      textureMode (NORMAL);
      texture (iteration_graphics[i]);

      beginShape();
        vertex (-iteration_graphics[i].width/2, -iteration_graphics[i].height/2, 0, 0, 0);
        vertex ( iteration_graphics[i].width/2, -iteration_graphics[i].height/2, 0, 1, 0);
        vertex ( iteration_graphics[i].width/2,  iteration_graphics[i].height/2, 0, 1, 1);
        vertex (-iteration_graphics[i].width/2,  iteration_graphics[i].height/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}