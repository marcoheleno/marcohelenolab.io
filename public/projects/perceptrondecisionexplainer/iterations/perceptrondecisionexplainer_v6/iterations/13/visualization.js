


let visualization_moment;
let data_set, iteration_set, iteration_index, iteration_set_ran_once;
let perceptron;



function setResponsive() 
{
  const inner_window_dimensions = 
  {
    w: window.innerWidth, 
    h: window.innerHeight
  };

  document.body.style.width  = inner_window_dimensions.w + "px";
  document.body.style.height = inner_window_dimensions.h + "px";


  if (inner_window_dimensions.w <= 1024 && inner_window_dimensions.w > inner_window_dimensions.h || 
      inner_window_dimensions.w <= 1024 && deviceOrientation === "landscape") 
  {
    //document.getElementsByTagName("main")[0].remove();
    //Adiciona um overlayer com a informação de por o dispositivo em modo portrait
  }

  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth + "px";

  const canvas_container_dimensions = 
  {
    w: document.getElementById("p5_canvas_container").offsetWidth, 
    h: document.getElementById("p5_canvas_container").offsetHeight
  };

  return canvas_container_dimensions
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();
  visualization_moment = 1;
  iteration_index = 0;
  iteration_set_ran_once = false;
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas_container_dimensions = setResponsive();
  const canvas = createCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h, WEBGL);
  canvas.parent("p5_canvas_container");
  colorMode(RGB, 255, 255, 255, 100);
}



function windowResized() 
{
  const canvas_container_dimensions = setResponsive();
  resizeCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h);
}



function draw() 
{
  switch (visualization_moment) 
  {

    case 1:
      defineColorVariables();
      data_set = generateRandomDataSet (8, -width/2, width/2);

      for (let i=0; i<data_set.length; i++) 
      {
        createEachIterationGraphics (width, height/data_set.length);
      }

      perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);
      visualization_moment = 2;
      break;


    case 2:
      let i = 0;
      let perceptron_trained_with = 0;

      while (perceptron_trained_with < data_set.length) 
      {
        perceptron.runWith (data_set[i]);

        /*
        console.log (perceptron.getIteration().data_point_index);
        console.log (perceptron.getIteration().weight_x);
        console.log (perceptron.getIteration().weight_bias);
        console.log (perceptron.getIteration().feedforward_result);
        console.log (perceptron.getIteration().activation_result);
        console.log (perceptron.getIteration().training_error);
        console.log ("-----");
        */

        if (perceptron.getIteration().training_error === 0) 
        {
          perceptron_trained_with++;
        }

        if (i === data_set.length-1 && perceptron_trained_with < data_set.length)
        {
          i = 0;
          perceptron_trained_with = 0;
        }
        else i++;
      }
      
      iteration_set = perceptron.getIterationsSet();

      /*
      console.log (iteration_set[i].data_point_index);
      console.log (iteration_set[i].weight_x);
      console.log (iteration_set[i].weight_bias);
      console.log (iteration_set[i].feedforward_result);
      console.log (iteration_set[i].activation_result);
      console.log (iteration_set[i].training_error);
      console.log ("-----");
      */

      visualization_moment = 3;
      break;


    case 3:
      background (color_neg_2);

      for (let i=0; i<data_set.length; i++) 
      {
        clearEachIterationGraphics (i);
        addGoalStageToEachIterationGraphics (i, data_set[i].goal);
        addDataPointToEachIterationGraphics (i, data_set[i].inputs[0]);


        if (i === iteration_set[iteration_index].data_point_index) 
        {
          addDecisionIterationToEachIterationGraphics (i, iteration_set[iteration_index].feedforward_result);

          const data_point_w = width;
          const data_point_h = height/data_set.length;
          const data_point_x = 0;
          const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
          let  data_point_ry = 0;

          if (!iteration_set_ran_once) 
          {
            data_point_ry = map(iteration_set[iteration_index].feedforward_result, -data_point_w/2, data_point_w/2, -90, 90);
          }
          else 
          {
            data_point_ry = 0;
          }

          drawEachIterationGraphics (i, data_point_x, data_point_y, data_point_ry);

          iteration_index++;
          if (iteration_index >= iteration_set.length) 
          {
            iteration_index = 0;
            iteration_set_ran_once = true;
          }
        }
      }
      break;

    default: console.log ("Switch conditional out of case.");
  }

}



/*
function mousePressed() 
{
  switch_color_theme = !switch_color_theme;
}
*/
