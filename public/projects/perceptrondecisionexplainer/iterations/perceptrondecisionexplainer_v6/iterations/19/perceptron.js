


class Perceptron 
{

  constructor (num_weights) 
  {
    this.weights = new Array (num_weights);
    this.startWithRandomWeights();

    this.learning_rate = 0.000005;
    this.iterations = new Array (0);
  }



  getLearningRate() 
  {
    return this.learning_rate;
  }



  startWithRandomWeights() 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = Math.random()*2 - 1;
    }
  }



  getWeightsValues() 
  {
    return this.weights;
  }



  runFeedForwardWith (inputs)
  {
    this.sum_weighted_inputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs += inputs[i] * this.weights[i];
    }

    return this.sum_weighted_inputs;
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs > 0) this.activation_result =  1;
    else                              this.activation_result = -1;

    return this.activation_result;
  }



  getTrainingErrorWith (goal) 
  {
    this.training_error = goal - this.activation_result;

    return this.training_error;
  }
  
  
  
  runTrainingWith (inputs) 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * inputs[i];
    }
  }



  runWithMachineTraining (data_point) 
  {
    this.data_point = data_point;

    this.iteration_values = {};

    this.iteration_values.data_point_index = this.data_point.index;

    this.iteration_values.weight_x    = this.weights[0];
    this.iteration_values.weight_bias = this.weights[1];

    this.iteration_values.feedforward_result = this.runFeedForwardWith (this.data_point.inputs);
    this.iteration_values.activation_result  = this.runActivationFunction();
    this.iteration_values.training_error     = this.getTrainingErrorWith (this.data_point.goal);

    this.saveIteration();
    this.runTrainingWith (this.data_point.inputs);
  }



  runWithHumanTraining (data_point, weight_x, weight_bias) 
  {
    this.data_point = data_point;
    this.weights[0] = weight_x;
    this.weights[1] = weight_bias;

    this.iteration_values = {};

    this.iteration_values.data_point_index = this.data_point.index;

    this.iteration_values.weight_x    = this.weights[0];
    this.iteration_values.weight_bias = this.weights[1];

    this.iteration_values.feedforward_result = this.runFeedForwardWith (this.data_point.inputs);
    this.iteration_values.activation_result  = this.runActivationFunction();
    this.iteration_values.training_error     = this.getTrainingErrorWith (this.data_point.goal);

    this.saveIteration();
  }



  saveIteration() 
  {
    if (this.iterations.length === 0 && this.iteration_values.training_error === 0)
    {
      // For explainability reasons.
      // Do not want the Perceptron to accidentally hit the goal at first iteration.
      this.startWithRandomWeights();
      this.runWithMachineTraining (this.data_point);
    }

    else
    
    if (this.iterations.length === 0               || 
        this.iteration_values.training_error != 0  || 
        this.iteration_values.training_error === 0 && 
        this.iterations[this.iterations.length-2] != 0) 
    {
      this.iterations.push (this.iteration_values);
    }
  }



  getIteration()
  {
    return this.iterations[this.iterations.length-1];
  }



  getIterationsSet()
  {
    return this.iterations;
  }

}
