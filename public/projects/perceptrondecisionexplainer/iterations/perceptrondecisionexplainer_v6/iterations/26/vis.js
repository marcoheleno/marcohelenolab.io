


let visualization_moment;
let data_set, iteration_set;
let perceptron;

let iteration_index;

let perceptron_trained_with_data_point;
let input_range_weight_x_value;
let previous_input_range_weight_x_value;
let input_range_weights_disabled;

let num_iterations_per_data_point;
let input_range_weight_x_step;



function setResponsive() 
{
  const inner_window_dimensions = 
  {
    w: window.innerWidth, 
    h: window.innerHeight
  };

  document.body.style.width  = inner_window_dimensions.w + "px";
  document.body.style.height = inner_window_dimensions.h + "px";


  if (inner_window_dimensions.w <= 1024 && inner_window_dimensions.w > inner_window_dimensions.h || 
      inner_window_dimensions.w <= 1024 && deviceOrientation === "landscape") 
  {
    //document.getElementsByTagName("main")[0].remove();
    //Adiciona um overlayer com a informação de por o dispositivo em modo portrait
  }

  document.getElementById("p5_canvas_container").style.width = document.getElementsByTagName("main")[0].offsetWidth + "px";

  const canvas_container_dimensions = 
  {
    w: document.getElementById("p5_canvas_container").offsetWidth, 
    h: document.getElementById("p5_canvas_container").offsetHeight
  };

  return canvas_container_dimensions
}



function re_startSystem() 
{
  setResponsive();
  defineColorThemes();
  visualization_moment = 1;
  perceptron_trained_with_data_point = 0;
  previous_input_range_weight_x_value = 0;
  input_range_weights_disabled = false;
  num_iterations_per_data_point = 0;
  iteration_index = 0;
  input_range_weight_x_step = 0.1;
}



function preload() 
{
  re_startSystem();
}



function setup() 
{
  setAttributes("antialias", true);
  const canvas_container_dimensions = setResponsive();
  const canvas = createCanvas (canvas_container_dimensions.w, canvas_container_dimensions.h, WEBGL);
  canvas.parent("p5_canvas_container");
  colorMode(RGB, 255, 255, 255, 100);
}



function windowResized() 
{
  location.reload();
}



function draw() 
{
  //console.log (visualization_moment);

  if (visualization_moment>1 && visualization_moment<4)
  {
    if (perceptron_trained_with_data_point < data_set.length)
    {
      document.getElementById("input_range_weight_x").disabled = false;
      input_range_weight_x_value = Number(document.getElementById("input_range_weight_x").value);

      const difference_between_current_previous_input_range_values = abs(previous_input_range_weight_x_value-input_range_weight_x_value);

      if (difference_between_current_previous_input_range_values.toFixed(1)>=input_range_weight_x_step && perceptron.iterations.length>1) 
      {
        input_range_weight_x_value = Number(document.getElementById("input_range_weight_x").value);
        document.getElementById("input_range_weight_x").disabled = true;
      }

      if (previous_input_range_weight_x_value != input_range_weight_x_value) 
      {
        visualization_moment = 2;
      }
    }
    
    else
    
    if (!input_range_weights_disabled) 
    {
      document.getElementById("input_range_weight_x").disabled = true;

      document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_cursor", "default");
      document.querySelector(':root').style.setProperty("--input_range_weight_x_track_cursor", "default");
      
      input_range_weights_disabled = true;
      visualization_moment = 2;
    }
  }

  
  switch (visualization_moment) 
  {

    case 1:
      defineColorVariables();
      data_set = generateRandomDataSet (7, -width/2, width/2);

      for (let i=0; i<data_set.length; i++) 
      {
        createEachIterationGraphics (width, height/data_set.length);
        addGoalStageToEachIterationGraphics (i, data_set[i].goal);
        //addDataPointToEachIterationGraphics (i, data_set[i].inputs[0]);
        clearEachPerceptronDecisionIterationGraphics (i);
      }
      
      perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);

      document.getElementById("input_range_weight_x").setAttribute ("step", input_range_weight_x_step);
      document.getElementById("input_range_weight_x").value = parseFloat(perceptron.getWeightsValues()[0]).toFixed(6);

      document.getElementById("tooltip_or_button_container").innerHTML = "Adjust the slider above until you reach the green classification goal";

      visualization_moment = 2;
      break;


    case 2:
      background (color_neg_2);
      
      if (perceptron_trained_with_data_point < data_set.length) 
      {
        if (mouseIsPressed) 
        {
          for (let i=0; i<data_set.length; i++) 
          {
            perceptron.runWithHumanTraining (data_set[i], data_set.length, input_range_weight_x_value);

            const data_point_w = width;
            const data_point_h = height/data_set.length;
            const data_point_x = 0;
            const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
            const data_point_ry = map(perceptron.getIteration().feedforward_result, -data_point_w/2, data_point_w/2, -44, 44);

            drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
            drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
          }
        }

        else

        if (!mouseIsPressed) 
        {
          for (let i=0; i<data_set.length; i++) 
          {
            perceptron.runWithHumanTraining (data_set[i], data_set.length, input_range_weight_x_value);
            perceptron.saveIteration();

            /*
            console.log (perceptron.getIteration().data_point_index);
            console.log (perceptron.getIteration().weight_x);
            console.log (perceptron.getIteration().feedforward_result);
            console.log (perceptron.getIteration().activation_result);
            console.log (perceptron.getIteration().training_error);
            console.log ("-----");
            */

            if (perceptron.getIteration().training_error === 0) 
            {
              perceptron_trained_with_data_point++;
            }

            if (i === data_set.length-1 && perceptron_trained_with_data_point < data_set.length)
            {
              perceptron_trained_with_data_point = 0;
            }

            const data_point_w = width;
            const data_point_h = height/data_set.length;
            const data_point_x = 0;
            const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
            const data_point_ry = map(perceptron.getIteration().feedforward_result, -data_point_w/2, data_point_w/2, -44, 44);

            drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
            drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
          }

          previous_input_range_weight_x_value = input_range_weight_x_value;
        }

        iteration_set = perceptron.getIterationsSet();
      }

      visualization_moment = 3;
      break;


    case 3:
      background (color_neg_2);

      num_iterations_per_data_point = (iteration_set.length/data_set.length)-1;
      let num_data_points_perceptron_trained_in_this_iteration = 0;
      
      for (let i=0; i<data_set.length; i++) 
      {
        const data_point_w = width;
        const data_point_h = height/data_set.length;
        const data_point_x = 0;
        const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
        const data_point_ry = map(iteration_set[((iteration_set.length-1)-(data_set.length-1))+i].feedforward_result, -data_point_w/2, data_point_w/2, -44, 44);

        clearEachPerceptronDecisionIterationGraphics (i);

        for (let j=0; j<iteration_set.length; j++) 
        {
          if (i === iteration_set[j].data_point_index) 
          {
            /*
            console.log (iteration_set[i].data_point_index);
            console.log (iteration_set[i].weight_x);
            console.log (iteration_set[i].feedforward_result);
            console.log (iteration_set[i].activation_result);
            console.log (iteration_set[i].training_error);
            console.log ("-----");
            */
          
            addPerceptronDecisionToEachIterationGraphics (i, iteration_set[j].feedforward_result, iteration_set[j].training_error, false);
          }

          if ( j === ((num_iterations_per_data_point*data_set.length)+i) && iteration_set[j].training_error === 0) 
          {
            num_data_points_perceptron_trained_in_this_iteration++;
          }
        }
        
        drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
        drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);

        if (num_data_points_perceptron_trained_in_this_iteration === data_set.length) 
        {
          document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_color", "rgb("+red(goal_color)+","+green(goal_color)+","+blue(goal_color)+")");

          document.getElementById("tooltip_or_button_container").style.border = "1px solid var(--color_pos_2)";
          document.getElementById("tooltip_or_button_container").style.cursor = "pointer";
          document.getElementById("tooltip_or_button_container").setAttribute ("onclick", "runPerceptronWithMachineTraining()");
          document.getElementById("tooltip_or_button_container").innerHTML = "Great! Click here to see how Perceptron does this.";
        }
        else 
        {
          document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_color", "rgb("+red(non_goal_color)+","+green(non_goal_color)+","+blue(non_goal_color)+")");
        }
      }
      break;


    case 4:
      let i = 0;
      perceptron_trained_with_data_point = 0;

      while (perceptron_trained_with_data_point < data_set.length) 
      {
        perceptron.runWithMachineTraining (data_set[i]);

        /*
        console.log (perceptron.getIteration().data_point_index);
        console.log (perceptron.getIteration().weight_x);
        console.log (perceptron.getIteration().feedforward_result);
        console.log (perceptron.getIteration().activation_result);
        console.log (perceptron.getIteration().training_error);
        console.log ("-----");
        */

        if (perceptron.getIteration().training_error === 0) 
        {
          perceptron_trained_with_data_point++;
        }

        if (i === data_set.length-1 && perceptron_trained_with_data_point < data_set.length)
        {
          i = 0;
          perceptron_trained_with_data_point = 0;
        }
        else i++;
      }
      
      iteration_set = perceptron.getIterationsSet();
      
      num_iterations_per_data_point = (iteration_set.length/data_set.length) - 1;
      document.getElementById("input_range_iterations").setAttribute ("max", num_iterations_per_data_point);

      document.getElementById("input_range_weight_x").disabled = true;

      document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_cursor", "default");
      document.querySelector(':root').style.setProperty("--input_range_weight_x_track_cursor", "default");

      /*
      console.log (iteration_set[i].data_point_index);
      console.log (iteration_set[i].weight_x);
      console.log (iteration_set[i].feedforward_result);
      console.log (iteration_set[i].activation_result);
      console.log (iteration_set[i].training_error);
      console.log ("-----");
      */

      visualization_moment = 5;
      break;


    case 5:
      background (color_neg_2);

      for (let i=0; i<data_set.length; i++) 
      {
        const data_point_w = width;
        const data_point_h = height/data_set.length;
        const data_point_x = 0;
        const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
        //const data_point_ry = map(iteration_set[iteration_index].feedforward_result, -data_point_w/2, data_point_w/2, -44, 44);

        let data_point_ry = 0;

        if (data_set[i].goal === -1)
          data_point_ry = map(iteration_set[iteration_index].feedforward_result, -data_point_w/2, data_point_w/2, -44, 22);
        else if (data_set[i].goal === 1)
          data_point_ry = map(iteration_set[iteration_index].feedforward_result, -data_point_w/2, data_point_w/2, -22, 44);
        
        if (i === iteration_set[iteration_index].data_point_index) 
        {
          addPerceptronDecisionToEachIterationGraphics (i, iteration_set[iteration_index].feedforward_result, iteration_set[iteration_index].training_error);
          
          drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
          drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);

          document.getElementById("input_range_weight_x").value    = parseFloat(iteration_set[iteration_index].weight_x).toFixed(6);

          if (iteration_index === iteration_set.length-1) 
          {
            document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_color", "rgb("+red(goal_color)+","+green(goal_color)+","+blue(goal_color)+")");
          }
          else 
          {
            document.querySelector(':root').style.setProperty("--input_range_weight_x_thumb_color", "rgb("+red(non_goal_color)+","+green(non_goal_color)+","+blue(non_goal_color)+")");
          }

          iteration_index++;
          if (iteration_index >= iteration_set.length) 
          {
            document.getElementById("input_range_iterations").style.visibility = "visible";

            document.getElementById("tooltip_or_button_container").innerHTML = "Automatic training done!<br>Use the slider below to loop through all iterations.";
            visualization_moment = 6;
          }
        }

      }
      break;


    case 6:
      background (color_neg_2);
      frameRate(1);

      for (let i=0; i<data_set.length; i++) 
      {
        const data_point_w = width;
        const data_point_h = height/data_set.length;
        const data_point_x = 0;
        const data_point_y = -height/2 + data_point_h*i + data_point_h/2;
        const input_range_iterations_value = Number(document.getElementById("input_range_iterations").value);
        //const data_point_ry = map(iteration_set[((iteration_set.length-1)-(data_set.length-1))+i].feedforward_result, -data_point_w/2, data_point_w/2, -44, 44);
        
        let data_point_ry = 0;

        if (data_set[i].goal === -1)
        data_point_ry = map(iteration_set[((iteration_set.length-1)-(data_set.length-1))+i].feedforward_result, -data_point_w/2, data_point_w/2, -44, 22);
        else if (data_set[i].goal === 1)
        data_point_ry = map(iteration_set[((iteration_set.length-1)-(data_set.length-1))+i].feedforward_result, -data_point_w/2, data_point_w/2, -22, 44);

        clearEachPerceptronDecisionIterationGraphics (i);

        for (let j=0; j<iteration_set.length; j++) 
        {
          if (i === iteration_set[j].data_point_index) 
          {
            if ( j === ((input_range_iterations_value*data_set.length)+i) ) 
            {
              addPerceptronDecisionToEachIterationGraphics (i, iteration_set[j].feedforward_result, iteration_set[j].training_error, true);
            }
            else 
            {
              addPerceptronDecisionToEachIterationGraphics (i, iteration_set[j].feedforward_result, iteration_set[j].training_error, false);
            }
          }
        }
        
        drawEachGoalStageAndDataPointIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
        drawEachPerceptronDecisionIterationGraphics (i, data_point_x, data_point_y, data_point_ry);
      }
      break;


    default: console.log ("Switch conditional out of case.");
  }

  //console.log (frameRate());
}



function runPerceptronWithMachineTraining() 
{
  perceptron = new Perceptron (data_set[data_set.length-1].inputs.length);

  for (let i=0; i<data_set.length; i++) 
  {
    clearEachPerceptronDecisionIterationGraphics (i);
  }

  visualization_moment = 4;

  document.getElementById("tooltip_or_button_container").style.border = "0px none var(--color_neg_2)";
  document.getElementById("tooltip_or_button_container").style.cursor = "default";
  document.getElementById("tooltip_or_button_container").removeAttribute ("onclick");
  document.getElementById("tooltip_or_button_container").innerHTML = "Automatic training until reaching the green classification goal in progress...";
}
