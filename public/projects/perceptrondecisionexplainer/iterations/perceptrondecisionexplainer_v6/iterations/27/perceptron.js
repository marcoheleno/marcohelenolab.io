


class Perceptron 
{

  constructor (num_weights) 
  {
    this.weights = new Array (num_weights);
    this.startWithRandomWeights();

    this.learning_rate = 0.000005;
    this.iterations = new Array (0);
  }



  getLearningRate() 
  {
    return this.learning_rate;
  }



  startWithRandomWeights() 
  {
    // For explainability reasons.
    const upper_bound =  1;
    const lower_bound = -1;
    const wanted_percente_pos = upper_bound * 0.5;
    const wanted_percente_neg = lower_bound * 0.5;

    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = 0;

      while (this.weights[i]>wanted_percente_neg && this.weights[i]<wanted_percente_pos) 
      {
        this.weights[i] = Math.random()*2 - 1;
      }
    }

    document.getElementById("input_range_weight_x").value = parseFloat(this.weights[0]).toFixed(6);
  }



  getWeightsValues() 
  {
    return this.weights;
  }



  runFeedForwardWith (inputs)
  {
    this.sum_weighted_inputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs += inputs[i] * this.weights[i];
    }

    return this.sum_weighted_inputs;
  }



  runActivationFunction()
  {
    if (this.sum_weighted_inputs > 0) this.activation_result =  1;
    else                              this.activation_result = -1;

    return this.activation_result;
  }



  getTrainingErrorWith (goal) 
  {
    this.training_error = goal - this.activation_result;

    return this.training_error;
  }
  
  
  
  runTrainingWith (inputs) 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * inputs[i];
    }
  }



  runWithMachineTraining (data_point) 
  {
    this.data_point = data_point;

    do 
    {
      this.iteration_values = {};

      this.iteration_values.data_point_index = this.data_point.index;

      this.iteration_values.weight_x    = this.weights[0];

      this.iteration_values.feedforward_result = this.runFeedForwardWith (this.data_point.inputs);
      this.iteration_values.activation_result  = this.runActivationFunction();
      this.iteration_values.training_error     = this.getTrainingErrorWith (this.data_point.goal);

      // For explainability reasons.
      if (this.iterations.length === 0 && this.iteration_values.training_error === 0) this.startWithRandomWeights();
    }
    while (this.iterations.length === 0 && this.iteration_values.training_error === 0);

    this.saveIteration();
    this.runTrainingWith (this.data_point.inputs);
  }



  runWithHumanTraining (data_point, data_set_length, weight_x) 
  {
    this.data_point = data_point;
    this.weights[0] = weight_x;

    do 
    {
      this.iteration_values = {};

      this.iteration_values.data_point_index = this.data_point.index;

      this.iteration_values.weight_x = this.weights[0];

      this.iteration_values.feedforward_result = this.runFeedForwardWith (this.data_point.inputs);
      this.iteration_values.activation_result  = this.runActivationFunction();
      this.iteration_values.training_error     = this.getTrainingErrorWith (this.data_point.goal);

      // For explainability reasons.
      if (this.iterations.length <= data_set_length && this.iteration_values.training_error === 0) 
      {
        this.iterations = new Array (0);
        this.startWithRandomWeights();
      }
    }
    while (this.iterations.length <= data_set_length && this.iteration_values.training_error === 0);
  }



  saveIteration() 
  {
    if (this.iterations.length === 0               || 
        this.iteration_values.training_error != 0  || 
        this.iteration_values.training_error === 0 && 
        this.iterations[this.iterations.length-2] != 0) 
    {
      this.iterations.push (this.iteration_values);
    }
  }



  getIteration()
  {
    return this.iteration_values;
  }



  getIterationsSet()
  {
    return this.iterations;
  }

}
