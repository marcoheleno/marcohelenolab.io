

let data_set;


function threshold(x) 
{
  return 2 * x + 1;
}


function test(x) 
{
  //angleMode(RADIANS);
  // map(mouseX, 0, width, 0, 2)
  return map(mouseX, 0, width, -5, 5) * x + 0; //(mouseY-height/2)*-1;
}


function createDataSetWith (num_data_elements)
{
  data_set = new Array (num_data_elements);

  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }
}


class DataElement 
{
  constructor() 
  {
    this.x = random (-width/2,  width/2);
    this.y = random (-height/2, height/2);
    this.b = 1; // bias
    /*
    this.sml_radius = 1;
    this.big_radius = 1;
    */
    this.runSolutionProviderForPtronTraining();
    this.buildTrainingDataElement();
  }


  runSolutionProviderForPtronTraining()
  {
    this.solution = 0;

    if (this.y < threshold(this.x)) 
    {
      this.solution = -1;
    }
    else 
    {
      this.solution =  1;
    }
  }


  buildTrainingDataElement() 
  {
    this.inputs = [this.x, this.y, this.b];
    this.solution;
  }
}