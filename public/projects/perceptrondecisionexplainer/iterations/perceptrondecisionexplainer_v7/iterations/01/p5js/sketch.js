

function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().h, getCanvasContainerSize().h);
  canvas.parent ("result_vis_container");
  
  re_StartSystem();
}


function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById("result_vis_container").offsetWidth,
    h: document.getElementById("result_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}


function containersResized() 
{
  resizeCanvas (getCanvasContainerSize().h, getCanvasContainerSize().h);
}


function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().h, getCanvasContainerSize().h);
}


function re_StartSystem() 
{
  createDataSetWith (3); //num_data_elements
  startPerceptron   (3); //num_weights
}


function draw() 
{
  console.log (document.getElementById("input_range_goal").value);


  background (200);
  translate (width/2, height/2);
  scale (1, -1);

  //goal
  stroke(0, 0, 0);
  line (-width/2, test(-width/2), width/2, test(width/2));

  //gues
  //stroke(255, 0, 0);
  let weights = ptron.getCurrentWeights();

  x1 = -width/2;
  y1 = (-weights[2] - weights[0] * -width/2) / weights[1];
  x2 = width/2;
  y2 = (-weights[2] - weights[0] *  width/2) / weights[1];

  //line (x1, y1, x2, y2);




  for (let i=0; i<data_set.length; i++) 
  {
    //console.log (data_set[i].inputs);

    stroke(100);

    ptron.calSumWeightedInputsWith (data_set[i].inputs);
    ptron.calActivationFunction();

    const guess = ptron.getActivationResult();

    if (guess > 0) fill(255);
    else fill(100);

    const x = data_set[i].inputs[0];
    const y = data_set[i].inputs[1];

    ellipse(x, y, 6, 6);

    ptron.calTrainingErrorWith (data_set[i].solution);
    ptron.finishInTrainingWith (data_set[i].inputs);
  }
  
}

