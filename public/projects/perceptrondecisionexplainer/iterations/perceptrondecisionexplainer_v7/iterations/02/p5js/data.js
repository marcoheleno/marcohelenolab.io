

let data_set;


function dataThreshold (x) 
{
  // 2 * x + 1;
  return input_range_user_goal_value * x + 0;
}


function createDataSetWith (num_data_elements)
{
  data_set = new Array (num_data_elements);

  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }
}


class DataElement 
{
  constructor() 
  {
    this.x = random (-1, 1);
    this.y = random (-1, 1);
    this.bias = 1;
    this.inputs = [this.x, this.y, this.bias];

    this.sml_radius = 5;
    this.big_radius = 15;
  }


  runSolutionProviderForPtronTraining()
  {
    this.solution = 0;

    if (input_range_user_goal_value<=0) 
    {
      if (this.y < dataThreshold(this.x)) this.solution = -1;
      else this.solution =  1;
    }
    else
    {
      if (this.y < dataThreshold(this.x)) this.solution = 1;
      else this.solution =  -1;
    }
  }

  
  updateRadius (solution_or_ptron_guess) 
  {
    this.radius = 0;

    if (solution_or_ptron_guess === -1) 
    {
      this.radius = this.sml_radius;
    }

    else 

    if (solution_or_ptron_guess ===  1) 
    {
      this.radius = this.big_radius;
    }
  }


  selectElement() 
  {
    this.canvas_x = map (this.x, -1, 1, -width/2,  width/2);
    this.canvas_y = map (this.y, -1, 1, -height/2, height/2);

    if (mouseX-width/2  > this.canvas_x-this.radius && mouseX-width/2  < this.canvas_x+this.radius &&
        mouseY-height/2 > this.canvas_y-this.radius && mouseY-height/2 < this.canvas_y+this.radius &&
        mouseIsPressed === true)
    {
      this.x = map(mouseX, 0, width,  -1, 1);
      this.y = map(mouseY, 0, height, -1, 1);
    }

  }


  drawElement() 
  {
    this.canvas_x = map (this.x, -1, 1, -width/2,  width/2);
    this.canvas_y = map (this.y, -1, 1, -height/2, height/2);
    noFill();
    strokeWeight(2);
    ellipse (this.canvas_x, this.canvas_y, this.radius);
  }
}