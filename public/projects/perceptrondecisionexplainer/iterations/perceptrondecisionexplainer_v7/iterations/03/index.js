

let current_scene;


function jumpToScene (scene_number) 
{
  document.getElementById ("txt_expla_" + scene_number).scrollIntoView();
}


// IntersectionObserver

const options = 
{
  root: document.getElementsByTagName("article")[0], 
  rootMargin: "0px", 
  threshold: 1.0
}

const callback = (entries, observer) => 
{
  entries.forEach((entry) => 
  {
    // Each entry describes an intersection change for one observed target element:
    //   entry.boundingClientRect
    //   entry.intersectionRatio
    //   entry.intersectionRect
    //   entry.isIntersecting
    //   entry.rootBounds
    //	 entry.target
    //   entry.time
	
	  if (entry.isIntersecting) 
    {
      current_scene = entry.target.getAttribute('id');
      //console.log (current_scene + " yes");

      if (entry.target.getAttribute('id') === "txt_expla_1") 
      {
        document.getElementById("result_container").style.position = "fixed";
        document.getElementById("result_container").style.top = "0px";
        document.getElementById("result_container").style.left = "30vw";
        document.getElementById("result_container").style.zIndex = "1";
        document.getElementById("result_container").style.width = "70vw";
        document.getElementById("result_container").style.height = "100vh";

        document.getElementById("history_container").style.height = "100%";

        containersResized();
      }
    }

    else 
    {
      //console.log (current_scene + " no");

      if (entry.target.getAttribute('id') === "txt_expla_1") 
      {
        document.getElementById("result_container").attributeStyleMap.clear();
        document.getElementById("history_container").attributeStyleMap.clear();
        containersResized();
      }
    }

  });
};

const observer = new IntersectionObserver (callback, options);

observer.observe (document.getElementById("txt_expla_1"));
observer.observe (document.getElementById("txt_expla_2"));
observer.observe (document.getElementById("txt_expla_3"));
observer.observe (document.getElementById("txt_expla_4"));
observer.observe (document.getElementById("txt_expla_5"));