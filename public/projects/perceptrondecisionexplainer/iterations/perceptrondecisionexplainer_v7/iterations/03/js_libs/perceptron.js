

let ptron;


function startPerceptron (num_inputs)
{
  ptron = 0;
  ptron = new Perceptron (num_inputs);
}


class Perceptron 
{

  constructor (num_weights) 
  {
    this.learning_rate = 0.01;

    this.weights = new Array (num_weights); 
    this.startWithRandomWeights();
  }


  startWithRandomWeights() 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = this.calRandomNumberBetween (-1, 1);
    }
  }

  
  calRandomNumberBetween (min, max) {
    return Math.random() * (max - min) + min;
  }


  getCurrentWeights() 
  {
    return this.weights;
  }

  
  calSumWeightedInputsWith (inputs)
  {
    this.sum_weighted_inputs_result = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs_result += inputs[i] * this.weights[i];
    }
  }


  calActivationFunction()
  {
    if (this.sum_weighted_inputs_result > 0) 
      this.activation_result = 1;
    else
      this.activation_result = -1;
  }


  getActivationResult() 
  {
    return this.activation_result;
  }


  calTrainingErrorWith (goal) 
  {
    this.training_error = goal - this.activation_result;
  }

  
  endInTrainingWith (inputs) 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.training_error * inputs[i];
    }
  }
  
}