

let input_range_user_goal_value;
let count;


function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().h, getCanvasContainerSize().h);
  canvas.parent ("result_vis_container");
  
  re_StartSystem();
}


function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById("result_vis_container").offsetWidth,
    h: document.getElementById("result_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}


function containersResized() 
{
  resizeCanvas (getCanvasContainerSize().h, getCanvasContainerSize().h);
}


function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().h, getCanvasContainerSize().h);
}


function re_StartSystem() 
{
  ellipseMode (RADIUS);
  count = 0;

  createDataSetWith (3); //num_data_elements
  //input_range_user_goal_value = document.getElementById("input_range_goal").value = parseFloat(random(-5, 5).toFixed(2));

  startPerceptron (3); //num_weights
}


function draw() 
{
  clear();
  translate (width/2, height/2);
  //angleMode(DEGREES);
  //rotate(-90);


  if (current_scene === "txt_expla_1") 
  {
    document.getElementById("input_range_goal").style.display = "block";
    document.getElementById("input_random_data").style.display = "block";

    input_range_user_goal_value = document.getElementById("input_range_goal").value;
    //console.log (input_range_user_goal_value);

    stroke(0);
    strokeWeight (1);
    //line( -width/2, dataThreshold(-width/2), width/2, dataThreshold( width/2) );
    line( dataThreshold(-width/2), -width/2, dataThreshold( width/2), width/2 );

    for (let i=0; i<data_set.length; i++) 
    {
      data_set[i].runSolutionProviderForPtronTraining();
      data_set[i].updateRadius (data_set[i].solution);
      data_set[i].selectElement();
      stroke(0);
      data_set[i].drawElement();
    }
  }

  else 
  {
    document.getElementById("input_range_goal").style.display = "none";
    document.getElementById("input_random_data").style.display = "none";
  }


  if (current_scene === "txt_expla_2") 
  {
    stroke(0);
    strokeWeight (1);
    line( -width/2, dataThreshold(-width/2), width/2, dataThreshold( width/2) );

    count = (count + 1) % data_set.length;
    
    ptron.calSumWeightedInputsWith (data_set[count].inputs);
    ptron.calActivationFunction();

    data_set[count].updateRadius (ptron.getActivationResult());

    ptron.calTrainingErrorWith (data_set[count].solution);

    if (ptron.training_error === 0) stroke (0, 255, 0);
    else stroke (255, 0, 0);
    data_set[count].drawElement();

    ptron.endInTrainingWith (data_set[count].inputs);
  }


  



  /*
  for (let i=0; i<data_set.length; i++) 
  {
    //console.log (data_set[i].inputs);

    stroke(100);

    ptron.calSumWeightedInputsWith (data_set[i].inputs);
    ptron.calActivationFunction();

    const guess = ptron.getActivationResult();

    if (guess > 0) fill(255);
    else fill(100);

    const x = data_set[i].inputs[0];
    const y = data_set[i].inputs[1];

    ellipse(x, y, 6, 6);

    ptron.calTrainingErrorWith (data_set[i].solution);
    ptron.endInTrainingWith (data_set[i].inputs);
  }
  */
}