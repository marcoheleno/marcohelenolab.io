

let data_set;


function dataThreshold (y) 
{
  // 2 * x + 1; original
  // 2 * y + 1; adapted
  return input_range_user_goal_value * y + 0;
}


function calRandomNumberBetween (min, max) 
{
  return Math.random() * (max - min) + min;
}


function createDataSetWith (num_data_elements)
{
  data_set = new Array (num_data_elements);

  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }
}


class DataElement 
{
  constructor() 
  {
    this.x = calRandomNumberBetween (-1, 1);
    this.y = calRandomNumberBetween (-1, 1);
    this.bias = 1;
    this.inputs = [this.x, this.y, this.bias];

    this.sml_radius = 10;
    this.big_radius = 20;
  }


  runSolutionProviderForPtronTraining()
  {
    this.solution = 0;
    
    if (this.x <= dataThreshold(this.y)) 
    {
      this.solution = -1;
    }
    else 
    {
      this.solution =  1;
    }
  }

  
  updateRadius (solution_or_ptron_guess) 
  {
    this.radius = 0;

    if (solution_or_ptron_guess === -1) 
    {
      this.radius = this.sml_radius;
    }

    else 

    if (solution_or_ptron_guess ===  1) 
    {
      this.radius = this.big_radius;
    }
  }


  getCanvasPos (sketch) 
  {
    this.canvas_pos = 
    {
      x: sketch.map (this.x, -1, 1, -sketch.width/2,  sketch.width/2),
      y: sketch.map (this.y, -1, 1, -sketch.height/2, sketch.height/2)
    };

    return this.canvas_pos;
  }


  updateClassPos (sketch, canvas_x, canvas_y) 
  {
    this.x = sketch.map (canvas_x, -sketch.width/2,  sketch.width/2 , -1, 1);
    this.y = sketch.map (canvas_y, -sketch.height/2, sketch.height/2, -1, 1);
  }


  drawElement (sketch) 
  {
    this.canvas_x = sketch.map (this.x, -1, 1, -sketch.width/2,  sketch.width/2);
    this.canvas_y = sketch.map (this.y, -1, 1, -sketch.height/2, sketch.height/2);

    sketch.point (this.canvas_x, this.canvas_y);
  }


  drawElementBody (sketch) 
  {
    this.canvas_x = sketch.map (this.x, -1, 1, -sketch.width/2,  sketch.width/2);
    this.canvas_y = sketch.map (this.y, -1, 1, -sketch.height/2, sketch.height/2);
    
    sketch.noFill();
    sketch.ellipse (this.canvas_x, this.canvas_y, this.radius);
  }
}