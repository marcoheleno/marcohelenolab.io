

let current_scene;
let color_level_1, color_level_2, color_level_3, color_level_4;
let color_highlight_neg, color_highlight_pos;



function jumpToScene (scene_number) 
{
  document.getElementById ("txt_expla_" + scene_number).scrollIntoView();
}



// IntersectionObserver

const options = 
{
  root: document.getElementsByTagName("article")[0], 
  rootMargin: "0px", 
  threshold: 1.0
}

const callback = (entries, observer) => 
{
  entries.forEach((entry) => 
  {
    // Each entry describes an intersection change for one observed target element:
    //   entry.boundingClientRect
    //   entry.intersectionRatio
    //   entry.intersectionRect
    //   entry.isIntersecting
    //   entry.rootBounds
    //	 entry.target
    //   entry.time
	
	  if (entry.isIntersecting) 
    {
      current_scene = entry.target.getAttribute('id');
      //console.log (current_scene + " yes");

      if (entry.target.getAttribute('id') === "txt_expla_1") 
      {
        document.getElementById("result_container").style.position = "absolute";
        document.getElementById("result_container").style.top = "0px";
        document.getElementById("result_container").style.left = "30vw";
        document.getElementById("result_container").style.zIndex = "1";
        document.getElementById("result_container").style.width = "70vw";
        document.getElementById("result_container").style.height = "100vh";

        document.getElementById("history_container").style.height = "100%";

        result_sketch.containersResized();
      }
    }

    else 
    {
      //console.log (current_scene + " no");

      if (entry.target.getAttribute('id') === "txt_expla_1") 
      {
        document.getElementById("result_container").attributeStyleMap.clear();
        document.getElementById("history_container").attributeStyleMap.clear();
        
        result_sketch.containersResized();
      }
    }

  });
};

const observer = new IntersectionObserver (callback, options);

observer.observe (document.getElementById("txt_expla_1"));
observer.observe (document.getElementById("txt_expla_2"));
observer.observe (document.getElementById("txt_expla_3"));
observer.observe (document.getElementById("txt_expla_4"));
observer.observe (document.getElementById("txt_expla_5"));



// CSS colors for p5js

function convertCSScolorsTop5js (sketch)
{
    // Colors defined in index.css
    color_level_1 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_1");
    color_level_2 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_2");
    color_level_3 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_3");
    color_level_4 = getComputedStyle(document.documentElement).getPropertyValue("--color_level_4");
    color_highlight_neg = getComputedStyle(document.documentElement).getPropertyValue("--color_highlight_neg");
    color_highlight_pos = getComputedStyle(document.documentElement).getPropertyValue("--color_highlight_pos");

    color_level_1 = sketch.color( sketch.red(color_level_1), sketch.green(color_level_1), sketch.blue(color_level_1) );
    color_level_2 = sketch.color( sketch.red(color_level_2), sketch.green(color_level_2), sketch.blue(color_level_2) );
    color_level_3 = sketch.color( sketch.red(color_level_3), sketch.green(color_level_3), sketch.blue(color_level_3) );
    color_level_4 = sketch.color( sketch.red(color_level_4), sketch.green(color_level_4), sketch.blue(color_level_4) );
    color_highlight_neg = sketch.color( sketch.red(color_highlight_neg), sketch.green(color_highlight_neg), sketch.blue(color_highlight_neg) );
    color_highlight_pos = sketch.color( sketch.red(color_highlight_pos), sketch.green(color_highlight_pos), sketch.blue(color_highlight_pos) );
}