

// Colors defined in index.css


const s2 = function (sketch) 
{
  sketch.setup = function() 
  {
    const canvas = sketch.createCanvas (sketch.getCanvasContainerSize().h, sketch.getCanvasContainerSize().h);

    convertCSScolorsTop5js (sketch);
    sketch.ellipseMode (sketch.RADIUS);
  }


  sketch.getCanvasContainerSize = function() 
  {
    const canvas_container_size = 
    {
      w: document.getElementById("result_vis_container").offsetWidth,
      h: document.getElementById("result_vis_container").offsetHeight
    };
    
    return canvas_container_size;
  }


  sketch.containersResized = function() 
  {
    sketch.resizeCanvas (sketch.getCanvasContainerSize().h, sketch.getCanvasContainerSize().h);
  }


  sketch.windowResized = function() 
  {
    sketch.resizeCanvas (sketch.getCanvasContainerSize().h, sketch.getCanvasContainerSize().h);
  }


  sketch.draw = function() 
  {
    sketch.clear();
    sketch.translate (sketch.width/2, sketch.height/2);


    sketch.stroke (color_level_2);
    sketch.strokeWeight (1);
  //sketch.line ( -sketch.width/2, dataThreshold(-sketch.width/2), sketch.width/2, dataThreshold(sketch.width/2) );   original
    sketch.line ( dataThreshold(-sketch.width/2), -sketch.width/2, dataThreshold(sketch.width/2), sketch.width/2 ); //adapted


    if (current_scene === "txt_expla_1") 
    {
      document.getElementById("input_range_goal").style.display = "block";
      document.getElementById("input_button_random_data").style.display = "block";

      input_range_user_goal_value = document.getElementById("input_range_goal").value;
      //console.log (input_range_user_goal_value);
      
      for (let i=0; i<data_set.length; i++) 
      {
        data_set[i].runSolutionProviderForPtronTraining();
        data_set[i].updateRadius (data_set[i].solution);
        data_set[i].updateCanvasXY (sketch);

        const distanceBetweenCurrentElementToMouse = sketch.dist (sketch.mouseX-sketch.width/2, 
                                                                  sketch.mouseY-sketch.height/2, 
                                                                  data_set[i].canvas_x, 
                                                                  data_set[i].canvas_y);

        if (distanceBetweenCurrentElementToMouse<=data_set[i].radius && sketch.mouseIsPressed===true) 
        {
          data_set[i].updateClassXY (sketch, sketch.mouseX-sketch.width/2, sketch.mouseY-sketch.height/2);
        }
        
        for (let j=0; j<data_set.length; j++) 
        {
          if (i != j) 
          {
            const distanceBetweenElements = sketch.dist (data_set[i].canvas_x, data_set[i].canvas_y, 
                                                         data_set[j].canvas_x, data_set[j].canvas_y);

            if (distanceBetweenElements <= data_set[i].radius + data_set[j].radius + 5) 
            {
              if (data_set[i].canvas_x >= data_set[j].canvas_x) 
              {
                data_set[j].canvas_x-=3;
              }
              else 
              {
                data_set[j].canvas_x+=3;
              }
              
              data_set[j].updateClassXY (sketch, data_set[j].canvas_x, data_set[j].canvas_y);
            }
          }
        }
        
        if (data_set[i].canvas_x < -sketch.width/2) data_set[i].canvas_x = -sketch.width/2;
        else
        if (data_set[i].canvas_x >  sketch.width/2) data_set[i].canvas_x =  sketch.width/2;

        if (data_set[i].canvas_y < -sketch.height/2) data_set[i].canvas_y = -sketch.height/2;
        else
        if (data_set[i].canvas_y >  sketch.height/2) data_set[i].canvas_y =  sketch.height/2;
        
        data_set[i].updateClassXY (sketch, data_set[i].canvas_x, data_set[i].canvas_y);
        
        sketch.stroke (color_level_3);
        sketch.strokeWeight (1);
        data_set[i].drawElementBody (sketch);

        sketch.stroke (color_level_2);
        sketch.strokeWeight (3);
        data_set[i].drawElement (sketch);
      }
    }

    else 
    {
      document.getElementById("input_range_goal").style.display = "none";
      document.getElementById("input_button_random_data").style.display = "none";
    }



    if (current_scene === "txt_expla_2") 
    {
      count = (count + 1) % data_set.length;
      
      ptron.calSumWeightedInputsWith (data_set[count].inputs);
      ptron.calActivationFunction();

      data_set[count].updateRadius (ptron.getActivationResult());

      ptron.calTrainingErrorWith (data_set[count].solution);


      if (ptron.training_error === 0) 
      {
        sketch.stroke (color_highlight_pos);
      }
      else 
      {
        sketch.stroke (color_highlight_neg);
      }
      
      sketch.strokeWeight (1);
      data_set[count].drawElementBody (sketch);

      sketch.stroke (color_level_2);
      sketch.strokeWeight (3);
      data_set[count].drawElement (sketch);

      ptron.endInTrainingWith (data_set[count].inputs);
    }

  }

};

let result_sketch = new p5 (s2, "result_vis_container");