

let input_range_user_goal_value, count;


window.onload = function() 
{  
    re_StartSystem();
}


function calRandomNumberBetween (min, max) 
{
  return Math.random() * (max - min) + min;
}


function re_StartSystem() 
{
    count = 0;
    
    createDataSetWith (3); //num_data_elements
    
    input_range_user_goal_value = document.getElementById("input_range_goal").value = parseFloat(calRandomNumberBetween(-0.5, 0.5).toFixed(2));
    
    startPerceptron (3); //num_weights
}