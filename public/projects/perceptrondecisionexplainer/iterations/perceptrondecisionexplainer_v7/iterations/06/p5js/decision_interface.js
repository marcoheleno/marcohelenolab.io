


let world;
let ptron_sum_weighted_inputs_left_constraint_polygon, ptron_sum_weighted_inputs_right_constraint_polygon;
let ptron_activation_constraint_polygon;
let ptron_decision_attractor_x, ptron_decision_attractor_y, ptron_decision_attractor_h;





function addPhysicsWorld (sketch) 
{
  world = new c2.World (new c2.Rect (-sketch.width/2, -sketch.height/2-data_set[0].big_radius, 
                                      sketch.width/2,  sketch.height/2+data_set[0].big_radius));

  const collision  = new c2.Collision();
  world.addInteractionForce (collision);
  
  const gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);

  const floorForce = new c2.LineField (new c2.Line 
  (
    0, 
    sketch.height + data_set[0].big_radius*2, 
    sketch.width, 
    sketch.height + data_set[0].big_radius*2
  ), 1);

  world.addForce (floorForce);
}





function drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith (sketch) 
{
  const x = -sketch.width/2;
  const y = -sketch.height/2 - sketch.height*0.15;
  
  const ptron_sum_weighted_inputs_w = sketch.width*0.40;

  // ptron_sum_weighted_inputs_openness
  const ptron_sum_weighted_inputs_o = 20;//sketch.map(sketch.mouseX, 0, sketch.width, ptron_sum_weighted_inputs_w, 0);

  sketch.push();
    sketch.translate (x, y);
  
    sketch.angleMode (sketch.DEGREES);
    sketch.rotate (45);
    
    const ptron_sum_weighted_inputs_left_margin  = sketch.screenPosition (-ptron_sum_weighted_inputs_w, 0);
    const ptron_sum_weighted_inputs_left_center  = sketch.screenPosition (-ptron_sum_weighted_inputs_o, 0);

    const ptron_sum_weighted_inputs_right_center = sketch.screenPosition (0, 0);
    const ptron_sum_weighted_inputs_right_neck   = sketch.screenPosition (0, ptron_sum_weighted_inputs_o*0.5);
  sketch.pop();



  if (ptron_sum_weighted_inputs_left_constraint_polygon  != undefined) 
    world.removeConstraint (ptron_sum_weighted_inputs_left_constraint_polygon);

  if (ptron_sum_weighted_inputs_right_constraint_polygon != undefined) 
    world.removeConstraint (ptron_sum_weighted_inputs_right_constraint_polygon);
  
  const ptron_sum_weighted_inputs_left_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y), 
    new c2.Point (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y)
  ]);

  const ptron_sum_weighted_inputs_right_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_sum_weighted_inputs_right_center.x, ptron_sum_weighted_inputs_right_center.y), 
    new c2.Point (ptron_sum_weighted_inputs_right_neck.x,   ptron_sum_weighted_inputs_right_neck.y)
  ]);

  ptron_sum_weighted_inputs_left_constraint_polygon  = new c2.PolygonConstraint (ptron_sum_weighted_inputs_left_polygon);
  world.addConstraint (ptron_sum_weighted_inputs_left_constraint_polygon);
  
  ptron_sum_weighted_inputs_right_constraint_polygon = new c2.PolygonConstraint (ptron_sum_weighted_inputs_right_polygon);
  world.addConstraint (ptron_sum_weighted_inputs_right_constraint_polygon);



  sketch.noFill();
  sketch.strokeWeight (1);
  sketch.stroke (color_level_2);
  /*
  if (is_ptron_checking_error === false) sketch.stroke (color_level_2);
  else 
  {
    if (ptron_training_error_value === 0) sketch.stroke (color_highlight_pos);
    else sketch.stroke (color_highlight_neg);
  }
  */
  sketch.beginShape();
    sketch.vertex (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y);
    sketch.vertex (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y);
  sketch.endShape();

  sketch.beginShape();
    sketch.vertex (ptron_sum_weighted_inputs_right_center.x, ptron_sum_weighted_inputs_right_center.y);
    sketch.vertex (ptron_sum_weighted_inputs_right_neck.x,   ptron_sum_weighted_inputs_right_neck.y);
  sketch.endShape();
}





function drawPtronActivationAnalogy2PhysicsWorld (sketch) 
{
  const x = 0;
  const y = 0;
  const h = sketch.height*0.25;

  const ptron_activation_upper = {x:x, y:y};
  const ptron_activation_lower = {x:x, y:y+h};
  
  if (ptron_activation_constraint_polygon != undefined) 
    world.removeConstraint (ptron_activation_constraint_polygon);

  const ptron_activation_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_activation_upper.x, ptron_activation_upper.y), 
    new c2.Point (ptron_activation_lower.x, ptron_activation_lower.y)
  ]);

  ptron_activation_constraint_polygon = new c2.PolygonConstraint (ptron_activation_polygon);
  world.addConstraint (ptron_activation_constraint_polygon);



  sketch.stroke (color_level_2);
  sketch.strokeWeight (1);
  sketch.noFill();

  sketch.beginShape();
    sketch.vertex (ptron_activation_upper.x, ptron_activation_upper.y);
    sketch.vertex (ptron_activation_lower.x, ptron_activation_lower.y);
  sketch.endShape();
}





function drawParticles2PhysicsWorldWith 
(
  data_id, 
  ptron_activation_value, 
  is_ptron_checking_error, 
  ptron_training_error_value
) 
{
  if (world.particles.length < 1) 
    addParticle2PhysicsWorld (data_id, ptron_activation_value);

  world.update();

  

  for (let i=0; i<world.particles.length; i++) 
  {
    noStroke();

    if (is_ptron_checking_error === false)  fill (color_level_2);
    else 
    {
      if (ptron_training_error_value === 0) fill (color_highlight_pos);
      else fill (color_highlight_neg);
    }

    ellipseMode (RADIUS);
    ellipse 
    (
      world.particles[i].position.x, 
      world.particles[i].position.y, 
      world.particles[i].radius
    );
    ellipseMode (CORNER);

    if (world.particles[i].position.y > height*0.5) 
    {
      removeAttractor2PhysicsWorld();
    }

    if (world.particles[i].position.y > height) 
    {
      removeParticle2PhysicsWorld();
    }
  }
}



function addParticle2PhysicsWorld (data_id, ptron_activation_value) 
{
  const temp_particle  = new c2.Particle 
  (
    width*0.5 - data_set[0].radius*2, 
    -data_set[0].radius
  );

  temp_particle.radius = data_set[data_id].inputs[0];

  world.addParticle (temp_particle);

  addAttractor2PhysicsWorldWith (ptron_activation_value);
}



function removeParticle2PhysicsWorld() 
{
  world.removeParticle (world.particles[0]);
}






function addAttractor2PhysicsWorldWith (ptron_activation_value) 
{
  ptron_decision_attractor_x = width*0.5;

  if (ptron_activation_value < 0) 
  {
    ptron_decision_attractor_x = 10;
  }
  else 
  {
    ptron_decision_attractor_x = width-10;
  }

  ptron_decision_attractor_y = height*0.9;
  ptron_decision_attractor_h = height*0.1;
  
  ptron_decision_attractor = new c2.LineField (new c2.Line 
  (
    ptron_decision_attractor_x, 
    ptron_decision_attractor_y, 
    ptron_decision_attractor_x, 
    ptron_decision_attractor_y + ptron_decision_attractor_h), 
    0.9
  );
  world.addForce (ptron_decision_attractor);
}



function removeAttractor2PhysicsWorld() 
{
  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = undefined;
  }
}



function showPerceptronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    stroke (255, 255, 0);
    strokeWeight (1);
    noFill();

    line 
    (
      ptron_decision_attractor_x, 
      ptron_decision_attractor_y, 
      ptron_decision_attractor_x, 
      ptron_decision_attractor_y + ptron_decision_attractor_h
    );
  }
}


