

// Colors defined in index.css


const s1 = function (sketch) 
{
  sketch.setup = function() 
  {
    sketch.createCanvas (sketch.getCanvasContainerSize().w, sketch.getCanvasContainerSize().h);

    convertCSScolorsTop5js (sketch);
    addScreenPositionFunction (sketch);
    addPhysicsWorld (sketch);
    sketch.ellipseMode (sketch.RADIUS);
  }


  sketch.getCanvasContainerSize = function() 
  {
    const canvas_container_size = 
    {
      w: document.getElementById("decision_vis_container").offsetWidth,
      h: document.getElementById("decision_vis_container").offsetHeight
    };
    
    return canvas_container_size;
  }


  sketch.containersResized = function() 
  {
    sketch.resizeCanvas (sketch.getCanvasContainerSize().w, sketch.getCanvasContainerSize().h);
  }


  sketch.windowResized = function() 
  {
    sketch.resizeCanvas (sketch.getCanvasContainerSize().w, sketch.getCanvasContainerSize().h);
  }


  sketch.draw = function() 
  {
    sketch.clear();
    sketch.translate (sketch.width/2, sketch.height/2);

    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith (sketch);
    drawPtronActivationAnalogy2PhysicsWorld (sketch);
  }

};

const decision_sketch = new p5 (s1, "decision_vis_container");