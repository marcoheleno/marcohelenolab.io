

let input_range_user_goal_value;
let ptron_iterations_log_table;


const s3 = function (sketch) 
{
  sketch.setup = function() 
  {
    sketch.noCanvas();
    sketch.re_StartSystem();
  }


  sketch.sceneManager = function() 
  {
    if (current_scene === "txt_expla_1") sketch.re_StartSystem();
    else
    if (current_scene === "txt_expla_2") sketch.trainPerceptron();
  }


  sketch.re_StartSystem = function() 
  {
    createDataSetWith (3); //num_data_elements
    
    input_range_user_goal_value = document.getElementById("input_range_goal").value = parseFloat(sketch.random(-0.5, 0.5).toFixed(2));
  }


  sketch.trainPerceptron = function() 
  {
    startPerceptron (3); //num_weights
    sketch.createPerceptronIterationsTableLog();
    sketch.runPerceptron();
  }


  sketch.createPerceptronIterationsTableLog = function() 
  {
    ptron_iterations_log_table = 0;
    ptron_iterations_log_table = new p5.Table([0]);

    // data log
    ptron_iterations_log_table.addColumn ("data_id");
    ptron_iterations_log_table.addColumn ("data_input_x");
    ptron_iterations_log_table.addColumn ("data_input_y");
    ptron_iterations_log_table.addColumn ("data_input_bias");
    ptron_iterations_log_table.addColumn ("data_solution");

    // ptron log
    ptron_iterations_log_table.addColumn ("ptron_iteration_id");
    ptron_iterations_log_table.addColumn ("ptron_learning_rate");
    ptron_iterations_log_table.addColumn ("ptron_weight_x");
    ptron_iterations_log_table.addColumn ("ptron_weight_y");
    ptron_iterations_log_table.addColumn ("ptron_weight_bias");
    ptron_iterations_log_table.addColumn ("ptron_sum_weighted_inputs");
    ptron_iterations_log_table.addColumn ("ptron_activation_result");
    ptron_iterations_log_table.addColumn ("ptron_training_error");

    //console.log (ptron_iterations_log_table.columns);
    /*
    0: "data_id"
    1: "data_input_x"
    2: "data_input_y"
    3: "data_input_bias"
    4: "data_solution"
    5: "ptron_iteration_id"
    6: "ptron_learning_rate"
    7: "ptron_weight_x"
    8: "ptron_weight_y"
    9: "ptron_weight_bias"
   10: "ptron_sum_weighted_inputs"
   11: "ptron_activation_result"
   12: "ptron_training_error"
    */
  }


  sketch.runPerceptron = function() 
  {
    let ptron_trained_on_num_data_elements_in_first_lap = 0;
    let ptron_trained_on_num_data_elements = 0;
    let ptron_iteration_i = 0;

    while (ptron_trained_on_num_data_elements < data_set.length) 
    {

      for (let d=0; d<data_set.length; d++) //d==datapoint
      {
        ptron_iterations_log_table.addRow();
        const table_i = ptron_iterations_log_table.rows.length-1;

        // data log
        ptron_iterations_log_table.setNum (table_i, "data_id", d);
        ptron_iterations_log_table.setNum (table_i, "data_input_x",  data_set[d].inputs[0]);
        ptron_iterations_log_table.setNum (table_i, "data_input_y",  data_set[d].inputs[1]);
        ptron_iterations_log_table.setNum (table_i, "data_input_bias",  data_set[d].inputs[2]);
        ptron_iterations_log_table.setNum (table_i, "data_solution", data_set[d].solution);

        // ptron log
        ptron_iterations_log_table.setNum (table_i, "ptron_iteration_id", ptron_iteration_i);
        ptron_iterations_log_table.setNum (table_i, "ptron_learning_rate", ptron.learning_rate);
        ptron_iterations_log_table.setNum (table_i, "ptron_weight_x", ptron.weights[0]);
        ptron_iterations_log_table.setNum (table_i, "ptron_weight_y", ptron.weights[1]);
        ptron_iterations_log_table.setNum (table_i, "ptron_weight_bias", ptron.weights[2]);

        ptron.calSumWeightedInputsWith (data_set[d].inputs);
        ptron_iterations_log_table.setNum (table_i, "ptron_sum_weighted_inputs", ptron.sum_weighted_inputs_result);
        
        ptron.calActivationFunction();
        ptron_iterations_log_table.setNum (table_i, "ptron_activation_result", ptron.activation_result);

        ptron.calTrainingErrorWith (data_set[d].solution);
        ptron_iterations_log_table.setNum (table_i, "ptron_training_error", ptron.training_error);

        if (ptron.training_error === 0) 
        {
          ptron_trained_on_num_data_elements++;
        }

        ptron.endInTrainingWith (data_set[d].inputs);

        if (ptron.training_error === 0 && ptron_iteration_i < data_set.length) 
        {
          ptron_trained_on_num_data_elements_in_first_lap++;
        }
        else 
        {
          ptron_iteration_i++;
        }
      }

      if (ptron_trained_on_num_data_elements < data_set.length) 
      {
        ptron_trained_on_num_data_elements = 0;
      }
    }

    //console.log (data_set.length, ptron_trained_on_num_data_elements_in_first_lap);
    
    if (ptron_trained_on_num_data_elements_in_first_lap === data_set.length) 
    {
      sketch.trainPerceptron();
    }

    if (ptron_trained_on_num_data_elements === data_set.length) 
    {
      sketch.printPerceptronIterationsTableLog();
    }
  }


  sketch.printPerceptronIterationsTableLog = function() 
  {
    sketch.saveTable (ptron_iterations_log_table, "ptron_iterations_table_log", "csv");
  }


  sketch.getPerceptronIterationsRowLogAt = function (i) 
  {
    const ptron_iterations_log_row = 
    {
      data_id:                    ptron_iterations_log_table.getNum (i, "data_id"), 
      data_input_x:               ptron_iterations_log_table.getNum (i, "data_input_x"), 
      data_input_y:               ptron_iterations_log_table.getNum (i, "data_input_y"), 
      data_input_bias:            ptron_iterations_log_table.getNum (i, "data_input_bias"), 
      data_solution:              ptron_iterations_log_table.getNum (i, "data_solution"), 
      ptron_iteration_id:         ptron_iterations_log_table.getNum (i, "ptron_iteration_id"), 
      ptron_learning_rate:        ptron_iterations_log_table.getNum (i, "ptron_learning_rate"), 
      ptron_weight_x:             ptron_iterations_log_table.getNum (i, "ptron_weight_x"), 
      ptron_weight_y:             ptron_iterations_log_table.getNum (i, "ptron_weight_y"), 
      ptron_weight_bias:          ptron_iterations_log_table.getNum (i, "ptron_weight_bias"), 
      ptron_sum_weighted_inputs:  ptron_iterations_log_table.getNum (i, "ptron_sum_weighted_inputs"), 
      ptron_activation_result:    ptron_iterations_log_table.getNum (i, "ptron_activation_result"), 
      ptron_training_error:       ptron_iterations_log_table.getNum (i, "ptron_training_error")
    }
    
    return ptron_iterations_log_row;
  }

};

const perceptron_log_sketch = new p5 (s3);