

let data_set;


function dataThresholdWith (y) 
{
  // 2 * x + 1; original
  // 2 * y + 1; adapted
  return input_range_user_goal_value * y + 0;
}


function calRandomNumberBetween (min, max) 
{
  return Math.random() * (max - min) + min;
}


function createDataSetWith (num_data_elements)
{
  data_set = new Array (num_data_elements);

  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }
}


class DataElement 
{
  constructor () 
  {
    this.x = calRandomNumberBetween (-1, 1);
    this.y = calRandomNumberBetween (-1, 1);
    this.bias = 1;
    this.inputs = [this.x, this.y, this.bias];

    this.sml_radius = 0.3;
    this.big_radius = 0.9;
  }


  runSolutionProviderForPtronTraining()
  {
    this.solution = 0;
    
    if (this.x <= dataThresholdWith(this.y)) 
    {
      this.solution = -1;
    }
    else 
    {
      this.solution =  1;
    }
  }

  
  updateElementRadius (sketch, canvas_width) 
  {
    //this.canvas_radius = sketch.map (this.sml_radius, -3, 3, -canvas_width/2, canvas_width/2);

    if (this.solution === -1) 
    {
      //this.canvas_radius = sketch.abs(this.canvas_radius);
      this.canvas_radius = sketch.map (this.sml_radius, -3, 3, -canvas_width/2, canvas_width/2);
      console.log ("s " + this.canvas_radius);
    }
    else 
    if (this.solution ===  1) 
    {
      this.canvas_radius = sketch.map (this.big_radius, -3, 3, -canvas_width/2, canvas_width/2);
      //this.canvas_radius = sketch.abs(this.canvas_radius)*3;
      console.log ("b " + this.canvas_radius);
    }

    /*
    this.canvas_radius = 0;

    if (this.solution === -1) 
    {
      this.canvas_radius = sketch.map (this.sml_radius, -3, 3, -canvas_width/2, canvas_width/2);
      console.log ("s" + this.sml_radius + " - " + sketch.abs(this.canvas_radius));
    }

    else 

    if (this.solution ===  1) 
    {
      this.canvas_radius = sketch.map (this.big_radius, -3, 3, -canvas_width/2, canvas_width/2);
      console.log ("b" + this.big_radius + " - " + sketch.abs(this.canvas_radius));
    }
    */
  }


  updateElementCanvasXY (sketch, canvas_width) 
  {
    this.canvas_x = sketch.map (this.x, -1, 1, -canvas_width/2, canvas_width/2);
    this.canvas_y = sketch.map (this.y, -1, 1, -canvas_width/2, canvas_width/2);
  }


  updateElementClassXY (sketch, canvas_x, canvas_y, canvas_width) 
  {
    this.x = sketch.map (canvas_x, -canvas_width/2, canvas_width/2, -1, 1);
    this.y = sketch.map (canvas_y, -canvas_width/2, canvas_width/2, -1, 1);

    this.canvas_x = sketch.map (this.x, -1, 1, -canvas_width/2, canvas_width/2);
    this.canvas_y = sketch.map (this.y, -1, 1, -canvas_width/2, canvas_width/2);
  }


  drawElement (sketch) 
  {
    sketch.point (this.canvas_x, this.canvas_y);
  }


  drawElementBody (sketch) 
  {
    sketch.noFill();
    sketch.ellipse (this.canvas_x, this.canvas_y, this.canvas_radius);
  }
  
}