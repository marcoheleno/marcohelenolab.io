

let input_range_user_goal_value;
let ptron_i_table_log;
let ptron_i = 0;


const s3 = function (sketch) 
{
  sketch.setup = function() 
  {
    sketch.noCanvas();
    convertCSScolorsTop5js (sketch);
    sketch.re_StartSystem();
  }


  sketch.sceneManager = function() 
  {
    if (current_scene === "txt_expla_1") sketch.re_StartSystem();
    else
    if (current_scene === "txt_expla_2") sketch.re_StartPtron();
  }


  sketch.re_StartSystem = function() 
  {
    createDataSetWith (3); //num_data_elements
    input_range_user_goal_value = document.getElementById("input_range_goal").value = parseFloat(sketch.random(-0.5, 0.5).toFixed(2));
  }


  sketch.re_StartPtron = function() 
  {
    startPtronWith (3); //num_weights
    sketch.createPtronTableLog();
    sketch.runPtronWithLog();
    ptron_i = 0;
  }


  sketch.createPtronTableLog = function() 
  {
    ptron_i_table_log = 0;
    ptron_i_table_log = new p5.Table([0]);

    // data log
    ptron_i_table_log.addColumn ("data_id");
    ptron_i_table_log.addColumn ("data_input_x");
    ptron_i_table_log.addColumn ("data_input_y");
    ptron_i_table_log.addColumn ("data_input_bias");
    ptron_i_table_log.addColumn ("data_solution");

    // ptron log
    ptron_i_table_log.addColumn ("ptron_i_id");
    ptron_i_table_log.addColumn ("ptron_learning_rate");
    ptron_i_table_log.addColumn ("ptron_weight_x");
    ptron_i_table_log.addColumn ("ptron_weight_y");
    ptron_i_table_log.addColumn ("ptron_weight_bias");
    ptron_i_table_log.addColumn ("ptron_sum_weighted_inputs");
    ptron_i_table_log.addColumn ("ptron_activation_result");
    ptron_i_table_log.addColumn ("ptron_training_error");

    //console.log (ptron_i_table_log.columns);
    /*
    0: "data_id"
    1: "data_input_x"
    2: "data_input_y"
    3: "data_input_bias"
    4: "data_solution"
    5: "ptron_i_id"
    6: "ptron_learning_rate"
    7: "ptron_weight_x"
    8: "ptron_weight_y"
    9: "ptron_weight_bias"
   10: "ptron_sum_weighted_inputs"
   11: "ptron_activation_result"
   12: "ptron_training_error"
    */
  }


  sketch.runPtronWithLog = function() 
  {
    let ptron_trained = false;

    while (!ptron_trained) 
    {

      for (let d=0; d<data_set.length; d++) //d==datapoint
      {
        let new_log_row = ptron_i_table_log.addRow();

        // data log
        new_log_row.setNum ("data_id", d);
        new_log_row.setNum ("data_input_x", data_set[d].inputs[0]);
        new_log_row.setNum ("data_input_y", data_set[d].inputs[1]);
        new_log_row.setNum ("data_input_bias", data_set[d].inputs[2]);
        new_log_row.setNum ("data_solution", data_set[d].solution);

        // ptron log
        new_log_row.setNum ("ptron_i_id", ptron_i_table_log.getRowCount()-1);
        new_log_row.setNum ("ptron_learning_rate", ptron.learning_rate);
        new_log_row.setNum ("ptron_weight_x", ptron.weights[0]);
        new_log_row.setNum ("ptron_weight_y", ptron.weights[1]);
        new_log_row.setNum ("ptron_weight_bias", ptron.weights[2]);

        ptron.calSumWeightedInputsWith (data_set[d].inputs);
        new_log_row.setNum ("ptron_sum_weighted_inputs", ptron.sum_weighted_inputs_result);
        
        ptron.calActivationFunction();
        new_log_row.setNum ("ptron_activation_result", ptron.activation_result);

        ptron.calTrainingErrorWith (data_set[d].solution);
        new_log_row.setNum ("ptron_training_error", ptron.training_error);
        
        ptron.endInTrainingWith (data_set[d].inputs);
      }

      if 
      (
        ptron_i_table_log.getRow(0).getNum("ptron_training_error") === 0 && 
        ptron_i_table_log.getRow(1).getNum("ptron_training_error") === 0 && 
        ptron_i_table_log.getRow(2).getNum("ptron_training_error") === 0
      )
      {
        sketch.re_StartPtron();
      }

      if 
      (
        ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-3).getNum("ptron_training_error") === 0 && 
        ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-2).getNum("ptron_training_error") === 0 && 
        ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-1).getNum("ptron_training_error") === 0
      )
      {
        ptron_trained = true;
        
        //sketch.printPtron_i_TableLog();

        let temp_sml_radius = new Array (0);
        let temp_big_radius = new Array (0);

        for (let i=3; i>0; i--) 
        {
          if (ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-i).getNum("data_solution") < 0) 
          {
            temp_sml_radius.push (ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-i).getNum("ptron_sum_weighted_inputs"));
          }
          else 
          if (ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-i).getNum("data_solution") > 0) 
          {
            temp_big_radius.push (ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-i).getNum("ptron_sum_weighted_inputs"));
          }
        }
        
        temp_sml_radius.sort();
        temp_big_radius.sort();

        //console.log (temp_sml_radius[0]);
        //console.log (temp_big_radius[0]);

        for (let d=0; d<data_set.length; d++) 
        {
          data_set[d].sml_radius = temp_sml_radius[0];
          data_set[d].big_radius = temp_big_radius[0];
        }
      }

      /*
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-3).getNum("data_id"));
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-2).getNum("data_id"));
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-1).getNum("data_id"));

      console.log("---");

      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-3).getNum("ptron_i_id"));
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-2).getNum("ptron_i_id"));
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-1).getNum("ptron_i_id"));

      console.log("---");

      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-3).getNum("ptron_training_error"));
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-2).getNum("ptron_training_error"));
      console.log(ptron_i_table_log.getRow(ptron_i_table_log.getRowCount()-1).getNum("ptron_training_error"));

      console.log("------------");
      */
    }
    
  }


  sketch.printPtron_i_TableLog = function() 
  {
    sketch.saveTable (ptron_i_table_log, "ptron_iterations_table_log", "csv");
  }


  sketch.getPtron_i_Row = function (i) 
  {
    const ptron_iterations_log_row = 
    {
      data_id:                    ptron_i_table_log.getNum (i, "data_id"), 
      data_input_x:               ptron_i_table_log.getNum (i, "data_input_x"), 
      data_input_y:               ptron_i_table_log.getNum (i, "data_input_y"), 
      data_input_bias:            ptron_i_table_log.getNum (i, "data_input_bias"), 
      data_solution:              ptron_i_table_log.getNum (i, "data_solution"), 
      ptron_i_id:                 ptron_i_table_log.getNum (i, "ptron_i_id"), 
      ptron_learning_rate:        ptron_i_table_log.getNum (i, "ptron_learning_rate"), 
      ptron_weight_x:             ptron_i_table_log.getNum (i, "ptron_weight_x"), 
      ptron_weight_y:             ptron_i_table_log.getNum (i, "ptron_weight_y"), 
      ptron_weight_bias:          ptron_i_table_log.getNum (i, "ptron_weight_bias"), 
      ptron_sum_weighted_inputs:  ptron_i_table_log.getNum (i, "ptron_sum_weighted_inputs"), 
      ptron_activation_result:    ptron_i_table_log.getNum (i, "ptron_activation_result"), 
      ptron_training_error:       ptron_i_table_log.getNum (i, "ptron_training_error")
    }
    
    return ptron_iterations_log_row;
  }

};

const perceptron_log_sketch = new p5 (s3);