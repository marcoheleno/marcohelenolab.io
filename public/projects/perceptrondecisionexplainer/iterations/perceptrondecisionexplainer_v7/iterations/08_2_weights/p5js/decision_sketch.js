

// Colors defined in index.css


const s1 = function (sketch) 
{
  sketch.setup = function() 
  {
    sketch.createCanvas (sketch.getCanvasContainerSize().w, sketch.getCanvasContainerSize().h);
    sketch.ellipseMode  (sketch.RADIUS);

    addScreenPositionFunction (sketch);
    addPhysicsWorld (sketch);
  }


  sketch.getCanvasContainerSize = function() 
  {
    const canvas_container_size = 
    {
      w: document.getElementById ("decision_vis_container").offsetWidth,
      h: document.getElementById ("decision_vis_container").offsetHeight
    };
    
    return canvas_container_size;
  }


  sketch.containersResized = function() 
  {
    sketch.resizeCanvas (sketch.getCanvasContainerSize().w, sketch.getCanvasContainerSize().h);
  }


  sketch.windowResized = function() 
  {
    sketch.resizeCanvas (sketch.getCanvasContainerSize().w, sketch.getCanvasContainerSize().h);
  }


  sketch.draw = function() 
  {
    sketch.clear();
    sketch.translate (sketch.width/2, sketch.height/2);
    
    if (current_scene === "txt_expla_2") 
    {
      for (let d=0; d<data_set.length; d++) 
      {
        data_set[d].updateElementRadius (sketch, sketch.width);
      }

      drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith 
      (
        sketch, 
        perceptron_log_sketch.getPtron_i_Row(ptron_i).ptron_sum_weighted_inputs
      );

      drawPtronActivationAnalogy2PhysicsWorld (sketch);
    
      if (sketch.frameCount%60===0) 
      {
        if (ptron_i < ptron_i_table_log.getRowCount()-1) ptron_i++;
        else 
        {
          console.log ("terminou");
          sketch.noLoop();
        }
      }
    }
    
  }

};

const decision_sketch = new p5 (s1, "decision_vis_container");