

let color_level_1, color_level_2, color_level_3, color_level_4;
let color_empha_n, color_empha_p;



// IntersectionObserver

const options = 
{
  root: document.getElementsByTagName("article")[0], 
  rootMargin: "0px", 
  threshold: 1.0
}

const callback = (entries, observer) => 
{
  entries.forEach((entry) => 
  {
    // Each entry describes an intersection change for one observed target element:
    //   entry.boundingClientRect
    //   entry.intersectionRatio
    //   entry.intersectionRect
    //   entry.isIntersecting
    //   entry.rootBounds
    //	 entry.target
    //   entry.time
	
	  if (entry.isIntersecting) 
    {
      sceneManager (entry.target.getAttribute('id'));
    }

    else 
    {
    }

  });
};

const observer = new IntersectionObserver (callback, options);

observer.observe (document.getElementById("txt_expla_1"));
observer.observe (document.getElementById("txt_expla_2"));
observer.observe (document.getElementById("txt_expla_3"));
observer.observe (document.getElementById("txt_expla_4"));
observer.observe (document.getElementById("txt_expla_5"));



// CSS colors for p5js

function convertCSScolorsTop5js()
{
    // Colors defined in index.css
    color_level_1 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_1") + ")";
    color_level_2 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_2") + ")";
    color_level_3 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_3") + ")";
    color_level_4 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_4") + ")";

    color_empha_n = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_n") + ")";
    color_empha_p = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_p") + ")";


    color_level_1 = color (color_level_1);
    color_level_2 = color (color_level_2);
    color_level_3 = color (color_level_3);
    color_level_4 = color (color_level_4);

    color_empha_n = color (color_empha_n);
    color_empha_p = color (color_empha_p);
}