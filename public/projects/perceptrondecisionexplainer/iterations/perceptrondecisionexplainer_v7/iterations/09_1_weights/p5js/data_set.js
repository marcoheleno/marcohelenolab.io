

let data_set;


function createDataSetWith (num_data_elements)
{
  data_set = new Array (num_data_elements);

  for (let i=0; i<data_set.length; i++) 
  {
    data_set[i] = new DataElement();
  }
}


class DataElement 
{
  constructor () 
  {
    this.x = random (-1, 1);
    this.y = random (-1, 1);
    this.bias = 1;
    this.inputs = [this.x, this.y, this.bias];

    this.sml_radius = 0.3;
    this.big_radius = 0.9;

    this.runSolutionProviderForPtronTraining();
  }


  runSolutionProviderForPtronTraining()
  {
    this.solution = 0;
    
    if (this.x <= 0) 
    {
      this.solution = -1;
    }
    else 
    {
      this.solution =  1;
    }
  }

  
  updateElementRadius (canvas_width) 
  {
    //this.canvas_radius = map (this.sml_radius, -3, 3, -canvas_width/2, canvas_width/2);

    if (this.solution === -1) 
    {
      //this.canvas_radius = abs(this.canvas_radius);
      this.canvas_radius = map (this.sml_radius, -3, 3, -canvas_width/2, canvas_width/2);
      console.log ("s " + this.canvas_radius);
    }
    else 
    if (this.solution ===  1) 
    {
      this.canvas_radius = map (this.big_radius, -3, 3, -canvas_width/2, canvas_width/2);
      //this.canvas_radius = abs(this.canvas_radius)*3;
      console.log ("b " + this.canvas_radius);
    }

    /*
    this.canvas_radius = 0;

    if (this.solution === -1) 
    {
      this.canvas_radius = map (this.sml_radius, -3, 3, -canvas_width/2, canvas_width/2);
      console.log ("s" + this.sml_radius + " - " + abs(this.canvas_radius));
    }

    else 

    if (this.solution ===  1) 
    {
      this.canvas_radius = map (this.big_radius, -3, 3, -canvas_width/2, canvas_width/2);
      console.log ("b" + this.big_radius + " - " + abs(this.canvas_radius));
    }
    */
  }


  updateElementCanvasXY (canvas_width) 
  {
    this.canvas_x = map (this.x, -1, 1, -canvas_width/2, canvas_width/2);
    this.canvas_y = map (this.y, -1, 1, -canvas_width/2, canvas_width/2);
  }


  updateElementClassXY (canvas_x, canvas_y, canvas_width) 
  {
    this.x = map (canvas_x, -canvas_width/2, canvas_width/2, -1, 1);
    this.y = map (canvas_y, -canvas_width/2, canvas_width/2, -1, 1);

    this.canvas_x = map (this.x, -1, 1, -canvas_width/2, canvas_width/2);
    this.canvas_y = map (this.y, -1, 1, -canvas_width/2, canvas_width/2);
  }


  drawElement() 
  {
    point (this.canvas_x, this.canvas_y);
  }


  drawElementBody() 
  {
    noFill();
    ellipse (this.canvas_x, this.canvas_y, this.canvas_radius);
  }
  
}