

// Colors defined in index.css


function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  canvas.parent ("decision_vis_container");
  ellipseMode  (RADIUS);

  convertCSScolorsTop5js();

  addScreenPositionFunction();
  //addPhysicsWorld();
}


function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById ("decision_vis_container").offsetWidth,
    h: document.getElementById ("decision_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}


function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
}


function sceneManager (current_scene) 
{
  if (current_scene === "txt_expla_1") re_StartSystem();
  else
  if (current_scene === "txt_expla_2") re_StartPtron();
}


function re_StartSystem() 
{
  createDataSetWith (3);
}


function re_StartPtron() 
{
  startPtronWith (3);
  createPtronTableLog();
  runPtronWithLog();
}


function draw() 
{
  clear();
  translate (width/2, height/2);

  /*
  if (current_scene === "txt_expla_2") 
  {
    for (let d=0; d<data_set.length; d++) 
    {
      data_set[d].updateElementRadius (width);
    }

    drawPtronSumWeightedInputsAnalogy2PhysicsWorldWith (perceptron_log_getPtron_i_Row(ptron_i).ptron_sum_weighted_inputs);

    drawPtronActivationAnalogy2PhysicsWorld();
  
    if (frameCount%60===0) 
    {
      if (ptron_i < ptron_table_log.getRowCount()-1) ptron_i++;
      else 
      {
        console.log ("terminou");
        noLoop();
      }
    }
  }
  */
}