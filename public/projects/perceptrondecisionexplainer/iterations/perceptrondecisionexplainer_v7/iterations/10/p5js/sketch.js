

// Colors defined in index.css
let color_level_1, color_level_2, color_level_3, color_level_4;
let color_empha_n, color_empha_p;
let current_scene;
let log_i;


function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  canvas.parent ("decision_vis_container");
  ellipseMode (RADIUS);
  convertCSScolorsTop5js();
  addScreenPositionFunction();
}


function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById ("decision_vis_container").offsetWidth,
    h: document.getElementById ("decision_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}


function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  updateDataSet();
}


function convertCSScolorsTop5js()
{
    // Colors defined in index.css
    color_level_1 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_1") + ")";
    color_level_2 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_2") + ")";
    color_level_3 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_3") + ")";
    color_level_4 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_4") + ")";

    color_empha_n = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_n") + ")";
    color_empha_p = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_p") + ")";


    color_level_1 = color (color_level_1);
    color_level_2 = color (color_level_2);
    color_level_3 = color (color_level_3);
    color_level_4 = color (color_level_4);

    color_empha_n = color (color_empha_n);
    color_empha_p = color (color_empha_p);
}


function sceneManager (scene_input) 
{
  current_scene = scene_input;

  if (current_scene === "scene_1") re_StartSystem();
  else
  if (current_scene === "scene_2") re_StartPtron();

  console.log (current_scene);
}


function re_StartSystem() 
{
  createDataSet();
  addPhysicsWorld();
}


function re_StartPtron() 
{
  startPtron();
  createPtronTableLog();
  runPtronWithLog();
  log_i = 0;
}


function draw() 
{
  clear();
  translate (width/2, height/2);



  if (current_scene === "scene_1") 
  {
    drawPtronActivationAnalogy();

    for (let d=0; d<data_set.length; d++) 
    {
      let x = 0;
      if (data_set[d].solution === -1) x = (width*0.05) * data_set[d].solution + data_set[0].min_canvas_radius*data_set[d].solution;
      else
      if (data_set[d].solution ===  1) x = (width*0.05) * data_set[d].solution + data_set[0].max_canvas_radius*data_set[d].solution;

      const y = -(height*0.15) + data_set[0].max_canvas_radius + d*(data_set[0].max_canvas_radius*2);

      let r = 0;
      if (data_set[d].solution === -1) r = data_set[0].min_canvas_radius;
      else
      if (data_set[d].solution ===  1) r = data_set[0].max_canvas_radius;

      stroke (color_level_3);
      strokeWeight (1);
      noFill();
      ellipse (x, y, r);


      if (data_set[d].solution === -1) document.getElementById("data_element_radio_left_"  + d).setAttribute("checked", "checked");
      else
      if (data_set[d].solution ===  1) document.getElementById("data_element_radio_right_" + d).setAttribute("checked", "checked");

      for (let i=0; i<document.getElementsByName("data_element_radio_" + d).length; i++) 
      {
        if (document.getElementsByName("data_element_radio_" + d)[i].checked) 
        {
          data_set[d].calRandomRadius( int(document.getElementsByName("data_element_radio_" + d)[i].value) );
        }
      }

      document.getElementById("data_element_switch_" + d).style.height = (data_set[0].max_canvas_radius*2) + "px";
      document.getElementById("data_element_switch_" + d).style.left = (width/2  + width*0.05  +  data_set[0].max_canvas_radius*2   ) + "px";
      document.getElementById("data_element_switch_" + d).style.top  = (height/2 - height*0.15 + (data_set[0].max_canvas_radius*2)*d) + "px";
    }
  }



  else



  if (current_scene === "scene_2") 
  {
    drawPtronSumWeightedInputsAnalogyWith 
    (
      getPtronIterationAt(log_i).ptron_sum_weighted_inputs, 
      true, 
      getPtronIterationAt(log_i).ptron_training_error
    );

    drawPtronActivationAnalogy();

    drawDataElementAnalogy
    (
      getPtronIterationAt(log_i).data_id, 
      getPtronIterationAt(log_i).ptron_activation_result, 
      true, 
      getPtronIterationAt(log_i).ptron_training_error
    );

    drawPtronDecisionAttractor();

    if (world.particles.length === 0) 
    {
      if (log_i < ptron_iterations_log.getRowCount()-1) 
      {
        log_i++;
      }

      else 
      {
        console.log ("terminou");
      }
    }
  }



}