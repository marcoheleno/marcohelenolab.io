

let data_set;


function createDataSet()
{
  data_set = new Array (3);

  for (let d=0; d<data_set.length; d++) 
  {
    data_set[d] = new DataElement();
  }
}


function updateDataSet()
{
  for (let d=0; d<data_set.length; d++) 
  {
    data_set[d].updateCanvasRadius();
  }
}


class DataElement 
{
  constructor () 
  {
    this.min_radius = 0.3;
    this.max_radius = 0.7;

    this.calRandomRadius( int(random(0, 2)) );
  }


  calRandomRadius (radius_option)
  {
    this.ele_radius = 0;

    if (radius_option === 0) this.ele_radius = this.min_radius;
    else
    if (radius_option === 1) this.ele_radius = this.max_radius;

    this.updateCanvasRadius();
    this.calSolutionForPtronTraining();
  }


  updateCanvasRadius() 
  {
    this.min_canvas_radius = map (this.min_radius, 0, 1, 0, width/13);
    this.max_canvas_radius = map (this.max_radius, 0, 1, 0, width/13);
    this.ele_canvas_radius = map (this.ele_radius, 0, 1, 0, width/13);
  }


  calSolutionForPtronTraining()
  {
    this.threshold = 0.5;
    this.solution = 0;

    if (this.ele_radius <= this.threshold) 
      this.solution = -1;
    else 
      this.solution = 1;

    this.buildElement();
  }


  buildElement() 
  {
    const bias = 1;
    this.inputs = [this.ele_radius, bias];
    this.solution;
  }
  
}