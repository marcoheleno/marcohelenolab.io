

let world, collision, gravityForce, floorForce;
let ptron_sum_weighted_inputs_constraint;
let ptron_activation_constraint;
let ptron_decision_attractor, attractor_x, attractor_y, attractor_h;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect (0,          0 - data_set[0].max_canvas_radius, 
                                     width, height + data_set[0].max_canvas_radius*3) );
  
  if (collision != undefined) 
  world.removeInteractionForce (collision);
  collision  = new c2.Collision();
  world.addInteractionForce (collision);


  if (gravityForce != undefined) 
  world.removeForce (gravityForce);
  gravityForce = new c2.ConstForce (0, 1);
  world.addForce (gravityForce);


  if (floorForce != undefined) 
  world.removeForce (floorForce);
  floorForce = new c2.LineField (new c2.Line 
  (
    0, 
    height + data_set[0].max_canvas_radius*2, 
    width, 
    height + data_set[0].max_canvas_radius*2
  ), 1);

  world.addForce (floorForce);
}



function drawPtronSumWeightedInputsAnalogyWith 
(
  ptron_sum_weighted_inputs_value, 
  ptron_activation_value, 
  is_ptron_checking_error, 
  ptron_training_error_value
) 
{
  const x = width*0.5;
  const y = height*0.3;
  
  const ptron_sum_weighted_inputs_w = width*0.5;

  // ptron_sum_weighted_inputs_openness
  //const ptron_sum_weighted_inputs_o = map (mouseX, 0, width, ptron_sum_weighted_inputs_w, 0);
  //const ptron_sum_weighted_inputs_o = map (ptron_sum_weighted_inputs_value, -3, 0, ptron_sum_weighted_inputs_w, 0);// + data_set[0].ele_canvas_radius*2;
  const ptron_sum_weighted_inputs_o = map(data_set[0].threshold, 0, 1, 0, width/13)*2 - ptron_sum_weighted_inputs_value*2 - (data_set[0].ele_canvas_radius*3)*ptron_activation_value;

  console.log ( ptron_sum_weighted_inputs_o + " | " + (data_set[0].min_canvas_radius*2) + " | " + (data_set[0].max_canvas_radius*2) );
  

  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (45);
    
    const ptron_sum_weighted_inputs_left_margin = screenPosition (-ptron_sum_weighted_inputs_w, 0);
    const ptron_sum_weighted_inputs_left_center = screenPosition (-ptron_sum_weighted_inputs_o, 0);
  pop();


  if (ptron_sum_weighted_inputs_constraint != undefined) 
    world.removeConstraint (ptron_sum_weighted_inputs_constraint);
  
  const ptron_sum_weighted_inputs_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y), 
    new c2.Point (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y)
  ]);

  ptron_sum_weighted_inputs_constraint  = new c2.PolygonConstraint (ptron_sum_weighted_inputs_polygon);
  world.addConstraint (ptron_sum_weighted_inputs_constraint);


  noFill();
  strokeWeight (1);
  
  if (is_ptron_checking_error === false) stroke (color_level_2);
  else 
  {
    if (ptron_training_error_value === 0) stroke (color_empha_p);
    else stroke (color_empha_n);
  }
  

  beginShape();
    vertex (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y);
    vertex (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y);
  endShape();
}



function drawPtronActivationAnalogy() 
{
  const x = width*0.5;
  const y = height*0.3;
  const h = (data_set[0].max_canvas_radius*2)*3;

  const upper_point = {x:x, y:y};
  const lower_point = {x:x, y:y+h};
  
  if (ptron_activation_constraint != undefined) 
    world.removeConstraint (ptron_activation_constraint);

  const ptron_act_polygon = new c2.Polygon 
  ([
    new c2.Point (upper_point.x, upper_point.y), 
    new c2.Point (lower_point.x, lower_point.y)
  ]);

  ptron_activation_constraint = new c2.PolygonConstraint (ptron_act_polygon);
  world.addConstraint (ptron_activation_constraint);

  stroke (color_level_2);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (upper_point.x, upper_point.y);
    vertex (lower_point.x, lower_point.y);
  endShape();
}



function drawDataElementAnalogy 
(
  data_id_value, 
  ptron_activation_value, 
  is_ptron_checking_error, 
  ptron_training_error_value
) 
{
  if (world.particles.length < 1) 
  {
    addDataElementAnalogy (data_id_value, ptron_activation_value);
  }

  world.update();

  if (is_ptron_checking_error === false)  stroke (color_level_2);
  else 
  {
    if (ptron_training_error_value === 0) stroke (color_empha_p);
    else stroke (color_empha_n);
  }

  ellipse 
  (
    world.particles[0].position.x, 
    world.particles[0].position.y, 
    world.particles[0].radius
  );

  text(data_id_value, world.particles[0].position.x, world.particles[0].position.y);

  if (world.particles[0].position.y > height*0.5) 
  {
    removePtronDecisionAttractor();
  }

  if (world.particles[0].position.y > height) 
  {
    removeDataElementAnalogy();
  }
}



function addDataElementAnalogy (data_id_value, ptron_activation_value) 
{
  const data_element_analogy  = new c2.Particle 
  (
    width*0.5 - data_set[0].max_canvas_radius, 
            0 - data_set[0].max_canvas_radius
  );

  data_element_analogy.radius = data_set[data_id_value].ele_canvas_radius;

  world.addParticle (data_element_analogy);

  addPtronDecisionAttractor (ptron_activation_value);
}



function removeDataElementAnalogy() 
{
  world.removeParticle (world.particles[0]);
}



function addPtronDecisionAttractor (ptron_activation_value) 
{
  attractor_x = width*0.5;

  if (ptron_activation_value < 0) 
  {
    attractor_x = 10;
  }
  else 
  {
    attractor_x = width - 10;
  }

  attractor_h = 100;
  attractor_y = height - attractor_h;
  
  ptron_decision_attractor = new c2.LineField (new c2.Line 
  (
    attractor_x, 
    attractor_y, 
    attractor_x, 
    attractor_y + attractor_h), 
    0.9
  );
  world.addForce (ptron_decision_attractor);
}



function removePtronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    world.removeForce (ptron_decision_attractor);
    ptron_decision_attractor = undefined;
  }
}


function drawPtronDecisionAttractor() 
{
  if (ptron_decision_attractor != undefined) 
  {
    stroke (color_level_4);
    strokeWeight (1);

    line 
    (
      attractor_x, 
      attractor_y, 
      attractor_x, 
      attractor_y + attractor_h
    );
  }
}