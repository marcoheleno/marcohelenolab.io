

// Colors defined in index.css
let color_level_1, color_level_2, color_level_3, color_level_4;
let color_empha_n, color_empha_p;
let current_scene;
let log_i;


function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  canvas.parent ("decision_vis_container");
  ellipseMode (RADIUS);
  convertCSScolorsTop5js();
  addScreenPositionFunction();
  createDataSet();
}


function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById ("decision_vis_container").offsetWidth,
    h: document.getElementById ("decision_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}


function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  updateDataSet();
}


function convertCSScolorsTop5js()
{
    // Colors defined in index.css
    color_level_1 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_1") + ")";
    color_level_2 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_2") + ")";
    color_level_3 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_3") + ")";
    color_level_4 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_4") + ")";

    color_empha_n = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_n") + ")";
    color_empha_p = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_p") + ")";
    color_empha_t = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_t") + ")";


    color_level_1 = color (color_level_1);
    color_level_2 = color (color_level_2);
    color_level_3 = color (color_level_3);
    color_level_4 = color (color_level_4);

    color_empha_n = color (color_empha_n);
    color_empha_p = color (color_empha_p);
    color_empha_t = color (color_empha_t);
}


function sceneManager (scene_input) 
{
  current_scene = scene_input;

  if (current_scene === "scene_1") re_StartSystem();
  else
  if (current_scene === "scene_2") re_StartPtron();
}


function re_StartSystem() 
{
  addPhysicsWorld();

  document.getElementsByTagName("figcaption")[0].style.visibility = "hidden";
}


function re_StartPtron() 
{
  startPtron();
  createPtronTableLog();
  runPtronWithLog();
  log_i = 0;

  //document.getElementsByTagName("figcaption")[0].style.visibility = "visible";
}


function draw() 
{
  clear();



  if (current_scene === "scene_1") 
  {
    drawPtronActivationAnalogy (height*0.25);

    for (let d=0; d<data_set.length; d++) 
    {
      let x = 0;
      if (data_set[d].solution === -1) x = (width*0.05) * data_set[d].solution + width*0.5 + data_set[0].min_canvas_radius*data_set[d].solution;
      else
      if (data_set[d].solution ===  1) x = (width*0.05) * data_set[d].solution + width*0.5 + data_set[0].max_canvas_radius*data_set[d].solution;

      const y = height*0.25 + data_set[0].max_canvas_radius + d*(data_set[0].max_canvas_radius*2);

      let r = 0;
      if (data_set[d].solution === -1) r = data_set[0].min_canvas_radius;
      else
      if (data_set[d].solution ===  1) r = data_set[0].max_canvas_radius;

      stroke (color_level_3);
      strokeWeight (1);
      noFill();
      ellipse (x, y, r-2);


      if (data_set[d].solution === -1) document.getElementById("data_element_radio_left_"  + d).setAttribute("checked", "checked");
      else
      if (data_set[d].solution ===  1) document.getElementById("data_element_radio_right_" + d).setAttribute("checked", "checked");

      for (let i=0; i<document.getElementsByName("data_element_radio_" + d).length; i++) 
      {
        if (document.getElementsByName("data_element_radio_" + d)[i].checked) 
        {
          data_set[d].calRandomRadius( int(document.getElementsByName("data_element_radio_" + d)[i].value) );
        }
      }

      document.getElementById("data_element_switch_" + d).style.height = (data_set[0].max_canvas_radius*2) + "px";
      document.getElementById("data_element_switch_" + d).style.right = (width*0.5 - 15 + width*0.05 + data_set[0].min_canvas_radius*2) + "px"; //(width*0.05 + width*0.5 + data_set[0].max_canvas_radius*2) + "px";
      document.getElementById("data_element_switch_" + d).style.top  = (y - data_set[0].max_canvas_radius) + "px";
    }
  }



  else



  if (current_scene === "scene_2") 
  {
    drawPtronSumWeightedInputsAnalogyWith 
    (
      getPtronIterationAt(log_i).ptron_sum_weighted_inputs, 
      getPtronIterationAt(log_i).ptron_activation_result, 
      false, 
      getPtronIterationAt(log_i).ptron_training_error
    );
    
    drawPtronActivationAnalogy (height*0.4);

    drawDataElementAnalogy
    (
      getPtronIterationAt(log_i).data_id, 
      getPtronIterationAt(log_i).ptron_activation_result, 
      true, 
      getPtronIterationAt(log_i).ptron_training_error
    );

    //drawPtronTrainingAnalogy (getPtronIterationAt(log_i).ptron_activation_result);

    drawPtronDecisionAttractor();

    if (world.particles.length === 0) 
    {
      if (log_i<ptron_iterations_log.getRowCount()-1 && log_i<data_set.length*2-1) 
      {
        log_i++;
      }

      else 
      {
        console.log("next automático");
      }
    }
  }



  else



  if (current_scene === "scene_3") 
  {
    drawPtronSumWeightedInputsAnalogyWith 
    (
      getPtronIterationAt(log_i).ptron_sum_weighted_inputs, 
      getPtronIterationAt(log_i).ptron_activation_result, 
      false, 
      getPtronIterationAt(log_i).ptron_training_error
    );
    
    drawPtronActivationAnalogy (height*0.4);

    drawDataElementAnalogy
    (
      getPtronIterationAt(log_i).data_id, 
      getPtronIterationAt(log_i).ptron_activation_result, 
      true, 
      getPtronIterationAt(log_i).ptron_training_error
    );

    //drawPtronTrainingAnalogy (getPtronIterationAt(log_i).ptron_activation_result);

    drawPtronDecisionAttractor();

    if (world.particles.length === 0) 
    {
      if (log_i < ptron_iterations_log.getRowCount()-1) 
      {
        log_i++;
      }

      else 
      {
        console.log ("next histórico");
        //log_i-=2;
      }
    }
  }



}