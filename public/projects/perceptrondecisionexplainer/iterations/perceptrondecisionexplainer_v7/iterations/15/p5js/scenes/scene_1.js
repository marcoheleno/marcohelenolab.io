


function drawScene1() 
{
  if (data_set[0].solution === -1 && data_set[1].solution === -1 && data_set[2].solution === -1 
      || 
      data_set[0].solution ===  1 && data_set[1].solution ===  1 && data_set[2].solution ===  1)
  {
    createDataSet();
    alert("Don't put them all on the same side...");
  }
  
  
  drawPtronActivationAnalogy (height*0.25);

  for (let d=0; d<data_set.length; d++) 
  {
    let x = 0;
    if (data_set[d].solution===-1) x = width*0.5 - width*0.05 - data_set[0].min_canvas_radius;
    else
    if (data_set[d].solution=== 1) x = width*0.5 + width*0.05 + data_set[0].max_canvas_radius;

    const y = height*0.25 + data_set[0].max_canvas_radius + d*(data_set[0].max_canvas_radius*2);

    let r = 0;
    if (data_set[d].solution===-1) r = data_set[0].min_canvas_radius;
    else
    if (data_set[d].solution=== 1) r = data_set[0].max_canvas_radius;

    strokeWeight (1);
    stroke (color_level_3);
    noFill();
    ellipse (x, y, r-2);


    for (let i=0; i<document.getElementsByName("data_element_radio_" + d).length; i++) 
    {
      if (document.getElementsByName("data_element_radio_" + d)[i].checked) 
      {
        data_set[d].calRandomRadius( int(document.getElementsByName("data_element_radio_" + d)[i].value) );
      }
    }

    document.getElementById("data_element_switch_" + d).style.height = (data_set[0].max_canvas_radius*2) + "px";
    document.getElementById("data_element_switch_" + d).style.right  = (width*0.5 - 15 + width*0.05 + data_set[0].min_canvas_radius*2) + "px";
    document.getElementById("data_element_switch_" + d).style.top    = (y - data_set[0].max_canvas_radius) + "px";
  }
  
}