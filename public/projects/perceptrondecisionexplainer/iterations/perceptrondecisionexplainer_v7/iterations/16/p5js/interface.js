

let world, collision, gravityForce, floorForce;
let ptron_sum_weighted_inputs_constraint;
let ptron_activation_constraint;



function addPhysicsWorld() 
{
  world = new c2.World (new c2.Rect (0,          0 - data_set[0].max_canvas_radius, 
                                     width, height + data_set[0].max_canvas_radius*3) );
  
  if (collision != undefined) 
  world.removeInteractionForce (collision);
  collision  = new c2.Collision();
  world.addInteractionForce (collision);


  if (gravityForce != undefined) 
  world.removeForce (gravityForce);
  gravityForce = new c2.ConstForce (0, 1.2);
  world.addForce (gravityForce);


  if (floorForce != undefined) 
  world.removeForce (floorForce);
  floorForce = new c2.LineField (new c2.Line 
  (
    0, 
    height + data_set[0].max_canvas_radius*2, 
    width, 
    height + data_set[0].max_canvas_radius*2
  ), 1);

  world.addForce (floorForce);
}



function drawPtronSumWeightedInputsAnalogyWith (i, is_training) 
{
  const x = width*0.5;
  const y = height*0.35;
  
  const ptron_sum_weighted_inputs_w = width*0.25;

  // ptron_sum_weighted_inputs_openness
  //const ptron_sum_weighted_inputs_o = map (mouseX, 0, width, ptron_sum_weighted_inputs_w, 0);
  //const ptron_sum_weighted_inputs_o = map (getPtronIterationAt(i).ptron_sum_weighted_inputs, -3, 0, ptron_sum_weighted_inputs_w, 0);// + data_set[0].ele_canvas_radius*2;
  const ptron_sum_weighted_inputs_o = map(data_set[0].threshold, 0, 1, 0, width/13)*2 - getPtronIterationAt(i).ptron_sum_weighted_inputs*2 - (data_set[0].ele_canvas_radius*3)*getPtronIterationAt(i).ptron_activation_result;

  //console.log ( ptron_sum_weighted_inputs_o + " | " + (data_set[0].min_canvas_radius*2) + " | " + (data_set[0].max_canvas_radius*2) );
  
  push();
    translate (x, y);
  
    angleMode (DEGREES);
    rotate (45);
    
    const ptron_sum_weighted_inputs_left_margin = screenPosition (-ptron_sum_weighted_inputs_w, 0);
    const ptron_sum_weighted_inputs_left_center = screenPosition (-ptron_sum_weighted_inputs_o, 0);
  pop();

  if (ptron_sum_weighted_inputs_constraint != undefined) 
    world.removeConstraint (ptron_sum_weighted_inputs_constraint);
  
  const ptron_sum_weighted_inputs_polygon = new c2.Polygon 
  ([
    new c2.Point (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y), 
    new c2.Point (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y)
  ]);

  ptron_sum_weighted_inputs_constraint  = new c2.PolygonConstraint (ptron_sum_weighted_inputs_polygon);
  world.addConstraint (ptron_sum_weighted_inputs_constraint);

  strokeWeight (1);
  stroke (color_level_2);

  if (world.particles.length===1 && world.particles[0].position.y>height*0.4) 
  {
    if (getPtronIterationAt(i).ptron_training_error===0) stroke (color_empha_p);
    else stroke (color_empha_n);
  }

  if (is_training === true) 
  {
    strokeWeight(4);
    stroke(color_empha_t);
  }

  noFill();
  beginShape();
    vertex (ptron_sum_weighted_inputs_left_margin.x, ptron_sum_weighted_inputs_left_margin.y);
    vertex (ptron_sum_weighted_inputs_left_center.x, ptron_sum_weighted_inputs_left_center.y);
  endShape();
}



function drawPtronActivationAnalogy (y) 
{
  const x = width*0.5;
  const h = (data_set[0].max_canvas_radius*2)*3;

  const upper_point = {x:x, y:y};
  const lower_point = {x:x, y:y+h};
  
  if (ptron_activation_constraint != undefined) 
    world.removeConstraint (ptron_activation_constraint);

  const ptron_act_polygon = new c2.Polygon 
  ([
    new c2.Point (upper_point.x, upper_point.y), 
    new c2.Point (lower_point.x, lower_point.y)
  ]);

  ptron_activation_constraint = new c2.PolygonConstraint (ptron_act_polygon);
  world.addConstraint (ptron_activation_constraint);

  stroke (color_level_2);
  strokeWeight (1);
  noFill();

  beginShape();
    vertex (upper_point.x, upper_point.y);
    vertex (lower_point.x, lower_point.y);
  endShape();
}



function drawDataElementAnalogy (i) 
{
  if (world.particles.length < 1) 
  {
    addDataElementAnalogyWith (i);
  }

  world.update();
  

  strokeWeight (1);
  stroke (color_level_3);

  if (world.particles.length===1 && world.particles[0].position.y>height*0.4) 
  {
    if (getPtronIterationAt(i).ptron_training_error===0) stroke (color_empha_p);
    else stroke (color_empha_n);
  }

  if 
  (
    getPtronIterationAt(i).ptron_training_error!=0 && 
    world.particles.length===1 && 
    world.particles[0].position.y>height*0.7 && 
    frameCount%5===0
  ) 
  {
    let temp_i = 0;

    if (i < ptron_iterations_log.getRowCount()-1) 
    {
      temp_i = i+1;
    }
    else 
    {
      temp_i = i;
    }

    drawPtronSumWeightedInputsAnalogyWith (temp_i, true);

    strokeWeight(3);
    stroke(color_empha_t);
  }

  noFill();
  ellipse 
  (
    world.particles[0].position.x, 
    world.particles[0].position.y, 
    world.particles[0].radius-2
  );

  noStroke();
  fill (color_level_4);
  text (getPtronIterationAt(i).data_id, world.particles[0].position.x, world.particles[0].position.y);


  if (world.particles[0].position.y > height) 
  {
    removeDataElementAnalogy();
  }
}



function addDataElementAnalogyWith (i) 
{
  const data_element_analogy  = new c2.Particle 
  (
    width*0.5 - data_set[0].max_canvas_radius, 
            0 - data_set[0].max_canvas_radius
  );

  data_element_analogy.radius = data_set[getPtronIterationAt(i).data_id].ele_canvas_radius;

  world.addParticle (data_element_analogy);
}



function removeDataElementAnalogy() 
{
  world.removeParticle (world.particles[0]);
}



function drawLabel() 
{
  strokeWeight (1);
  stroke (color_level_2);
  noFill();

  const gap_margins = 100; 

  ellipse (gap_margins + data_set[0].min_canvas_radius*3, height/2, data_set[0].min_canvas_radius);
  ellipse (width-15-gap_margins - data_set[0].max_canvas_radius, height/2, data_set[0].max_canvas_radius);
}