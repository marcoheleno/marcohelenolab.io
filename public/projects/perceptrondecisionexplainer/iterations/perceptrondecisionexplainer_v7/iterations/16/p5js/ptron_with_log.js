

let ptron_iterations_log;


function createPtronTableLog() 
{
  ptron_iterations_log = 0;
  ptron_iterations_log = new p5.Table([0]);

  // data log
  ptron_iterations_log.addColumn ("data_id");
  ptron_iterations_log.addColumn ("data_input_radius");
  ptron_iterations_log.addColumn ("data_input_bias");
  ptron_iterations_log.addColumn ("data_solution");

  // ptron log
  ptron_iterations_log.addColumn ("ptron_iteration");
  ptron_iterations_log.addColumn ("ptron_learning_rate");
  ptron_iterations_log.addColumn ("ptron_weight_radius");
  ptron_iterations_log.addColumn ("ptron_weight_bias");
  ptron_iterations_log.addColumn ("ptron_sum_weighted_inputs");
  ptron_iterations_log.addColumn ("ptron_activation_result");
  ptron_iterations_log.addColumn ("ptron_training_error");

  //console.log (ptron_iterations_log.columns);
  /*
  0: "data_id"
  1: "data_input_radius"
  2: "data_input_bias"
  3: "data_solution"
  4: "ptron_iteration"
  5: "ptron_learning_rate"
  6: "ptron_weight_radius"
  7: "ptron_weight_bias"
  8: "ptron_sum_weighted_inputs"
  9: "ptron_activation_result"
 10: "ptron_training_error"
  */
}


function runPtronWithLog() 
{
  let ptron_trained = false;

  while (!ptron_trained) 
  {

    for (let d=0; d<data_set.length; d++)
    {
      let new_log_row = ptron_iterations_log.addRow();

      // data log
      new_log_row.setNum ("data_id", d);
      new_log_row.setNum ("data_input_radius", data_set[d].inputs[0]);
      new_log_row.setNum ("data_input_bias", data_set[d].inputs[1]);
      new_log_row.setNum ("data_solution", data_set[d].solution);

      // ptron log
      new_log_row.setNum ("ptron_iteration", ptron_iterations_log.getRowCount()-1);
      new_log_row.setNum ("ptron_learning_rate", ptron.learning_rate);
      new_log_row.setNum ("ptron_weight_radius", ptron.weights[0]);
      new_log_row.setNum ("ptron_weight_bias", ptron.weights[1]);

      ptron.calSumWeightedInputsWith (data_set[d].inputs);
      new_log_row.setNum ("ptron_sum_weighted_inputs", ptron.sum_weighted_inputs_result);
      
      ptron.calActivationFunction();
      new_log_row.setNum ("ptron_activation_result", ptron.activation_result);

      ptron.calTrainingErrorWith (data_set[d].solution);
      new_log_row.setNum ("ptron_training_error", ptron.training_error);
      
      ptron.endInTrainingWith (data_set[d].inputs);
    }

    if 
    (
      ptron_iterations_log.getRow(0).getNum("ptron_training_error") === 0 && 
      ptron_iterations_log.getRow(1).getNum("ptron_training_error") === 0 && 
      ptron_iterations_log.getRow(2).getNum("ptron_training_error") === 0
    )
    {
      re_StartPtron();
    }

    if 
    (
      ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-3).getNum("ptron_training_error") === 0 && 
      ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-2).getNum("ptron_training_error") === 0 && 
      ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-1).getNum("ptron_training_error") === 0
    )
    {
      ptron_trained = true;
      
      //printPtronIterationsLog();
    }

    /*
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-3).getNum("data_id"));
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-2).getNum("data_id"));
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-1).getNum("data_id"));

    console.log("---");

    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-3).getNum("ptron_iteration"));
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-2).getNum("ptron_iteration"));
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-1).getNum("ptron_iteration"));

    console.log("---");

    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-3).getNum("ptron_training_error"));
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-2).getNum("ptron_training_error"));
    console.log(ptron_iterations_log.getRow(ptron_iterations_log.getRowCount()-1).getNum("ptron_training_error"));

    console.log("------------");
    */
  }
  
  if (ptron_iterations_log.getRowCount() < 9) 
  {
    re_StartPtron();
  }
}


function printPtronIterationsLog() 
{
  saveTable (ptron_iterations_log, "ptron_iterations_log", "csv");
}


function getPtronIterationAt (i) 
{
  const ptron_iterations_log_row = 
  {
    data_id:                    ptron_iterations_log.getNum (i, "data_id"), 
    data_input_radius:          ptron_iterations_log.getNum (i, "data_input_radius"), 
    data_input_bias:            ptron_iterations_log.getNum (i, "data_input_bias"), 
    data_solution:              ptron_iterations_log.getNum (i, "data_solution"), 
    ptron_iteration:            ptron_iterations_log.getNum (i, "ptron_iteration"), 
    ptron_learning_rate:        ptron_iterations_log.getNum (i, "ptron_learning_rate"), 
    ptron_weight_radius:        ptron_iterations_log.getNum (i, "ptron_weight_radius"), 
    ptron_weight_bias:          ptron_iterations_log.getNum (i, "ptron_weight_bias"), 
    ptron_sum_weighted_inputs:  ptron_iterations_log.getNum (i, "ptron_sum_weighted_inputs"), 
    ptron_activation_result:    ptron_iterations_log.getNum (i, "ptron_activation_result"), 
    ptron_training_error:       ptron_iterations_log.getNum (i, "ptron_training_error")
  }
  
  return ptron_iterations_log_row;
}