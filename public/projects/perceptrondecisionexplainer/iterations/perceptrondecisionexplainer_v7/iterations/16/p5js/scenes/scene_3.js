


function drawScene3() 
{
  drawLabel();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false);
  drawPtronActivationAnalogy (height*0.4);

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i);
  }

  if (world.particles.length === 0) 
  {
    if (log_i < ptron_iterations_log.getRowCount()-1) 
    {
      log_i++;
    }

    else 
    {
      console.log ("terminou");
      //log_i-=2; //Se quiseres que ele fique em loop na ultima lap

      scene_paused = true;

      document.getElementById("scroll_btn_container_in_scene_3").style.visibility = "visible";
    }
  }
}