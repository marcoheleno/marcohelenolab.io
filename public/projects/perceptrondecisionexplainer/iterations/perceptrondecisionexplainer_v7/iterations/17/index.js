

// IntersectionObserver

const options = 
{
  root: document.getElementsByTagName("article")[0], 
  rootMargin: "0px", 
  threshold: 0.1
}

const callback = (entries, observer) => 
{
  entries.forEach((entry) => 
  {
    // Each entry describes an intersection change for one observed target element:
    //   entry.boundingClientRect
    //   entry.intersectionRatio
    //   entry.intersectionRect
    //   entry.isIntersecting
    //   entry.rootBounds
    //	 entry.target
    //   entry.time
	
	  if (entry.isIntersecting) 
    {
      sceneManager (entry.target.getAttribute('id'));
    }

  });
};

const observer = new IntersectionObserver (callback, options);

observer.observe (document.getElementById("scene_1"));
observer.observe (document.getElementById("scene_2"));
observer.observe (document.getElementById("scene_3"));
observer.observe (document.getElementById("scene_4"));
observer.observe (document.getElementById("scene_5"));

function scrollToScene (scene_number) 
{
  document.getElementById ("scene_" + scene_number).scrollIntoView ({ behavior: "smooth", block: "start"});
}