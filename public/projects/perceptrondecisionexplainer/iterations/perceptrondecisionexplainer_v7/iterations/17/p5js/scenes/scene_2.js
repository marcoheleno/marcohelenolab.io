


function drawScene2() 
{
  drawDataLabel();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false, false);
  drawPtronActivationAnalogy (height*0.4);

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i, false);
  }

  if (world.particles.length === 0) 
  {
    const stop_at_lap_1 = data_set.length-1;

    if (log_i<ptron_iterations_log.getRowCount()-1 && log_i<stop_at_lap_1) 
    {
      log_i++;
    }

    else 
    {
      //console.log("next automático");
      //log_i-=2; //Se quiseres que ele fique em loop na ultima lap

      scene_paused = true;

      document.getElementById("scroll_btn_container_in_scene_2").style.visibility = "visible";
    }
  }
}