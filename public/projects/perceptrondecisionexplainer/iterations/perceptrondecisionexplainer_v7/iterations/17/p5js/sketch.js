


let color_level_1, color_level_2, color_level_3, color_level_4;
let color_empha_n, color_empha_p;
let current_scene, scene_paused;
let log_i;



function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  canvas.parent ("decision_vis_container");
  ellipseMode (RADIUS);
  convertCSScolorsTop5js();
  addScreenPositionFunction();
  createDataSet();
}



function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById ("decision_vis_container").offsetWidth,
    h: document.getElementById ("decision_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}



function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
}



function convertCSScolorsTop5js()
{
    // Colors defined in index.css
    color_level_1 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_1") + ")";
    color_level_2 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_2") + ")";
    color_level_3 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_3") + ")";
    color_level_4 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_4") + ")";

    color_empha_n = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_n") + ")";
    color_empha_p = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_p") + ")";
    color_empha_t = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_t") + ")";


    color_level_1 = color (color_level_1);
    color_level_2 = color (color_level_2);
    color_level_3 = color (color_level_3);
    color_level_4 = color (color_level_4);

    color_empha_n = color (color_empha_n);
    color_empha_p = color (color_empha_p);
    color_empha_t = color (color_empha_t);
}



function sceneManager (scene_input) 
{
  current_scene = scene_input;
  

  if (current_scene==="scene_1") 
  {
    addPhysicsWorld();
    document.getElementById("vis_label_colors").style.visibility = "hidden";
  }


  else


  if (current_scene==="scene_2") 
  {
    re_StartPtron();
    scene_paused = false;
    document.getElementById("scroll_btn_container_in_scene_2").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "visible";
  }


  else


  if (current_scene==="scene_3") 
  {
    const start_after_lap_1 = data_set.length;
    log_i = start_after_lap_1;
    scene_paused = false;
    document.getElementById("scroll_btn_container_in_scene_3").style.visibility = "hidden";
  }


  else


  if (current_scene==="scene_4") 
  {
    const history_vis_container = document.getElementById("history_vis_container");

    for (let i=0; i<ptron_iterations_log.getRowCount(); i++) 
    {
        const div = document.createElement("div");

        div.setAttribute("id", "log_i_" + i);
        div.setAttribute("onmouseenter", "log_i=" + i);
        div.setAttribute("onmouseout", "removeDataElementAnalogy()");
        div.setAttribute("title", "Perceptron's decision-making iteration " + i);
        div.innerHTML = "Iteration " + (i+1).toString().padStart(2, "0");

        history_vis_container.append(div);
    }

    removeDataElementAnalogy();
  }


  else 


  if (current_scene==="scene_5") 
  {
    document.getElementById("vis_label_colors").style.visibility = "hidden";
  }




  
  if (current_scene!="scene_1" || current_scene!="scene_2") 
  {
    if (ptron_iterations_log === undefined)
    {

      re_StartPtron();

    }
  }
  
  
  if (current_scene!="scene_4") 
  {
    if (ptron_iterations_log != undefined)
    {

      for (let i=0; i<ptron_iterations_log.getRowCount(); i++) 
      {
        if (document.getElementById("log_i_" + i) != null) 
        {
          document.getElementById("log_i_" + i).remove();
        }
      }

    }
  }



}



function re_StartPtron() 
{
  startPtron();
  createPtronTableLog();
  runPtronWithLog();
  log_i = 0;
}



function draw() 
{
  clear();
  updateDataSet();
  //console.log(log_i);
  
  if (current_scene==="scene_1") drawScene1();
  else 
  if (current_scene==="scene_2") drawScene2();
  else 
  if (current_scene==="scene_3") drawScene3();
  else 
  if (current_scene==="scene_4") drawScene4();
}