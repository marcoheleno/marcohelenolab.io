


function drawScene4() 
{
  drawDataLabel();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false, false);
  drawPtronActivationAnalogy (height*0.4);

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i, false);
  }

  if (world.particles.length === 0) 
  {
    if (log_i < ptron_iterations_log.getRowCount()-1) 
    {
      log_i++;
    }

    else 
    {
      console.log ("terminou");
      //log_i-=2; //Se quiseres que ele fique em loop na ultima lap

      scene_paused = true;

      document.getElementById("call_for_action_scene_4").style.visibility = "visible";
    }
  }
}