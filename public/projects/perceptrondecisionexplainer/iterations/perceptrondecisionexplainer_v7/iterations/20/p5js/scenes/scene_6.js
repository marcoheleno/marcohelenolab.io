


function drawScene6() 
{
  drawDataLabel();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false, false, false, height*0.35);
  drawPtronActivationAnalogy (height*0.4);

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i, false, false);
  }

  if (world.particles.length === 0) 
  {
    if (log_i < ptron_iterations_log.getRowCount()-1) 
    {
      log_i++;
    }

    else 
    {
      console.log ("terminou");
      //log_i-=2; //Se quiseres que ele fique em loop na ultima lap

      scene_paused = true;

      document.getElementById("call_for_action_scene_6").style.visibility = "visible";
    }
  }
}


function replayScene6() 
{
  const start_after_lap_2 = data_set.length*2;
  log_i = start_after_lap_2;
  scene_paused = false;
  document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
}