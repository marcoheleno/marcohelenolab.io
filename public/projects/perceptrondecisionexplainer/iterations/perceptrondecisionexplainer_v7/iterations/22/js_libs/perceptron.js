

let ptron;


function startPtron() 
{
  ptron = 0;
  ptron = new Perceptron (2);
}


class Perceptron 
{

  constructor (num_weights) 
  {
    this.learning_rate = 0.1;

    this.weights = new Array (num_weights); 
    this.startWithRandomWeights();
  }


  startWithRandomWeights() 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] = this.calRandomNumberBetween (-1, 1);
    }
  }

  
  calRandomNumberBetween (min, max) 
  {
    return Math.random() * (max - min) + min;
  }

  
  calSumWeightedInputsWith (inputs) 
  {
    this.sum_weighted_inputs_result = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sum_weighted_inputs_result += inputs[i] * this.weights[i];
    }
  }


  calActivationFunction() 
  {
    if (this.sum_weighted_inputs_result < 0) 
    {
      this.activation_result = -1;
    }
    else
    {
      this.activation_result = 1;
    }
  }


  calLearningErrorWith (goal) 
  {
    this.learning_error = goal - this.activation_result;
  }

  
  endInLearningWith (inputs) 
  {
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learning_rate * this.learning_error * inputs[i];
    }
  }
  
}