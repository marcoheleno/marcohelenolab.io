


function drawScene6() 
{
  drawDataLabel();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false, false, false);
  drawPtronActivationAnalogy();

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i, true, false);
  }

  if (world.particles.length === 0) 
  {


    if (log_i < ptron_iterations_log.getRowCount()-1) 
    {
      log_i++;
    }

    else 
    {
      console.log ("no more iterations");
      //log_i-=2; //If you want it to loop on the last lap
      scene_paused = true;
      document.getElementById("call_for_action_scene_6").style.visibility = "visible";
    }
  }
}


function replayScene6() 
{
  const start_after_lap_2 = data_set.length*2;
  log_i = start_after_lap_2;
  scene_paused = false;
  document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
}