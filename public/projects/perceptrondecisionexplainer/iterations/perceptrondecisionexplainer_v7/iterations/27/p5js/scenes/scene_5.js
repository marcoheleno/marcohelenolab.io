


function drawScene5() 
{
  drawDataLabel();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false, false, false);
  drawPtronActivationAnalogy();

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i, true, false);
  }

  if (world.particles.length === 0) 
  {
    const stop_at_lap_2 = data_set.length*2 - 1;

    if (log_i<ptron_iterations_log.getRowCount()-1 && log_i<stop_at_lap_2) 
    {
      log_i++;
    }

    else 
    {
      
      //log_i-=2; //If you want it to loop on the last lap
      scene_paused = true;
      document.getElementById("call_for_action_scene_5").style.visibility = "visible";
    }
  }
}


function replayScene5() 
{
  const start_after_lap_1 = data_set.length;
  log_i = start_after_lap_1;
  scene_paused = false;
  document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
}