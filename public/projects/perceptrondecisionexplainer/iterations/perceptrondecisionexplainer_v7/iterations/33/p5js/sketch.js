


let color_level_1, color_level_2, color_level_3, color_level_4;
let color_empha_n, color_empha_p;
let current_scene, scene_paused;
let log_i;
let mouse_clicked_radio_btn;



function setup() 
{
  const canvas = createCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  canvas.parent ("decision_vis_container");
  ellipseMode (RADIUS);
  convertCSScolorsTop5js();
  addScreenPositionFunction();
  createDataSet();
  addPhysicsWorld();
}



function getCanvasContainerSize() 
{
  const canvas_container_size = 
  {
    w: document.getElementById ("decision_vis_container").offsetWidth,
    h: document.getElementById ("decision_vis_container").offsetHeight
  };
  
  return canvas_container_size;
}



function windowResized() 
{
  resizeCanvas (getCanvasContainerSize().w, getCanvasContainerSize().h);
  scrollToScene (current_scene);
}



function convertCSScolorsTop5js()
{
    // colors defined in index.css
    color_level_1 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_1") + ")";
    color_level_2 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_2") + ")";
    color_level_3 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_3") + ")";
    color_level_4 = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_level_4") + ")";

    color_empha_n = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_n") + ")";
    color_empha_p = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_p") + ")";
    color_empha_l = "rgb(" + getComputedStyle(document.documentElement).getPropertyValue("--color_empha_l") + ")";


    color_level_1 = color (color_level_1);
    color_level_2 = color (color_level_2);
    color_level_3 = color (color_level_3);
    color_level_4 = color (color_level_4);

    color_empha_n = color (color_empha_n);
    color_empha_p = color (color_empha_p);
    color_empha_l = color (color_empha_l);
}



function sceneManager (scene_input) 
{
  current_scene = scene_input;
  console.log(current_scene);


  if (current_scene === "scene_1") 
  {
    document.getElementById("sticky_header").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "hidden";
  }


  else
  

  if (current_scene === "scene_2") 
  {
    document.getElementById("sticky_header").style.visibility = "visible";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "hidden";
  }


  else


  if (current_scene === "scene_3") 
  {
    document.getElementById("sticky_header").style.visibility = "visible";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "hidden";
    mouse_clicked_radio_btn = false;
  }


  else


  if (current_scene === "scene_4") 
  {
    if (world!=undefined && world.particles.length>0) 
    {
      removeDataElementAnalogy();
    }

    if (ptron_iterations_log === undefined)
    {
      re_StartPtron();
    }

    const start_at_the_beginning = 0;
    log_i = start_at_the_beginning;

    scene_paused = false;
    document.getElementById("sticky_header").style.visibility = "visible";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "hidden";
  }


  else


  if (current_scene === "scene_5") 
  {
    if (world!=undefined && world.particles.length>0) 
    {
      removeDataElementAnalogy();
    }

    if (ptron_iterations_log === undefined)
    {
      re_StartPtron();
    }
    
    const start_after_lap_1 = data_set.length;
    log_i = start_after_lap_1;

    scene_paused = false;
    document.getElementById("sticky_header").style.visibility = "visible";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "visible";
  }


  else


  if (current_scene === "scene_6") 
  {
    if (world!=undefined && world.particles.length>0) 
    {
      removeDataElementAnalogy();
    }
    
    if (ptron_iterations_log === undefined)
    {
      re_StartPtron();
    }

    const start_after_lap_2 = data_set.length*2;
    log_i = start_after_lap_2;
    
    scene_paused = false;
    document.getElementById("sticky_header").style.visibility = "visible";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "visible";
  }


  else


  if (current_scene === "scene_7") 
  {
    if (ptron_iterations_log === undefined)
    {
      re_StartPtron();
    }

    const history_vis_container = document.getElementById("history_vis_container");
    const history_vis_container_list = history_vis_container.getElementsByTagName("ol")[0];

    for (let i=0; i<ptron_iterations_log.getRowCount(); i++) 
    {
        const li = document.createElement("li");

        li.setAttribute("id", "log_i_" + i);
        li.setAttribute("onmouseenter", "log_i=" + i);
        li.setAttribute("onmouseout", "removeDataElementAnalogy()");
        li.setAttribute("title", "Perceptron's decision-making iteration " + (i+1) );
        li.innerHTML = (i+1);

        history_vis_container_list.append(li);
    }

    log_i = 0;

    if (world!=undefined && world.particles.length>0) 
    {
      removeDataElementAnalogy();
    }

    document.getElementById("sticky_header").style.visibility = "visible";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "visible";
  }


  else 


  if (current_scene === "scene_8") 
  {
    document.getElementById("sticky_header").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_3-2").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_5").style.visibility = "hidden";
    document.getElementById("call_for_action_scene_6").style.visibility = "hidden";
    document.getElementById("vis_label_colors").style.visibility = "hidden";
  }
  
  
  


  if (current_scene != "scene_7") 
  {
    if (ptron_iterations_log != undefined)
    {

      for (let i=0; i<ptron_iterations_log.getRowCount(); i++) 
      {
        if (document.getElementById("log_i_" + i) != null) 
        {
          document.getElementById("log_i_" + i).remove();
        }
      }

    }
  }

}



function re_StartPtron() 
{
  startPtron();
  createPtronTableLog();
  runPtronWithLog();
  log_i = 0;
}



function draw() 
{
  clear();
  updateDataSet();
  //console.log(log_i);
  
  if (current_scene === "scene_1") drawScene1();
  else 
  if (current_scene === "scene_2") drawScene2();
  else 
  if (current_scene === "scene_3") drawScene3();
  else 
  if (current_scene === "scene_4") drawScene4();
  else 
  if (current_scene === "scene_5") drawScene5();
  else 
  if (current_scene === "scene_6") drawScene6();
  else 
  if (current_scene === "scene_7") drawScene7();
  else 
  if (current_scene === "scene_8") drawScene8();
}