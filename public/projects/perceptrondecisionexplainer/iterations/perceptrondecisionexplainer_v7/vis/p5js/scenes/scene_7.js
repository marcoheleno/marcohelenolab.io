


function drawScene7() 
{
    drawDataLegend();
    drawPtronSumWeightedInputsAnalogyWith (log_i, false, false, false);
    drawPtronActivationAnalogy (height*0.35);
    drawDataElementAnalogy (log_i, false, true);

    if (log_i === 0)
    {
        document.getElementById("call_for_action_scene_7").style.top = ((height*0.3) + 20) + "px";
    } 
    else 
    {
        document.getElementById("call_for_action_scene_7").style.visibility = "hidden";
        document.getElementById("call_for_action_scene_7").getElementsByTagName("p")[0].style.visibility = "hidden";
    }
    
    document.getElementById("history_vis_container").style.top = height*0.35 + (data_set[0].max_canvas_radius*2)*3 + 30 + "px";

    for (let i=0; i<ptron_iterations_log.getRowCount(); i++) 
    {
        if (i === log_i) 
        {
            document.getElementById("log_i_" + log_i).style.fontWeight = "700";
            document.getElementById("log_i_" + i).style.color = "rgb(var(--color_level_4))";
        }
        else 
        {
            document.getElementById("log_i_" + i).style.fontWeight = "200";
            document.getElementById("log_i_" + i).style.color = "rgb(var(--color_level_3))";
        }
    }

    removeDataElementAnalogy();
}