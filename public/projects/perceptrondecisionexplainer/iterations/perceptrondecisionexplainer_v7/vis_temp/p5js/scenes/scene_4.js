


function drawScene4() 
{
  drawDataLegend();
  drawPtronSumWeightedInputsAnalogyWith (log_i, false, false, false);
  drawPtronActivationAnalogy (height*0.35);

  if (!scene_paused) 
  {
    drawDataElementAnalogy (log_i, false, false);
  }

  if (world.particles.length === 0) 
  {
    const stop_at_lap_1 = data_set.length-1;

    if (log_i<ptron_iterations_log.getRowCount()-1 && log_i<stop_at_lap_1) 
    {
      log_i++;
    }

    else 
    {

      //log_i-=2; //If you want it to loop on the last lap
      scene_paused = true;
      document.getElementById("call_for_action_scene_4").style.cursor = "pointer";
      document.getElementById("call_for_action_scene_4").style.visibility = "visible";
    }
  }
}


function replayScene4() 
{
  
  log_i = 0;
  scene_paused = false;
  document.getElementById("call_for_action_scene_4").style.cursor = "default";
  document.getElementById("call_for_action_scene_4").style.visibility = "hidden";
}