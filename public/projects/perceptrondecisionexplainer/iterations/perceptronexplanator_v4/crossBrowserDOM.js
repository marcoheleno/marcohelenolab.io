

//
// Cross Browser/Device
// DOM functions
//


let p5_containerSize;
let colorBackground, colorBackgroundMiddle, colorMiddle, colorForegroundMiddle, colorForeground;
let normalBrowserZoomLevel;


window.onload = function() 
{
    normalBrowserZoomLevel = (window.outerWidth - 8) / window.innerWidth;

    setBodyDOMsize2innerBrowserWindowSize();
    get_p5_containerSize();
    getColorsFromCSSFile();
    setIntroTextSizeRelativeToParentDiv ();
    
    /*
    const dataset = getRandomData (1, -1, -p5_containerSize.w/2, p5_containerSize.w/2);
    runPtronGetIterations (dataset); // See data.js
    
    drawUserIteration (dataset);
    let DOMinputRangeValue = document.getElementsByTagName("input")[0].value;
    drawAllPtronIterations (dataset, DOMinputRangeValue);
    drawRouteFromSelected2TrainedIteration (dataset, DOMinputRangeValue);

    drawRouteFromUser2PtronIteration (dataset);
    */
}


//
// Set Body Width & Height
// to Inner window Width & Height
//
function setBodyDOMsize2innerBrowserWindowSize() 
{
    document.body.style.width  = window.innerWidth +"px";
    document.body.style.height = window.innerHeight+"px";
}


//
// Get a define DOM container Width & Height
//
function get_p5_containerSize() 
{
    p5_containerSize = 
    {
        w: document.getElementById("content_container").offsetWidth,
        h: document.getElementById("content_container").offsetHeight
    };
}


//
// If the browser is Resized 
// or Orientation is changed
// Reload DOM
//
window.addEventListener ("resize", function reloadDOM() 
{
    //let browserZoomLevel = Math.round(window.devicePixelRatio * 100);
    let browserZoomLevel = (window.outerWidth - 8) / window.innerWidth;

    if (browserZoomLevel > normalBrowserZoomLevel) 
    {
        document.body.style.zoom = 1.0;

        let viewport = document.querySelector('meta[name="viewport"]');
        viewport.content = "initial-scale=1.0";
        
        const scale = 'scale(1)';
        document.body.style.msTransform = scale;     // IE 9
        document.body.style.transform = scale;       // General
        document.body.style.webkitTransform = scale; // Chrome, Opera, Safari
    }
    
    location.reload();
});


//
// Get colors from style.css
//
function getColorsFromCSSFile()
{
    // See style.css
    colorBackground       = getComputedStyle(document.documentElement).getPropertyValue("--colorBackground");
    colorBackgroundMiddle = getComputedStyle(document.documentElement).getPropertyValue("--colorBackgroundMiddle");
    colorMiddle           = getComputedStyle(document.documentElement).getPropertyValue("--colorMiddle");
    colorForegroundMiddle = getComputedStyle(document.documentElement).getPropertyValue("--colorForegroundMiddle");
    colorForeground       = getComputedStyle(document.documentElement).getPropertyValue("--colorForeground");
}


//
// Update DOM Input Range Slider
// with with 
//
function updateDOMinputRange (numIterations, goal)
{
    document.getElementsByTagName("input")[0].value = 0;

    if (numIterations > 1) 
    {
        //console.log(numIterations);

        document.getElementsByTagName("input")[0].setAttribute ("max", numIterations-1);
        document.getElementsByTagName("input")[0].style.visibility = "visible";

        if (goal === -1) 
        document.getElementsByTagName("input")[0].style.transform = "rotate(180deg)";
        else if (goal === 1) 
        document.getElementsByTagName("input")[0].style.transform = "rotate(0deg)";
    }

    else 
    {
        document.getElementsByTagName("input")[0].style.visibility = "hidden";
        document.getElementsByTagName("input")[0].setAttribute ("max",   0);
    }
}


function addDOMTooltip()
{
    document.getElementById("p5_interchangeableTooltip2ButtonContainer").appendChild (document.createElement("p"));
    document.getElementsByTagName("p")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
}


function updateDOMTooltipWith (message)
{
    document.getElementsByTagName("p")[0].innerHTML = message;
}


function addDOMButton()
{
    document.getElementById("p5_interchangeableTooltip2ButtonContainer").appendChild (document.createElement("button"));
    document.getElementsByTagName("button")[0].setAttribute ("id", "p5_interchangeableTooltip2Button");
    document.getElementsByTagName("button")[0].setAttribute ("type", "button");
}


function updateDOMButtonWith (action, label)
{
    document.getElementsByTagName("button")[0].setAttribute ("onclick", action);
    document.getElementsByTagName("button")[0].innerHTML = label;
}


function setIntroTextSizeRelativeToParentDiv ()
{
    let parentElement = document.getElementById("content_container");

    if (parentElement.firstChild) 
    {
        parentElement.getElementsByTagName("p")[0].style.fontSize = p5_containerSize.w/22+"px";
        parentElement.getElementsByTagName("p")[1].style.fontSize = p5_containerSize.w/22+"px";
        parentElement.getElementsByTagName("p")[2].style.fontSize = p5_containerSize.w/22+"px";
    }
}