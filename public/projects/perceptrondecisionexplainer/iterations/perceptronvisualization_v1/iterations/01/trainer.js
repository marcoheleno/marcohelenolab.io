class Trainer
{
  // A "Trainer" object stores the inputs and the correct answer.
  constructor (x, y, a) 
  {
    this.inputs = new Array (3);
    this.inputs[0] = x;
    this.inputs[1] = y;
    this.inputs[2] = 1; // Note that the Trainer has the bias input built into its array.

    this.answer = a;
  }
}

