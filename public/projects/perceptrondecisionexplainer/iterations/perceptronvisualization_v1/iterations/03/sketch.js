

let ptron;
let points = new Array(20);
let count = 0;


function setup() 
{
  createCanvas (windowWidth, windowHeight);

  ptron = new Perceptron(2, 0.1); //3 inputs for x, y, and bias //Learning Rate lower==slower

  
  for (let i=0; i<points.length; i++) 
  {
    // Generate random points.
    let randomX = random (width);
    let randomY = random (height);

    // Add Bias
    let givenBias = 1;

    // Add answer for Supervised Learning
    let desiredOutput = 1;
    if (randomX < width/2) desiredOutput = -1;

    points[i] = {
      x: randomX,
      y: randomY,
      b: givenBias,
      inputs: [randomX, givenBias],
      output: desiredOutput
    };
  }
}


function draw() 
{
  background(255);

  line (width/2, 0, width/2, height);

  // For visualization, we are training one point at each frame.
  ptron.train (points[count].inputs, points[count].output);
  count = (count + 1) % points.length;
  

  for (let i=0; i<points.length; i++) 
  {
    stroke(0);
    let guess = ptron.feedforward (points[i].inputs);

    //Show the classification—no fill for -1, black for +1.
    if (guess > 0) noFill();
    else           fill(0);

    ellipse (points[i].x, points[i].y, 8, 8);
  }
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}

