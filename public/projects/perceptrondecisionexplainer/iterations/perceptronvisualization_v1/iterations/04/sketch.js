

let artificialNeuron;
let points, count;


function setup() 
{
  createCanvas (windowWidth, windowHeight);

  // 3 inputs for x, y and bias
  // Learning Rate lower == slower
  artificialNeuron = new Perceptron(3, 0.001);
  

  points = new Array(2000);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random points
    let x = random (-width/2 , width/2);
    let y = random (-height/2, height/2);

    // Add bias
    let bias = 1;

    // Add desired output for Supervised Learning
    let desiredOutput = 1;
    if (y < f(x)) desiredOutput = -1;

    // Point
    points[i] = {
      inputs: [x, y, bias],
      output: desiredOutput
    };
  }

  count = 0;
}


function draw() 
{
  background (255);

  push();
    // (0, 0) at the center of the canvas
    translate (width/2, height/2);
    
    // Red line divider
    stroke (255, 0, 0);
    strokeWeight (3);
    let x1 = -width/2;
    let y1 = f(x1);
    let x2 = width/2;
    let y2 = f(x2);
    line (x1, y1, x2, y2);

    // For visualization, we are training a Point at each frame
    artificialNeuron.train (points[count].inputs, points[count].output);

    // Iterate count to train next point
    count = (count + 1) % points.length;


    for (let i=0; i<points.length; i++) 
    {
      stroke (0);
      strokeWeight (2);

      // "guess" can be from -1 to +1
      let guess = artificialNeuron.feedforward (points[i].inputs);
      if (guess > 0) noFill();
      else fill(0);

      ellipse (points[i].inputs[0], points[i].inputs[1], 10, 10);
    }
    pop();
}


// Function to describe the line
function f(x) 
{
  let y = 2 * x + 1; // y = mx + b
  return y;
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}

