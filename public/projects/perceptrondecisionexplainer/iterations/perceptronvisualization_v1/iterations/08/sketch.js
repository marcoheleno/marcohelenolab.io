

let neuron;
let points, yArray, count;


function setup() 
{
  createCanvas (200, 200);

  // 2 inputs — x and bias.
  // Learning Rate — lower == slower.
  neuron = new Perceptron(2, 0.01);
  

  points = new Array(2);
  yArray = new Array(2);

  for (let i=0; i<points.length; i++) 
  {
    // Generates random x, y coordinates.
    let x = random (-width/2 , width/2);
    yArray [i] = random (-height/2, height/2);

    // Add "bias".
    let bias = 1;

    // Add "desiredOutput" for Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;

    // Point, only x is processed through the Perceptron
    points[i] = {
      inputs: [x, bias],
      output: desiredOutput
    };
  }

  count = 0;
  noLoop(); //4 Telephone Game
}


function draw() 
{
  // For visualization, we are training a Point at each frame.
  neuron.train (points[count].inputs, points[count].output);

  // If difference between desired output and guessed output = 0
  if (neuron.trained()) background (0, 255, 0);
  else background (255, 0, 0);

  //print (neuron.showWeightsStatus());
  print (neuron.showGuessingError());

  
  push();
    translate (width/2, height/2);
    
    stroke (0);
    let x1 = 0;
    let y1 = -height/2;
    let x2 = 0;
    let y2 = height/2;
    line (x1, y1, x2, y2);


    // Draw each ellipse with or without fill, 
    // depending on which side is drawn.
    for (let i=0; i<points.length; i++) 
    {
      stroke (0);
      strokeWeight (2);

      let guess = neuron.feedForward (points[i].inputs);
      if (guess > 0) noFill(); // "guess" can be from -1 to +1
      else fill(0);

      ellipse (points[i].inputs[0], yArray[i], 10, 10);
    }
  pop();


  // Iterate count to train next point.
  count = (count + 1) % points.length;
  //print(count);
}


function keyPressed() 
{
  redraw();
  print("redraw");
}

