/*
a step-by-step visualization is triggered. 
Each step visualization reveals the transformations 
that are occurring in the introduced information 
at each iteration. This allows for “passive observation” 
and interpretation at each iteration. As the entire 
process reaches the end — the output, the system 
reveals to the observer a visual history of the 
accumulated transformations that the information 
endured at each iteration.

Weights Viz = accumulated transformations 
that the information 
endured at each iteration
= reasoning
= information mutation
= decision-making
= at each iteration
*/

let neuron;
let points, yArray;//, count;


function setup() 
{
  createCanvas (400, 400);

  // 2 inputs — x and bias.
  // Learning Rate — lower == slower.
  neuron = new Perceptron(2, 0.0001);
  

  points = new Array(1);
  yArray = new Array(points.length);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random points.
    let x = random (-width/2 , width/2);

    // Add "bias".
    let bias = 1;

    // Add "desiredOutput" for Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;
    //if (y < f(x)) desiredOutput = -1;

    // Point
    points[i] = {
      inputs: [x, bias],
      output: desiredOutput
    };

    yArray [i] = random (-height/2, height/2);
  }

  //count = 0;
  noLoop();
}


function draw() 
{
  background(255);


  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, height/2);
    
    // Draw a line divider
    stroke (0);
    let x1 = 0;
    let y1 = -height/2;
    let x2 = 0;
    let y2 = height/2;
    line (x1, y1, x2, y2);


    // Draw each ellipse with or without fill, 
    // depending on which side is drawn.
    for (let i=0; i<points.length; i++) 
    {
      // For visualization, we are training a Point at each frame.
      neuron.train (points[i].inputs, points[i].output);
      
      // If difference between desired output and guessed output = 0
      if (neuron.trained()) stroke (0, 255, 0);
      else stroke (255, 0, 0);
      strokeWeight (2);
      
      //print (neuron.showWeightsStatus()); //Reasoning
      //print (neuron.showGuessingError());
      print(points[i].output+" | "+neuron.showGuessedOutput());


      let guess = neuron.feedForward (points[i].inputs);
      if (guess > 0) noFill(); // "guess" can be from -1 to +1
      else fill(0);

      ellipse (points[i].inputs[0], yArray[i], 10, 10);
      ellipse (points[i].output*neuron.showGuessedOutput()*points[i].inputs[0], yArray[i], 20, 20);
    }
  pop();
}


function keyPressed() 
{
  redraw();
  print("train");
}