/*
a step-by-step visualization is triggered. 
Each step visualization reveals the transformations 
that are occurring in the introduced information 
at each iteration. This allows for “passive observation” 
and interpretation at each iteration. As the entire 
process reaches the end — the output, the system 
reveals to the observer a visual history of the 
accumulated transformations that the information 
endured at each iteration.

Weights Viz = accumulated transformations 
that the information 
endured at each iteration
= reasoning
= information mutation
= decision-making
= at each iteration
*/

let neuron;
let points, yArray;//, count;
let pointSize = 20;


function setup() 
{
  createCanvas (400, 400);

  // 2 inputs — x and bias.
  // Learning Rate — lower == slower.
  neuron = new Perceptron(2, 0.0001);
  

  points = new Array(1);
  yArray = new Array(points.length);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random points.
    let x = random (-width/2 , width/2);

    // Add "bias".
    let bias = 1;

    // Add "desiredOutput" for Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;
    //if (y < f(x)) desiredOutput = -1;

    // Point
    points[i] = {
      inputs: [x, bias],
      output: desiredOutput
    };
    
    yArray [i] = (-height/2) + i + pointSize/2;
  }

  //count = 0;
  noLoop();
}


function draw() 
{
  background(255);

  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, height/2);

    for (let i=0; i<points.length; i++) 
    {
      // Coloring the graph scetion background based on the point's "desiredOutput"
      let backgroundFirstQuad =  color (0);
      let backgroundSecondQuad = color (0);

      if (points[i].output < 0) 
      {
        backgroundFirstQuad =  color (120, 220, 120);
        backgroundSecondQuad = color (220, 130, 130);
      }
      else 
      {
        backgroundFirstQuad =  color (220, 130, 130);
        backgroundSecondQuad = color (120, 220, 120);
      }
      
      noStroke();      
      fill (backgroundFirstQuad);
      rect(-width/2, -height/2, width/2, pointSize);
      fill (backgroundSecondQuad);
      rect(0, -height/2, width/2, pointSize);



      // "feedForward()" each point's input to the neuron to guess or predict 
      // on the "desiredOutput",
      // left or Right Quad, 
      // -1 or +1
      neuron.feedForward (points[i].inputs);
      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = neuron.getActivationResult();



      // "train()" the neuron on each point's input and output.
      // the neuron will calculate the error,
      // the difference between the 
      // "desiredOutput" and "neuronGuessing".
      neuron.train (points[i].inputs, points[i].output);

      
  
      
      
      //print (neuron.showWeightsStatus()); //Reasoning
      //print (neuron.showGuessingError());
      print(points[i].output+" | "+neuronGuessing);//neuron.showGuessedOutput());


      noStroke();
      fill(0);
      ellipse (points[i].inputs[0], yArray[i], pointSize/2, pointSize/2);

      strokeWeight(1);
      stroke(0);
      noFill();
      ellipse (points[i].output*neuronGuessing*points[i].inputs[0], yArray[i], pointSize/2, pointSize/2);
    }
  pop();
}


function keyPressed() 
{
  redraw();
  print("train");
}