
class Perceptron 
{
  constructor (numWeights, learningRate) 
  {
    this.weights = new Array (numWeights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // Start with random weights.
      this.weights[i] = random (-1, 1);
    }
    this.learningRate = learningRate;
    this.activationResult = 0;
  }


  // Function to train the Perceptron against known data.
  // Weights are adjusted based on "desiredOutput"
  train (desiredOutput, guessedOutput) 
  {
    // Guess according to the received inputs.
    //this.feedForward (inputs);
    this.guessedOutput = guessedOutput;

    // Calculate the error 
    // difference between 
    // "desiredOutput" and "guessedOutput".
    // Note this can only be -2, 0, or 2
    this.error = desiredOutput - this.guessedOutput;

    // Adjust all the "weights" according
    // to the "error" and "learningRate".
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * this.error * this.inputs[i];
    }
  }


  getWeights()
  {
    return this.weights[0];
  }

  getBias()
  {
    return this.weights[1];
  }


  trained()
  {
    this.trainedStatus = false;

    // If difference between desired output and guessed output = 0
    if (this.error == 0) this.trainedStatus = true;
    
    return (this.trainedStatus);
  }




  //Guessing, Prediction Making
  feedForward (inputs) 
  {
    this.inputs = inputs;

    this.sum = 0;
    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs
      this.sum += inputs[i] * this.weights[i];
    }

    // Result is sign of the sum, -1 or 1
    this.activationResult = this.activationFunction (this.sum);
  }

  activationFunction (sum) 
  {
    if (sum > 0) return 1;
    else return -1;
  }

  getActivationResult() 
  {
    return this.activationResult;
  }
}

