

let neuron;
let points, pointSize, prevIterationCoordinate;
let interationNum, interationQuadWidth, interationQuadHeight;
let font;


function preload() 
{
  font = loadFont ("IBM_Plex_Mono/IBMPlexMono-Medium.ttf");
}


function setup() 
{
  createCanvas (windowWidth, windowHeight);
  startSystem();
}


function startSystem() 
{
  loop();
  background (30);


  // To keep track of the number of interations.
  interationNum = 0;
  // Controlling the "frameRate()" for "interationNum" visualization.
  frameRate (1);

  // Defining "interationQuad" Width and Height
  interationQuadWidth  = width;
  interationQuadHeight = 30;
  

  // Defining each point's size
  pointSize = interationQuadHeight/2;
  // Working with 1 point only
  points = new Array (1);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random "x" coordinate for each point.
    let x = random (-width/2, width/2);


    // Defining "bias" for each point.
    let bias = 1;


    // Defining "desiredOutput" for each point
    // This means that this Neuron is going to be 
    // trained with Supervised Learning.
    let desiredOutput = 1;
    if (x < 0) desiredOutput = -1;


    // Defining the point object 
    // and adding it to the points array.
    // Each point instance has:
    // inputs: [horizontal coordinate, bias]
    // output: desiredOutput for Supervised Learning
    // y: vertical coordinate.
    points[i] = 
    {
      inputs: [x, bias],
      output: desiredOutput,
      y: 0
    };
  }


  // "neuron" receives 2 inputs:
  // 1º input: Number of weights, they are "x" and "bias".
  // 2º input: "learningRate", lower means slower.
  neuron = new Perceptron (2, 0.0001);
}



function draw() 
{
  // Keeping track of the number of interations.
  print ("Interation Number: " + (interationNum+1) );


  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, interationQuadHeight*(interationNum) + interationQuadHeight/2);


    for (let i=0; i<points.length; i++) 
    {
      // "feedForward()" each point's input to the neuron
      // for it to guess or predict 
      // towards the "desiredOutput",
      // left or Right Quad, 
      // -1 or +1.
      neuron.feedForward (points[i].inputs);

      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = neuron.getActivationResult();
      print ("Neuron's Guess: " + neuronGuessing);

      // Keeping track of the 
      // tranformations/calculations 
      // of each point and iteration
      // Aka the horizontal coordinate of the Neuron's Guess.
      let x = neuron.getFitnessValue();
      // Simplifying the vertical coordinate.
      let y = points[i].y;


      // "train()" the neuron based on
      // the difference between the 
      // "desiredOutput" and "neuronGuessing".
      // This will result on a deviation, the guessing error.
      // This deviation/error is used to adjust the weight and bias.
      neuron.train (points[i].output, neuronGuessing);



      // Coloring the iteration graph background based on the point's "desiredOutput".
      let backgroundFirstQuad =  color (0);
      let backgroundSecondQuad = color (0);

      if (points[i].output < 0) 
      {
        if (points[i].output == neuronGuessing) backgroundFirstQuad =  color (120, 230, 120);
        else backgroundFirstQuad = color (255);
        backgroundSecondQuad = color (200);
      }
      else 
      {
        backgroundFirstQuad =  color (200);
        if (points[i].output == neuronGuessing) backgroundSecondQuad =  color (120, 230, 120);
        else backgroundSecondQuad = color (255);
      }

      noStroke();   
      fill (backgroundFirstQuad);
      rect (-width/2, -interationQuadHeight/2, interationQuadWidth, interationQuadHeight);
      fill (backgroundSecondQuad);
      rect (0,        -interationQuadHeight/2, interationQuadWidth, interationQuadHeight);
      print ("Desired Output: " + points[i].output);

      // Drawing the number of the iteration.
      fill (30);
      textSize (12);
      textFont (font);
      textAlign (CENTER, CENTER);
      text (interationNum+1, -interationQuadWidth/2 + 12, -2);



      // Drawing a graphical representation 
      // of the neuron's guessing.
      let s = pointSize;
      stroke (30);
      strokeWeight (s/3);
      point (x, y);

      strokeWeight (1.5);
      noFill();
      ellipse (x, y, s, s);

      // Drawing a graphical representation 
      // of the connection between iterations.
      // A representation of the continuous 
      // transformations the neuron is apply to the data.
      if (interationNum > 0) 
      {
        line (prevIterationCoordinate.x, prevIterationCoordinate.y, x, y);
      }
      // Keeping track of the previous 
      // iteration coordinates.
      prevIterationCoordinate = 
      {
        x: x, 
        y: -interationQuadHeight
      };



      // Stopping iteration once the Neuron is trained
      if (neuronGuessing == points[i].output) 
      {
        print ("Neuron trained!");
        noLoop();
      }
      else print ("Neuron not yeat trained...");



      // Iteration divider
      print ("--------------------------");
      interationNum++;
    }
  pop();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  // Restart system
  startSystem();
}

