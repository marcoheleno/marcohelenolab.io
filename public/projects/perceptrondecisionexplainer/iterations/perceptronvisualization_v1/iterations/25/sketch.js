

let neuron;
let desiredOutput, points, pointSize, prevIterationCoordinate, x, y;
let interationNum, interationQuadWidth, interationQuadHeight;
let font, guessedOutputPanel;


function preload() 
{
  font = loadFont ("IBM_Plex_Mono/IBMPlexMono-Medium.ttf");
}


function setup() 
{
  createCanvas (windowWidth, windowHeight);
  startSystem();
}


function startSystem() 
{
  noSmooth();
  loop();
  background (30);


  // To keep track of the number of interations.
  interationNum = 0;
  // Controlling the "frameRate()" for "interationNum" visualization.
  //frameRate (1);

  // Defining "interationQuad" Width and Height
  interationQuadWidth  = width/2;
  interationQuadHeight = 30;
  

  // Defining each point's size
  pointSize = interationQuadHeight/2;
  // Working with 1 point only
  points = new Array (1);

  for (let i=0; i<points.length; i++) 
  {
    // Generate random "x" coordinate for each point.
    let x = 0.1;//random (-width/4, width/4); //width/4*2;//


    // Defining "bias" for each point.
    let bias = 1;


    // Defining "desiredOutput" for each point
    // This means that this Neuron is going to be 
    // trained with Supervised Learning.
    desiredOutput = 1;
    if (x < 0) desiredOutput = -1;


    // Defining the point object 
    // and adding it to the points array.
    // Each point instance has:
    // inputs: [horizontal coordinate, bias]
    // output: desiredOutput for Supervised Learning
    // y: vertical coordinate.
    points[i] = 
    {
      inputs: [x, bias],
      output: desiredOutput,
      y: 0
    };
  }


  // "neuron" receives 2 inputs:
  // 1º input: Number of weights, they are "x" and "bias".
  // 2º input: "learningRate", lower means slower.
  neuron = new Perceptron (2, 0.0005);

  x = 0;
  y = 0;
  prevIterationCoordinate = 
  {
    x: x, 
    y: y
  };
}



function draw() 
{
  // Keeping track of the number of interations.
  print ("Interation Number: " + (interationNum+1) );

  let titleHeight = 30;

  push();
    // (0, 0) at the center of the canvas.
    translate (width/2, titleHeight + interationQuadHeight*interationNum + interationQuadHeight/2);

    for (let i=0; i<points.length; i++) 
    {
      // "feedForward()" each point's input to the neuron
      // for it to guess or predict 
      // towards the "desiredOutput",
      // left or Right Quad, 
      // -1 or +1.
      neuron.feedForward (points[i].inputs);

      // "neuronGuessing" can be -1 or +1
      let neuronGuessing = neuron.getActivationResult();
      print ("Neuron's Guess: " + neuronGuessing);



      if (interationNum == 0) 
      {
        let backgroundFirstQuad =  color (0);
        let backgroundSecondQuad = color (0);

        if (points[i].output < 0) 
        {
          backgroundFirstQuad = color (255);
          backgroundSecondQuad = color (200);
        }
        else 
        {
          backgroundFirstQuad =  color (200);
          backgroundSecondQuad = color (255);
        }
        
        noStroke();   
        fill (backgroundFirstQuad);
        rect (-width/2, -interationQuadHeight/2 - titleHeight, interationQuadWidth, titleHeight);
        fill (backgroundSecondQuad);
        rect (0,        -interationQuadHeight/2 - titleHeight, interationQuadWidth, titleHeight);

        fill (30);
        textSize (12);
        textFont (font);
        textAlign (LEFT, CENTER);

        let desiredOutputPanel = "";
        let desiredOutputPanelX = 0;
        if (desiredOutput < 0) 
        {
          desiredOutputPanel = "LEFT";
          desiredOutputPanelX = -interationQuadWidth;
        }
        else 
        {
          desiredOutputPanel = "RIGHT";
          desiredOutputPanelX = 0;
        }
        let  desiredOutputText =  "RANDOM CHOOSEN OBJECTIVE: " + desiredOutputPanel +" PANEL";
        text (desiredOutputText, desiredOutputPanelX + 12, -titleHeight/2 - interationQuadHeight/2);
      }



      let s = pointSize;
      /*
      if (interationNum == 0) 
      {
      stroke (30);
      strokeWeight (s/3);
      point (x, y);

      strokeWeight (1.5);
      noFill();
      ellipse (x, y, s, s);
      }
      */

      // Keeping track of the 
      // tranformations/calculations 
      // of each point and iteration
      // Aka the horizontal coordinate of the Neuron's Guess.
      x = neuron.getFeedForwardGuess();
      // Simplifying the vertical coordinate.
      y = points[i].y;


      // "train()" the neuron based on
      // the difference between the 
      // "desiredOutput" and "neuronGuessing".
      // This will result on a deviation, the guessing error.
      // This deviation/error is used to adjust the weight and bias.
      neuron.train (points[i].output, neuronGuessing);

        
        guessedOutputPanel = "";
        //let guessedOutputPanelX = 0;
        if (neuronGuessing < 0) 
        {
          guessedOutputPanel = "LEFT";
          //guessedOutputPanelX = -interationQuadWidth;
        }
        else 
        {
          guessedOutputPanel = "RIGHT";
          //guessedOutputPanelX = 0;
        }
        /*
        let guessedOutputText =  "INITIAL PERCEPTRON GUESS: " + guessedOutputPanel +" PANEL";
        text (guessedOutputText, guessedOutputPanelX + 12, -(titleHeight/4)*1 - interationQuadHeight/2);
        */
      



      // Coloring the iteration graph background based on the point's "desiredOutput".
      let backgroundFirstQuad =  color (0);
      let backgroundSecondQuad = color (0);

      if (points[i].output < 0) 
      {
        if (points[i].output == neuronGuessing) backgroundFirstQuad =  color (120, 230, 120);
        else backgroundFirstQuad = color (255);
        backgroundSecondQuad = color (200);
      }
      else 
      {
        backgroundFirstQuad =  color (200);
        if (points[i].output == neuronGuessing) backgroundSecondQuad =  color (120, 230, 120);
        else backgroundSecondQuad = color (255);
      }

      noStroke();   
      fill (backgroundFirstQuad);
      rect (-width/2, -interationQuadHeight/2, interationQuadWidth, interationQuadHeight);
      fill (backgroundSecondQuad);
      rect (0,        -interationQuadHeight/2, interationQuadWidth, interationQuadHeight);
      print ("Desired Output: " + points[i].output);

      // Drawing the number of the iteration.
      fill (30);
      textSize (12);
      textFont (font);
      textAlign (LEFT, CENTER);
      // Stopping iteration once the Neuron is trained
      let trainingStatus = "";
      
      if (neuronGuessing == points[i].output) 
      {
        noLoop();
        
        if (interationNum==0) 
        {
          trainingStatus = "- INITIAL RANDOM GUESS: " + guessedOutputPanel +" PANEL - WEIGHTS OPTIMIZED AT: " + neuron.getTeste1();
        }
        
        else 
        {
          trainingStatus = "- WEIGHTS OPTIMIZED AT: " + neuron.getTeste1();
        }
      }

      else
      {
        if (interationNum==0) 
        {
          trainingStatus = "- INI RANDOM WEIGHTS: " + neuron.getTeste1() + " - " + guessedOutputPanel +" PANEL";
        }
        else 
        {
          trainingStatus = "- OPTIMIZING WEIGHTS: " + neuron.getTeste1();
        }
      }
      
      text ("ITERATION " + (interationNum+1) +" "+ trainingStatus, -interationQuadWidth + 12, -2);
      print("Optimizing: " + neuron.getTeste1());




      // Drawing a graphical representation 
      // of the neuron's guessing.
      s = pointSize;
      stroke (30);
      strokeWeight (s/3);
      point (x, y);

      strokeWeight (1.5);
      noFill();
      ellipse (x, y, (s/4)*3, (s/4)*3);

      // Drawing a graphical representation 
      // of the connection between iterations.
      // A representation of the continuous 
      // transformations the neuron is apply to the data.
      //if (interationNum+1 > 1) 
      {
        //print("Dist: " + (x - prevIterationCoordinate.x) );
        line (prevIterationCoordinate.x, 0, x, 0);
        /*
        stroke(0, 255, 255);
        strokeWeight (5);
        point ( abs(prevIterationCoordinate.x - x), 0);
        */
        //line (prevIterationCoordinate.x, prevIterationCoordinate.y, x, y);
      }
      // Keeping track of the previous 
      // iteration coordinates.
      prevIterationCoordinate = 
      {
        x: x, 
        y: -interationQuadHeight
      };



      



      

      /*
      stroke(255, 0, 0);
      strokeWeight (5);
      point (neuron.getTeste1(), 0);
      */
    }

  // Iteration divider
  print ("--------------------------");
  interationNum++;

  pop();
}


function keyPressed() 
{
  //redraw();
}



function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  // Restart system
  startSystem();
}

