

let startSys, interfaceDrawn;


function setup() 
{
  setAttributes("antialias", true);
  createCanvas (windowWidth, windowHeight, WEBGL);
  startSys = true;
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;
    interfaceDrawn = true;

    preferences();
    background (colorLayer1);
    createStatusPanels();

    runPerceptron();
    createPerceptronVizIterations();
  }

  
  if (!perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() ) 
  {
    renderPerceptronVizIterations();
  }


  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() && 
     !perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() ) 
  {
    drawPerceptronVizIterations();
    interfaceDrawn = false;
  }


  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() && 
      perceptronIterationsViz.length > 1) 
  {
    background (colorLayer1);
    revealPerceptronVizIterations();
  }


  if (!interfaceDrawn) 
  {
    updateTopStatusPanel ('<a href="javascript:window.location.reload()">CLICK TO RUN THE SYSTEM AGAIN</a>');

    if (perceptronIterationsViz.length > 1) 
    {
      updateMiddleStatusPanel ('<a href="javascript:switch2D3D()">CLICK TO SWITCH BETWEEN 2D/3D</a>');
    }

    interfaceDrawn = true;
  }
  
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  startSys = true;
}
