

let topStatusPanel, bottomStatusPanel, perceptronIterationRepPanel;
let repPanGridColumn, repPanelGridLine;
let perceptronFlowEllipseDiameter;
let leftXweightSlider, rightBiasWeightSlider;


function createStatusPanels() 
{
  topStatusPanel    = createGraphics (statusPanelsWidth, statusPanelsHeight, P2D);

  bottomStatusPanel = createGraphics (statusPanelsWidth, statusPanelsHeight, P2D);
  bottomStatusPanel.push();
    bottomStatusPanel.translate(statusPanelsWidth/2, statusPanelsHeight/2 - 2); // 2D coordinate system

      bottomStatusPanel.background (whiteColor);
      bottomStatusPanel.fill (darkGrayColor);
      bottomStatusPanel.noStroke();
      bottomStatusPanel.textFont (fontRegular);
      bottomStatusPanel.textAlign (CENTER, CENTER);
      bottomStatusPanel.textSize (10);
      bottomStatusPanel.text ("TRANSPARENT PERCEPTRON v0.2", 0, 0);

  bottomStatusPanel.pop();
}


function updateTopStatusPanel (status) 
{
  topStatusPanel.push();
    topStatusPanel.translate(statusPanelsWidth/2, statusPanelsHeight/2 + 2); // 2D coordinate system

      topStatusPanel.background (whiteColor);
      topStatusPanel.fill (darkGrayColor);
      topStatusPanel.noStroke();
      topStatusPanel.textFont (fontRegular);
      topStatusPanel.textAlign (CENTER, CENTER);
      topStatusPanel.textSize (10);
      topStatusPanel.text (status, 0, 0);

  topStatusPanel.pop();
}


function drawTopStatusPanel() 
{
  push();
    translate (topStatusPanelX, topStatusPanelY, 0);
      
      textureMode (NORMAL);
      texture (topStatusPanel);
      noStroke();
        
      beginShape();
        vertex (-statusPanelsWidth/2, -statusPanelsHeight/2, 0, 0, 0);
        vertex ( statusPanelsWidth/2, -statusPanelsHeight/2, 0, 1, 0);
        vertex ( statusPanelsWidth/2,  statusPanelsHeight/2, 0, 1, 1);
        vertex (-statusPanelsWidth/2,  statusPanelsHeight/2, 0, 0, 1);
      endShape (CLOSE);
    pop();
}


function drawBottomStatusPanel() 
{
  push();
    translate (bottomStatusPanelX, bottomStatusPanelY, 0);
      
      textureMode (NORMAL);
      texture (bottomStatusPanel);
      noStroke();
        
      beginShape();
        vertex (-statusPanelsWidth/2, -statusPanelsHeight/2, 0, 0, 0);
        vertex ( statusPanelsWidth/2, -statusPanelsHeight/2, 0, 1, 0);
        vertex ( statusPanelsWidth/2,  statusPanelsHeight/2, 0, 1, 1);
        vertex (-statusPanelsWidth/2,  statusPanelsHeight/2, 0, 0, 1);
      endShape (CLOSE);
  pop();
}


function createPerceptronIterationRepPanel() 
{
  leftXweightSlider = createSlider (-1, 1, 0, 0.1);
  leftXweightSlider.hide();

  rightBiasWeightSlider = createSlider (-1, 1, 0, 0.1);
  rightBiasWeightSlider.hide();
  
  if (match(navigator.userAgent, "Firefox") != null) 
  {
    leftXweightSlider.style ("margin-top", "-7px");
    rightBiasWeightSlider.style ("margin-top", "-7px");
  }

  perceptronIterationRepPanel = createGraphics (perceptronIterationPanelWidth, perceptronIterationPanelHeight, P2D);
  repPanGridColumn = perceptronIterationPanelWidth /6;
  repPanelGridLine = perceptronIterationPanelHeight/6;
  perceptronFlowEllipseDiameter = repPanGridColumn;
}


function updatePerceptronIterationRepPanel (ptronIterationVals)
{
  perceptronIterationRepPanel.pixelDensity( displayDensity()*2 );
  perceptronIterationRepPanel.background (whiteColor);
  perceptronIterationRepPanel.push();
    perceptronIterationRepPanel.translate (perceptronIterationPanelWidth/2, perceptronIterationPanelHeight/2); // 2D coordinate system

    // Input section
    perceptronIterationRepPanel.stroke (darkGrayColor);
    perceptronIterationRepPanel.strokeWeight (1);
    perceptronIterationRepPanel.line (-repPanGridColumn, -repPanelGridLine, 0, -repPanelGridLine);
    perceptronIterationRepPanel.line ( repPanGridColumn, -repPanelGridLine, 0, -repPanelGridLine);
    
    perceptronIterationRepPanel.fill (lightGrayColor);
    perceptronIterationRepPanel.ellipse (-repPanGridColumn, -repPanelGridLine*2-perceptronFlowEllipseDiameter/2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse ( repPanGridColumn, -repPanelGridLine*2-perceptronFlowEllipseDiameter/2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    
    perceptronIterationRepPanel.noStroke();
    perceptronIterationRepPanel.textAlign (CENTER, CENTER);
    perceptronIterationRepPanel.textFont (fontMedium);
    perceptronIterationRepPanel.fill (darkGrayColor);
    perceptronIterationRepPanel.textSize (10);
    perceptronIterationRepPanel.text ("INPUTS", 0, -repPanelGridLine*2-perceptronFlowEllipseDiameter/2-2);
    perceptronIterationRepPanel.text ("X:"    + randomDataPoint.inputs[0], -repPanGridColumn, -repPanelGridLine*2-perceptronFlowEllipseDiameter/2 - 2);
    perceptronIterationRepPanel.text ("BIAS:" + randomDataPoint.inputs[1],  repPanGridColumn, -repPanelGridLine*2-perceptronFlowEllipseDiameter/2 - 2);


    // Output section
    perceptronIterationRepPanel.stroke (darkGrayColor);
    perceptronIterationRepPanel.strokeWeight (1);
    perceptronIterationRepPanel.line (0, -repPanelGridLine , 0, repPanelGridLine*2);
    perceptronIterationRepPanel.line (0, repPanelGridLine*2, repPanGridColumn*2, repPanelGridLine*2);
    perceptronIterationRepPanel.line (0, repPanelGridLine*2,-repPanGridColumn*2, repPanelGridLine*2);
    perceptronIterationRepPanel.line ( repPanGridColumn*2, repPanelGridLine*2, repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2+1);
    perceptronIterationRepPanel.line (-repPanGridColumn*2, repPanelGridLine*2,-repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2+1);
    perceptronIterationRepPanel.line ( repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2+1,  repPanGridColumn, -repPanelGridLine-repPanelGridLine/2+1);
    perceptronIterationRepPanel.line (-repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2+1, -repPanGridColumn, -repPanelGridLine-repPanelGridLine/2+1);
    perceptronIterationRepPanel.line (0, 0,-repPanGridColumn*3, 0);

    perceptronIterationRepPanel.fill (lightGrayColor);
    perceptronIterationRepPanel.ellipse (0, 0, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse (0, repPanelGridLine*2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    //perceptronIterationRepPanel.ellipse (-repPanGridColumn*3, 0, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse (-repPanGridColumn*2, repPanelGridLine, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse ( repPanGridColumn*2, repPanelGridLine, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse (-repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse ( repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);

    perceptronIterationRepPanel.fill (darkGrayColor);
    perceptronIterationRepPanel.ellipse (0, -repPanelGridLine, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse (0,  repPanelGridLine, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse ( repPanGridColumn*2, repPanelGridLine*2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);
    perceptronIterationRepPanel.ellipse (-repPanGridColumn*2, repPanelGridLine*2, perceptronFlowEllipseDiameter, perceptronFlowEllipseDiameter);

    perceptronIterationRepPanel.noStroke();
    perceptronIterationRepPanel.textAlign (CENTER, CENTER);
    
    perceptronIterationRepPanel.textFont (fontLight);
    perceptronIterationRepPanel.fill (lightGrayColor);
    perceptronIterationRepPanel.textSize (20);
    perceptronIterationRepPanel.text ("∑", 0, -repPanelGridLine-5);
    perceptronIterationRepPanel.text ("A", 0,  repPanelGridLine-3);

    perceptronIterationRepPanel.textFont (fontMedium);
    perceptronIterationRepPanel.textSize (10);
    perceptronIterationRepPanel.text ("ERROR CAL.", repPanGridColumn*2, repPanelGridLine*2-2);
    perceptronIterationRepPanel.text ("ERROR CAL.",-repPanGridColumn*2, repPanelGridLine*2-2);

    perceptronIterationRepPanel.textFont (fontMedium);
    perceptronIterationRepPanel.fill (darkGrayColor);
    perceptronIterationRepPanel.textSize (10);
    perceptronIterationRepPanel.text (ptronIterationVals.feedForwardCalculation.toFixed(2), 0, -2);
    perceptronIterationRepPanel.text (ptronIterationVals.activationResult, 0, repPanelGridLine*2-2);
    perceptronIterationRepPanel.text (ptronIterationVals.trainingError, -repPanGridColumn*2, repPanelGridLine-2);
    perceptronIterationRepPanel.text (ptronIterationVals.trainingError,  repPanGridColumn*2, repPanelGridLine-2);
    perceptronIterationRepPanel.text (ptronIterationVals.transformedWeights[0].toFixed(3), -repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2-2);
    perceptronIterationRepPanel.text (ptronIterationVals.transformedWeights[1].toFixed(3),  repPanGridColumn*2, -repPanelGridLine-repPanelGridLine/2-2);
    perceptronIterationRepPanel.text ("OUTPUT", -repPanGridColumn, 0-10);
      
  perceptronIterationRepPanel.pop();
}


function drawPerceptronIterationRepPanel (x, y, z)
{
  push();
    translate (x, y, z-0.1);
    
      stroke (mediumGrayColor);
      strokeWeight (1);
      noFill();
            
      beginShape();
        vertex (-perceptronIterationPanelWidth/2, -perceptronIterationPanelHeight/2, 0);
        vertex ( perceptronIterationPanelWidth/2, -perceptronIterationPanelHeight/2, 0);
        vertex ( perceptronIterationPanelWidth/2,  perceptronIterationPanelHeight/2, 0);
        vertex (-perceptronIterationPanelWidth/2,  perceptronIterationPanelHeight/2, 0);
      endShape (CLOSE);
  pop();
  
  push();
    translate (x, y, z);

      textureMode (NORMAL);
      texture (perceptronIterationRepPanel);
      noStroke();

      beginShape();
        vertex (-perceptronIterationPanelWidth/2, -perceptronIterationPanelHeight/2, 0, 0, 0);
        vertex ( perceptronIterationPanelWidth/2, -perceptronIterationPanelHeight/2, 0, 1, 0);
        vertex ( perceptronIterationPanelWidth/2,  perceptronIterationPanelHeight/2, 0, 1, 1);
        vertex (-perceptronIterationPanelWidth/2,  perceptronIterationPanelHeight/2, 0, 0, 1);
      endShape (CLOSE);
  pop();


  let weightSlidersHeight = repPanelGridLine;

  let leftXweightSliderPos = domRotatedSliderTranslate
  (
    x - repPanGridColumn, 
    y - weightSlidersHeight*2, 
    weightSlidersHeight
  )

  let rightBiasWeightSliderPos = domRotatedSliderTranslate
  (
    x + repPanGridColumn, 
    y - weightSlidersHeight*2, 
    weightSlidersHeight
  )

  leftXweightSlider.position (leftXweightSliderPos.x, leftXweightSliderPos.y);
  leftXweightSlider.style ("width", weightSlidersHeight+"px"); // Width, because sliders are rotated, see style.css

  rightBiasWeightSlider.position (rightBiasWeightSliderPos.x, rightBiasWeightSliderPos.y);
  rightBiasWeightSlider.style ("width", weightSlidersHeight+"px"); // Width, because sliders are rotated, see style.css

  leftXweightSlider.show();
  rightBiasWeightSlider.show();
}