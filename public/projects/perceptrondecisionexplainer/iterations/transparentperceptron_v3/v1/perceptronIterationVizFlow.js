

let perceptronActivationFunction = function (sumWeightedInputs) 
{
  // Here you can define the 
  // Perceptron's Activation Function

  // The below result is the sign of the sum, -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}


let randomDataPoint, transparentPerceptronInstance;
let perceptronIterationsCalculations, perceptronIterationsViz, iterationsCounter;
let revealPerceptronVizIterationsInitialized, tempReOrderIndex;
let spaceBetweenPanelsZ, z, animatePanelsZ, x;


function runPerceptron() 
{
  updateTopStatusPanel ("Running Perceptron");

  randomDataPoint = getData (perceptronIterationPanelWidth); // See data.js

  transparentPerceptronInstance = new TransparentPerceptron (randomDataPoint.inputs.length, perceptronActivationFunction, 0.0005);

  let i = 0;
  perceptronIterationsCalculations = [];
  do 
  {
    transparentPerceptronInstance.feedForward (randomDataPoint.inputs);
    transparentPerceptronInstance.train (randomDataPoint.goal);
    
    perceptronIterationsCalculations[i] = transparentPerceptronInstance.getAllTransparentPerceptronCalculations();

    i++;
  }
  while (perceptronIterationsCalculations[i-1].trainingError != 0);
}


function createPerceptronVizIterations() 
{
  perceptronIterationsViz = [];

  for (let i=0; i<perceptronIterationsCalculations.length; i++) 
  {
    perceptronIterationsViz[i] = new PerceptronIterationViz 
    (
      perceptronIterationsCalculations[i], 
      perceptronIterationPanelWidth,
      perceptronIterationPanelHeight
    );
  }

  iterationsCounter = 0;
  revealPerceptronVizIterationsInitialized = false;
}


function renderPerceptronVizIterations() 
{
  updateTopStatusPanel ("RENDERING PERCEPTRON VISUALIZATION ITERATION " + (iterationsCounter+1) + " of " + perceptronIterationsViz.length);

  perceptronIterationsViz[iterationsCounter].renderPerceptronIterationViz();
  iterationsCounter++;
  
  if (iterationsCounter === perceptronIterationsViz.length) iterationsCounter = 0;
}


function drawPerceptronVizIterations() 
{
  updateTopStatusPanel ("");

  perceptronIterationsViz[iterationsCounter].drawPerceptronIterationViz (perceptronIterationPanelX, perceptronIterationPanelY, 0);

  if (iterationsCounter  <  perceptronIterationsViz.length) iterationsCounter++;
}


function revealPerceptronVizIterations() 
{
  if (!revealPerceptronVizIterationsInitialized) 
  {
    spaceBetweenPanelsZ = -100;
    
    x = -perceptronIterationPanelWidth/2;
    z = 0;
    animatePanelsZ  = 0;

    revealPerceptronVizIterationsInitialized = true;
  }

  else 
  {
    if (animatePanelsZ > spaceBetweenPanelsZ) animatePanelsZ -= 2;
  }
  
  push();
    translate (perceptronIterationPanelX, perceptronIterationPanelY, 0);

      for (let i=0; i<perceptronIterationsViz.length; i++) 
      {
        tempReOrderIndex = map(i, 0, perceptronIterationsViz.length-1, perceptronIterationsViz.length, 0);
        z = animatePanelsZ * tempReOrderIndex;
        perceptronIterationsViz[i].drawPerceptronIterationViz (x, 0, z);
      }
  pop();
}
