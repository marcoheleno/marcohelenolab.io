

let data = new Array(10000);
let ptron;

let count = 0;


let xmin = -1;
let ymin = -1;
let xmax = 1;
let ymax = 1;


// The function to describe a line
function f(x) 
{
  let y = 90 * x + 0;
  return y;
}


let perceptronActivationFunction = function (sumWeightedInputs) 
{
  // Here you can define the 
  // Perceptron's Activation Function

  // The below result is the sign of the sum, -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
}


function setup() 
{
  createCanvas(500, 600);
  
  ptron = new TransparentPerceptron (3, perceptronActivationFunction, 0.000005);

  
  for (let i = 0; i < data.length; i++) 
  {
    let x = random (xmin, xmax);
    let y = random (ymin, ymax);
    let answer = 0;

    if (y < f(x) ) answer = -1;
    else answer = 1;

    data[i] = 
    {
      input: [x, y, 1],
      output: answer
    };
  }
}


function draw() 
{
  background(255);

  strokeWeight(1);
  stroke(0);
  let x1 = map (xmin, xmin, xmax, 0, width);
  let y1 = map (f(xmin), ymin, ymax, height, 0);
  let x2 = map (xmax, xmin, xmax, 0, width);
  let y2 = map (f(xmax), ymin, ymax, height, 0);
  line (x1, y1, x2, y2);
  

  
  strokeWeight(1);
  stroke(0);
  let weights = ptron.getTransformedWeights();

  x1 = xmin;
  y1 = (-weights[2] - weights[0] * x1) / weights[1];
  x2 = xmax;
  y2 = (-weights[2] - weights[0] * x2) / weights[1];

  x1 = map (x1, xmin, xmax, 0, width);
  y1 = map (y1, ymin, ymax, height, 0);
  x2 = map (x2, xmin, xmax, 0, width);
  y2 = map (y2, ymin, ymax, height, 0);
  line (x1, y1, x2, y2);
  

  for (let i=0; i<data.length; i++) 
  {
    strokeWeight(1);
    stroke(100);

    ptron.feedForward(data[i].input);
    let guess = ptron.getActivationResult();

    if (guess > 0) noFill();
    else fill(100);

    let x = map(data[i].input[0], xmin, xmax, 0, width);
    let y = map(data[i].input[1], ymin, ymax, height, 0);

    ellipse(x, y, 3, 3);

    ptron.train( data[i].output, data[i].input );
  }
}
