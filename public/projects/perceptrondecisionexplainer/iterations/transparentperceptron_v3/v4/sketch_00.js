
// The function to describe a line
function f(x)   
{
  let y = 90 * x + 0;
  return y;
};

let perceptronActivationFunction = function (sumWeightedInputs) 
{
  // Here you can define the 
  // Perceptron's Activation Function

  // The below result is the sign of the sum, -1 or 1.
  if (sumWeightedInputs > 0) return 1; // Right Quad
  else return -1;                      // Left  Quad
};


let data = new Array(1500);
let ptron;

let count = 0;

let xmin = -1;
let ymin = -1;
let xmax = 1;
let ymax = 1;


let sketch00 = function(p00) {

p00.setup = function() 
{
  p00.noCanvas();

  ptron = new TransparentPerceptron (3, perceptronActivationFunction, 0.000005);
  
  for (let i = 0; i < data.length; i++) 
  {
    let x = p00.random (xmin, xmax);
    let y = p00.random (ymin, ymax);
    let answer = 0;

    if (y < f(x) ) answer = -1;
    else answer = 1;

    data[i] = 
    {
      input: [x, y, 1],
      output: answer
    };
  };
};


p00.draw = function() 
{
  /*
  ptron.feedForward(data[count].input);
  ptron.train( data[count].output, data[count].input );
  count = (count + 1) % data.length;
  */

  for (let i=0; i<data.length; i++) 
  {
    ptron.feedForward(data[i].input);
    ptron.train( data[i].output, data[i].input );
  }
};

};

let myp500 = new p5(sketch00);