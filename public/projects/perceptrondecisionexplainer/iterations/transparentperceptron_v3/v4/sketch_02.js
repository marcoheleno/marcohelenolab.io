

let sketch02 = function(p02) {

p02.setup = function() 
{
  p02.createCanvas (p02.windowWidth/3, p02.windowHeight);
};


p02.draw = function() 
{
  p02.background(255);
  /*

  p02.strokeWeight(1);
  p02.stroke(0);
  let weights = ptron.getTransformedWeights();

  let x1 = xmin;
  let y1 = (-weights[2] - weights[0] * x1) / weights[1];
  let x2 = xmax;
  let y2 = (-weights[2] - weights[0] * x2) / weights[1];

  x1 = p02.map (x1, xmin, xmax, 0, p02.width);
  y1 = p02.map (y1, ymin, ymax, p02.height, 0);
  x2 = p02.map (x2, xmin, xmax, 0, p02.width);
  y2 = p02.map (y2, ymin, ymax, p02.height, 0);
  p02.line (x1, y1, x2, y2);
  */
  

  for (let i=0; i<data.length; i++) 
  {
    p02.strokeWeight(1);
    p02.stroke(100);

    ptron.feedForward(data[i].input);
    let guess = ptron.getActivationResult();

    if (guess > 0) p02.fill(255);
    else p02.fill(100);

    let x = p02.map(data[i].input[0], xmin, xmax, 0, p02.width);
    let y = p02.map(data[i].input[1], ymin, ymax, p02.height, 0);

    p02.ellipse(x, y, 6, 6);
  };

  p02.strokeWeight(1);
  p02.stroke(0);
  p02.noFill();
  p02.rect(0, 0, p02.width-1, p02.height-1);
};

p02.windowResized = function() 
{
  p02.resizeCanvas (p02.windowWidth/3, p02.windowHeight);
};

};

let myp502 = new p5(sketch02);