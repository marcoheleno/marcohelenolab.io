


class TransparentPerceptron 
{

  constructor (numWeights, activFun, learnRate) 
  {
    this.weights = new Array (numWeights);
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // Start with random weights.
      this.weights[i] = (Math.random()*2) - 1; //random (-1, 1);
    }

    this.activFun = activFun;
    this.learnRate = learnRate;
  }




  
  feedForward (inputs)
  {
    // This function will sum the weighted inputs
    // and save the activation function result/calculation

    this.sumWeightedInputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      // sum all the weighted inputs.
      this.sumWeightedInputs += inputs[i] * this.weights[i];
    }

    // Calling and saving the resultes from the activFun.
    this.activationResult = this.activFun (this.sumWeightedInputs);
  }


  getFeedForwardCalculation() 
  {
    // Keeping track of the 
    // tranformations/calculations 
    // ocurring to the data
    // at each iteration.
    return this.sumWeightedInputs;
  }

  
  getTransformedWeights() 
  {
    // Keeping track of the 
    // tranformations/calculations 
    // ocurring in the weights
    // at each iteration.
    return this.weights;
  }

  getActivationResult() 
  {
    // Function for getting the Activation Result.
    return this.activationResult;
  }





  // Function to train the Perceptron against known data.
  train (desiredOutput, inputs) 
  {
    // When training, an Error is calculated 
    // with the difference between the 
    // desiredOutput — left or Right Section, -1 or +1,
    // and perceptronActivationResult (Guess), also -1 or +1,
    //
    // This will result on a deviation, the Perceptron's guessing error.
    // This can be -2, 0, or 2.
    this.error = desiredOutput - this.activationResult;
    
    // This deviation/error is used to adjust the weight and bias.
    // It will adjust the these according
    // to the learnRate, error and the data.
    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learnRate * this.error * inputs[i];
    }
  }

  getTrainingError() 
  {
    // Function for getting the Training Error result.
    return this.error;
  }





  // Every interaction possible with 
  // the TransparentPerceptron
  // is in this fucntion.
  getAllTransparentPerceptronCalculations() 
  {
    this.allTransparentPerceptronCalculations = 
    {
      // View the result from the FeedForward calculation.
      // The result from the sum of the weighted inputs.
      // This happens befor the Activation function.
      feedForwardCalculation: this.getFeedForwardCalculation(),

      // View the result from the calculations/transformations in the Weights.
      // This functions returns an array with the same lenght as the number os weights.
      // This happens befor the Activation function.
      //transformedWeights: this.getTransformedWeights(),

      // View the result from the Activation function.
      // The Result is the sign of the sum, -1 or 1.
      activationResult: this.getActivationResult(),

      // View the result from the calculated Error.
      // The Result can be -2, 0, or 2.
      trainingError: this.getTrainingError()
    };

    return this.allTransparentPerceptronCalculations;
  }
}

