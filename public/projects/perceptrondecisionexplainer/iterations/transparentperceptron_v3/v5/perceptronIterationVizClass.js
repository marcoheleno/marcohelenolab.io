


class PerceptronIterationViz 
{

  constructor (ptronIterationVals, vizPanelWidth, vizPanelHeight) 
  {
    this.feedForwardCalculation = ptronIterationVals.feedForwardCalculation;
    this.transformedWeights = ptronIterationVals.transformedWeights;
    this.activationResult = ptronIterationVals.activationResult;
    this.trainingError = ptronIterationVals.trainingError;
    
    this.vizPanelWidth  = vizPanelWidth;
    this.vizPanelHeight = vizPanelHeight;
    
    this.perceptronIterationVizGraphic = 0;
    this.perceptronIterationVizDrawn = false;
  }


  renderPerceptronIterationViz()
  {
    this.x = this.feedForwardCalculation;
    this.y = 0;

    this.perceptronIterationVizGraphic = createGraphics (this.vizPanelWidth, this.vizPanelHeight, P2D);
    this.perceptronIterationVizGraphic.pixelDensity( displayDensity()*2 );
    this.perceptronIterationVizGraphic.background (250, 50);
    this.perceptronIterationVizGraphic.push();

      this.perceptronIterationVizGraphic.translate (this.vizPanelWidth/2, this.vizPanelHeight/2); // 2D coordinate system

        this.perceptronIterationVizGraphic.stroke (colorLayer4);
        this.perceptronIterationVizGraphic.strokeWeight (5);
        this.perceptronIterationVizGraphic.point (this.x, this.y);

        this.perceptronIterationVizGraphic.stroke (colorLayer4);
        this.perceptronIterationVizGraphic.strokeWeight (1);
        this.perceptronIterationVizGraphic.noFill();
        this.perceptronIterationVizGraphic.ellipse(this.x, this.y, this.vizPanelWidth/2, this.vizPanelWidth/2);

    this.perceptronIterationVizGraphic.pop();
    this.perceptronIterationVizRendered = true;
  }

  isPerceptronIterationVizRendered()
  {
    if (this.perceptronIterationVizRendered) return true;
    else return false;
  }
  
  
  drawPerceptronIterationViz (x, y, z) 
  {
    push();
      translate (x, y, z-0.1);
      
        stroke (colorLayer2);
        strokeWeight (1);
        noFill();
        
        beginShape();
          vertex (-this.vizPanelWidth/2, -this.vizPanelHeight/2, 0);
          vertex (                    0, -this.vizPanelHeight/2, 0);
          vertex (                    0,  this.vizPanelHeight/2, 0);
          vertex (-this.vizPanelWidth/2,  this.vizPanelHeight/2, 0);
        endShape (CLOSE);

        beginShape();
          vertex (                    0, -this.vizPanelHeight/2, 0);
          vertex ( this.vizPanelWidth/2, -this.vizPanelHeight/2, 0);
          vertex ( this.vizPanelWidth/2,  this.vizPanelHeight/2, 0);
          vertex (                    0,  this.vizPanelHeight/2, 0);
        endShape (CLOSE);
    pop();
    
    
    push();
      translate (x, y, z);
      
        textureMode (NORMAL);
        texture (this.perceptronIterationVizGraphic);
        noStroke();
        
        beginShape();
          vertex (-this.vizPanelWidth/2, -this.vizPanelHeight/2, 0, 0, 0);
          vertex ( this.vizPanelWidth/2, -this.vizPanelHeight/2, 0, 1, 0);
          vertex ( this.vizPanelWidth/2,  this.vizPanelHeight/2, 0, 1, 1);
          vertex (-this.vizPanelWidth/2,  this.vizPanelHeight/2, 0, 0, 1);
        endShape (CLOSE);
    pop();

    this.perceptronIterationVizDrawn = true;
  }

  isPerceptronIterationVizDrawn()
  {
    if (this.perceptronIterationVizDrawn) return true;
    else return false;
  }
}
