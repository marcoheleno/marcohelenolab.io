

let colorLayer1, colorLayer2, colorLayer3, colorLayer4;
let bottomStatusPanelX, bottomStatusPanelY, statusPanelsWidth, statusPanelsHeight;
let perceptronIterationPanelX, perceptronIterationPanelY;
let perceptronIterationPanelWidth, perceptronIterationPanelHeight;


function preferences() 
{
  colorLayer1 = color (250);
  colorLayer2 = color (210);
  colorLayer3 = color (150);
  colorLayer4 = color (50);

  statusPanelsWidth  = width;
  statusPanelsHeight = 30;
  topStatusPanelX    = 0;
  topStatusPanelY    = -height/2 + statusPanelsHeight/2;
  bottomStatusPanelX = 0;
  bottomStatusPanelY = height/2 - statusPanelsHeight/2;

  perceptronIterationPanelX = 0;
  perceptronIterationPanelY = 0;

  if (height > width) 
  {
    perceptronIterationPanelWidth  = percentage2Pixels (60, width);
    perceptronIterationPanelHeight = percentage2Pixels (60, height);
  }

  else 
  {
    perceptronIterationPanelWidth  = percentage2Pixels (30, width);
    perceptronIterationPanelHeight = percentage2Pixels (70, height); 
  }
}
