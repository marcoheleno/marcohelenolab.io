

let startSys;


function setup() 
{
  setAttributes("antialias", true);
  createCanvas (windowWidth, windowHeight, WEBGL);
  startSys = true;
}


function draw() 
{
  if (startSys) 
  {
    startSys = false;

    preferences();
    background (colorLayer1);
    createStatusPanels();

    runPerceptron();
    createPerceptronVizIterations();
  }
  
  if (!perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() ) 
  {
    renderPerceptronVizIterations();
  }

  if (perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizRendered() && 
     !perceptronIterationsViz[perceptronIterationsViz.length-1].isPerceptronIterationVizDrawn() ) 
  {
    drawPerceptronVizIterations();
  }

  drawTopStatusPanel();
  drawBottomStatusPanel();
}


function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
  startSys = true;
}
