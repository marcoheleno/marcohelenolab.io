

function getData () 
{
  // Random horizontal coordinate
  const x = random(-preferences.gridCellW*2, preferences.gridCellW*2);//Math.random()*(preferences.gridCellW*2)-preferences.gridCellW*2;
  
  // Bias
  const b = 1;

  // Goal
  let g = 1;
  if (x < 0) g = -1;
  
  const randomDataPoint = 
  {
    inputs: [x, b],
    goal: g
  };

  return randomDataPoint;
}
