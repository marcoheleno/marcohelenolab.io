

class PerceptronIterationViz 
{

  constructor (weights, sumWeightedInputs, activationResult, guessError) 
  {
    this.weights = weights;
    /*
    console.log (this.weights[0]);
    console.log (this.weights[1]);
    console.log ("------------");
    */

    this.sumWeightedInputs = sumWeightedInputs;
    this.activationResult = activationResult; //console.log (this.activationResult);
    this.guessError = guessError;

    this.perceptronIterationVizRendered = false;
    this.perceptronIterationVizDrawn = false;
  }


  render () 
  {
    this.perceptronIterationVizRender = createGraphics (preferences.gridCellW, preferences.gridCellH, P2D); // 2D coordinate system
    this.perceptronIterationVizRender.pixelDensity( displayDensity()*2 );
    //this.perceptronIterationVizRender.background (255, 0, 0);
    
    this.perceptronIterationVizRender.noStroke();
    this.perceptronIterationVizRender.fill (preferences.colorLayer4);
    this.perceptronIterationVizRender.rect (0, 0, preferences.gridCellW, preferences.gridCellH/4);
    this.perceptronIterationVizRender.rect (0, preferences.gridCellH/4, preferences.gridCellW, preferences.gridCellH/4);

    this.perceptronIterationVizRender.strokeCap (SQUARE);
    this.perceptronIterationVizRender.stroke (preferences.colorLayer1);
    this.perceptronIterationVizRender.strokeWeight (1);

    //console.log (this.weights[0]);
    this.xWeightX = preferences.gridCellW/2 + map( this.weights[0], -1, 1, -preferences.gridCellW/2, preferences.gridCellW/2 );
    this.perceptronIterationVizRender.line (this.xWeightX, 0, this.xWeightX, preferences.gridCellH/4);

    //console.log (this.weights[1]);
    this.bWeightX = preferences.gridCellW/2 + map( this.weights[1], -1, 1, -preferences.gridCellW/2, preferences.gridCellW/2 );
    this.perceptronIterationVizRender.line (this.bWeightX, preferences.gridCellH/4, this.bWeightX, preferences.gridCellH/2);
    //console.log ("--------------");

    this.perceptronIterationVizRender.stroke (preferences.colorLayer4);
    this.perceptronIterationVizRender.strokeWeight (2);
    this.perceptronIterationVizRender.line (1, preferences.gridCellH/2, 1, preferences.gridCellH);
    this.perceptronIterationVizRender.line (preferences.gridCellW-1, preferences.gridCellH/2, preferences.gridCellW-1, preferences.gridCellH);

    this.perceptronIterationVizRender.noStroke();
    this.perceptronIterationVizRender.fill (preferences.colorLayer4);
    this.perceptronIterationVizRender.textFont ("IBM Plex Mono");
    this.perceptronIterationVizRender.textAlign (CENTER, CENTER);
    this.perceptronIterationVizRender.textSize (10);
    this.perceptronIterationVizRender.text ("-1", 15, (preferences.gridCellH/4)*3);
    this.perceptronIterationVizRender.text ("+1", preferences.gridCellW - 15, (preferences.gridCellH/4)*3);

    this.perceptronIterationVizRender.noFill();
    this.perceptronIterationVizRender.stroke (preferences.colorLayer4);
    this.perceptronIterationVizRender.strokeWeight (1);
    this.perceptronIterationVizRender.ellipse (preferences.gridCellW/2+((preferences.gridCellW/2-15)*this.activationResult), (preferences.gridCellH/4)*3, 17, 17);

    this.perceptronIterationVizRendered = true;
  }

  isRendered()
  {
    if (this.perceptronIterationVizRendered) return true;
    else return false;
  }


  draw (x, y, z) 
  {
    push();
      translate (x, y, z);
      
        textureMode (NORMAL);
        texture (this.perceptronIterationVizRender);
        noStroke();
        
        beginShape();
          vertex (-this.perceptronIterationVizRender.width/2, 0, 0, 0, 0);
          vertex ( this.perceptronIterationVizRender.width/2, 0, 0, 1, 0);
          vertex ( this.perceptronIterationVizRender.width/2, this.perceptronIterationVizRender.height, 0, 1, 1);
          vertex (-this.perceptronIterationVizRender.width/2, this.perceptronIterationVizRender.height, 0, 0, 1);
        endShape (CLOSE);
    pop();

    this.perceptronIterationVizDrawn = true;
  }

  isDrawn()
  {
    if (this.perceptronIterationVizDrawn) return true;
    else return false;
  }

}
