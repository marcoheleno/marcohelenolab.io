

let novelty;


function setup() 
{
  setAttributes("antialias", true);
  createCanvas (windowWidth, windowHeight, WEBGL);

  novelty = true;
}


function draw() 
{
  if (novelty) 
  {
    frameRate (60);
    novelty = false;
    updateCanvas();
  }
  else frameRate (1);
}


function updateCanvas() 
{
  if (transparentPerceptronVizFlow[0] === 0) 
  {
    background (preferences.colorLayer1);
    
    runPerceptron();
    updateCanvasSize (transparentPerceptronIterationsVisualizations.length);

    console.log (transparentPerceptronIterationsVisualizations.length);
  }

  if (transparentPerceptronVizFlow[0] === 1 && 
      transparentPerceptronVizFlow[1] === 0 ) 
  {
    renderPerceptronVizIterations();
  }

  if (transparentPerceptronVizFlow[1] === 1 && 
      transparentPerceptronVizFlow[2] === 0 ) 
  {
    novelty = true;
    drawPerceptronVizIterations();
  }
}


function updateCanvasSize (i) 
{
  if (preferences.gridCellH*i > height) 
  {
    resizeCanvas (windowWidth, preferences.gridCellH * i);
    novelty = true;
  }
}


function windowResized() 
{
  resizeCanvas (windowWidth, height);

  novelty = true;
  transparentPerceptronVizFlow[2] = 0;
}
