


class TransparentPerceptron // v0.6
{

  constructor (numWeights, activationFunction, learningRate) 
  {
    this.weights = [];
    
    for (let i=0; i<numWeights; i++) 
    {
      // Start with random weights (-1, 1).
      //this.weights.push( (Math.random()*2) - 1 );
      this.weights.push( random(-1, 1) );
    }

    this.activationFunction = activationFunction;
    this.learningRate = learningRate;
  }


  getWeights() 
  {
    // This is only to keep track 
    // of the weights before current 
    // iteration training/adjustWeights

    return this.weights; //returns array
  }

  
  sumOfWeightedInputs (data)
  {
    // This function will sum the weighted data

    this.sumWeightedInputs = 0;
    
    for (let i=0; i<this.weights.length; i++) 
    {
      this.sumWeightedInputs += data[i] * this.weights[i];
    }

    return this.sumWeightedInputs;
  }


  runActivationFunction ()
  {
    // Calling and saving the resultes from the activationFunction.

    this.activationResult = this.activationFunction (this.sumWeightedInputs);

    return this.activationResult;
  }


  calculateError (goal) 
  {
    // When training, an Error is calculated 
    // with the difference between the 
    // Goal (left or right Section, -1 or +1)
    // and
    // Guess (left or right Section, -1 or +1)
    //
    // This will result on a deviation, 
    // the Perceptron's guessing error.
    // This can be -2, 0, or 2.

    this.error = goal - this.activationResult;

    return this.error;
  }


  adjustWeights (data) 
  {
    // The deviation/error is used to adjust the weights.
    // It will adjust the these according
    // to the learningRate, error and the data.

    for (let i=0; i<this.weights.length; i++) 
    {
      this.weights[i] += this.learningRate * this.error * data[i];
    }
  }

}
