

function percentage2Pixels (percentage, numPixels)
{
  let percentage2Pixels = Math.trunc( (percentage*numPixels) / 100 );

  if (percentage2Pixels%2 != 0) percentage2Pixels++;
  
  return percentage2Pixels;
}
